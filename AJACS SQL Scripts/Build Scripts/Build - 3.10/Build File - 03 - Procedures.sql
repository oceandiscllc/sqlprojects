USE AJACS
GO

--Begin procedure dbo.GetPersonByPersonID
EXEC Utility.DropObject 'dbo.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.07
-- Description:	A stored procedure to return data from the dbo.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added the IsAccountLockedOut bit
--
-- Author:			John Lyons
-- Create date:	2015.07.29
-- Description:	Added two factor support
--
-- Author:			Damon Miller
-- Create date:	2017.05.26
-- Description:	Added the IsEmailDigest bit
-- ============================================================================================
CREATE PROCEDURE dbo.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		P.CanReceiveEmail,
		P.CountryCallingCodeID,
		P.DocumentCountCap,
		P.DocumentSizeCap,
		dbo.GetFileSize(P.DocumentSizeCap) AS DocumentSizeCapFormatted,
		P.EmailAddress,
		P.FirstName, 
		P.IsAccountLockedOut,
		P.IsEmailDigest,
		P.IsNewsLetterSubscriber,
		P.IsPhoneVerified,
		P.LastName,
		P.Organization,
		P.PersonID,
		ISNULL((SELECT COUNT(DL.PersonID) FROM document.DocumentLog DL WHERE DL.PersonID = P.PersonID), 0) AS DocumentCount,
		ISNULL((SELECT SUM(DL.PhysicalFileSize) FROM document.DocumentLog DL WHERE DL.PersonID = P.PersonID), 0) AS DocumentSize,
		dbo.GetFileSize(ISNULL((SELECT SUM(DL.PhysicalFileSize) FROM document.DocumentLog DL WHERE DL.PersonID = P.PersonID), 0)) AS DocumentSizeFormatted,
		P.Phone, 
		P.Title,
		P.UserName,
		R.RoleID, 
		R.RoleName
	FROM dbo.Person P
		JOIN dropdown.Role R ON R.RoleID = P.RoleID
			AND P.PersonID = @PersonID

END
GO
--End procedure dbo.GetPersonByPersonID

--Begin procedure document.GetDocumentByDocumentID
EXEC Utility.DropObject 'document.GetDocumentByDocumentID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date: 2015.05.07
-- Description:	A stored procedure to get data from the document.Document table
--
-- Author:			Todd Pires
-- Create date:	2016.01.25
-- Description:	Implemented the IsForDonorPortal field
--
-- Author:			Brandon Green
-- Create date:	2016.08.16
-- Description:	Implemented the DisplayInCommunityNewsFeed field
--
-- Author:			John Lyons
-- Create date:	2017.06.05
-- Description:	Refactored to support the new document system
-- =======================================================================
CREATE PROCEDURE document.GetDocumentByDocumentID

@DocumentID INT,
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDownloadPermitted BIT = 1
	DECLARE @nDocumentID INT = @DocumentID

	IF @PersonID > 0 AND document.CanDownloadDocuments(@PersonID) = 0
		BEGIN

		SET @bDownloadPermitted = 0

		SELECT @nDocumentID = D.DocumentID
		FROM document.Document D
		WHERE D.DocumentName = 'DownloadCapExceeded'

		END
	--ENDIF

	SELECT 
		D.ContentSubtype,
		D.ContentType, 
		dbo.FormatPersonNameByPersonID(D.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		D.DisplayInCommunityNewsFeed,
		D.DocumentData, 
		D.DocumentDate, 
		dbo.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.DocumentFileName,
		D.Extension,
		D.IsForDonorPortal,
		D.PhysicalFileSize,
		DT.DocumentTypeID, 
		DT.DocumentTypeName
	FROM document.Document D
		JOIN dropdown.DocumentType DT ON DT.DocumentTypeID = D.DocumentTypeID
			AND D.DocumentID = @nDocumentID

	SELECT 
		P.ProvinceID,
		P.ProvinceName
	FROM dbo.Province P
		JOIN document.DocumentEntity DE ON DE.EntityID = P.ProvinceID
			AND DE.EntityTypeCode = 'Province'
			AND DE.DocumentID = @nDocumentID

	SELECT 
		C.CommunityID,
		C.CommunityName,
		P.ProvinceName
	FROM dbo.Community C
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN document.DocumentEntity DE ON DE.EntityID = C.CommunityID
			AND DE.EntityTypeCode = 'Community'
			AND DE.DocumentID = @nDocumentID

	IF @PersonID > 0
		BEGIN

		INSERT INTO document.DocumentLog
			(PersonID, DocumentID, DocumentTypeCode, PhysicalFileSize, DownloadPermitted)
		SELECT
			@PersonID,
			D.DocumentID,
			'Download',
			D.PhysicalFileSize,
			@bDownloadPermitted
		FROM document.Document D
		WHERE D.DocumentID = @DocumentID

		END
	--ENDIF
	
END
GO
--End procedure document.GetDocumentByDocumentID

--Begin procedure document.GetDocumentByDocumentName
EXEC utility.DropObject 'document.GetDocumentByDocumentName'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2016.11.01
-- Description:	A stored procedure to get data from the document.Document table
-- ============================================================================
CREATE PROCEDURE document.GetDocumentByDocumentName

@DocumentName VARCHAR(50),
@PersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDownloadPermitted BIT = 1
	DECLARE @cDocumentName VARCHAR(50) = @DocumentName

	IF @PersonID > 0 AND document.CanDownloadDocuments(@PersonID) = 0
		BEGIN

		SET @bDownloadPermitted = 0
		SET @cDocumentName = 'DownloadCapExceeded'

		END
	--ENDIF

	SELECT TOP 1
		D.ContentSubtype,
		D.ContentType, 
		D.DocumentData,
		D.DocumentDate, 
		core.FormatDate(D.DocumentDate) AS DocumentDateFormatted, 
		D.DocumentDescription, 
		D.DocumentID, 
		D.DocumentName, 
		D.DocumentTitle,
		D.DocumentFileName,
		D.PhysicalFileSize,
		D.Extension
	FROM document.Document D
	WHERE D.DocumentName = @cDocumentName

	IF @PersonID > 0 AND @DocumentName <> 'DownloadCapExceeded'
		BEGIN

		INSERT INTO document.DocumentLog
			(PersonID, DocumentID, DocumentTypeCode, PhysicalFileSize, DownloadPermitted)
		SELECT
			@PersonID,
			D.DocumentID,
			'Download',
			D.PhysicalFileSize,
			@bDownloadPermitted
		FROM document.Document D
		WHERE D.DocumentName = @DocumentName

		END
	--ENDIF
	
END
GO
--End procedure document.GetDocumentByDocumentName

--Begin procedure dropdown.GetDocumentTypeData
EXEC Utility.DropObject 'dropdown.GetDocumentTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored procedure to return data from the dropdown.DocumentType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetDocumentTypeData

@IncludeZero BIT = 0,
@PersonID INT = 0,
@HasAddUpdate BIT = 0,
@HasView BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		DG.DocumentGroupID,
		DG.DocumentGroupName,
		DT.DocumentTypeID,
		DT.DocumentTypePermissionCode,
		DT.DocumentTypeName
	FROM dropdown.DocumentType DT
		JOIN dropdown.DocumentGroup DG ON DG.DocumentGroupID = DT.DocumentGroupID
			AND DT.IsActive = 1
			AND (DT.DocumentTypeID > 0 OR @IncludeZero = 1)
			AND EXISTS
				(
				SELECT 1
				FROM permissionable.PersonPermissionable PP
				WHERE PP.PersonID = @PersonID
					AND 
						(
						@HasAddUpdate = 1 AND (PP.PermissionableLineage = 'Document.Add.' + DT.DocumentTypePermissionCode OR PP.PermissionableLineage = 'Document.Update.' + DT.DocumentTypePermissionCode)
							OR @HasView = 1 AND PP.PermissionableLineage = 'Document.View.' + DT.DocumentTypePermissionCode
							OR DT.DocumentTypePermissionCode IS NULL
						)
				)
	ORDER BY DG.DisplayOrder, DT.DocumentTypeName

END
GO
--End procedure dropdown.GetDocumentTypeData

--Begin procedure reporting.GetAssetExportAssetUnitData
EXEC Utility.DropObject 'reporting.GetAssetExportAssetUnitData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.07
-- Description:	A stored procedure to get data for the asset list export
-- =====================================================================
CREATE PROCEDURE reporting.GetAssetExportAssetUnitData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @tContactVettingData TABLE (ContactID INT, VettingTypeCode CHAR(2), VettingExpirationDate DATE, VettingOutcomeID INT)

	;
	WITH CVD AS
		(
		SELECT 
			MAX(CV.ContactVettingID) AS ContactVettingID,
			CV.ContactID,
			CV.ContactVettingTypeID
		FROM dbo.ContactVetting CV
		WHERE EXISTS
			(
			SELECT 1
			FROM asset.AssetUnit AU
				JOIN reporting.SearchResult SR ON SR.EntityID = AU.AssetID
					AND SR.EntityTypeCode = 'Asset'
					AND SR.PersonID = @PersonID
					AND AU.CommanderContactID = CV.ContactID
			)
		GROUP BY CV.ContactID, CV.ContactVettingTypeID
		)

	INSERT INTO @tContactVettingData
		(ContactID, VettingTypeCode, VettingExpirationDate, VettingOutcomeID)
	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		(SELECT C.UKVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = CVD.ContactID),
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'UK'

	UNION

	SELECT
		CVD.ContactID,
		CVT.ContactVettingTypeCode,
		(SELECT C.USVettingExpirationDate FROM dbo.Contact C WHERE C.ContactID = CVD.ContactID),
		CV.VettingOutcomeID
	FROM CVD
		JOIN dbo.ContactVetting CV ON CV.ContactVettingID = CVD.ContactVettingID
		JOIN dropdown.ContactVettingType CVT ON CVT.ContactVettingTypeID = CVD.ContactVettingTypeID
			AND CVT.ContactVettingTypeCode = 'US'

	;
	WITH SD AS
		(
		SELECT 
			C.AssetUnitID,
			COUNT(C.ContactID) AS ItemCount,
			SUM(S.StipendAmount) AS StipendAmount,
			S.StipendCategory
		FROM dbo.Contact C
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.AssetUnitID > 0
				AND C.IsActive = 1
				AND C.StipendID > 0
				AND S.StipendTypeCode = 'PoliceStipend'
		GROUP BY C.AssetUnitID, S.StipendCategory
		)

	SELECT
		A.AssetName,
		AT.AssetTypeName,
		AST.AssetStatusName,
		CASE WHEN A.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAsset,
		C.CommunityName,
		P.ProvinceName,
		AU.AssetUnitName,
		CASE WHEN AU.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAssetUnit,
		dbo.FormatContactNameByContactID(AU.CommanderContactID, 'LastFirst') AS AssetUnitCommanderNameFormatted,
		dbo.FormatContactNameByContactID(AU.DeputyCommanderContactID, 'LastFirst') AS AssetUnitDeputyCommanderNameFormatted,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'UK') AS UKVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'UK')) AS UKVettingExpirationDateFormatted,
		(SELECT VO.VettingOutcomeName FROM dropdown.VettingOutcome VO JOIN @tContactVettingData CVD ON CVD.VettingOutcomeID = VO.VettingOutcomeID AND CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'US') AS USVettingOutcomeName,
		dbo.FormatDate((SELECT CVD.VettingExpirationDate FROM @tContactVettingData CVD WHERE CVD.ContactID = AU.CommanderContactID AND CVD.VettingTypeCode = 'US')) AS USVettingExpirationDateFormatted,
		AU.DepartmentSizeName,
		AUC.AssetUnitCostName,
		(SELECT SUM(SD.StipendAmount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS StipendTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS ContactTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Command') AS CommandTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Commissioned Officer') AS CommissionedOfficerTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Non Commissioned Officer') AS NonCommissionedOfficerTotal,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID AND SD.StipendCategory = 'Police') AS PoliceTotal
	FROM asset.Asset A
		JOIN reporting.SearchResult SR ON SR.EntityID = A.AssetID
			AND SR.EntityTypeCode = 'Asset'
			AND SR.PersonID = @PersonID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN dropdown.AssetUnitCost AUC ON AUC.AssetUnitCostID = AU.AssetUnitCostID
	ORDER BY A.AssetName

END
GO
--End procedure reporting.GetAssetExportAssetUnitData

--Begin procedure reporting.GetAssetExportEquipmentData
EXEC Utility.DropObject 'reporting.GetAssetExportEquipmentData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create date:	2018.06.07
-- Description:	A stored procedure to get data for the asset list export
-- =====================================================================
CREATE PROCEDURE reporting.GetAssetExportEquipmentData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
		(
		SELECT 
			C.AssetUnitID,
			COUNT(C.ContactID) AS ItemCount,
			SUM(S.StipendAmount) AS StipendAmount,
			S.StipendCategory
		FROM dbo.Contact C
			JOIN dropdown.Stipend S ON S.StipendID = C.StipendID
				AND C.AssetUnitID > 0
				AND C.IsActive = 1
				AND C.StipendID > 0
				AND S.StipendTypeCode = 'PoliceStipend'
		GROUP BY C.AssetUnitID, S.StipendCategory
		)

	SELECT
		A.AssetName,
		AT.AssetTypeName,
		AST.AssetStatusName,
		CASE WHEN A.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAsset,
		C.CommunityName,
		P.ProvinceName,
		AU.AssetUnitName,
		CASE WHEN AU.IsActive = 1 THEN 'Yes' ELSE 'No' END AS IsActiveAssetUnit,
		(SELECT SUM(ItemCount) FROM SD WHERE SD.AssetUnitID = AU.AssetUnitID) AS ContactTotal,
		E.ItemName,
		E.TotalQuantity,
		E.TotalCost
	FROM asset.Asset A
		JOIN reporting.SearchResult SR ON SR.EntityID = A.AssetID
			AND SR.EntityTypeCode = 'Asset'
			AND SR.PersonID = @PersonID
		JOIN asset.AssetUnit AU ON AU.AssetID = A.AssetID
		JOIN dbo.Community C ON C.CommunityID = A.CommunityID
		JOIN dbo.Province P ON P.ProvinceID = C.ProvinceID
		JOIN dropdown.AssetStatus AST ON AST.AssetStatusID = A.AssetStatusID
		JOIN dropdown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
		JOIN
			(
			SELECT
				D.EquipmentCatalogID,
				D.AssetUnitID, 
				D.ItemName,
				SUM(D.TotalQuantity) AS TotalQuantity,
				SUM(D.TotalCost) AS TotalCost
			FROM
				(
				SELECT
					EC.EquipmentCatalogID,
					DI.EndUserEntityID AS AssetUnitID, 
					EC.ItemName,
					DI.Quantity AS TotalQuantity,
					DI.Quantity * EI.UnitCost AS TotalCost
				FROM procurement.DistributedInventory DI
					JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = DI.EquipmentInventoryID
					JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
						AND DI.EndUserEntityTypeCode = 'AssetUnit'

				UNION ALL

				SELECT
					EC.EquipmentCatalogID,
					C.AssetUnitID, 
					EC.ItemName,
					DI.Quantity AS TotalQuantity,
					DI.Quantity * EI.UnitCost AS TotalCost
				FROM procurement.DistributedInventory DI
					JOIN dbo.Contact C ON C.ContactID = DI.EndUserEntityID
					JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = DI.EquipmentInventoryID
					JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
						AND DI.EndUserEntityTypeCode = 'Contact'
				) D
			GROUP BY D.AssetUnitID, D.EquipmentCatalogID, D.ItemName
			) E ON E.AssetUnitID = AU.AssetUnitID
	ORDER BY A.AssetName

END
GO
--End procedure reporting.GetAssetExportEquipmentData

--Begin procedure reporting.GetEquipmentTrackerData
EXEC Utility.DropObject 'reporting.GetEquipmentTrackerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2018.07.03
-- Description:	A stored procedure to get data for the equipment tracker export
-- ============================================================================
CREATE PROCEDURE reporting.GetEquipmentTrackerData

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		ROW_NUMBER() OVER (ORDER BY EC.ItemName, EC.EquipmentCatalogID, EI.EquipmentInventoryID) AS RowIndex,
		EC.ItemName,
		EI.Supplier,
		EI.SerialNumber,
		EI.PONumber,
		EI.Quantity - EI.QuantityDistributed AS Quantity,
		core.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
		NULL AS Location
	FROM procurement.EquipmentInventory EI
		JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
		JOIN reporting.SearchResult SR ON SR.EntityID = EI.EquipmentInventoryID
			AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
			AND SR.PersonID = @PersonID
	ORDER BY EC.ItemName, EC.EquipmentCatalogID, EI.EquipmentInventoryID

END
GO
--End procedure reporting.GetEquipmentTrackerData

--Begin procedure reporting.GetSerialNumberTracker
EXEC Utility.DropObject 'reporting.GetSerialNumberTracker'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================
-- Author:			Todd Pires
-- Create Date: 2015.03.30
-- Description:	A stored procedure to get data from the procurement.EquipmentInventory table
-- =========================================================================================
CREATE PROCEDURE reporting.GetSerialNumberTracker

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

SELECT
	0 AS ContactID,
	0 AS EquipmentDistributionID,
	0 AS ConceptNoteID,
	dbo.FormatConceptNoteReferenceCode(EI.ConceptNoteID) AS ReferenceCode,
	CN.Title AS ConceptNoteTitle,
	CNT.ConceptNoteTypeName,
	EC.ItemName,
	EI.SerialNumber,
	CN.TaskCode,
	EI.IMEIMACAddress,
	EI.Sim,
	EI.BudgetCode,
	EI.UnitCost,
	EI.Quantity - PDI.DistributedInventoryTotal AS Quantity,
	EC.QuantityOfIssue,
	(EI.UnitCost * EI.Quantity * EC.QuantityOfIssue) AS TotalCost,
	'' AS DeliveredToRecipientDateFormatted,
	dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
	dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
	EI.Quantity - EI.QuantityDistributed AS QuantityInStock,
	0 AS QuantityDistributed,
	'' AS RecipientContactFullName,
	'' AS AssetName,
	'' AS AssetTypeName,
	'' AS EndUserContact,
	'' AS DeliveredToEndUserDateDateFormatted,
	NULL AS LastEquipmentAuditDate,
	'' AS LastEquipmentAuditOutcome,
	EC.ItemMake + '/' + EC.ItemModel AS MakeAndModel,
	ECC.EquipmentCatalogCategoryName,
	'' AS ContactGender,
	'' AS EmployerName,
	'' AS EmployerTypeName,
	EI.LicenseKey,
	EI.EquipmentUsageMinutes,
	EI.EquipmentUsageGB,
	dbo.FormatDate(EI.InServiceDate) AS InServiceDateDateFormatted,
	EI.Comments,
	'' AS ContactCommunity,
	'' AS AssetUnitCommunity,
	'' AS ContactProvince,
	'' AS AssetUnitProvince,
	'' AS ContactAssetUnit,
	'' AS ContactAssetUnitCommunity,
	'' AS ContactAssetUnitProvince,
	NULL AS AuditRecordUpdateDate
FROM Reporting.SearchResult SR
	JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = SR.EntityID
		AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
		AND SR.PersonID = @PersonID
	JOIN dbo.ConceptNote CN ON EI.ConceptNoteID = CN.ConceptNoteID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID 
	JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
	OUTER APPLY
		(
		SELECT ISNULL(SUM(DI.Quantity), 0) AS DistributedInventoryTotal
		FROM procurement.DistributedInventory DI
		WHERE DI.EquipmentInventoryID = SR.EntityID
		) PDI
	JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID
		AND EI.Quantity - PDI.DistributedInventoryTotal > 0 

UNION ALL

SELECT
	CO.ContactID,
	EI.ConceptNoteID,
	DI.EquipmentDistributionID,
	dbo.FormatConceptNoteReferenceCode(EI.ConceptNoteID) AS ReferenceCode,
	CN.Title AS ConceptNoteTitle,
	CNT.ConceptNoteTypeName,
	EC.ItemName,
	EI.SerialNumber,
	CN.TaskCode,
	EI.IMEIMACAddress,
	EI.Sim,
	EI.BudgetCode,
	EI.UnitCost,
	DI.Quantity,
	EC.QuantityOfIssue,
	(EI.UnitCost * EI.Quantity * EC.QuantityOfIssue) AS TotalCost,
	dbo.FormatDate(ED.DeliveredToRecipientDate) AS DeliveredToRecipientDateFormatted,
	dbo.FormatDate(EI.EquipmentDeliveredToImplementerDate) AS EquipmentDeliveredToImplementerDateFormatted,
	dbo.FormatDate(EI.EquipmentOrderDate) AS EquipmentOrderDateFormatted,
	EI.Quantity - EI.QuantityDistributed AS QuantityInStock,
	EI.QuantityDistributed,
	dbo.FormatContactNameByContactID(ED.RecipientContactID, 'LastFirst') AS RecipientContactFullName,
	AU.AssetName,
	AU.AssetTypeName,
	CASE WHEN ISNULL(AU.AssetName, '') = '' THEN dbo.FormatContactNameByContactID(CO.ContactID, 'LastFirst') ELSE AU.AssetName END AS EndUserContact,
	dbo.FormatDate(DI.DeliveredToEndUserDate) AS DeliveredToEndUserDateDateFormatted,
	CASE ISNULL(DI.EquipmentDistributionID,0) WHEN 0 THEN NULL ELSE dbo.FormatDate(procurement.GetLastEquipmentAuditDate(DI.DistributedInventoryID) ) END AS LastEquipmentAuditDate       ,
	procurement.GetLastEquipmentAuditOutcome(DI.DistributedInventoryID) AS LastEquipmentAuditOutcome,
	EC.ItemMake + '/' + EC.ItemModel,
	ECC.EquipmentCatalogCategoryName,
	CO.Gender AS ContactGender,
	CO.EmployerName,
	CO.EmployerTypeName,
	EI.LicenseKey,
	EI.EquipmentUsageMinutes,
	EI.EquipmentUsageGB,
	dbo.FormatDate(EI.InServiceDate) AS InServiceDateDateFormatted,
	EI.Comments,
	CO.CommunityName AS ContactCommunity,
	AU.CommunityName AS AssetUnitCommunity,
	CO.ProvinceName AS ContactProvince,
	AU.ProvinceName AS AssetUnitProvince,
	CO.ContactAssetUnit,
	CO.ContactAssetUnitCommunity,
	CO.ContactAssetUnitProvince,
	dbo.FormatDate((SELECT TOP 1 DIA.CreateDateTime FROM procurement.DistributedInventoryAudit DIA WHERE DIA.DistributedInventoryID = DI.DistributedInventoryID ORDER BY DIA.CreateDateTime DESC)) AS AuditRecordUpdateDate
FROM Reporting.SearchResult SR
	JOIN procurement.EquipmentInventory EI ON EI.EquipmentInventoryID = SR.EntityID
		AND SR.EntityTypeCode = 'TerritoryEquipmentInventory'
		AND SR.PersonID = @PersonID
	JOIN dbo.ConceptNote CN ON EI.ConceptNoteID = CN.ConceptNoteID
	JOIN dropdown.ConceptNoteType CNT ON CNT.ConceptNoteTypeID = CN.ConceptNoteTypeID 
	JOIN procurement.DistributedInventory DI ON EI.EquipmentInventoryID = DI.EquipmentInventoryID
	JOIN procurement.EquipmentDistribution ED ON ED.EquipmentDistributionID = DI.EquipmentDistributionID
	JOIN procurement.EquipmentCatalog EC ON EC.EquipmentCatalogID = EI.EquipmentCatalogID
	JOIN dropdown.EquipmentCatalogCategory ECC ON ECC.EquipmentCatalogCategoryID = EC.EquipmentCatalogCategoryID 
	OUTER APPLY
		(
		SELECT
			AU.AssetID,
			A.AssetName,
			AT.AssetTypeName,
			dbo.GetCommunityNameByCommunityID(A.CommunityID) AS CommunityName,
			dbo.GetProvinceNameByProvinceID( dbo.GetProvinceIDByCommunityID(A.CommunityID)) AS ProvinceName
		FROM Asset.AssetUnit AU
			JOIN Asset.Asset A ON AU.AssetID = A.AssetID
			JOIN DropDown.AssetType AT ON AT.AssetTypeID = A.AssetTypeID
				AND AU.AssetUnitID = DI.EndUserEntityID
				AND DI.EndUserEntityTypeCode = 'AssetUnit'
				AND DI.EquipmentInventoryID  = EI.EquipmentInventoryID
		) AU 
	OUTER APPLY
		(
		SELECT
			ContactID,
			(SELECT A.AssetName FROM asset.Asset A JOIN asset.AssetUnit AU ON A.AssetID = Au.AssetID AND AU.AssetUnitID = C.AssetUnitID ) AS ContactAssetUnit,
			(SELECT dbo.GetCommunityNameByCommunityID(A.CommunityID) FROM asset.Asset A JOIN asset.AssetUnit AU ON A.AssetID = Au.AssetID AND AU.AssetUnitID = C.AssetUnitID) AS ContactAssetUnitCommunity,
			(SELECT dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(A.CommunityID)) FROM asset.Asset A JOIN asset.AssetUnit AU ON A.AssetID = Au.AssetID AND AU.AssetUnitID = C.AssetUnitID) AS ContactAssetUnitProvince,
			dbo.GetCommunityNameByCommunityID(CommunityID) AS CommunityName,
			C.Gender,
			C.EmployerName,
			ET.EmployerTypeName,
			dbo.GetProvinceNameByProvinceID(dbo.GetProvinceIDByCommunityID(C.CommunityID)) AS ProvinceName
		FROM dbo.Contact C
			JOIN dropDown.EmployerType ET ON ET.EmployerTypeID = C.EmployerTypeID
				AND DI.EndUserEntityTypeCode = 'Contact'
				AND DI.EndUserEntityID = C.ContactID
				AND ED.IsActive = 0 
		) CO

END
GO
--End procedure reporting.GetSerialNumberTracker