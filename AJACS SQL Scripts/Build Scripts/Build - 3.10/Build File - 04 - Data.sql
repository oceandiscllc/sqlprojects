USE AJACS
GO

UPDATE T
SET T.PermissionableLineage = REPLACE(T.PermissionableLineage, 'Document.AddUpdate', 'Document.Add')
FROM dbo.MenuItemPermissionableLineage T
WHERE T.PermissionableLineage LIKE 'Document.AddUpdate%'
GO

UPDATE T
SET T.PermissionableLineage = REPLACE(T.PermissionableLineage, 'Document.AddUpdate', 'Document.Add')
FROM permissionable.PermissionableTemplatePermissionable T
WHERE T.PermissionableLineage LIKE 'Document.AddUpdate%'
GO

UPDATE T
SET T.PermissionableLineage = REPLACE(T.PermissionableLineage, 'Document.AddUpdate', 'Document.Add')
FROM permissionable.PersonPermissionable T
WHERE T.PermissionableLineage LIKE 'Document.AddUpdate%'
GO

INSERT INTO permissionable.PermissionableTemplatePermissionable
	(PermissionableTemplateID, PermissionableID, PermissionableLineage)
SELECT
	PTP.PermissionableTemplateID,
	P2.PermissionableID,
	P2.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP, permissionable.Permissionable P2
WHERE EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P1 
	WHERE P1.PermissionableID = PTP.PermissionableID
		AND P1.PermissionableLineage = 'ConceptNote.View.Export'
	)
	AND P2.PermissionableLineage IN ('ConceptNote.View.ExportConceptNote','ConceptNote.View.ExportActivitySheet','ConceptNote.View.ExportActivityCloseDown')
GO

INSERT INTO permissionable.PermissionableTemplatePermissionable
	(PermissionableTemplateID, PermissionableID, PermissionableLineage)
SELECT
	PTP.PermissionableTemplateID,
	P2.PermissionableID,
	P2.PermissionableLineage
FROM permissionable.PermissionableTemplatePermissionable PTP, permissionable.Permissionable P2
WHERE EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P1 
	WHERE P1.PermissionableID = PTP.PermissionableID
		AND P1.PermissionableLineage = 'WeeklyReport.AddUpdate.Export'
	)
	AND P2.PermissionableLineage IN ('WeeklyReport.AddUpdate.ExportReport', 'WeeklyReport.AddUpdate.ExportSummary')
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PP1.PersonID,
	P1.PermissionableLineage
FROM permissionable.PersonPermissionable PP1, permissionable.Permissionable P1
WHERE EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P2
	WHERE P2.PermissionableLineage = 'ConceptNote.View.Export'
	)
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PersonPermissionable PP2
		WHERE PP2.PersonID = PP1.PersonID
			AND PP2.PermissionableLineage IN ('ConceptNote.View.ExportConceptNote','ConceptNote.View.ExportActivitySheet','ConceptNote.View.ExportActivityCloseDown')
		)
	AND P1.PermissionableLineage IN ('ConceptNote.View.ExportConceptNote','ConceptNote.View.ExportActivitySheet','ConceptNote.View.ExportActivityCloseDown')
GO

INSERT INTO permissionable.PersonPermissionable
	(PersonID, PermissionableLineage)
SELECT
	PP1.PersonID,
	P1.PermissionableLineage
FROM permissionable.PersonPermissionable PP1, permissionable.Permissionable P1
WHERE EXISTS
	(
	SELECT 1
	FROM permissionable.Permissionable P2
	WHERE P2.PermissionableLineage = 'WeeklyReport.AddUpdate.Export'
	)
	AND NOT EXISTS
		(
		SELECT 1
		FROM permissionable.PersonPermissionable PP2
		WHERE PP2.PersonID = PP1.PersonID
			AND PP2.PermissionableLineage IN ('WeeklyReport.AddUpdate.ExportReport', 'WeeklyReport.AddUpdate.ExportSummary')
		)
	AND P1.PermissionableLineage IN ('WeeklyReport.AddUpdate.ExportReport', 'WeeklyReport.AddUpdate.ExportSummary')
GO

EXEC utility.MenuItemAddUpdate 
	@ParentMenuItemCode='Admin', 
	@NewMenuItemCode='DocumentLogList', 
	@NewMenuItemLink='/documentlog/list', 
	@NewMenuItemText='Download Logs', 
	@BeforeMenuItemCode='PermissionableList', 
	@PermissionableLineageList='DocumentLog.List'
GO

EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Admin'
GO