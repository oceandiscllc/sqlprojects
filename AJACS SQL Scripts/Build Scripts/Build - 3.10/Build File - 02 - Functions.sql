USE AJACS
GO

--Begin function document.CanDownloadDocuments
EXEC Utility.DropObject 'document.CanDownloadDocuments'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2015.02.20
-- Description:	A stored function to verify that a person has not exceeded his download caps
-- ==========================================================================================
CREATE FUNCTION document.CanDownloadDocuments
(
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0

	SELECT @bReturn = 
		CASE 
			WHEN P.DocumentCountCap - ISNULL((SELECT COUNT(DL.PersonID) FROM document.DocumentLog DL WHERE DL.PersonID = P.PersonID), 0) >= 0
				AND P.DocumentSizeCap - ISNULL((SELECT SUM(DL.PhysicalFileSize) FROM document.DocumentLog DL WHERE DL.PersonID = P.PersonID), 0) >= 0
			THEN 1
			ELSE 0
		END 

	FROM dbo.Person P
	WHERE P.PersonID = @PersonID

	RETURN @bReturn

END
GO
--End function document.CanDownloadDocuments

