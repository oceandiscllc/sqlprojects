USE AJACS
GO

--Begin table dbo.Person
DECLARE @TableName VARCHAR(250) = 'dbo.Person'

EXEC utility.AddColumn @TableName, 'DocumentCountCap', 'INT', '10'
EXEC utility.AddColumn @TableName, 'DocumentSizeCap', 'INT', '2097152'
GO
--End table dbo.Person

--Begin table document.DocumentLog
DECLARE @TableName VARCHAR(250) = 'document.DocumentLog'

EXEC utility.DropObject @TableName

CREATE TABLE document.DocumentLog
	(
	DocumentLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	DocumentID INT,
	DocumentTypeCode VARCHAR(50),
	DataExportReportType VARCHAR(50),
	PhysicalFileSize BIGINT,
	DownloadPermitted BIT,
	LogDateTime DATETIME
	)
	
EXEC utility.SetDefaultConstraint @TableName, 'DocumentID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DownloadPermitted', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'LogDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PhysicalFileSize', 'BIGINT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DocumentLogID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_DocumentLog', 'PersonID,LogDateTime DESC'
GO
--End table document.DocumentLog

