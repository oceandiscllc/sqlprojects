USE AJACS
GO

--Begin table dbo.Contact
DECLARE @TableName VARCHAR(250) = 'dbo.Contact'

EXEC utility.AddColumn @TableName, 'ArabicFirstName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'ArabicMiddleName', 'NVARCHAR(200)'
EXEC utility.AddColumn @TableName, 'ArabicLastName', 'NVARCHAR(200)'
EXEC utility.DropColumn @TableName, 'BrandingRequirements'
GO
--End table dbo.Contact

--Begin table dbo.ConceptNote
DECLARE @TableName VARCHAR(250) = 'dbo.ConceptNote'

EXEC utility.AddColumn @TableName, 'BrandingRequirements', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'TaskCode', 'VARCHAR(20)'
EXEC utility.AddColumn @TableName, 'VettingRequirements', 'VARCHAR(MAX)'
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.ConceptNote') AND SC.Name = 'AwardeeSubContractor1')
	EXEC sp_RENAME 'dbo.ConceptNote.AwardeeSubContractor1', 'AwardeeSubContractorID1', 'COLUMN';
--ENDIF
GO

IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.ID = OBJECT_ID('dbo.ConceptNote') AND SC.Name = 'AwardeeSubContractor2')
	EXEC sp_RENAME 'dbo.ConceptNote.AwardeeSubContractor2', 'AwardeeSubContractorID2', 'COLUMN';
--ENDIF
GO
--End table dbo.ConceptNote

--Begin table procurement.CommunityEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.CommunityEquipmentInventory'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE procurement.CommunityEquipmentInventory
		(
		CommunityEquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
		CommunityID INT,
		EquipmentInventoryID INT,
		Quantity INT,
		ConceptNoteID INT,
		DistributionDate DATE
		)
	
	EXEC utility.SetDefaultConstraint @TableName, 'CommunityID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0
	
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityEquipmentInventoryID'
	EXEC utility.SetIndexClustered 'IX_ConceptNoteEthnicity', @TableName, 'CommunityID,EquipmentInventoryID,Quantity'

	END
--ENDIF
GO
--End table procurement.CommunityEquipmentInventory

--Begin table procurement.ProvinceEquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.ProvinceEquipmentInventory'

IF NOT EXISTS (SELECT 1 FROM sys.tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @TableName)
	BEGIN

	CREATE TABLE procurement.ProvinceEquipmentInventory
		(
		ProvinceEquipmentInventoryID INT IDENTITY(1,1) NOT NULL,
		ProvinceID INT,
		EquipmentInventoryID INT,
		Quantity INT,
		ConceptNoteID INT,
		DistributionDate DATE
		)
	
	EXEC utility.SetDefaultConstraint @TableName, 'ConceptNoteID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0
	EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', 0
	
	EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProvinceEquipmentInventoryID'
	EXEC utility.SetIndexClustered 'IX_ConceptNoteEthnicity', @TableName, 'ProvinceID,EquipmentInventoryID,Quantity'

	END
--ENDIF
GO
--End table procurement.ProvinceEquipmentInventory
