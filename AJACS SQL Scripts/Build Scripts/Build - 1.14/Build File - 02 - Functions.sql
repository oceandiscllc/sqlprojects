USE AJACS
GO

--Begin function dbo.FormatStaticGoogleMapForSpotReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForSpotReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForSpotReport
(
@SpotReportID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @GResult VARCHAR(MAX) = ''
	DECLARE @GLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=640x480'
	
	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + REPLACE(ID.HexColor, '#', '') +  '.png' + '|' 
			+ CAST(C.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(C.Longitude AS VARCHAR(MAX))
	 FROM SpotReportCommunity SRC
		JOIN dbo.Community C ON SRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND SRC.SpotReportID = @SpotReportID
		

	SELECT
		@GResult += '&markers=icon:' 
			+ dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') 
			+ '/assets/img/icons/' + IT.Icon + '|' 
			+ CAST(I.Latitude AS VARCHAR(MAX)) 
			+ ','
			+ CAST(I.Longitude AS VARCHAR(MAX))
	 FROM SpotReportIncident SRI
		JOIN dbo.Incident I ON SRI.IncidentID = I.IncidentID 
		JOIN dropdown.IncidentType IT ON IT.IncidentTypeID =I.IncidentTypeID
			AND SRI.SpotReportID = @SpotReportID

	IF (LEN(@GResult) - LEN(REPLACE(@GResult, 'markers', ''))) / len('markers') <= 1
		SELECT @GLink += '&zoom=10'
	--ENDIF

	RETURN RTRIM(@GLink + @GResult)

END
GO
--End function dbo.FormatStaticGoogleMapForSpotReport

--Begin function dbo.FormatStaticGoogleMapForWeeklyReport
EXEC utility.DropObject 'dbo.FormatStaticGoogleMapForWeeklyReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			John Lyons
-- Create date:	2015.04.29
-- Description:	A function to return communities for placement on a google map
--
-- Author:			John Lyons & Todd Pires
-- Create date:	2015.05.09
-- Description:	Implemented the date range and icon grouping
-- ===========================================================================
CREATE FUNCTION dbo.FormatStaticGoogleMapForWeeklyReport
(
@ProvinceID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cLink VARCHAR(MAX) = 'https://maps.googleapis.com/maps/api/staticmap?size=600x480'
	DECLARE @cMarkers VARCHAR(MAX) = ''
	
	;
	WITH D AS
		(
		SELECT
			REPLACE(ID.HexColor, '#', '') +  '.png' AS Icon,
			LEFT(CAST(C.Latitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(C.Longitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(C.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM WeeklyReport.Community WRC
		JOIN dbo.Community C ON WRC.CommunityID = C.communityID 
		JOIN dropdown.ImpactDecision ID ON ID.ImpactDecisionID = C.ImpactDecisionID
			AND C.ProvinceID = @ProvinceID
	
		UNION
	
		SELECT
			it.icon AS Icon,
			LEFT(CAST(I.Latitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) 
			+ ','
			+ LEFT(CAST(I.Longitude AS VARCHAR(MAX)), CHARINDEX('.', CAST(I.Latitude AS VARCHAR(MAX))) + 5) AS LatLong
		FROM dbo.Incident I
		JOIN Dropdown.IncidentType IT ON i.incidenttypeid = it.incidenttypeid
			AND I.IncidentDate >= (SELECT TOP 1 weeklyreport.WeeklyReport.StartDate FROM weeklyreport.WeeklyReport)
			AND I.IncidentDate <= (SELECT TOP 1 weeklyreport.WeeklyReport.EndDate FROM weeklyreport.WeeklyReport)
			AND EXISTS
				(
				SELECT 1
				FROM dbo.IncidentCommunity IC
				WHERE IC.IncidentID = I.IncidentID 
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Community C 
						WHERE C.CommunityID = IC.CommunityID AND C.ProvinceID = @ProvinceID
						)
	
				UNION
	
				SELECT 1
				FROM dbo.IncidentProvince IP
				WHERE IP.IncidentID = I.IncidentID AND  IP.ProvinceID = @ProvinceID
					AND EXISTS
						(
						SELECT 1
						FROM weeklyreport.Province P 
						WHERE P.ProvinceID = IP.ProvinceID AND P.ProvinceID = @ProvinceID
						)
				)
		)
	
	SELECT @cMarkers +=
		'&markers=icon:' 
		+ REPLACE(dbo.GetServerSetupValueByServerSetupKey('SiteURL', '') , 'https', 'http') 
		+ '/i/'
		+ E.Icon
		+ (
			SELECT '|' + D.LatLong 
			FROM D
			WHERE D.Icon = E.Icon
			FOR XML PATH(''), TYPE
			).value('.[1]', 'VARCHAR(MAX)')
	FROM D AS E
	GROUP BY E.Icon
	ORDER BY E.Icon

	IF (LEN(@cMarkers) - LEN(REPLACE(@cMarkers, 'markers', ''))) / len('markers') <= 1
		SELECT @cLink += '&zoom=10'
	--ENDIF

	RETURN (@cLink + @cMarkers)

END
GO
--End function dbo.FormatStaticGoogleMapForWeeklyReport