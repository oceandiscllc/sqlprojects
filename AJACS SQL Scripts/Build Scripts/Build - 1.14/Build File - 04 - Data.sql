USE AJACS
GO

SET IDENTITY_INSERT dbo.SubContractor ON
GO

IF NOT EXISTS (SELECT 1 FROM dbo.SubContractor SC WHERE SC.SubContractorID = 0)
	INSERT INTO dbo.SubContractor (SubContractorID) VALUES (0)
--ENDIF
GO

SET IDENTITY_INSERT dbo.SubContractor OFF
GO

UPDATE dbo.ConceptNote 
SET ConceptNoteTypeID = 0
GO

UPDATE dropdown.FundingSource 
SET FundingSourceName = 'US' 
WHERE FundingSourceName = 'CSO'
GO

UPDATE dropdown.FundingSource 
SET FundingSourceName = 'EU' 
WHERE FundingSourceName = 'Secretariat'
GO

TRUNCATE TABLE dropdown.ConceptNoteType
GO
SET IDENTITY_INSERT dropdown.ConceptNoteType ON
GO

INSERT INTO dropdown.ConceptNoteType
	(ConceptNoteTypeID)
VALUES
	(0)
GO

SET IDENTITY_INSERT dropdown.ConceptNoteType OFF
GO

INSERT INTO dropdown.ConceptNoteType 
	(ConceptNoteTypeName, DisplayOrder, IsActive)
VALUES
	('Integrated Legitimate Structures', 1, 1),
	('Access to Justice', 2, 1),
	('Police Development', 3, 1),
	('Community Engagement', 4, 1),
	('M&E', 5, 1),
	('Research', 6, 1)
GO

INSERT INTO dropdown.BudgetSubType 
	(BudgetSubTypeName, DisplayOrder, IsActive)
VALUES 
	('N/A', 7, 1)
GO

DELETE 
FROM workflow.WorkflowStep 
WHERE WorkflowID = 1
GO

INSERT INTO workflow.WorkflowStep 
	(ParentWorkflowStepID, WorkflowID, WorkflowStepNumber, WorkflowStepName)
VALUES
	(0, 1, 1, 'Concept Note Draft'),
	(0, 1, 2, 'Concept Note Edit'),
	(0, 1, 3, 'Concept Note Approval'),
	(0, 1, 4, 'Activity Sheet Draft'),
	(4, 1, 4, 'M&E Review'),
	(4, 1, 4, 'Activity Sheet Draft'),
	(0, 1, 5, 'Activity Sheet Final Review'),
	(0, 1, 6, 'Activity Sheet Approval'),
	(0, 1, 7, 'Activity Sheet Donor Approval'),
	(0, 1, 8, 'Activity Implementation'),
	(0, 1, 9, 'Activity Closedown')
GO

UPDATE workflow.WorkflowStep 
SET ParentWorkflowStepID = (SELECT WorkflowStepID FROM workflow.WorkflowStep WHERE WorkflowID = 1 AND WorkflowStepNumber = 4 AND ParentWorkflowStepID = 0)
WHERE WorkflowID = 1 
	AND ParentWorkflowStepID = 4
GO

DELETE 
FROM workflow.WorkflowStepWorkflowAction 
WHERE WorkflowID = 1 
	AND (WorkflowStepNumber = 7 OR WorkflowStepNumber = 6 OR (WorkflowStepNumber = 8 AND WorkflowActionID = 2))
GO

INSERT INTO workflow.WorkflowStepWorkflowAction 
	(WorkflowID, WorkflowStepNumber, WorkflowActionID, DisplayOrder)
VALUES
	(1, 6, 2, 4),
	(1, 6, 6, 1),
	(1, 7, 1, 2),
	(1, 7, 2, 4),
	(1, 7, 3, 3),
	(1, 7, 7, 1)
GO