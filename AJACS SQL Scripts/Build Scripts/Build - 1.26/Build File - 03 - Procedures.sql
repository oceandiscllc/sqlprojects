USE AJACS
GO

--Begin procedure logicalframework.GetObjectiveByObjectiveID
EXEC Utility.DropObject 'logicalframework.GetObjectiveByObjectiveID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	A stored procedure to return Objective data
--
-- Author:			Todd Pires
-- Create date:	2015.03.03
-- Description:	Added fields
-- ========================================================
CREATE PROCEDURE logicalframework.GetObjectiveByObjectiveID

@ObjectiveID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		LFS.LogicalFrameworkStatusID,
		LFS.LogicalFrameworkStatusName,
		O1.IsActive,
		O1.ObjectiveDescription,
		O1.ObjectiveID, 
		O1.ObjectiveName, 
		O1.ParentObjectiveID, 

		CASE
			WHEN O1.ParentObjectiveID = 0
			THEN NULL
			ELSE (SELECT O2.ObjectiveName FROM logicalframework.Objective O2 WHERE O2.ObjectiveID = O1.ParentObjectiveID)
		END AS ParentObjectiveName,
		
		O1.StatusUpdateDescription,
		OT.ObjectiveTypeID, 
		OT.ObjectiveTypeName,
		dbo.GetEntityTypeNameByEntityTypeCode('Objective') AS EntityTypeName
	FROM logicalframework.Objective O1
		JOIN dropdown.ObjectiveType OT ON OT.ObjectiveTypeID = O1.ObjectiveTypeID
		JOIN dropdown.LogicalFrameworkStatus LFS ON LFS.LogicalFrameworkStatusID = O1.LogicalFrameworkStatusID
			AND O1.ObjectiveID = @ObjectiveID

END
GO
--End procedure logicalframework.GetObjectiveByObjectiveID