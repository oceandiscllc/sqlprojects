USE AJACS
GO

--Begin table logicalframework.Objective
DECLARE @TableName VARCHAR(250) = 'logicalframework.Objective'

EXEC utility.AddColumn @TableName, 'LogicalFrameworkStatusID', 'INT'
EXEC utility.AddColumn @TableName, 'StatusUpdateDescription', 'NVARCHAR(MAX)'

EXEC utility.SetDefaultConstraint @TableName, 'LogicalFrameworkStatusID', 'INT', 0
GO
--End table logicalframework.Objective
