USE AJACS
GO

--Begin table dbo.CommunityAssetUnitExpense
DECLARE @TableName VARCHAR(250) = 'dbo.CommunityAssetUnitExpense'

EXEC utility.DropObject @TableName

CREATE TABLE dbo.CommunityAssetUnitExpense
	(
	CommunityAssetUnitExpenseID INT IDENTITY(1,1) NOT NULL,
	CommunityAssetUnitID INT,
	ExpenseAmountAuthorized NUMERIC(18,2),
	ExpenseAuthorizedDate DATE,
	ExpenseAmountPaid NUMERIC(18,2),
	ExpensePaidDate DATE,
	Notes VARCHAR(MAX),
	ProvinceID INT,
	PaymentMonth INT,
	PaymentYear INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CommunityAssetUnitID', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountAuthorized', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'ExpenseAmountPaid', 'NUMERIC(18,2)', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentMonth', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'PaymentYear', 'INT', 0
EXEC utility.SetDefaultConstraint @TableName, 'ProvinceID', 'INT', 0

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'CommunityAssetUnitExpenseID'
EXEC utility.SetIndexClustered @TableName, 'IX_CommunityAssetUnitExpense', 'PaymentYear,PaymentMonth,ProvinceID,CommunityAssetUnitID'
GO
--End table dbo.CommunityRound

--Begin table dbo.ContactStipendPayment
DECLARE @TableName VARCHAR(250) = 'dbo.ContactStipendPayment'

EXEC utility.AddColumn @TableName, 'StipendTypeCode', 'VARCHAR(50)'
EXEC utility.DropColumn @TableName, 'Notes'
GO
--End table dbo.ContactStipendPayment

--Begin table dropdown.Stipend
DECLARE @TableName VARCHAR(250) = 'dropdown.Stipend'

EXEC utility.AddColumn @TableName, 'StipendTypeCode', 'VARCHAR(50)'
EXEC utility.AddColumn @TableName, 'StipendCategory', 'VARCHAR(50)'
GO
--End table dropdown.Stipend
