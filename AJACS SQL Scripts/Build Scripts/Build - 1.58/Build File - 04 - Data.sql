USE AJACS
GO

--Begin table dbo.ContactStipendPayment
UPDATE CST
SET CST.StipendTypeCode = 'PoliceStipend'
FROM dbo.ContactStipendPayment CST
WHERE CST.StipendTypeCode IS NULL
GO
--End table dbo.ContactStipendPayment

--Begin table dbo.EntityType
--End table dbo.EntityType

--Begin table dbo.MenuItem
UPDATE MI
SET 
	MI.MenuItemCode = 'PoliceStipend',
	MI.MenuItemText = 'Police Stipends'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'Stipends'
GO

UPDATE MI
SET 
	MI.MenuItemCode = 'PoliceStipendList',
	MI.MenuItemLink = '/contact/list?ContactTypeCode=PoliceStipend'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode IN ('StipendList', 'PoliceStipendList')
GO

UPDATE MI
SET MI.MenuItemCode = 'PolicePaymentList'
FROM dbo.MenuItem MI
WHERE MI.MenuItemCode = 'PaymentList'
GO

EXEC utility.MenuItemAddUpdate @NewMenuItemCode='JusticeStipend', @NewMenuItemText='Justice Stipends', @ParentMenuItemCode='Implementation', @AfterMenuItemCode='PoliceStipend'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='JusticeStipendList', @NewMenuItemLink='/contact/list?ContactTypeCode=JusticeStipend', @NewMenuItemText='Recipients', @ParentMenuItemCode='JusticeStipend'
GO
EXEC utility.MenuItemAddUpdate @NewMenuItemCode='JusticePaymentList', @NewMenuItemLink='/contact/justicepaymentlist', @NewMenuItemText='Payments', @ParentMenuItemCode='JusticeStipend', @AfterMenuItemCode='JusticeStipendList', @PermissionableLineageList='Contact.JusticePaymentList'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='JusticeStipend'
GO
EXEC utility.UpdateParentPermissionableLineageByMenuItemCode @MenuItemCode='Implementation'
GO
--Begin table dbo.MenuItem

--Begin table dropdown.ContactType
UPDATE CT
SET CT.ContactTypeName = 'Police Stipend'
FROM dropdown.ContactType CT
WHERE CT.ContactTypeCode = 'PoliceStipend'
GO
--End table dropdown.ContactType

--Begin table dropdown.Stipend
UPDATE S
SET S.StipendTypeCode = 'PoliceStipend'
FROM dropdown.Stipend S
WHERE S.StipendTypeCode IS NULL
GO

IF NOT EXISTS (SELECT 1 FROM dropdown.Stipend S WHERE S.StipendTypeCode = 'JusticeStipend')
	BEGIN

	INSERT INTO dropdown.Stipend 
		(StipendTypeCode,StipendCategory,StipendName,StipendAmount)
	VALUES
		('JusticeStipend', 'Category 1', 'Headquarters Management', 300),
		('JusticeStipend', 'Category 1', 'Head Office / Center', 250),
		('JusticeStipend', 'Category 1', 'Assistance', 250),
		('JusticeStipend', 'Category 1', 'Dep Manager', 250),
		('JusticeStipend', 'Category 2', 'Registrar', 200),
		('JusticeStipend', 'Category 3', 'Correspondent', 125),
		('JusticeStipend', 'Category 3', 'Cleaner', 100)
	
	END
--ENDIF
GO
--Begin table dropdown.Stipend
