USE AJACS
GO

--Begin function workflow.CanHaveAddUpdate
EXEC utility.DropObject 'workflow.CanHaveAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to determine if a PeronID has AddUpdate accesss to a specific EntityTypeCode and EntityID
--
-- Author:			Todd Pires
-- Create date:	2016.04.20
-- Description:	Added support for the "recurring" entitytypecodes to check workflow.Workflow and not workflow.EntityWorkflowStepGroupPerson
-- ========================================================================================================================================
CREATE FUNCTION workflow.CanHaveAddUpdate
(
@EntityTypeCode VARCHAR(50),
@EntityID INT,
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @nCanHaveAddUpdate BIT = 0
	DECLARE @nEntityID INT = @EntityID
	DECLARE @nWorkflowStepCount INT = 0
	DECLARE @nWorkflowStepNumber INT = 0

	IF EXISTS (SELECT 1 FROM dbo.EntityType ET WHERE ET.EntityTypeCode = @EntityTypeCode AND ET.HasMenuItemAccessViaWorkflow = 1)
		BEGIN
	
		IF @EntityTypeCode = 'CommunityProvinceEngagementUpdate'
			SELECT TOP 1 @nEntityID = T.CommunityProvinceEngagementUpdateID FROM communityprovinceengagementupdate.CommunityProvinceEngagementUpdate T ORDER BY T.CommunityProvinceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'FIFUpdate'
			SELECT TOP 1 @nEntityID = T.FIFUpdateID FROM fifupdate.FIFUpdate T ORDER BY T.FIFUpdateID DESC
		ELSE IF @EntityTypeCode = 'JusticeUpdate'
			SELECT TOP 1 @nEntityID = T.JusticeUpdateID FROM justiceupdate.[JusticeUpdate] T ORDER BY T.JusticeUpdateID DESC
		ELSE IF @EntityTypeCode = 'PoliceEngagementUpdate'
			SELECT TOP 1 @nEntityID = T.PoliceEngagementUpdateID FROM policeengagementupdate.PoliceEngagementUpdate T ORDER BY T.PoliceEngagementUpdateID DESC
		ELSE IF @EntityTypeCode = 'RecommendationUpdate'
			SELECT TOP 1 @nEntityID = T.RecommendationUpdateID FROM recommendationupdate.RecommendationUpdate T ORDER BY T.RecommendationUpdateID DESC
		ELSE IF @EntityTypeCode = 'RiskUpdate'
			SELECT TOP 1 @nEntityID = T.RiskUpdateID FROM riskupdate.RiskUpdate T ORDER BY T.RiskUpdateID DESC
		ELSE IF @EntityTypeCode = 'WeeklyReport'
			SELECT TOP 1 @nEntityID = T.WeeklyReportID FROM weeklyreport.WeeklyReport T ORDER BY T.WeeklyReportID DESC
		--ENDIF
			
		END
	--ENDIF

	IF @nEntityID = 0
		BEGIN
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.Workflow W
				JOIN workflow.WorkflowStep WS ON WS.WorkflowID = W.WorkflowID
				JOIN workflow.WorkflowStepGroup WSG ON WSG.WorkflowStepID = WS.WorkflowStepID
				JOIN workflow.WorkflowStepGroupPerson WSGP ON WSGP.WorkflowStepGroupID = WSG.WorkflowStepGroupID
					AND W.IsActive = 1
					AND W.EntityTypeCode = @EntityTypeCode
					AND WS.WorkflowStepNumber = 1
					AND WSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
			
		END
	ELSE 
		BEGIN
	
		SET @nWorkflowStepCount = workflow.GetWorkflowStepCount(@EntityTypeCode, @nEntityID)
		SET @nWorkflowStepNumber = workflow.GetWorkflowStepNumber(@EntityTypeCode, @nEntityID)
	
		IF EXISTS 
			(
			SELECT 1
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @nEntityID
					AND EWSGP.WorkflowStepNumber = 
						CASE
							WHEN @nWorkflowStepNumber > @nWorkflowStepCount AND ET.CanRejectAfterFinalApproval = 1
							THEN @nWorkflowStepCount
							ELSE @nWorkflowStepNumber
						END
					AND EWSGP.PersonID = @PersonID
			)
			SET @nCanHaveAddUpdate = 1
		--ENDIF
	
		END
	--ENDIF
	
	RETURN @nCanHaveAddUpdate
END
GO
--End function workflow.CanHaveAddUpdate

--Begin function workflow.GetEntityWorkflowData
EXEC utility.DropObject 'workflow.GetEntityWorkflowData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================================
-- Author:			Todd Pires
-- Create date:	2015.10.28
-- Description:	A function to return workflow metadata for a specific entity type code and entity id
-- =================================================================================================

CREATE FUNCTION workflow.GetEntityWorkflowData
(
@EntityTypeCode VARCHAR(50),
@EntityID INT
)

RETURNS @tTable TABLE 
	(
	WorkflowStepName VARCHAR(250),
	WorkflowStepNumber INT,
	WorkflowStepCount INT,
	CanRejectAfterFinalApproval BIT
	)

AS
BEGIN

	IF @EntityID = 0
		BEGIN
	
		INSERT INTO @tTable
			(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
		SELECT 
			WS1.WorkflowStepName,
			WS1.WorkflowStepNumber,
			(SELECT MAX(WS2.WorkflowStepNumber) FROM workflow.Workflow W2 JOIN workflow.WorkflowStep WS2 ON WS2.WorkflowID = W2.WorkflowID AND W2.IsActive = 1 AND W2.EntityTypeCode = W1.EntityTypeCode) AS WorkflowStepCount,
			ET.CanRejectAfterFinalApproval
		FROM workflow.Workflow W1
			JOIN workflow.WorkflowStep WS1 ON WS1.WorkflowID = W1.WorkflowID
				AND W1.IsActive = 1
				AND W1.EntityTypeCode = @EntityTypeCode
				AND WS1.WorkflowStepNumber = 1
			JOIN dbo.EntityType ET ON ET.EntityTypeCode = W1.EntityTypeCode
	
		END
	ELSE
		BEGIN
	
		IF workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) <= workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID)
			BEGIN
			
			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT TOP 1
				EWSGP.WorkflowStepName,
				EWSGP.WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM workflow.EntityWorkflowStepGroupPerson EWSGP
				JOIN dbo.EntityType ET ON ET.EntityTypeCode = EWSGP.EntityTypeCode
					AND EWSGP.EntityTypeCode = @EntityTypeCode
					AND EWSGP.EntityID = @EntityID
					AND EWSGP.IsComplete = 0
			ORDER BY EWSGP.WorkflowStepNumber

			END
		ELSE
			BEGIN

			INSERT INTO @tTable
				(WorkflowStepName, WorkflowStepNumber, WorkflowStepCount, CanRejectAfterFinalApproval)
			SELECT

				CASE
					WHEN @EntityTypeCode = 'ConceptNote'
					THEN 'Activity Closed'
					ELSE 'Approved'
				END,
				
				workflow.GetWorkflowStepNumber(@EntityTypeCode, @EntityID) AS WorkflowStepNumber,
				workflow.GetWorkflowStepCount(@EntityTypeCode, @EntityID) AS WorkflowStepCount,
				ET.CanRejectAfterFinalApproval
			FROM dbo.EntityType ET 
			WHERE ET.EntityTypeCode = @EntityTypeCode
			
			END
		--ENDIF
			
		END
	--ENDIF
	
	RETURN

END
GO
--End function workflow.GetEntityWorkflowData