USE AJACS
GO

--Begin procedure procurement.GetEquipmentDistributionPlanByConceptNoteID
EXEC Utility.DropObject 'procurement.GetEquipmentDistributionPlanByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Kevin Ross
-- Create date:	2015.06.28
-- Description:	A stored procedure to get data from the procurement.EquipmentDistributionPlan table
-- ================================================================================================
CREATE PROCEDURE procurement.GetEquipmentDistributionPlanByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @EquipmentDistributionPlanID INT

	SELECT @EquipmentDistributionPlanID = EDP.EquipmentDistributionPlanID
	FROM procurement.EquipmentDistributionPlan EDP
	WHERE EDP.ConceptNoteID = @ConceptNoteID

	SELECT 
		EDP.EquipmentDistributionPlanID,
		EDP.ConceptNoteID,
		EDP.Title,
		EDP.Summary,
		EDP.ExportRoute,
		EDP.CurrentSituation,
		EDP.Aim,
		EDP.PlanOutline,
		EDP.Phase1,
		EDP.Phase2,
		EDP.Phase3,
		EDP.Phase4,
		EDP.Phase5,
		EDP.OperationalResponsibility,
		EDP.Annexes,
		EDP.Distribution,
		EDP.WorkflowStepNumber,
		CN.Title AS ConceptNoteTitle, 
		EDP.Errata,
		EDP.ErrataTitle,
		EDP.OperationalSecurity
	FROM procurement.EquipmentDistributionPlan EDP
		JOIN dbo.ConceptNote CN ON CN.ConceptNoteID = EDP.ConceptNoteID
			AND EDP.ConceptNoteID = @ConceptNoteID

	SELECT
		WA.WorkflowActionCode,
		WA.WorkflowActionName
	FROM workflow.WorkflowStepWorkflowAction WSWA
		JOIN workflow.WorkflowAction WA ON WA.WorkflowActionID = WSWA.WorkflowActionID
		JOIN workflow.Workflow W ON W.WorkflowID = WSWA.WorkflowID
			AND W.EntityTypeCode = 'EquipmentDistributionPlan'
			AND WSWA.WorkflowStepNumber = 
				CASE
					WHEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID) > 0
					THEN (SELECT EDP.WorkflowStepNumber FROM procurement.EquipmentDistributionPlan EDP WHERE EDP.EquipmentDistributionPlanID = @EquipmentDistributionPlanID)
					ELSE 1
				END
	ORDER BY WSWA.DisplayOrder

END
GO
--End procedure procurement.GetEquipmentDistributionPlanByConceptNoteID


--Begin procedure reporting.EDPProvincesAndCommunitiesByConceptNoteID
EXEC Utility.DropObject 'reporting.EDPProvincesAndCommunitiesByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2015.10.10
-- Description:	A stored procedure to get reporting data
-- =====================================================
CREATE PROCEDURE reporting.EDPProvincesAndCommunitiesByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cCommunityList VARCHAR(MAX) = ''
	DECLARE @nProvinceID INT
	DECLARE @tTable1 TABLE (CommunityID INT, ProvinceID INT)
	DECLARE @tTable2 TABLE (ProvinceID INT, CommunityNameList VARCHAR(MAX))

	INSERT INTO @tTable1
		(CommunityID, ProvinceID)
	SELECT DISTINCT
		C1.CommunityID,

		CASE
			WHEN C1.CommunityID > 0
			THEN dbo.GetProvinceIDByCommunityID(C1.CommunityID)
			ELSE C1.ProvinceID
		END AS ProvinceID
	FROM dbo.ConceptNote CN
		JOIN dbo.ConceptNoteContactEquipment CNCE ON CNCE.ConceptNoteID = CN.ConceptNoteID
			AND CN.ConceptNoteID = @ConceptNoteID
		JOIN dbo.Contact C1 ON C1.ContactID = CNCE.ContactID

	INSERT INTO @tTable2
		(ProvinceID)
	SELECT DISTINCT
		T1.ProvinceID
	FROM @tTable1 T1

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
		SELECT T1.ProvinceID
		FROM @tTable1 T1
		ORDER BY T1.ProvinceID
		
	OPEN oCursor
	FETCH oCursor INTO @nProvinceID
	WHILE @@fetch_status = 0
		BEGIN
	
		SET @cCommunityList = NULL
	
		SELECT @cCommunityList = COALESCE(@cCommunityList + ', ', '') + dbo.GetCommunityNameByCommunityID(T1.CommunityID)
		FROM @tTable1 T1
		WHERE T1.ProvinceID = @nProvinceID
			AND T1.CommunityID > 0

		UPDATE T2
		SET T2.CommunityNameList = @cCommunityList
		FROM @tTable2 T2
		WHERE T2.ProvinceID = @nProvinceID

		FETCH oCursor INTO @nProvinceID
	
		END
	--END WHILE
	
	CLOSE oCursor
	DEALLOCATE oCursor

	SELECT
		P.ProvinceName,
		T2.CommunityNameList,
		dbo.FormatStaticGoogleMapForEquipmentDistributionPlan(@ConceptNoteID, T2.ProvinceID) AS MapLink
	FROM @tTable2 T2
		JOIN dbo.Province P ON P.ProvinceID = T2.ProvinceID
	ORDER BY P.ProvinceName 
END
GO
--End procedure reporting.EDPProvincesAndCommunitiesByConceptNoteID

--Begin procedure procurement.getEDPByConceptNoteID
EXEC Utility.DropObject 'procurement.getEDPByConceptNoteID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			John Lyons
-- Create date:	2015.10.14
-- Description:	A stored procedure to get data from the procurement.EquipmentDistributionPlan table
-- ================================================================================================
CREATE PROCEDURE procurement.getEDPByConceptNoteID

@ConceptNoteID INT

AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		dbo.FormatConceptNoteReferenceCode(CN.ConceptNoteID) AS ReferenceCode,

		CASE 
			WHEN CN.ConceptNoteContactEquipmentDistributionDate IS NULL
			THEN 'Planned'
			ELSE 'Distributed'
		END AS EquipmentDistributionPlanStatus,

		EDP.ExportRoute, 
		EDP.CurrentSituation, 
		EDP.Aim, 
		EDP.PlanOutline, 
		EDP.Phase1, 
		EDP.Phase2, 
		EDP.Phase3, 
		EDP.Phase4, 
		EDP.Phase5, 
		EDP.OperationalResponsibility, 
		EDP.Annexes, 
		EDP.Distribution, 
		EDP.Title, 
		EDP.Summary,
		EDP.OperationalSecurity,
		EDP.ErrataTitle,
		EDP.Errata
	FROM procurement.EquipmentDistributionPlan EDP 
		JOIN dbo.ConceptNote CN ON EDP.ConceptNoteID = CN.ConceptNoteID 
			AND EDP.ConceptNoteID = @ConceptNoteID

END
GO
--End procedure procurement.getEDPByConceptNoteID

--Begin procedure reporting.GetOpsFundReport
EXEC Utility.DropObject 'reporting.GetOpsFundReport'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			John Lyons
-- Create Date: 2015.07.01
-- Description:	A stored procedure to data for the Stipend Payment Report
-- ======================================================================
CREATE PROCEDURE reporting.GetOpsFundReport

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	WITH SD AS
	(
	SELECT
		CSP.ContactStipendPaymentID,
		CSP.StipendAmountAuthorized,
		CSP.CommunityID,

	CASE
		WHEN CSP.CommunityID > 0
		THEN [dbo].[GetProvinceIDByCommunityID](CSP.CommunityID)
		ELSE CSP.ProvinceID
	END AS ProvinceID
	FROM dbo.ContactStipendPayment CSP
		JOIN reporting.SearchResult SR ON SR.EntityID = CSP.ContactStipendPaymentID 
			AND SR.EntityTypeCode = 'ContactStipendPayment'
			AND SR.PersonID = @PersonID
	)

	SELECT 
		
		(SELECT P.ProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ProvinceName,
		(SELECT P.ArabicProvinceName FROM dbo.Province P WHERE P.ProvinceID = SD.ProvinceID) AS ArabicProvinceName,
		(SELECT C.CommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS CommunityName,
		(SELECT C.ArabicCommunityName FROM dbo.Community C WHERE C.CommunityID = SD.CommunityID) AS ArabicCommunityName,
		CSP.StipendName,
		CA.CommunityAssetName,
		CAU.CommunityAssetUnitName,
		CSP.ContactID,
		dbo.FormatPersonNameByPersonID(@PersonID ,'LastFirst') AS AJACSRepresentative,
		dbo.FormatContactNameByContactID(CSP.ContactID, 'LastFirst') AS BeneficaryName,
		C.ArabicFirstName +' ' + C.ArabicMiddlename + ' ' + C.ArabicLastName  AS ArabicBeneficaryName,
		SD.StipendAmountAuthorized,
		dbo.FormatDate(StipendAuthorizedDate) AS StipendAuthorizedDateFormatted,
		DateName(month, DateAdd(month, CSP.PaymentMonth, 0 ) - 1) + ' - ' + CAST(PaymentYear AS VARCHAR(50)) AS PaymentMonthYear
	FROM dbo.ContactStipendPayment CSP
		JOIN SD ON SD.ContactStipendPaymentID = CSP.ContactStipendPaymentID
		JOIN dbo.Contact   C  ON C.contactid = csp.ContactID
		JOIN dbo.CommunityAsset CA ON CA.CommunityAssetID = C.CommunityAssetID
		JOIN dbo.CommunityAssetUnit CAU ON CAU.CommunityAssetUnitID = C.CommunityAssetUnitID
	ORDER BY ProvinceName , CommunityName, StipendName, BeneficaryName
	
END
GO
--End procedure reporting.GetOpsFundReport