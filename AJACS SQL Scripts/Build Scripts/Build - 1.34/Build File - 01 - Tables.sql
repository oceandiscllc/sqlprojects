USE AJACS
GO

--Begin table procurement.EquipmentDistributionPlan
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentDistributionPlan'

EXEC utility.AddColumn @TableName, 'Errata', 'NVARCHAR(MAX)'
EXEC utility.AddColumn @TableName, 'ErrataTitle', 'NVARCHAR(250)'
EXEC utility.AddColumn @TableName, 'OperationalSecurity', 'NVARCHAR(MAX)'
GO
--End table procurement.EquipmentDistributionPlan
