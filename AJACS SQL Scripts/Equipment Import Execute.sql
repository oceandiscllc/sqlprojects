USE AJACS
GO

--TRUNCATE TABLE procurement.EquipmentCatalog
--TRUNCATE TABLE procurement.EquipmentInventory
--TRUNCATE TABLE procurement.CommunityEquipmentInventory
--TRUNCATE TABLE procurement.ProvinceEquipmentInventory
--TRUNCATE TABLE procurement.EquipmentAuditOutcome
--GO

--UPDATE EII
--SET EII.CommunityID = C.CommunityID
--FROM procurement.EquipmentInventoryImport EII
--	JOIN dbo.Community C ON C.CommunityName = EII.CommunityName
--GO

--UPDATE EII
--SET EII.ProvinceID = P.ProvinceID
--FROM procurement.EquipmentInventoryImport EII
--	JOIN dbo.Province P ON P.ProvinceName = EII.ProvinceName
--GO

SET IDENTITY_INSERT procurement.EquipmentCatalog ON
GO

INSERT INTO procurement.EquipmentCatalog
	(EquipmentCatalogID, ItemName, EquipmentCatalogCategoryID, UnitOfIssue, UnitCost, Notes, IsCommon, Impact, IsActive, Risk)
SELECT
	 EquipmentCatalogID, ItemName, EquipmentCatalogCategoryID, UnitOfIssue, UnitCost, Notes, IsCommon, Impact, IsActive, Risk
FROM procurement.EquipmentCatalogImport
GO

SET IDENTITY_INSERT procurement.EquipmentCatalog OFF
GO

--Begin table procurement.EquipmentInventory
DECLARE @TableName VARCHAR(250) = 'procurement.EquipmentInventory'

EXEC utility.AddColumn @TableName, 'EquipmentInventoryImportID', 'INT'

EXEC utility.SetDefaultConstraint @TableName, 'EquipmentInventoryImportID', 'INT', 0

GO
--End table procurement.EquipmentInventory

INSERT INTO procurement.EquipmentInventory
	(EquipmentInventoryImportID, BudgetCode, Supplier, UnitCost, Quantity, SerialNumber, Comments, ConceptNoteID, EquipmentCatalogID, EquipmentStatusID)
SELECT
	 EquipmentInventoryImportID, BudgetCode, Supplier, UnitCost, Quantity, SerialNumber, Comments, ConceptNoteID, EquipmentCatalogID, EquipmentStatusID
FROM procurement.EquipmentInventoryImport
GO

INSERT INTO procurement.EquipmentAuditOutcome
	(EquipmentInventoryID, AuditOutcomeID, EquipmentAuditOutcomeDate)
SELECT
	EI.EquipmentInventoryID,
	EII.AuditOutcomeID, 
	EII.AuditDate
FROM procurement.EquipmentInventory EI
	JOIN procurement.EquipmentInventoryImport EII ON EII.EquipmentInventoryImportID = EI.EquipmentInventoryImportID
GO

INSERT INTO procurement.CommunityEquipmentInventory
	(CommunityID, EquipmentInventoryID, Quantity, DistributionDate)
SELECT
	EII.CommunityID,
	EI.EquipmentInventoryID,
	EII.Quantity,
	EII.AuditDate
FROM procurement.EquipmentInventory EI
	JOIN procurement.EquipmentInventoryImport EII ON EII.EquipmentInventoryImportID = EI.EquipmentInventoryImportID
		AND EII.CommunityID > 0
GO	

INSERT INTO procurement.ProvinceEquipmentInventory
	(ProvinceID, EquipmentInventoryID, Quantity, DistributionDate)
SELECT
	EII.ProvinceID,
	EI.EquipmentInventoryID,
	EII.Quantity,
	EII.AuditDate
FROM procurement.EquipmentInventory EI
	JOIN procurement.EquipmentInventoryImport EII ON EII.EquipmentInventoryImportID = EI.EquipmentInventoryImportID
		AND EII.CommunityID = 0
GO	