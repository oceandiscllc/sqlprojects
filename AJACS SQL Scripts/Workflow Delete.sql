DELETE W
FROM workflow.Workflow W
WHERE WorkflowID = 

DELETE WS
FROM workflow.WorkflowStep WS
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.Workflow W
	WHERE W.WorkflowID = WS.WorkflowID
	)

DELETE WSG
FROM workflow.WorkflowStepGroup WSG
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.WorkflowStep WS
	WHERE WS.WorkflowStepID = WSG.WorkflowStepID
	)

DELETE WSGP
FROM workflow.WorkflowStepGroupPerson WSGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.WorkflowStepGroup WSG
	WHERE WSG.WorkflowStepGroupID = WSGP.WorkflowStepGroupID
	)

DELETE EWSGP
FROM workflow.EntityWorkflowStepGroupPerson EWSGP
WHERE NOT EXISTS
	(
	SELECT 1
	FROM workflow.Workflow W
	WHERE W.WorkflowID = EWSGP.WorkflowID
	)
	OR NOT EXISTS
		(
		SELECT 1
		FROM workflow.WorkflowStep WS
		WHERE WS.WorkflowStepID = EWSGP.WorkflowStepID
		)
	OR NOT EXISTS
		(
		SELECT 1
		FROM workflow.WorkflowStepGroup WSG
		WHERE WSG.WorkflowStepGroupID = EWSGP.WorkflowStepGroupID
		)
	OR NOT EXISTS
		(
		SELECT 1
		FROM dbo.Person P
		WHERE P.PersonID = EWSGP.PersonID
		)