DECLARE @nSpotReportID INT
DECLARE @tOutput TABLE (SpotReportID INT)

UPDATE T
SET T.IsCritical = 0
OUTPUT INSERTED.SpotReportID INTO @tOutput
FROM dbo.SpotReport T
WHERE T.IsCritical = 1
	AND DATEDIFF(d, T.SpotReportDate, getDate()) > CAST(AJACS.dbo.GetServerSetupValueByServerSetupKey('SpotReportIsCriticalExpirationDays', '10') AS INT)
	
DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT O.SpotReportID
	FROM @tOutput O
	ORDER BY O.SpotReportID
	
OPEN oCursor
FETCH oCursor INTO @nSpotReportID
WHILE @@fetch_status = 0
	BEGIN
	
	EXEC eventlog.LogSpotReportAction @nSpotReportID, 'update', 0, 'System job expired the IsCritical flag'

	FETCH oCursor INTO @nSpotReportID
	
	END
--END WHILE
		
CLOSE oCursor
DEALLOCATE oCursor	
GO