-- File Name:	MakeMy_Menu.sql
-- Build Key:	Build - 1.1 - 2019.01.22 09.20.28

USE MakeMy_Menu
GO

-- ==============================================================================================================================
-- Tables:
--		client.ClientContact
--		client.ClientProductInventory
--		client.ClientProductLocation
--		client.Contact
--
-- Triggers:
--		client.TR_Client ON client.Client
--		client.TR_ClientContact ON client.ClientContact
--		client.TR_ClientPerson ON client.ClientPerson
--		client.TR_ClientProduct ON client.ClientProduct
--		client.TR_ClientProductLocation ON client.ClientProductLocation
--		client.TR_Contact ON client.Contact
--		client.TR_PricingModel ON client.PricingModel
--		person.TR_Person ON person.Person
--		product.TR_Distributor ON product.Distributor
--		product.TR_Producer ON product.Producer
--		product.TR_Product ON product.Product
--		product.TR_ProductProducer ON product.ProductProducer
--		product.TR_ProductStyle ON product.ProductStyle
--
-- Procedures:
--		client.DeletePricingModelByPricingModelID
--		client.GetClientProductLocationData
--		client.SetIsMenuItem
--		client.SetMenuPrice
--		client.UpdateMenuPricesByClientID
--		client.UpdateMenuPricesByPricingModelID
--		person.DeletePersonByPersonID
-- ==============================================================================================================================

--Begin file Build File - 01 - Tables.sql
--Begin table client.ClientContact
DECLARE @TableName VARCHAR(250) = 'client.ClientContact'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientContact
	(
	ClientContactID INT IDENTITY(1, 1) NOT NULL,
	ContactID INT,
	ClientID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientContact', 'ClientID,ContactID'
GO
--End table client.ClientContact

--Begin table client.Contact
DECLARE @TableName VARCHAR(250) = 'client.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE client.Contact
	(
	ContactID INT IDENTITY(1, 1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	JobTitle VARCHAR(100),
	EmailAddress1 VARCHAR(320),
	EmailAddress2 VARCHAR(320),
	PhoneNumber VARCHAR(50),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	PostalCode VARCHAR(10),
	ISOCountryCode3 CHAR(3),
	Notes VARCHAR(MAX),
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_Contact', 'LastName,FirstName'
GO
--End table client.Contact

--Begin table client.ClientProduct
DECLARE @TableName VARCHAR(250) = 'client.ClientProduct'

EXEC utility.DropColumn @TableName, 'InStock'

EXEC utility.AddColumn @TableName, 'OrderPoint', 'INT', '0'
GO
--End table client.ClientProduct

--Begin table client.ClientProductInventory
DECLARE @TableName VARCHAR(250) = 'client.ClientProductInventory'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientProductInventory
	(
	ClientProductInventoryID INT IDENTITY(1, 1) NOT NULL,
	ClientProductID INT,
	ClientProductLocationID INT,
	Quantity INT,
	CreatePersonID INT,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientProductLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientProductInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientProductInventory', 'ClientProductID'
GO
--End table client.ClientProductInventory

--Begin table client.ClientProductLocation
DECLARE @TableName VARCHAR(250) = 'client.ClientProductLocation'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientProductLocation
	(
	ClientProductLocationID INT IDENTITY(0, 1) NOT NULL,
	ClientID INT,
	ClientProductLocationName VARCHAR(250),
	Capacity INT, 
	FillSequence INT, 
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'Capacity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FillSequence', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientProductLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientProductLocation', 'ClientID,ClientProductLocationName'

EXEC utility.InsertIdentityValue @TableName, 'ClientProductLocationID', 0
GO
--End table client.ClientProductLocation

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.AddColumn @TableName, 'UniversalProductCode', 'VARCHAR(25)'
GO
--End table product.Product

EXEC utility.AddColumn 'client.Client', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientContact', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientPerson', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientProduct', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientProductLocation', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.Contact', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.PricingModel', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'person.Person', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Distributor', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Producer', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Product', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductProducer', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductStyle', 'UpdatePersonID', 'INT', '0'
--End file Build File - 01 - Tables.sql

--Begin file Build File - 02 - Functions.sql

--End file Build File - 02 - Functions.sql

--Begin file Build File - 03 - Procedures.sql
--Begin procedure client.DeletePricingModelByPricingModelID
EXEC utility.DropObject 'client.DeletePricingModelByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.08
-- Description:	A stored procedure to delete data from the client.PricingModel table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- =================================================================================
CREATE PROCEDURE client.DeletePricingModelByPricingModelID

@PricingModelID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'PricingModel',
		@PricingModelID,
		(
		SELECT PM.*
		FOR XML RAW('PricingModel'), ELEMENTS
		)
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	DELETE PM
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	UPDATE CP
	SET 
		CP.PricingModelID = 0,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.PricingModel PM
		WHERE PM.PricingModelID = CP.PricingModelID
		)

END
GO
--End procedure client.DeletePricingModelByPricingModelID

--Begin procedure client.GetClientProductLocationData
EXEC utility.DropObject 'client.GetClientProductLocationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.10
-- Description:	A stored procedure to return data from the client.ClientProductLocation table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ==========================================================================================
CREATE PROCEDURE client.GetClientProductLocationData

@ClientID INT,
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientProductLocationID,
		T.ClientProductLocationName,
		T.DisplayOrder,
		T.IsActive
	FROM client.ClientProductLocation T
	WHERE (T.ClientProductLocationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientProductLocationName, T.ClientProductLocationID

END
GO
--End procedure client.GetClientProductLocationData

--Begin procedure client.SetIsMenuItem
EXEC utility.DropObject 'client.SetIsMenuItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.29
-- Description:	A stored procedure to update the IsMenuItem bit in the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ==============================================================================================
CREATE PROCEDURE client.SetIsMenuItem

@ClientProductID INT = 0,
@IsMenuItem BIT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.IsMenuItem = @IsMenuItem,
		CP.MenuPriceDateTime = CASE WHEN @IsMenuItem = 1 THEN getDate() ELSE NULL END,
		CP.MenuPricePersonID = CASE WHEN @IsMenuItem = 1 THEN @UpdatePersonID ELSE 0 END,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID 

END
GO
--End procedure client.SetIsMenuItem

--Begin procedure client.SetMenuPrice
EXEC utility.DropObject 'client.SetMenuPrice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.SetMenuPrice

@ClientProductID INT = 0,
@Upcharge NUMERIC(18,2) = 0,
@PricingModelID INT = 0,
@MenuPrice NUMERIC(18,2) = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @PricingModelID != 0
		BEGIN

		SELECT @MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + @Upcharge) / C.BaseRound, 0) * C.BaseRound
		FROM client.ClientProduct CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
				AND CP.ClientProductID = @ClientProductID
				AND PM.PricingModelID = @PricingModelID

		END
	--ENDIF

	UPDATE CP
	SET 
		CP.MenuPrice = @MenuPrice,
		CP.PricingModelID = @PricingModelID,
		CP.Upcharge = @Upcharge,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID

	SELECT FORMAT(@MenuPrice, 'C', 'en-us') AS MenuPrice

END
GO
--End procedure client.SetMenuPrice

--Begin procedure client.UpdateMenuPricesByClientID
EXEC utility.DropObject 'client.UpdateMenuPricesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByClientID

@ClientID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ClientID = @ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = CP.PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByClientID

--Begin procedure client.UpdateMenuPricesByPricingModelID
EXEC utility.DropObject 'client.UpdateMenuPricesByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByPricingModelID

@PricingModelID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = @PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByPricingModelID

--Begin procedure person.DeletePersonByPersonID
EXEC utility.DropObject 'person.DeletePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to delete data from the person.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ===============================================================================================
CREATE PROCEDURE person.DeletePersonByPersonID

@PersonID INT,
@ClientID INT,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'ClientPerson',
		CP.PersonID,
		(
		SELECT CP.*
		FOR XML RAW('ClientPerson'), ELEMENTS
		)
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	DELETE CP
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'Person',
		P.PersonID,
		(
		SELECT P.*
		FOR XML RAW('Person'), ELEMENTS
		)
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

	DELETE P
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

END
GO
--End procedure person.DeletePersonByPersonID
--End file Build File - 03 - Procedures.sql

--Begin file Build File - 04 - Data.sql
--Begin table client.ClientProductLocation
TRUNCATE TABLE client.ClientProductLocation
GO

EXEC utility.InsertIdentityValue 'client.ClientProductLocation', 'ClientProductLocationID', 0
GO
--End table client.ClientProductLocation

--End file Build File - 04 - Data.sql

--Begin file Build File - 05 - Triggers.sql
--Begin trigger client.TR_Client
EXEC utility.DropObject 'client.TR_Client'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.Client table
-- ================================================================
CREATE TRIGGER client.TR_Client ON client.Client AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Client',
		I.ClientID,
		(
		SELECT I.*
		FOR XML RAW('Client'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.Client ENABLE TRIGGER TR_Client
GO
--End trigger client.TR_Client

--Begin trigger client.TR_ClientContact
EXEC utility.DropObject 'client.TR_ClientContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientContact table
-- =======================================================================
CREATE TRIGGER client.TR_ClientContact ON client.ClientContact AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientContact',
		I.ClientContactID,
		(
		SELECT I.*
		FOR XML RAW('ClientContact'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientContact ENABLE TRIGGER TR_ClientContact
GO
--End trigger client.TR_ClientContact

--Begin trigger client.TR_ClientPerson
EXEC utility.DropObject 'client.TR_ClientPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientPerson table
-- ======================================================================
CREATE TRIGGER client.TR_ClientPerson ON client.ClientPerson AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientPerson',
		I.ClientPersonID,
		(
		SELECT I.*
		FOR XML RAW('ClientPerson'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientPerson ENABLE TRIGGER TR_ClientPerson
GO
--End trigger client.TR_ClientPerson

--Begin trigger client.TR_ClientProduct
EXEC utility.DropObject 'client.TR_ClientProduct'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientProduct table
-- =======================================================================
CREATE TRIGGER client.TR_ClientProduct ON client.ClientProduct AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientProduct',
		I.ClientProductID,
		(
		SELECT I.*
		FOR XML RAW('ClientProduct'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientProduct ENABLE TRIGGER TR_ClientProduct
GO
--End trigger client.TR_ClientProduct

--Begin trigger client.TR_ClientProductLocation
EXEC utility.DropObject 'client.TR_ClientProductLocation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientProductLocation table
-- ===============================================================================
CREATE TRIGGER client.TR_ClientProductLocation ON client.ClientProductLocation AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientProductLocation',
		I.ClientProductLocationID,
		(
		SELECT I.*
		FOR XML RAW('ClientProductLocation'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientProductLocation ENABLE TRIGGER TR_ClientProductLocation
GO
--End trigger client.TR_ClientProductLocation

--Begin trigger client.TR_Contact
EXEC utility.DropObject 'client.TR_Contact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.Contact table
-- =================================================================
CREATE TRIGGER client.TR_Contact ON client.Contact AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Contact',
		I.ContactID,
		(
		SELECT I.*
		FOR XML RAW('Contact'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.Contact ENABLE TRIGGER TR_Contact
GO
--End trigger client.TR_Contact

--Begin trigger client.TR_PricingModel
EXEC utility.DropObject 'client.TR_PricingModel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.PricingModel table
-- ======================================================================
CREATE TRIGGER client.TR_PricingModel ON client.PricingModel AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'PricingModel',
		I.PricingModelID,
		(
		SELECT I.*
		FOR XML RAW('PricingModel'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.PricingModel ENABLE TRIGGER TR_PricingModel
GO
--End trigger client.TR_PricingModel

--Begin trigger person.TR_Person
EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the person.Person table
-- ================================================================
CREATE TRIGGER person.TR_Person ON person.Person AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Person',
		I.PersonID,
		(
		SELECT I.*
		FOR XML RAW('Person'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End trigger person.TR_Person

--Begin trigger product.TR_Distributor
EXEC utility.DropObject 'product.TR_Distributor'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Distributor table
-- ======================================================================
CREATE TRIGGER product.TR_Distributor ON product.Distributor AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Distributor',
		I.DistributorID,
		(
		SELECT I.*
		FOR XML RAW('Distributor'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Distributor ENABLE TRIGGER TR_Distributor
GO
--End trigger product.TR_Distributor

--Begin trigger product.TR_Producer
EXEC utility.DropObject 'product.TR_Producer'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Producer table
-- ===================================================================
CREATE TRIGGER product.TR_Producer ON product.Producer AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Producer',
		I.ProducerID,
		(
		SELECT I.*
		FOR XML RAW('Producer'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Producer ENABLE TRIGGER TR_Producer
GO
--End trigger product.TR_Producer

--Begin trigger product.TR_Product
EXEC utility.DropObject 'product.TR_Product'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Product table
-- ==================================================================
CREATE TRIGGER product.TR_Product ON product.Product AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Product',
		I.ProductID,
		(
		SELECT I.*
		FOR XML RAW('Product'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Product ENABLE TRIGGER TR_Product
GO
--End trigger product.TR_Product

--Begin trigger product.TR_ProductProducer
EXEC utility.DropObject 'product.TR_ProductProducer'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.ProductProducer table
-- ==========================================================================
CREATE TRIGGER product.TR_ProductProducer ON product.ProductProducer AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ProductProducer',
		I.ProductProducerID,
		(
		SELECT I.*
		FOR XML RAW('ProductProducer'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.ProductProducer ENABLE TRIGGER TR_ProductProducer
GO
--End trigger product.TR_ProductProducer

--Begin trigger product.TR_ProductStyle
EXEC utility.DropObject 'product.TR_ProductStyle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.ProductStyle table
-- =======================================================================
CREATE TRIGGER product.TR_ProductStyle ON product.ProductStyle AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ProductStyle',
		I.ProductStyleID,
		(
		SELECT I.*
		FOR XML RAW('ProductStyle'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.ProductStyle ENABLE TRIGGER TR_ProductStyle
GO
--End trigger product.TR_ProductStyle

EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
EXEC utility.DropObject 'eventlog.LogClientAction'
EXEC utility.DropObject 'eventlog.LogLoginAction'
EXEC utility.DropObject 'person.SetAccountLockedOut'
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO
--End file Build File - 05 - Triggers.sql

--Begin build tracking
INSERT INTO core.BuildLog (BuildKey) VALUES ('Build - 1.1 - 2019.01.22 09.20.28')
GO
--End build tracking

