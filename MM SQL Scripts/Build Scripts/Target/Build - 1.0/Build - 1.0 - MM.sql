-- File Name:	Build - 1.0 - MM.sql
-- Build Key:	Build - 1.0 - 2018.12.19 23.03.20

USE christophercrouch_com
GO

-- ==============================================================================================================================
-- Schemas:
--		client
--		core
--		dropdown
--		eventlog
--		import
--		person
--		product
--
-- Tables:
--		client.Client
--		client.ClientPerson
--		client.ClientProduct
--		client.PricingModel
--		core.Announcement
--		core.BuildLog
--		core.EntityType
--		core.SystemSetup
--		dropdown.Country
--		dropdown.PackageType
--		dropdown.ProductType
--		dropdown.Region
--		dropdown.Status
--		eventlog.EventLog
--		person.Person
--		product.Distributor
--		product.Producer
--		product.Product
--		product.ProductProducer
--		product.ProductStyle
--
-- Functions:
--		core.FormatDate
--		core.FormatDateTime
--		core.FormatTime
--		core.GetDescendantMenuItemsByMenuItemCode
--		core.GetEntityTypeNameByEntityTypeCode
--		core.GetEntityTypeNamePluralByEntityTypeCode
--		core.GetSystemSetupValueBySystemSetupKey
--		core.ListToTable
--		core.NullIfEmpty
--		core.YesNoFormat
--		dropdown.GetCountryNameByISOCountryCode
--		dropdown.GetRegionNameByISORegionCode
--		person.FormatPersonNameByPersonID
--		person.HashPassword
--		person.IsSuperAdministrator
--		person.ValidatePassword
--		product.GetAdditionalProductProducerNames
--		product.GetIsMenuItemButtonHTML
--		product.GetProducerNameByProductID
--		product.GetProducerNameHTMLByProductID
--		product.GetProducerNameListByProductID
--		utility.StripCharacters
--
-- Procedures:
--		client.DeletePricingModelByPricingModelID
--		client.GetClientByClientID
--		client.GetPricingModel
--		client.SetIsMenuItem
--		client.SetMenuPrice
--		client.UpdateMenuPricesByClientID
--		client.UpdateMenuPricesByPricingModelID
--		core.DeleteAnnouncementByAnnouncementID
--		core.EntityTypeAddUpdate
--		core.GetAnnouncementByAnnouncementID
--		core.GetAnnouncements
--		core.GetAnnouncementsByDate
--		core.GetSystemSetupDataBySystemSetupID
--		core.GetSystemSetupValuesBySystemSetupKey
--		core.GetSystemSetupValuesBySystemSetupKeyList
--		core.MenuItemAddUpdate
--		core.SystemSetupAddUpdate
--		dropdown.GetCountryData
--		dropdown.GetPackageTypeData
--		dropdown.GetProductTypeData
--		dropdown.GetRegionData
--		dropdown.GetStatusData
--		eventlog.GetEventLogDataByEventLogID
--		eventlog.LogAnnouncementAction
--		eventlog.LogClientAction
--		eventlog.LogLoginAction
--		eventlog.LogPersonAction
--		person.DeletePersonByPersonID
--		person.GeneratePassword
--		person.GetPersonByPersonID
--		person.SetAccountLockedOut
--		person.SetPersonPermissionables
--		person.ValidateEmailAddress
--		person.ValidateLogin
--		product.GetDistributorByDistributorID
--		product.GetDistributorData
--		product.GetMenuDataByClientID
--		product.GetProducerByProducerID
--		product.GetProducerData
--		product.GetProductByProductID
--		product.GetProductsByProducerID
--		product.GetProductStyleData
--		utility.AddColumn
--		utility.AddSchema
--		utility.DropColumn
--		utility.DropIndex
--		utility.DropObject
--		utility.InsertIdentityValue
--		utility.SetDefaultConstraint
--		utility.SetIndexClustered
--		utility.SetIndexNonClustered
--		utility.SetPrimaryKeyClustered
--		utility.SetPrimaryKeyNonClustered
-- ==============================================================================================================================

--Begin file Build File - 01 - Prerequisites.sql
--Begin schema utility
IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
	BEGIN
	
	DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
	
	EXEC (@cSQL)
	
	END
--ENDIF
GO
--End schema utility

--Begin dependencies

--Begin procedure utility.DropObject
IF EXISTS (SELECT 1 FROM sys.objects O WHERE O.object_id = OBJECT_ID('utility.DropObject') AND O.type IN ('P','PC'))
	DROP PROCEDURE utility.DropObject
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to drop objects from the database
-- =================================================================
CREATE PROCEDURE utility.DropObject
@ObjectName VARCHAR(MAX)

AS
BEGIN

DECLARE @cSQL VARCHAR(MAX)
DECLARE @cType VARCHAR(10)

IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	SET @ObjectName = 'dbo.' + @ObjectName
--ENDIF

SELECT @cType = O.Type
FROM sys.objects O 
WHERE O.Object_ID = OBJECT_ID(@ObjectName)

IF @cType IS NOT NULL
	BEGIN
	
	IF @cType IN ('D', 'PK')
		BEGIN
		
		SELECT
			@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
		FROM sys.objects O1
			JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
			JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
			JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
				AND S1.Name + '.' + O1.Name = @ObjectName
			
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('FN','IF','TF','FS','FT')
		BEGIN
		
		SET @cSQL = 'DROP FUNCTION ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType IN ('P','PC')
		BEGIN
		
		SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'SN'
		BEGIN
		
		SET @cSQL = 'DROP SYNONYM ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'TR'
		BEGIN
		
		SET @cSQL = 'DROP TRIGGER ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'U'
		BEGIN
		
		SET @cSQL = 'DROP TABLE ' + @ObjectName
		EXEC (@cSQL)
		
		END
	ELSE IF @cType = 'V'
		BEGIN
		
		SET @cSQL = 'DROP VIEW ' + @ObjectName
		EXEC (@cSQL)
		
		END
	--ENDIF

	END
ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP SCHEMA ' + @ObjectName
	EXEC (@cSQL)

	END
ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
	BEGIN

	SET @cSQL = 'DROP TYPE ' + @ObjectName
	EXEC (@cSQL)

	END
--ENDIF
		
END	
GO
--End procedure utility.DropObject
--End dependencies

--Begin procedure utility.AddColumn
EXEC utility.DropObject 'utility.AddColumn'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddColumn

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD ' + @ColumnName + ' ' + @DataType
		EXEC (@cSQL)

		IF @Default IS NOT NULL
			EXEC utility.SetDefaultConstraint @TableName, @ColumnName, @DataType, @Default
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.AddColumn

--Begin procedure utility.AddSchema
EXEC utility.DropObject 'utility.AddSchema'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.AddSchema

@SchemaName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
		BEGIN
	
		DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
	
		EXEC (@cSQL)
	
		END
	--ENDIF

END
GO
--End procedure utility.AddSchema

--Begin procedure utility.DropColumn
EXEC utility.DropObject 'utility.DropColumn'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropColumn
	@TableName VARCHAR(250),
	@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + DC.Name
		FROM sys.default_constraints DC 
			JOIN sys.objects O ON O.object_id = DC.parent_object_id 
				AND O.type = 'U' 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = O.object_id 
				AND C.column_id = DC.parent_column_id 
				AND S.Name + '.' + O.Name = @TableName
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	IF EXISTS (SELECT 1 FROM sys.foreign_keys	FK JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id JOIN sys.objects O ON O.object_id = FK.parent_object_id JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = FKC.parent_object_id AND C.Column_ID = FKC.parent_Column_ID AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName) 
		BEGIN
		
		SELECT 
			@cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + FK.Name
		FROM sys.foreign_keys FK 
			JOIN sys.foreign_key_columns FKC ON FK.object_id = FKC.constraint_object_id 
			JOIN sys.objects O ON O.object_id = FK.parent_object_id 
			JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
			JOIN sys.columns C ON C.object_id = FKC.parent_object_id 
				AND C.Column_ID = FKC.parent_Column_ID 
				AND S.Name + '.' + O.Name = @TableName 
				AND C.Name = @ColumnName

		EXEC (@cSQL)
		
		END
	--ENDIF

	DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR 
		SELECT 
			'DROP STATISTICS ' + @TableName + '.' + S1.Name + ''
		FROM sys.stats S1 
			JOIN sys.tables T1 ON T1.Object_ID = S1.Object_ID
			JOIN sys.schemas S2 ON S2.schema_ID = T1.schema_ID 
			JOIN sys.stats_columns SC ON SC.stats_id = S1.stats_id 
				AND T1.Object_ID = SC.Object_ID
			JOIN sys.columns C ON C.column_id = SC.column_id 
				AND T1.Object_ID = C.Object_ID
			JOIN sys.types T2 ON T2.system_type_id = C.system_type_id
				AND S1.user_created = 1
				AND S2.Name + '.' + T1.Name = @TableName 
				AND C.Name = @ColumnName
	
	OPEN oCursor
	FETCH oCursor INTO @cSQL
	WHILE @@fetch_status = 0
		BEGIN
		
		EXECUTE (@cSQL)
		
		FETCH oCursor INTO @cSQL
		
		END
	--END WHILE
		
	CLOSE oCursor
	DEALLOCATE oCursor

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' DROP COLUMN [' + @ColumnName + ']'
		EXEC (@cSQL)

		END
	--ENDIF

END
GO
--End procedure utility.DropColumn

--Begin procedure utility.DropIndex
EXEC utility.DropObject 'utility.DropIndex'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.DropIndex

@TableName VARCHAR(250),
@IndexName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF
	
	IF EXISTS (SELECT 1 FROM sys.indexes I WHERE I.Object_ID = OBJECT_ID(@TableName) AND I.Name = @IndexName)
		BEGIN

		SET @cSQL = 'DROP INDEX ' + @IndexName + ' ON ' + @TableName
		EXEC (@cSQL)
		
		END
	--ENDIF

END
GO
--End procedure utility.DropIndex

--Begin procedure utility.InsertIdentityValue
EXEC utility.DropObject 'utility.InsertIdentityValue'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.InsertIdentityValue

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@ColumnValue INT

AS
BEGIN
	SET NOCOUNT ON;

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	DECLARE @cSQL VARCHAR(MAX) = 'SET IDENTITY_INSERT ' + @TableName + ' ON;'
	SET @cSQL += ' INSERT INTO ' + @TableName + ' (' + @ColumnName + ') VALUES (' + CAST(@ColumnValue AS VARCHAR(10)) + ');'
	SET @cSQL += ' SET IDENTITY_INSERT ' + @TableName + ' OFF;'

	EXEC (@cSQL)

END
GO
--End procedure utility.InsertIdentityValue

--Begin procedure utility.SetDefaultConstraint
EXEC Utility.DropObject 'utility.SetDefaultConstraint'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetDefaultConstraint

@TableName VARCHAR(250),
@ColumnName VARCHAR(250),
@DataType VARCHAR(250),
@Default VARCHAR(MAX),
@OverWriteExistingConstraint BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bDefaultIsFunction BIT
	DECLARE @bDefaultIsNumeric BIT
	DECLARE @cArchiveTableName VARCHAR(275)
	DECLARE @cConstraintName VARCHAR(500)
	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT

	SET @bDefaultIsFunction = 0

	IF @Default = 'getDate()' OR @Default = 'getUTCDate()' OR @Default = 'newID()' 
		SET @bDefaultIsFunction = 1
	--ENDIF
	
	SET @bDefaultIsNumeric = ISNUMERIC(@Default)

	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cArchiveTableName = @TableName + '_Archive'

	IF EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@TableName) AND SC.name = @ColumnName)
		BEGIN
	
		IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
			SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ''' + @Default + ''' WHERE [' + @ColumnName + '] IS NULL'
		ELSE
			SET @cSQL = 'UPDATE ' + @TableName + ' SET [' + @ColumnName + '] = ' + @Default + ' WHERE [' + @ColumnName + '] IS NULL'
		--ENDIF

		EXEC (@cSQL)

		SET @cSQL = 'ALTER TABLE ' + @TableName + ' ALTER COLUMN [' + @ColumnName + '] ' + @DataType + ' NOT NULL'
		EXEC (@cSQL)

		SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
		SET @cConstraintName = 'DF_' + RIGHT(@TableName, @nLength) + '_' + REPLACE(@ColumnName, ' ', '_')
		
		IF @OverWriteExistingConstraint = 1
			BEGIN	

			SELECT @cSQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT [' + DC.Name + ']'
			FROM sys.default_constraints DC 
				JOIN sys.objects O ON O.object_id = DC.parent_object_id 
					AND O.type = 'U' 
				JOIN sys.schemas S ON S.schema_ID = O.schema_ID 
				JOIN sys.columns C ON C.object_id = O.object_id 
					AND C.column_id = DC.parent_column_id 
					AND S.Name + '.' + O.Name = @TableName 
					AND C.Name = @ColumnName

			IF @cSQL IS NOT NULL
				EXECUTE (@cSQL)
			--ENDIF
			
			END
		--ENDIF

		IF NOT EXISTS (SELECT 1 FROM sys.default_constraints DC JOIN sys.objects O ON O.object_id = DC.parent_object_id AND O.type = 'U' JOIN sys.schemas S ON S.schema_ID = O.schema_ID JOIN sys.columns C ON C.object_id = O.object_id AND C.column_id = DC.parent_column_id AND S.Name + '.' + O.Name = @TableName AND C.Name = @ColumnName)
			BEGIN	

			IF @bDefaultIsFunction = 0 AND @bDefaultIsNumeric = 0
				SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ''' + @Default + ''' FOR [' + @ColumnName + ']'
			ELSE
				SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT ' + @cConstraintName + ' DEFAULT ' + @Default + ' FOR [' + @ColumnName + ']'
		--ENDIF
		
			EXECUTE (@cSQL)

			END
		--ENDIF

		IF EXISTS (SELECT 1 FROM sys.Tables T JOIN sys.Schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @cArchiveTableName)	AND EXISTS (SELECT 1 FROM dbo.syscolumns SC WHERE SC.id = OBJECT_ID(@cArchiveTableName) AND SC.name = @ColumnName)
			EXEC utility.SetDefaultConstraint @cArchiveTableName, @ColumnName, @DataType, @Default, @OverWriteExistingConstraint
		--ENDIF

		END
	--ENDIF

END
GO
--End procedure utility.SetDefaultConstraint

--Begin procedure utility.SetIndexClustered
EXEC utility.DropObject 'utility.SetIndexClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexClustered

--Begin procedure utility.SetIndexNonClustered
EXEC utility.DropObject 'utility.SetIndexNonClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetIndexNonClustered

@TableName VARCHAR(250),
@IndexName VARCHAR(250),
@Columns VARCHAR(MAX),
@Include VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @cSQL = 'CREATE NONCLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ')'
	IF @Include IS NOT NULL
		SET @cSQL += ' INCLUDE (' + @Include + ')'
	--ENDIF
	SET @cSQL += ' WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetIndexNonClustered

--Begin procedure utility.SetPrimaryKeyClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY CLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyClustered

--Begin procedure utility.SetPrimaryKeyNonClustered
EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
GO

-- =========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A helper stored procedure for table upgrades
-- =========================================================
CREATE PROCEDURE utility.SetPrimaryKeyNonClustered

@TableName VARCHAR(250),
@ColumnName VARCHAR(250)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cSQL VARCHAR(MAX)
	DECLARE @nLength INT
	
	IF CHARINDEX('.', @TableName) = 0
		SET @TableName = 'dbo.' + @TableName
	--ENDIF

	SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
	SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

	EXECUTE (@cSQL)

END
GO
--End procedure utility.SetPrimaryKeyNonClustered

EXEC utility.AddSchema 'client'
EXEC utility.AddSchema 'core'
EXEC utility.AddSchema 'dropdown'
EXEC utility.AddSchema 'import'
EXEC utility.AddSchema 'eventlog'
EXEC utility.AddSchema 'person'
EXEC utility.AddSchema 'product'
GO

--End file Build File - 01 - Prerequisites.sql

--Begin file Build File - 02 - Tables - Core.sql
--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table core.BuildLog
DECLARE @TableName VARCHAR(250) = 'core.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE core.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table core.BuildLog

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	PhoneNumber VARCHAR(50),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person
--End file Build File - 02 - Tables - Core.sql

--Begin file Build File - 02 - Tables.sql
--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropObject @TableName

CREATE TABLE client.Client
	(
	ClientID INT IDENTITY(1,1) NOT NULL,
	ClientName VARCHAR(250),
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	WebsiteURL VARCHAR(1000),
	ImagePath VARCHAR(250),
	StatusID INT,

	PhoneNumber VARCHAR(50),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	PostalCode VARCHAR(10),
	ISOCountryCode3 CHAR(3),

	BaseCharge NUMERIC(18,2),
	BaseFactor NUMERIC(18,2),
	BaseRound NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BaseCharge', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BaseFactor', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BaseRound', 'NUMERIC(18,2)', '.5'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientID'
GO
--End table client.Client

--Begin table client.ClientDistributor
DECLARE @TableName VARCHAR(250) = 'client.ClientDistributor'

EXEC utility.DropObject @TableName
GO
--End table client.ClientDistributor

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPerson
	(
	ClientPersonID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	PersonID INT,
	IsClientAdministrator BIT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsClientAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPerson', 'ClientID,PersonID'
GO
--End table client.ClientPerson

--Begin table client.ClientProduct
DECLARE @TableName VARCHAR(250) = 'client.ClientProduct'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientProduct
	(
	ClientProductID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	ProductID INT,
	Description VARCHAR(1000),
	IconHTML VARCHAR(1000),
	ImagePath VARCHAR(250),
	IsFeatured BIT,
	DisplayOrder INT,
	DistributorID INT,
	IsMenuItem BIT,
	ServingSize VARCHAR(50),
	ServingCount INT,
	PackagePrice NUMERIC(18,2),
	PricingModelID INT,
	UpCharge NUMERIC(18,2),
	MenuPrice NUMERIC(18,2),
	MenuPriceDateTime DATETIME,
	MenuPricePersonID INT,
	InStock INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DistributorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InStock', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFeatured', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsMenuItem', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuPrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuPricePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PackagePrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ServingCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpCharge', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientProductID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientProduct', 'ClientID,ProductID'
GO
--End table client.ClientProduct

--Begin table client.PricingModel
DECLARE @TableName VARCHAR(250) = 'client.PricingModel'

EXEC utility.DropObject @TableName

CREATE TABLE client.PricingModel
	(
	PricingModelID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	PricingModelName VARCHAR(250),
	PricingModelCharge NUMERIC(18,2),
	PricingModelFactor NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelCharge', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelFactor', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PricingModelID'
EXEC utility.SetIndexClustered @TableName, 'IX_PricingModel', 'ClientID,PricingModelName'
GO
--End table client.PricingModel

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0, 1) NOT NULL,
	CountryName NVARCHAR(250),
	ISOCountryCode2 CHAR(2), 
	ISOCountryCode3 CHAR(3),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CountryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CountryName', 'DisplayOrder,CountryName', 'CountryID'
GO
--End table dropdown.Country

--Begin table dropdown.Region
DECLARE @TableName VARCHAR(250) = 'dropdown.Region'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Region
	(
	RegionID INT IDENTITY(0, 1) NOT NULL,
	RegionName NVARCHAR(250),
	ISOCountryCode3 CHAR(3),
	ISORegionCode2 CHAR(2), 
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RegionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_RegionName', 'DisplayOrder,RegionName', 'RegionID'
GO
--End table dropdown.Region

--Begin table dropdown.PackageType
DECLARE @TableName VARCHAR(250) = 'dropdown.PackageType'

EXEC utility.DropObject 'dropdown.PackageType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PackageType
	(
	PackageTypeID INT IDENTITY(0,1) NOT NULL,
	PackageTypeCode VARCHAR(50),
	PackageTypeName VARCHAR(50),
	PackageTypeSizePlural VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PackageTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PackageType', 'DisplayOrder,PackageTypeName', 'PackageTypeID'
GO
--End table dropdown.PackageType

--Begin table dropdown.ProductType
DECLARE @TableName VARCHAR(250) = 'dropdown.ProductType'

EXEC utility.DropObject 'dropdown.ProductType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProductType
	(
	ProductTypeID INT IDENTITY(0,1) NOT NULL,
	ProductTypeCode VARCHAR(50),
	ProductTypeName VARCHAR(50),
	IconHTML VARCHAR(1000),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProductType', 'DisplayOrder,ProductTypeName', 'ProductTypeID'
GO
--End table dropdown.ProductType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC utility.DropObject 'dropdown.Status'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Status', 'DisplayOrder,StatusName', 'StatusID'
GO
--End table dropdown.Status

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table product.Distributor
DECLARE @TableName VARCHAR(250) = 'product.Distributor'

EXEC utility.DropObject 'dropdown.Distributor'
EXEC utility.DropObject @TableName

CREATE TABLE product.Distributor
	(
	DistributorID INT IDENTITY(0, 1) NOT NULL,
	DistributorName VARCHAR(250),
	ImagePath VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DistributorID'
GO
--End table product.Distributor

--Begin table product.Producer
DECLARE @TableName VARCHAR(250) = 'product.Producer'

EXEC utility.DropObject 'dropdown.Producer'
EXEC utility.DropObject @TableName

CREATE TABLE product.Producer
	(
	ProducerID INT IDENTITY(0, 1) NOT NULL,
	ProducerName VARCHAR(250),
	ImagePath VARCHAR(250),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProducerID'
GO
--End table product.Producer

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.DropObject @TableName

CREATE TABLE product.Product
	(
	ProductID INT IDENTITY(1, 1) NOT NULL,
	ProductName NVARCHAR(250),
	ProductTypeID INT,
	ProductStyleID INT,
	PackageTypeID INT,
	PackageSize VARCHAR(50),
	ABV NUMERIC(18,2),
	IBU NUMERIC(18,2),
	ImportProductID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ABV', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IBU', ' NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImportProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PackageTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductStyleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductID'
GO
--End table product.Product

--Begin table product.ProductProducer
DECLARE @TableName VARCHAR(250) = 'product.ProductProducer'

EXEC utility.DropObject @TableName

CREATE TABLE product.ProductProducer
	(
	ProductProducerID INT IDENTITY(1, 1) NOT NULL,
	ProductID INT,
	ProducerID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProducerID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProductProducerID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProductProducer', 'ProductID,ProducerID'
GO
--End table product.ProductProducer

--Begin table product.ProductStyle
DECLARE @TableName VARCHAR(250) = 'product.ProductStyle'

EXEC utility.DropObject 'dropdown.ProductStyle'
EXEC utility.DropObject @TableName

CREATE TABLE product.ProductStyle
	(
	ProductStyleID INT IDENTITY(0,1) NOT NULL,
	ProductStyleCode VARCHAR(50),
	ProductStyleName NVARCHAR(250),
	ProductStyleDescription VARCHAR(MAX),
	IconHTML VARCHAR(1000),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductStyleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProductStyle', 'ProductStyleName', 'ProductStyleID'
GO
--End table product.ProductStyle
--End file Build File - 02 - Tables.sql

--Begin file Build File - 03 - Functions - Core.sql
--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 100)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN CONVERT(CHAR(20), @DateTimeData, 100)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(LTRIM(RIGHT(CONVERT(CHAR(20), @TimeData, 100), 9)), '')


END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	RETURN IIF(@String IS NULL OR LEN(RTRIM(@String)) = 0, NULL, @String)

END
GO
--End function core.NullIfEmpty

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	DECLARE @cSuffix VARCHAR(50)
	DECLARE @cTitle VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cSuffix = ISNULL(P.Suffix, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format LIKE '%FirstLast%'
		BEGIN
			
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @Format LIKE '%Suffix'
			SET @cRetVal += @cSuffix
		--ENDIF
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cRetVal + @cTitle
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO
--End function person.HasPermission

--Begin function person.IsSuperAdministrator
EXEC utility.DropObject 'person.IsSuperAdministrator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format a numeric value for javascript
-- ================================================================

CREATE FUNCTION person.IsSuperAdministrator
(
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	RETURN CASE WHEN EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1) THEN 1 ELSE 0 END

END
GO
--End function person.IsSuperAdministrator

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = P.Password,
		@cPasswordSalt = P.PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword

--Begin function utility.StripCharacters
EXEC utility.DropObject 'utility.StripCharacters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION utility.StripCharacters
(
@String NVARCHAR(MAX), 
@Pattern VARCHAR(255)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	SET @Pattern =  '%[' + @Pattern + ']%'

	WHILE PatIndex(@Pattern, @String) > 0
		SET @String = Stuff(@String, PatIndex(@Pattern, @String), 1, '')
	--END WHILE

	RETURN @String

END
GO
--End function utility.StripCharacters
--End file Build File - 03 - Functions - Core.sql

--Begin file Build File - 03 - Functions.sql

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function dropdown.GetRegionNameByISORegionCode
EXEC utility.DropObject 'dropdown.GetRegionNameByISORegionCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a Region name from an ISORegionCode
-- =======================================================================

CREATE FUNCTION dropdown.GetRegionNameByISORegionCode
(
@ISORegionCode2 VARCHAR(2)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRegionName VARCHAR(50)

	SELECT @cRegionName = R.RegionName
	FROM dropdown.Region R
	WHERE R.ISORegionCode2 = @ISORegionCode2

	RETURN ISNULL(@cRegionName, '')

END
GO
--End function dropdown.GetRegionNameByISOCountryCode

--Begin function product.GetIsMenuItemButtonHTML
EXEC utility.DropObject 'product.GetIsMenuItemButtonHTML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create Date:	2018.11.05
-- Description:	A function to return an IsMenuItem button for a product
-- ====================================================================

CREATE FUNCTION product.GetIsMenuItemButtonHTML
(
@ProductID INT,
@ClientID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @bIsMenuItem BIT
	DECLARE @cClassName VARCHAR(50)
	DECLARE @cIsMenuItemButtonHTML VARCHAR(MAX)
	DECLARE @cPacakgeTypeName VARCHAR(50)
	DECLARE @cProductName VARCHAR(50)
	DECLARE @nClientProductID INT

	SELECT TOP 1 
		@cProductName = P1.ProducerName + ' ' + P2.ProductName,
		@cPacakgeTypeName = ISNULL((SELECT PT.PackageTypeName FROM dropdown.PackageType PT WHERE P2.PackageTypeID = PT.PackageTypeID), ''),
		@cClassName = CAST(@ClientID AS VARCHAR(10)) + '-' + CAST(@ProductID AS VARCHAR(10)) + '-' + CAST(P1.ProducerID AS VARCHAR(10))
	FROM product.ProductProducer PP
		JOIN product.Producer P1 ON P1.ProducerID = PP.ProducerID
		JOIN product.Product P2 ON P2.ProductID = PP.ProductID
			AND P2.ProductID = @ProductID
	ORDER BY P1.ProducerName + ' ' + P2.ProductName

	SELECT
		@bIsMenuItem = CP.IsMenuItem,
		@nClientProductID = CP.ClientProductID
	FROM client.ClientProduct CP 
	WHERE CP.Clientid = @ClientID 
		AND CP.ProductID = @ProductID

	IF @nClientProductID IS NULL OR @bIsMenuItem = 0
		BEGIN

		SET @cIsMenuItemButtonHTML = '<span class="badge badge-pill badge-success ' 
			+ @cClassName 
			+ '" data-buttonclass="' 
			+ @cClassName 
			+ '" data-clientproductid="' 
			+ CAST(ISNULL(@nClientProductID, 0) AS VARCHAR(10)) 
			+ '" data-ismenuitem="0" data-productid="' 
			+ CAST(@ProductID AS VARCHAR(10)) 
			+ '" data-packagetypename="'
			+ @cPacakgeTypeName
			+ '" data-productname="'
			+ @cProductName
			+ '" onclick="manageMenuItem(''' + @cClassName + ''')" style="cursor:pointer" title="Add to menu"><i class="fas fa-plus"></i></span>'

		END
	ELSE
		BEGIN

		SET @cIsMenuItemButtonHTML = '<span class="badge badge-pill badge-danger ' 
			+ @cClassName 
			+ '" data-buttonclass="' 
			+ @cClassName 
			+ '" data-clientproductid="' 
			+ CAST(ISNULL(@nClientProductID, 0) AS VARCHAR(10)) 
			+ '" data-ismenuitem="1" data-productid="' 
			+ CAST(@ProductID AS VARCHAR(10)) 
			+ '" data-packagetypename="'
			+ @cPacakgeTypeName
			+ '" data-productname="'
			+ @cProductName
			+ '" onclick="manageMenuItem(''' + @cClassName + ''')" style="cursor:pointer" title="Remove from menu"><i class="fas fa-minus"></i></span>'

		END
	--ENDIF

	RETURN @cIsMenuItemButtonHTML

END
GO
--End function product.GetIsMenuItemButtonHTML

--Begin function product.GetAdditionalProductProducerNames
EXEC utility.DropObject 'product.GetAdditionalProductProducerNames'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create Date:	2018.11.05
-- Description:	A function to return product producer names
-- ========================================================

CREATE FUNCTION product.GetAdditionalProductProducerNames
(
@ProductID INT, 
@ProducerID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = ''

	IF EXISTS (SELECT 1 FROM product.ProductProducer PP JOIN product.Producer P ON P.ProducerID = PP.ProducerID AND PP.ProductID = @ProductID AND PP.ProducerID <> @ProducerID)
		BEGIN

		SELECT @cReturn = COALESCE(@cReturn + ', ', '') + P.ProducerName
		FROM product.ProductProducer PP 
			JOIN product.Producer P ON P.ProducerID = PP.ProducerID 
				AND PP.ProductID = @ProductID
				AND PP.ProducerID <> @ProducerID
		ORDER BY P.ProducerName

		SET @cReturn = STUFF(@cReturn, 1, 2, '')

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function product.GetAdditionalProductProducerNames

--Begin function product.GetProducerNameByProductID
EXEC utility.DropObject 'product.GetProducerNameByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX)

	SELECT @cReturn = 
		STUFF(
			(
			SELECT '|' + P.ProducerName AS [text()]
			FROM product.Producer P
				JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
					AND PP.ProductID = @ProductID
			ORDER BY P.ProducerName
			FOR XML PATH('')
			), 1, 1, '')

	RETURN @cReturn

END
GO
--End function product.GetProducerNameByProductID

--Begin function product.GetProducerNameHTMLByProductID
EXEC utility.DropObject 'product.GetProducerNameHTMLByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameHTMLByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX)

	SELECT @cReturn = 
		REPLACE(STUFF(REPLACE(REPLACE(
			(
			SELECT '|<a href="/producer/view/id/' + CAST(P.ProducerID AS VARCHAR(10)) + '">' + P.ProducerName + '</a>' AS [text()]
			FROM product.Producer P
				JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
					AND PP.ProductID = @ProductID
			ORDER BY P.ProducerName
			FOR XML PATH('')
			), '&lt;', '<'), '&gt;', '>'), 1, 1, ''), '|', ' &amp;<br />')

	RETURN @cReturn

END
GO
--End function product.GetProducerNameHTMLByProductID

--Begin function product.GetProducerNameListByProductID
EXEC utility.DropObject 'product.GetProducerNameListByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameListByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = ''

	SELECT @cReturn = COALESCE(@cReturn + ' & ', '') + P.ProducerName
	FROM product.Producer P
		JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
			AND PP.ProductID = @ProductID
	ORDER BY P.ProducerName

	RETURN STUFF(@cReturn, 1, 3, '')

END
GO
--End function product.GetProducerNameListByProductID

--End file Build File - 03 - Functions.sql

--Begin file Build File - 04 - Procedures - Core.sql
--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE MI
			SET 
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab
			FROM core.MenuItem MI
			WHERE MI.MenuItemID = @nNewMenuItemID

			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin procedure core.UpdateParentPermissionableLineageByMenuItemCode
EXEC utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO
--End procedure permissionable.GetPermissionableGroups

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO
--End procedure permissionable.GetPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionsByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionsByPermissionableTemplateID'
GO
--End procedure permissionable.GetPermissionsByPermissionableTemplateID

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.EmailAddress,
		P.FirstName,
		P.IsAccountLockedOut,
		P.IsActive,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PersonID,
		CASE WHEN @PersonID = 0 THEN 'New User' ELSE person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END AS PersonNameFormatted,
		P.PhoneNumber,
		P.Suffix,
		P.Title
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	SELECT
		C.ClientID,
		C.ClientName,
		CP.IsClientAdministrator
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
	ORDER BY C.ClientName, C.ClientID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.SetPersonPermissionables
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add data to the person.PersonPermissionables table based on a PersonID
-- =========================================================================================================
CREATE PROCEDURE person.SetPersonPermissionables

@PersonID INT,
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	IF @PermissionableIDList IS NOT NULL AND LEN(RTRIM(@PermissionableIDList)) > 0
		BEGIN

		INSERT INTO person.PersonPermissionable
			(PersonID, PermissionableLineage)
		SELECT
			@PersonID,
			P.PermissionableLineage
		FROM permissionable.Permissionable P
			JOIN core.ListToTable(@PermissionableIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PermissionableID

		END
	--ENDIF

END
GO
--End procedure person.SetPersonPermissionables

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@EmailAddress VARCHAR(320),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsValidPassword BIT = 0
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nInvalidLoginAttempts INT = 0
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE (PersonID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	SELECT
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

	SET @nPersonID = ISNULL(@nPersonID, 0)

	INSERT INTO @tPerson (PersonID) VALUES (@nPersonID)

	IF @nPersonID > 0
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF

			UPDATE person.Person
			SET InvalidLoginAttempts = @nInvalidLoginAttempts
			WHERE PersonID = @nPersonID
			
			END
		ELSE 
			BEGIN

			UPDATE P
			SET 
				P.InvalidLoginAttempts = 0,
				P.LastLoginDateTime = getDate()
			FROM person.Person P
			WHERE PersonID = @nPersonID

			END
		--ENDIF
	
		END
	--ENDIF	
		
	--Person
	SELECT 
		TP.PersonID,

		CASE 
			WHEN TP.PersonID > 0 AND ISNULL((SELECT P.PasswordExpirationDateTime FROM person.Person P WHERE P.PersonID = TP.PersonID), SYSUTCDateTime()) < SYSUTCDateTime() 
			THEN 1 
			ELSE 0 
		END AS IsPasswordExpired,

		ISNULL((SELECT P.IsSuperAdministrator FROM person.Person P WHERE P.PersonID = TP.PersonID), 0) AS IsSuperAdministrator,
		@bIsValidPassword AS IsValidPassword,

		CASE
			WHEN TP.PersonID > 0
			THEN 1
			ELSE 0
		END AS IsValidEmailAddress,

		person.FormatPersonNameByPersonID(TP.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM @tPerson TP

	IF EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @nPersonID AND P.IsSuperAdministrator = 1)
		BEGIN

		--PersonClient
		SELECT 
			C.ClientID,
			C.ClientName,
			1 AS IsClientAdministrator
		FROM client.Client C
		ORDER BY C.ClientName, C.ClientID

		--PersonClient
		SELECT 
			C.ClientID
		FROM client.Client C
		ORDER BY C.ClientID

		END
	ELSE
		BEGIN

		--PersonClient
		SELECT 
			C.ClientID,
			C.ClientName,
			CP.IsClientAdministrator
		FROM client.ClientPerson CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
				AND CP.PersonID = @nPersonID
		ORDER BY C.ClientName, C.ClientID

		--PersonClientAdministrator
		SELECT 
			C.ClientID
		FROM client.ClientPerson CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
				AND CP.PersonID = @nPersonID
				AND CP.IsClientAdministrator = 1
		ORDER BY C.ClientID

		END
	--ENDIF

END
GO
--End procedure person.ValidateLogin
--End file Build File - 04 - Procedures - Core.sql

--Begin file Build File - 04 - Procedures.sql
--Begin procedure client.DeletePricingModelByPricingModelID
EXEC utility.DropObject 'client.DeletePricingModelByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.08
-- Description:	A stored procedure to delete data from the client.PricingModel table
-- =================================================================================
CREATE PROCEDURE client.DeletePricingModelByPricingModelID

@PricingModelID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PM
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	UPDATE CP
	SET CP.PricingModelID = 0
	FROM client.ClientProduct CP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.PricingModel PM
		WHERE PM.PricingModelID = CP.PricingModelID
		)

END
GO
--End procedure client.DeletePricingModelByPricingModelID

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.07
-- Description:	A stored procedure to return data from the client.Client table
-- ===========================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.BaseFactor,
		C.BaseCharge,
		C.BaseRound,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FirstName,
		C.ImagePath,
		C.ISOCountryCode3,
		dropdown.GetCountryNameByISOCountryCode(C.ISOCountryCode3) AS CountryName,
		C.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(C.ISORegionCode2) AS RegionName,
		C.LastName,
		C.Municipality,
		C.PhoneNumber,
		C.PostalCode,
		C.WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM client.Client C
		JOIN dropdown.Status S ON S.StatusID = C.StatusID
			AND C.ClientID = @ClientID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetPricingModel
EXEC utility.DropObject 'client.GetPricingModel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.04
-- Description:	A stored procedure to get data from the client.PricingModel table
-- ==============================================================================
CREATE PROCEDURE client.GetPricingModel

@ClientProductID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PricingModel
	SELECT
		--C.BaseFactor,
		--C.BaseCharge,
		--C.BaseRound,
		--CP.PackagePrice,
		--CP.ServingCount,
		--CP.PackagePrice / CP.ServingCount AS BasePrice,
		--(CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge AS ComputedPrice,
		C.BaseRound,
		CP.ClientProductID,
		CP.MenuPrice AS ClientProductMenuPrice,
		CP.PricingModelID AS ClientProductPricingModelID,
		CP.UpCharge,
		PM.PricingModelCharge,
		PM.PricingModelFactor,
		PM.PricingModelID,
		PM.PricingModelName,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge) / C.BaseRound, 0) * C.BaseRound AS MenuPrice,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge) / C.BaseRound, 0) * C.BaseRound AS ModelPrice
	FROM client.PricingModel PM
		JOIN client.Client C ON C.ClientID = PM.ClientID
		JOIN client.ClientProduct CP ON CP.ClientID = PM.ClientID
			AND CP.ClientProductID = @ClientProductID

END
GO
--End procedure client.GetPricingModel

--Begin procedure client.SetIsMenuItem
EXEC utility.DropObject 'client.SetIsMenuItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.29
-- Description:	A stored procedure to update the IsMenuItem bit in the client.ClientProduct table
-- ==============================================================================================
CREATE PROCEDURE client.SetIsMenuItem

@ClientProductID INT = 0,
@IsMenuItem BIT = 0,
@nPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.IsMenuItem = @IsMenuItem,
		CP.MenuPriceDateTime = CASE WHEN @IsMenuItem = 1 THEN getDate() ELSE NULL END,
		CP.MenuPricePersonID = CASE WHEN @IsMenuItem = 1 THEN @nPersonID ELSE 0 END
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID 

END
GO
--End procedure client.SetIsMenuItem

--Begin procedure client.SetMenuPrice
EXEC utility.DropObject 'client.SetMenuPrice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.SetMenuPrice

@ClientProductID INT = 0,
@Upcharge NUMERIC(18,2) = 0,
@PricingModelID INT = 0,
@MenuPrice NUMERIC(18,2) = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @PricingModelID != 0
		BEGIN

		SELECT @MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + @Upcharge) / C.BaseRound, 0) * C.BaseRound
		FROM client.ClientProduct CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
				AND CP.ClientProductID = @ClientProductID
				AND PM.PricingModelID = @PricingModelID

		END
	--ENDIF

	UPDATE CP
	SET 
		CP.MenuPrice = @MenuPrice,
		CP.PricingModelID = @PricingModelID,
		CP.Upcharge = @Upcharge
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID

	SELECT FORMAT(@MenuPrice, 'C', 'en-us') AS MenuPrice

END
GO
--End procedure client.SetMenuPrice

--Begin procedure client.UpdateMenuPricesByClientID
EXEC utility.DropObject 'client.UpdateMenuPricesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByClientID

@ClientID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ClientID = @ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = CP.PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByClientID

--Begin procedure client.UpdateMenuPricesByPricingModelID
EXEC utility.DropObject 'client.UpdateMenuPricesByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByPricingModelID

@PricingModelID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = @PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByPricingModelID

--Begin procedure dropdown.GetCountryData
EXEC utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.ISOCountryCode3,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetRegionData
EXEC utility.DropObject 'dropdown.GetRegionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Region table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetRegionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RegionID,
		T.ISOCountryCode3,
		T.ISORegionCode2,
		T.RegionName
	FROM dropdown.Region T
	WHERE (T.RegionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RegionName, T.RegionID

END
GO
--End procedure dropdown.GetRegionData

--Begin procedure dropdown.GetPackageTypeData
EXEC utility.DropObject 'dropdown.GetPackageTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.PackageType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetPackageTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PackageTypeID,
		T.PackageTypeCode,
		T.PackageTypeName,
		T.PackageTypeSizePlural
	FROM dropdown.PackageType T
	WHERE (T.PackageTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PackageTypeName, T.PackageTypeID

END
GO
--End procedure dropdown.GetPackageTypeData

--Begin procedure dropdown.GetProductTypeData
EXEC utility.DropObject 'dropdown.GetProductTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ProductType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetProductTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductTypeID,
		T.ProductTypeCode,
		T.ProductTypeName
	FROM dropdown.ProductType T
	WHERE (T.ProductTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductTypeName, T.ProductTypeID

END
GO
--End procedure dropdown.GetProductTypeData

--Begin procedure dropdown.GetStatusData
EXEC utility.DropObject 'dropdown.GetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Status table
-- =============================================================================
CREATE PROCEDURE dropdown.GetStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE (T.StatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.21
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Client:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM Client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonPermissionableXML VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionableXML = COALESCE(@cPersonPermissionableXML, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonPermissionableData>' + ISNULL(@cPersonPermissionableXML, '') + '</PersonPermissionableData>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.EmailAddress,
		P.FirstName,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsSuperAdministrator,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PhoneNumber,
		P.Suffix,
		P.Title
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.DeletePersonByPersonID
EXEC utility.DropObject 'person.DeletePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to delete data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.DeletePersonByPersonID

@PersonID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CP
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	DELETE P
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

END
GO
--End procedure person.DeletePersonByPersonID

--Begin procedure product.GetDistributorByDistributorID
EXEC utility.DropObject 'product.GetDistributorByDistributorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Distributor table based on a DistributorID
-- ==========================================================================================================
CREATE PROCEDURE product.GetDistributorByDistributorID

@DistributorID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Distributor
	SELECT
		D.DistributorID,
		D.DistributorName,
		D.ImagePath
	FROM product.Distributor D
	WHERE D.DistributorID = @DistributorID

	SELECT DISTINCT
		C.CountryName,
		P.ImagePath,
		P.ISOCountryCode3,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM client.ClientProduct CP
		JOIN product.ProductProducer PP ON PP.ProductID = CP.ProductID
		JOIN product.Producer P ON P.ProducerID = PP.ProducerID
		JOIN dropdown.Country C ON C.ISOCountryCode3 = P.ISOCountryCode3
			AND CP.DistributorID = @DistributorID
			AND CP.ClientID = @ClientID
			AND P.IsActive = 1
	ORDER BY P.ProducerName, P.ProducerID

END
GO
--End procedure product.GetDistributorByDistributorID

--Begin procedure product.GetDistributorData
EXEC utility.DropObject 'product.GetDistributorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.Distributor table
-- ===================================================================================
CREATE PROCEDURE product.GetDistributorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DistributorID,
		T.DistributorName,
		T.ImagePath
	FROM product.Distributor T
	WHERE (T.DistributorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DistributorName, T.DistributorID

END
GO
--End procedure product.GetDistributorData

--Begin procedure product.GetMenuDataByClientID
EXEC utility.DropObject 'product.GetMenuDataByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create date:	2018.12.07
-- Description:	A stored procedure to return menu data
-- ===================================================
CREATE PROCEDURE product.GetMenuDataByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.ClientName,
		C.ImagePath,
		C.ISOCountryCode3,
		C.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(C.ISORegionCode2) AS RegionName,
		C.Municipality,
		C.PhoneNumber,
		C.PostalCode,
		C.WebsiteURL
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Draft'
			AND PT2.ProductTypeName <> 'Wine'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName IN ('Bottle', 'Can')
			AND PT2.ProductTypeName = 'Beer'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Bomber'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT2.ProductTypeName = 'Wine'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName NOT IN ('Bomber', 'Draft')
			AND PT2.ProductTypeName = 'Cider'
	ORDER BY 4, PT1.DisplayOrder, P1.ProductName, P1.ProductID

END
GO
--End procedure product.GetMenuDataByClientID

--Begin procedure product.GetProducerByProducerID
EXEC utility.DropObject 'product.GetProducerByProducerID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Producer table based on a ProducerID
-- ====================================================================================================
CREATE PROCEDURE product.GetProducerByProducerID

@ProducerID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Producer
	SELECT
		P.ImagePath,
		P.ISOCountryCode3,
		dropdown.GetCountryNameByISOCountryCode(P.ISOCountryCode3) AS CountryName,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM product.Producer P
	WHERE P.ProducerID = @ProducerID

END
GO
--End procedure product.GetProducerByProducerID

--Begin procedure product.GetProducerData
EXEC utility.DropObject 'product.GetProducerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.Producer table
-- ===================================================================================
CREATE PROCEDURE product.GetProducerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	 SELECT
		C.CountryName,
		T.ImagePath,
		T.ISOCountryCode3,
		T.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(T.ISORegionCode2) AS RegionName,
		T.Municipality,
		T.ProducerID,
		T.ProducerName
	 FROM product.Producer T
	  JOIN dropdown.Country C ON C.ISOCountryCode3 = T.ISOCountryCode3
	 		AND (T.ProducerID > 0 OR @IncludeZero = 1)
	  	AND T.IsActive = 1
	 ORDER BY T.DisplayOrder, T.ProducerName, T.ProducerID

END
GO
--End procedure product.GetProducerData

--Begin procedure product.GetProductsByProducerID
EXEC utility.DropObject 'product.GetProductByProducerIDAndClientID'
EXEC utility.DropObject 'product.GetProductsByProducerID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Product table based on a ProductID
-- ==================================================================================================
CREATE PROCEDURE product.GetProductsByProducerID

@ProducerID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Product
	SELECT
		P.ProductID,
		P.ProductName,
		P.ABV,
		P.IBU,
		PKT.PackageTypeName,
		PRT.ProductTypeName,
		PS.ProductStyleName,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientProduct CP WHERE CP.ProductID = P.ProductID) THEN 1 ELSE 0 END AS IsClientProduct,	
		product.GetIsMenuItemButtonHTML(P.ProductID, @ClientID)	AS IsMenuItemButton,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientProduct CP WHERE CP.ProductID = P.ProductID AND CP.IsMenuItem = 1) THEN 1 ELSE 0 END AS IsMenuItem,
		product.GetAdditionalProductProducerNames(P.ProductID, @ProducerID) AS AdditionalProductProducerNames
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
		JOIN dropdown.ProductType PRT ON PRT.ProductTypeID = P.ProductTypeID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P.ProductStyleID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductProducer PP
				WHERE PP.ProductID = P.ProductID
					AND PP.ProducerID = @ProducerID
				)
	ORDER BY PKT.DisplayOrder, PRT.ProductTypeName, P.ProductName, P.ProductID

END
GO
--End procedure product.GetProductsByProducerID

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Product table based on a ProductID
-- ==================================================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientProductID INT = ISNULL((SELECT CP.ClientProductID FROM client.ClientProduct CP WHERE CP.ClientID = @ClientID AND CP.ProductID = @ProductID), 0)

	--Product
	SELECT
		P.ABV,
		P.IBU,
		P.PackageSize,
		P.ProductID,
		P.ProductName,
		PKT.PackageTypeID,
		PKT.PackageTypeName,
		PRT.ProductTypeID,
		PRT.ProductTypeName,
		PS.ProductStyleDescription,
		PS.ProductStyleID,
		PS.ProductStyleName
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P.ProductStyleID
		JOIN dropdown.ProductType	PRT ON PRT.ProductTypeID = P.ProductTypeID
			AND P.ProductID = @ProductID

	--ClientProduct
	SELECT
		CP.ClientProductID,
		CP.Description,
		CP.IconHTML,
		CP.ImagePath,
		CP.IsFeatured,
		CP.IsMenuItem,
		CP.ServingSize,
		CP.ServingCount,
		CP.PackagePrice,
		CP.MenuPrice,
		CP.UpCharge,
		core.FormatDateTime(CP.MenuPriceDateTime) AS MenuPriceDateTimeFormatted,
		person.FormatPersonNameByPersonID(CP.MenuPricePersonID, 'LastFirst') AS PersonNameFormatted,
		D.DistributorID,
		D.DistributorName
	FROM client.ClientProduct CP
		JOIN product.Distributor D ON D.DistributorID = CP.DistributorID
			AND CP.ClientProductID = @nClientProductID

	--PricingModel
	EXEC client.GetPricingModel @nClientProductID

	--ProductProducer
	SELECT
		C.CountryName,
		P.ImagePath,
		P.ISOCountryCode3,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM product.ProductProducer PP
		JOIN product.Producer P ON P.ProducerID = PP.ProducerID
		JOIN dropdown.Country C ON C.ISOCountryCode3 = P.ISOCountryCode3
			AND PP.ProductID = @ProductID
	ORDER BY P.ProducerName, P.ProducerID

END
GO
--End procedure product.GetProductByProductID

--Begin procedure product.GetProductStyleData
EXEC utility.DropObject 'product.GetProductStyleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.ProductStyle table
-- ===================================================================================
CREATE PROCEDURE product.GetProductStyleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductStyleCode,
		T.ProductStyleDescription,
		T.ProductStyleID,
		T.ProductStyleName
  FROM product.ProductStyle T
	WHERE (T.ProductStyleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.ProductStyleName, T.ProductStyleID

END
GO
--End procedure product.GetProductStyleData

--End file Build File - 04 - Procedures.sql

--Begin file Build File - 05 - Data.sql
--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Client',
	@EntityTypeName = 'Client',
	@EntityTypeNamePlural = 'Clients',
	@SchemaName = 'client',
	@TableName = 'Client',
	@PrimaryKeyFieldName = 'ClientID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Distributor',
	@EntityTypeName = 'Distributor',
	@EntityTypeNamePlural = 'Distributors',
	@SchemaName = 'product',
	@TableName = 'Distributor',
	@PrimaryKeyFieldName = 'DistributorID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'eventlog', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'Person', 
	@EntityTypeNamePlural = 'People',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Producer',
	@EntityTypeName = 'Producer',
	@EntityTypeNamePlural = 'Producers',
	@SchemaName = 'product',
	@TableName = 'Producer',
	@PrimaryKeyFieldName = 'ProducerID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Product',
	@EntityTypeName = 'Product',
	@EntityTypeNamePlural = 'Products',
	@SchemaName = 'product',
	@TableName = 'Product',
	@PrimaryKeyFieldName = 'ProductID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,stchris2opher@gmail.com'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@menumaker.com'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'http://MenuMaker.christophercrouch.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'MenuMaker'
GO
--End table core.SystemSetup

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder) 
VALUES 
	(N'AT', N'AUT', N'Austria', 99),
	(N'AU', N'AUS', N'Australia', 99),
	(N'BE', N'BEL', N'Belgium', 99),
	(N'CA', N'CAN', N'Canada', 99),
	(N'CH', N'CHE', N'Switzerland', 99),
	(N'DE', N'DEU', N'Germany', 99),
	(N'DK', N'DNK', N'Denmark', 99),
	(N'EN', N'ENG', N'England', 99),
	(N'ES', N'ESP', N'Spain', 99),
	(N'FR', N'FRA', N'France', 99),
	(N'GU', N'GUM', N'Guam', 99),
	(N'IE', N'IRL', N'Ireland', 99),
	(N'IT', N'ITA', N'Italy', 99),
	(N'JP', N'JPN', N'Japan', 99),
	(N'MX', N'MEX', N'Mexico', 99),
	(N'NL', N'NLD', N'Netherlands', 99),
	(N'NO', N'NOR', N'Norway', 99),
	(N'PH', N'PHL', N'Philippines', 99),
	(N'RU', N'RUS', N'Russian Federation', 99),
	(N'SC', N'SCO', N'Scotland', 99),
	(N'SE', N'SWE', N'Sweden', 99),
	(N'US', N'USA', N'United States', 1)
GO
--End table dropdown.Country

--Begin table dropdown.Region
TRUNCATE TABLE dropdown.Region
GO

EXEC utility.InsertIdentityValue 'dropdown.Region', 'RegionID', 0
GO

INSERT INTO dropdown.Region
	(ISOCountryCode3, ISORegionCode2, RegionName) 
VALUES 
	('USA', 'AL','Alabama'),
	('USA', 'AK','Alaska'),
	('USA', 'AZ','Arizona'),
	('USA', 'AR','Arkansas'),
	('USA', 'CA','California'),
	('USA', 'CO','Colorado'),
	('USA', 'CT','Connecticut'),
	('USA', 'DE','Delaware'),
	('USA', 'DC','District of Columbia'),
	('USA', 'FL','Florida'),
	('USA', 'GA','Georgia'),
	('USA', 'HI','Hawaii'),
	('USA', 'ID','Idaho'),
	('USA', 'IL','Illinois'),
	('USA', 'IN','Indiana'),
	('USA', 'IA','Iowa'),
	('USA', 'KS','Kansas'),
	('USA', 'KY','Kentucky'),
	('USA', 'LA','Louisiana'),
	('USA', 'ME','Maine'),
	('USA', 'MD','Maryland'),
	('USA', 'MA','Massachusetts'),
	('USA', 'MI','Michigan'),
	('USA', 'MN','Minnesota'),
	('USA', 'MS','Mississippi'),
	('USA', 'MO','Missouri'),
	('USA', 'MT','Montana'),
	('USA', 'NE','Nebraska'),
	('USA', 'NV','Nevada'),
	('USA', 'NH','New Hampshire'),
	('USA', 'NJ','New Jersey'),
	('USA', 'NM','New Mexico'),
	('USA', 'NY','New York'),
	('USA', 'NC','North Carolina'),
	('USA', 'ND','North Dakota'),
	('USA', 'OH','Ohio'),
	('USA', 'OK','Oklahoma'),
	('USA', 'OR','Oregon'),
	('USA', 'PA','Pennsylvania'),
	('USA', 'PR','Puerto Rico'),
	('USA', 'RI','Rhode Island'),
	('USA', 'SC','South Carolina'),
	('USA', 'SD','South Dakota'),
	('USA', 'TN','Tennessee'),
	('USA', 'TX','Texas'),
	('USA', 'UT','Utah'),
	('USA', 'VT','Vermont'),
	('USA', 'VA','Virginia'),
	('USA', 'VI','Virgin Islands'),
	('USA', 'WA','Washington'),
	('USA', 'WV','West Virginia'),
	('USA', 'WI','Wisconsin'),
	('USA', 'WY','Wyoming')
GO
--End table dropdown.Region

--Begin table dropdown.PackageType
TRUNCATE TABLE dropdown.PackageType
GO

EXEC utility.InsertIdentityValue 'dropdown.PackageType', 'PackageTypeID', 0
GO

INSERT INTO dropdown.PackageType
	(PackageTypeName, PackageTypeSizePlural, DisplayOrder) 
VALUES 
	('Bomber', 'Bombers', 4),
	('Bottle', 'Bottles', 3),
	('Can', 'Cans', 2),
	('Draft', 'Gallons', 1)
GO
--End table dropdown.PackageType

--Begin table dropdown.ProductType
TRUNCATE TABLE dropdown.ProductType
GO

EXEC utility.InsertIdentityValue 'dropdown.ProductType', 'ProductTypeID', 0
GO

INSERT INTO dropdown.ProductType
	(ProductTypeName, IconHTML) 
VALUES 
	('Beer', '<i class="fas fa-beer"></i>'),
	('Cider', '<i class="fas fa-wine-bottle"></i>'),
	('Wine', '<i class="fas fa-wine-glass-alt"></i>')
GO
--End table dropdown.ProductType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

EXEC utility.InsertIdentityValue 'dropdown.Status', 'StatusID', 0
GO

INSERT INTO dropdown.Status
	(StatusCode, StatusName, DisplayOrder) 
VALUES 
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Deleted', 'Deleted', 3)
GO
--End table dropdown.Status

--Begin table client.Client
TRUNCATE TABLE client.Client
GO

INSERT INTO client.Client
	(ClientName, EmailAddress, FirstName, LastName, WebsiteURL, StatusID, Address1, Municipality, ISORegionCode2, PostalCode, ISOCountryCode3, BaseCharge, BaseFactor, BaseRound)
VALUES
	(
	'Magnolia Tree Tavern',
	'eclayton@caprocktexas.com',
	'Eric',
	'Clayton',
	'http://www.magnoliatreetavern.com',
	(SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Active'),
	'1229 7th Ave',
	'Fort Worth',
	'TX',
	'76104',
	'USA',
	1.25,
	1,
	.5
	),
	(
	'Chris'' Place',
	'stchris2opher@gmail.com',
	'Chris',
	'Crouch',
	'http://www.chriscrouch.com',
	(SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Active'),
	'6036 Red Drum Drive',
	'Fort Worth',
	'TX',
	'76179',
	'USA',
	1.75,
	2,
	.25
	)

GO
--End table client.Client

--Begin table client.PricingModel
TRUNCATE TABLE client.PricingModel
GO

INSERT INTO client.PricingModel
	(ClientID, PricingModelName, PricingModelCharge, PricingModelFactor)
VALUES
	(1, 'x 2.0', 0, 2),
	(1, 'x 2.5', 0, 2.5),
	(1, 'x 3.0', 0, 3),
	(2, 'x 2.25', 0, 2.25),
	(2, 'x 2.50', 0, 2.5),
	(2, 'x 2.75', 0, 2.75)	
GO
--End table client.PricingModel

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person
	(FirstName,LastName,Title,EmailAddress,LastLoginDateTime,InvalidLoginAttempts,IsAccountLockedOut,IsActive,IsSuperAdministrator,Password,PasswordSalt,PasswordExpirationDateTime)
VALUES
	('Christopher', 'Crouch', 'Mr.', 'stchris2opher@gmail.com', NULL, 0, 0, 1, 1, '2DE46DAAD0102EEC8D1C73B287D154595FA09345D88E20DDF418B5FC6CB8ED0F', 'C9B13203-3259-4128-8221-C18D5C2BDAF1', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Client1', 'Test User', 'Mr.', 'testuser@client1.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2018-12-01T00:00:00.000' AS DateTime)),
	('Client2', 'Test User', 'Mr.', 'testuser@client2.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2018-12-01T00:00:00.000' AS DateTime))
GO
--End table person.Person

--Begin table client.ClientPerson
TRUNCATE TABLE client.ClientPerson
GO

INSERT INTO client.ClientPerson
	(ClientID, PersonID, IsClientAdministrator)
SELECT
	CAST(RIGHT(P.FirstName, 1) AS INT),
	P.PersonID,
	1
FROM person.Person P
WHERE P.FirstName LIKE 'Client%'
GO
--End table client.ClientPerson

--Begin table product.Distributor
TRUNCATE TABLE product.Distributor
GO

EXEC utility.InsertIdentityValue 'product.Distributor', 'DistributorID', 0
GO

INSERT INTO product.Distributor (DistributorName) VALUES ('(512)');
INSERT INTO product.Distributor (DistributorName) VALUES ('Andrews');
INSERT INTO product.Distributor (DistributorName) VALUES ('Austin Beerworks');
INSERT INTO product.Distributor (DistributorName) VALUES ('Bearded Eel');
INSERT INTO product.Distributor (DistributorName) VALUES ('Ben E Keith');
INSERT INTO product.Distributor (DistributorName) VALUES ('Buffalo Bayou');
INSERT INTO product.Distributor (DistributorName) VALUES ('Community');
INSERT INTO product.Distributor (DistributorName) VALUES ('Cuvee Coffee');
INSERT INTO product.Distributor (DistributorName) VALUES ('Favorite Brands');
INSERT INTO product.Distributor (DistributorName) VALUES ('Flood');
INSERT INTO product.Distributor (DistributorName) VALUES ('Four Corners');
INSERT INTO product.Distributor (DistributorName) VALUES ('Full Clip');
INSERT INTO product.Distributor (DistributorName) VALUES ('Fullclip');
INSERT INTO product.Distributor (DistributorName) VALUES ('Grapevine');
INSERT INTO product.Distributor (DistributorName) VALUES ('HopFusion');
INSERT INTO product.Distributor (DistributorName) VALUES ('Independence');
INSERT INTO product.Distributor (DistributorName) VALUES ('Live Oak');
INSERT INTO product.Distributor (DistributorName) VALUES ('Martin House');
INSERT INTO product.Distributor (DistributorName) VALUES ('Oak Highlands');
INSERT INTO product.Distributor (DistributorName) VALUES ('Panther Island');
INSERT INTO product.Distributor (DistributorName) VALUES ('Peticolas');
INSERT INTO product.Distributor (DistributorName) VALUES ('Pioneer');
INSERT INTO product.Distributor (DistributorName) VALUES ('Rainwater Bev');
INSERT INTO product.Distributor (DistributorName) VALUES ('Revolver');
INSERT INTO product.Distributor (DistributorName) VALUES ('Save The World');
INSERT INTO product.Distributor (DistributorName) VALUES ('Shannon');
INSERT INTO product.Distributor (DistributorName) VALUES ('Sons of John');
INSERT INTO product.Distributor (DistributorName) VALUES ('Texas Keeper');
INSERT INTO product.Distributor (DistributorName) VALUES ('Three Nations');
INSERT INTO product.Distributor (DistributorName) VALUES ('Tupps Brewery');
INSERT INTO product.Distributor (DistributorName) VALUES ('Twisted X');
INSERT INTO product.Distributor (DistributorName) VALUES ('Whistle Post');
INSERT INTO product.Distributor (DistributorName) VALUES ('Wild Acre');
GO
--End table product.Distributor

--Begin table product.Producer
TRUNCATE TABLE product.Producer
GO

EXEC utility.InsertIdentityValue 'product.Producer', 'ProducerID', 0
GO

INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('(512)','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('903','Sherman','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('3 Floyds','Munster','IN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Abita','Abita Springs','LA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ace','Sebastopol','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Adelbert''s','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alaskan','Juneau','AK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('AleSmith','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alltech','Lexington','KY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alpine','Alpine','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alvinne','Zwevegem',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Anderson Valley','Booneville','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Apogee','Clear Lake','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Argus','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Armadillo','Denton','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Atwater','Detroit','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Audacity','Denton','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Austin Beerworks','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Austin Eastciders','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Avery','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ayinger','Aying',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bacchus','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Backcountry','Frisco','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Baird','Tamuning',NULL,'GUM')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ballast Point','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bayou Teche','Amaudville','LA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Beachwood','Long Beach','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bear Republic','Healdsburg','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bell''s','Kalamazoo','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('BFM','Saignelegier',NULL,'CHE')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Big Bend','Farmers Branch','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Big Sky','Missoula','MT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Birra del Borgo','Borgorose',NULL,'ITA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bishop','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Blue Owl','Austin ','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Boulevard','Kansas City','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Braindead','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brash','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brasserie de Silly','Silly',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Breckenridge','Denver','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bridgeport','Portland','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brooklyn','Brooklyn','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bud','Saint Louis','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Buffalo Bayou','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Buffalo Bill''s','Hayward','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cedar Creek','Kemp','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cigar City','Tampa','FL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Clown Shoes','Ipswich','MA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cobra','Lewisville','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Collective Brewing','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Colorado','Colorado Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Community','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('COOP Ale Works','Oklahoma City','OK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Coors','Golden','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Corona','Mexico City',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crabbie''s','Glasgow',NULL,'SCO')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crafted Artisan Meadery','Mogadore','OH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crazy Mountain','Edwards','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('De Troch','Wambeek',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Deep Ellum','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Delirium','Melle',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Deschutes','Bend','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Destihl','Bloomington','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Dogfish Head','Milton','DE','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Duvel','Breendonk-Puurs',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Einbecker','Einbeck',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('El Gobernador','Villaviciosa',NULL,'ESP')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Elevation','Poncha Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Epic','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Erdinger','Erding',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Evil Twin','Kobenhavn',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Finnriver','Chimacum','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Firestone Walker','Paso Robles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Flying Dog','Frederick','MD','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Founders','Grand Rapids','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Four Corners','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Franziskaner','Munchen',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Full Sail','Hood River','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Funkwerks','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Gavroche','Saint Sylvestre Cappel',NULL,'FRA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Goose Island','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Grapevine','Grapevine','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Great Divide','Denver','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Green Flash','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Green''s','Baildon Shipley',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Guinness','Dublin',NULL,'IRL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Half Acre','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('He''Brew','Clifton Park','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Heretic','Fairfield','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hitachino Nest','Ibaraki-ken Naka-gun',NULL,'JPN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hollows & Fentimans','Hexham',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('HopFusion','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hops & Grain','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Humboldt','Paso Robles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Independence','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Iron Maiden','Stockport',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ishii','Izu-shi',NULL,'JPN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Jester King','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Jolly Pumpkin','Dexter','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Julius Echter','Wurzburg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Karbach','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Kasteel','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Kostritzer','Bad KÃ¶stritz',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lagunitas','Petiluma','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lakewood','Garland','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Laughing Dog','Ponderay','ID','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lazy Magnolia','Kiln','MS','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lefebvre','Rebecq-Quenast',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leffe','Dinant',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Left Hand','Longmont','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leibinger','Ravensburg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leprechaun','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Les 3 Fourquets','Courtil',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Liefman''s','Oudenaarde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Live Oak','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Locust','Woodinville','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lone Pint','Magnolia','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lone Star','Los Angeles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Magic Hat','South Burlington','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Martin House','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Maui','Lahaina','HI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mckenzie''s','West Seneca ','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Meridian Hive','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mikkeller','Copenhagen',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Miller','Milwaukee','WI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mission','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Modelo','Mexico City',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Monk''s','Ertvelde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Moonlight Meadery','Londonderry','NH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Moylans','Novato','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Naughty','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Nebraska','Papillion','NE','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('New Belgium','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('New Holland','Holland','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ninkasi','Eugene ','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('No Label','Katy','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Noble Rey','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('No-Li','Spokane','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('North Coast','Fort Bragg','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oasis','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oddwood','Austin ','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Odell','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Old Speckled Hen','Suffolk',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ommegang','Cooperstown','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oskar Blues','Longmont','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Panther Island','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Pedernales','Fredericksburg','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Perennial','Saint Louis','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Peticolas','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Petrus','Bavikhove',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Pinkus','Munster',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Poperings','Watou',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Prairie','Krebs','OK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Professor Fritz Briem','Freising',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rabbit Hole','Justin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rahr','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ranger Creek','San Antonio','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Real Ale','Blanco','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Redstone','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Revolver','Granbury','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rince','Oudenaarde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Robinsons','Stockport',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rogness','Pflugerville','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rogue','Newport','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Russian River','Santa Rosa','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Saint Arnold','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Saison','Tourpes',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sam Adams','Jamaica Plain','MA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Santa Fe','Santa Fe','NM','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('SanTan','Chandler','AZ','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Save The World','Marble Falls','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schilling ','Seattle','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schlenkerla','Bamberg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schmaltz ','Clifton Park','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sea Dog','Bangor','ME','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shacksbury','Vergennes','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shannon','Keller','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shiner','Shiner','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shipyard','Portland','ME','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sierra Madre','San Pedro',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sierra Nevada','Chico','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sixpoint','Brooklyn','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ska','Durango','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Small Brewpub','Oak Cliff','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Smuttynose','Hampton','NH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Southern Star','Conroe','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Southern Tier','Lakewood','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Squatters','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St Arnold','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St Louis','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St. Feuillien','Le Roeuix',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stella','Leuven',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stevens Point','Stevens Point','WI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stiegl','Salzburg',NULL,'AUT')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stone','Escondido','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Strange Land','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Summit','Saint Paul','MN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Surly','Brooklyn Center','MN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('De Proef','Lochristi',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sweetwater','Atlanta','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tecate','Monterrey',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Terrapin','Athens','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Terrapin','Athens','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Ale Project','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Keeper','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Select','San Antonio','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('The Bruery','Placentia','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('The Manhattan Project','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Three Nations','Farmers Branch','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tieton','Tieton','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('To Ol','Frederiksberg',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tommyknocker','Idaho Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tupps','McKinney','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Twisted Pine','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Twisted X','Dripping Springs','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Uinta','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Unibroue','Chambly',NULL,'CAN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Unity','Ypsilanti','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Upland','Bloomington','IN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Upslope','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Urban Family','Seattle','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Urthel','Ruiselede',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Van Diest','Melle',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Verzet','Anzegem',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Victory','Downingtown','PA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Virtue','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wandering Aengus','Salem','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wasatch','Park City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Well''s & Young','Bedford',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wexford','Suffolk',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wild Acre','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wild Beer','Westcombe',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Woodchuck','Middlebury','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wyndridge','Dallastown','PA','USA')
GO

INSERT INTO product.Producer
	(ProducerName,ImagePath,Municipality,ISORegionCode2,ISOCountryCode3)
VALUES
	('3 Nations', '/assets/img/breweries/3nationslogo.png', 'Farmers Branch', 'TX', 'USA'),
	('Elysian', '/assets/img/breweries/elysianlogo.jpg', 'Seattle', 'WA', 'USA')
GO
--End table product.Producer

--Begin table product.ProductStyle
TRUNCATE TABLE product.ProductStyle
GO

EXEC utility.InsertIdentityValue 'product.ProductStyle', 'ProductStyleID', 0
GO

INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('4 Grain Breakfast Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ale Brewed w/ Star Anise & Licorice Root');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Altbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Amber Ale/Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ameriacn Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Adjunct Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber/Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber/Red Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Dark Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Doube/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Imperial/Double Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American IPA w/ Mango');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ae');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ale brewed w/ Blood Orange');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Session Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Sour Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wheat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wile Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American/Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Americn IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Applewine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot American Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Asain Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('BA Cherry Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Baltic Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Baltic Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Adambier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Brett Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Coffee & Chicory Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Sour Ale w/ Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Tart Pomegranate Wit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Triple Ale w/ Marionberries & Meyer Lemons');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Vanilla Porter w/ Tart Cherry');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Tart Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bavarian Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bavarian Witbier brewed w/ Raspberry & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('BBA Scotch Ale/Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Blonde/Golden');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Inspired Wheat Ale w/ Michigan Cherry Juice');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Quad Brewed w/ Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Quadruple');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Wheat Ale w/ Michigan Cherry Juice');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian White');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian-Style Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian-Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berline Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weissebier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Biere de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('BiÃ¨re de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bitter Chocolate Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Chamomile Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Currant Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black IPA/Cascadian Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Tea & Chai Spiced Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Tea, Lemon & Apple Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry & Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Orange Blossom Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blended Barrel Aged Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale brewed w/ Cherry');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale Brewed w/ Lemon Peel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange & Spice Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blossom Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry & Blackberry Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Berliner Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry, Blackberry & Black Currant Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bohemiam Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Dopplebock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Malt Liquor');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Milk/Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Stout w/ Peanuts');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Wheatwine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel-Aged Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel-Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Peach Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Braggot');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brandy Barrel Aged Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brass Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brett Blend Barrel Fermented/Aged Farmhouse Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brett Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cabernet Barrel Aged Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('California Common/Steam Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('California White Burgundy Barrel Aged Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cap''n Crunch & Fruity Pebbles Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cascadian Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chai Tea Spiced Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chardonnay Barrel Aged Belgian Strong Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry & Hibiscus Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry & Vanilla Sour Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Ale Aged In Maple Syrup Bourbon Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Hibiscus Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Pie Sour Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry, Salt & Lime Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chile Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Chip Cookie Dough Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate/Peanut Butter Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cider Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon & Vanilla Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon & Vanilla English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon, Allspice & Clove Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cocoa Nib Aged Golden Ale Fermented w/ Cherries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coconut & Vanilla Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coconut Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee & Chicory Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Baltic Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry/Raspberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Ale brewed w/ Lychee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Soda Spiced Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cucumber Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsener');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsner w/ Falconers Flight Hops');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Double Alt Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Sour w/ Berries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Doppelbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dortmunder/Export Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dortmunder/Helles');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Barrel Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA Aged in Gin Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA w/Sorachi Ace Hops');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Douple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry-Hopped Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dunkelweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('East Coast IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Bitter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Brown Ale Fermented w/ Ghost Peppers');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Mild Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Pale Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Style Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Style Brown Ale w/ Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Eruo Dark Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Euro Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Expiramental IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Export Stout Brewed with Strawberries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Extra Special/Strong Bitter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale Brewed with Honey and Tangerines');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale Brewed with Smoked Figs');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse/Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flanders Oud Bruin');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flanders Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flowering Citrus Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Foreign/Export Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Foreign/Export Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Fruit/Vegetable Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Funk Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Fusion Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Pilsener');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Style Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin & Juice Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin & Tonic Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel-Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gluten Free Euro Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gluten Free Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gold Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale w/ Apricot & Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale/Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Chocolate Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Oatmeal Stout brewed w/ Chocolate & Beets');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gose Aged In Cabernet Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grand Cru');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit & Mint Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit & Passionfruit Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gratzer/Grodziskie');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Green Tea Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gruit/Ancient Herbed Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Guava Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gueuze');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hard Root Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hatch Chile Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Helles Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Herb/Spiced Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus & Cherry Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus & Ginger Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Holiday Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Holiday Spiced Coffee Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey & Apple Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey & Vanilla Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Vanilla Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hop Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy American Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Belgian-style Golden Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hop-Rocketed American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imp Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Bourbon Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Dark Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Milk Stout w/ Coconut');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Oak-Aged Cranberry Spiced Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pumpkin Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Red IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Red Rye');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Smoked Black Rye Oaked Raspberry IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Smoked Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Taiji IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Tart Cherry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Dobule IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Black IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Coconut Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Peanut Butter Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Peppermint Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Porter with Chocolate');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('India Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('India-Style Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('IPA Brewed w/ Citra Hops & Avocado Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('IPL');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Dry Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Juniper & Rosemary Biere de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kellerbier/Zwickelbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kombucha Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kriek');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kristallweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kvass');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lagered Belgian-Style Wit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic/Framboise');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic-Fruit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lemon Ginger Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lemonade Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lichtenhainer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Light Ale Brewed With Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Macallan Barrel Aged Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Maibock/Helles Bock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mandarina Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango & Apricot American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Session Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango, Passionfruit & Hibiscus Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen/Oktoberfest');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Melomel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Melon Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mesquite Bean Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mexican  Coffee Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mexican Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mezcal & Tequila Barrel Aged Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Milk/Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mocha Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Multigrain Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Munich Dunkel Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Munich Helles Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Cold Coffee - Nitro');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Hopped Tonic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Lemonade');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Root Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('New England IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('New England Style IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Non-Alcoholic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('North American Adjunct Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Northwest Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Norwegian Spiced Belgian Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Nut Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged Amber Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Bourbon Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Whiskey Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak-Aged Raspberry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oat Wine Aged in Whistlepig Rye Whiskey Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Raisin Cookie Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Okroberfest/Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oktoberfest/Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Orange & Vanilla Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Orange and Lemon IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oud Bruin');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Ale Brewed w/ Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Sour Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit White IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit & Pineapple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit Citra Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach & Honey Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Belgian Blonde w/ Coriander');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Spiked Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter & Berries Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter & Jelly Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pickle Juice Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pina Colada IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Passionfruit Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Poblano Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pomegranate Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pomegranate Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pre-Prohibition Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Prickly Pear Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Ale w/ Cuvee Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Spiced Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin/Yam Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ramen Noodle Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Ginger Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Maibock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Northwest Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rauchbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Currant Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Belgian Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Saison w/ Raspberries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel-Aged Belgian-Style Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel-Aged Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rhubarb Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Robust Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Robust Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Roggenbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rose Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Northwest Style Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Pumpkin Spice Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout aged in Bourbon Barrels with Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout Brewed w/ Toasted Coconut');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Whiskey Barrel Aged Rye Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Whisky BA Belgian Style Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison brewed w/ Blood Orange');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Salted Caramel Brownie Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sauvignon Blanc Aged Christmas Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Schwarzbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Ale/Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Ale/Wee Heavy brewed w/ Chocolate');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Barrel Aged Scotch Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Barrel-Aged Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scottish Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scottish Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Session IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Shandy/Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sherry Barrel Aged Czech Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Single Hop IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('S''more Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Apple Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Farmhouse IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Lemon Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Session Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Session Wheat with Salt & Coriander');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Southern Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spanish Style Sour Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Specialty American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Brown Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Chocolate Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Golden Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Honey & Banana Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Yam Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Steam Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Stout brewed w/ Ginger and Spices');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry CrÃ¨me Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Rhubarb Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strong Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strong Golden Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer Ale Brewed w/ Hibiscus, Lemon Peel & Chamomile Flowers');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer Cherry Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer-Style IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sweet Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tangerine IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Melomel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Peach Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Session Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tequila Barrel-Aged Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas Steam Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas-Style Champagne Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Toasted Coconut Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Triple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical & Floral Unfiltered IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered American Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Baltic Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla, Cinnamon & Pecan Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vienna Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Lime Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hop American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hop IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hopped Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Ale w/ Strawberry Puree');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Wine Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey BA Black Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Barley Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Strong Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('White IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('White Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Porter Brewed w/ Currants');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Strawberry Honeywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wine & Mead Hybrid/Ancient Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wine Bare Aged Pretzel Stout w/ Brett');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Spiced Imperial Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Warmer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier brewed w/ Orange Zest');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier w/ Chili, Lime & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier w/ Raspberry & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wood Aged Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Zephyr Oat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Zwickel Lager');
GO

UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer for beer lovers, the German-style helles is a malt accented lager beer that balances a pleasant malt sweetness and body with floral Noble hops and restrained bitterness. The helles is a masterclass in restraint, subtly and drinkability which makes it an enduring style for true beer lovers and an elusive style for craft brewers to recreate. The German helles reminds beer lovers that the simple things in life are usually the most rewarding and worth pursuing.' WHERE ProductStyleName = 'Dortmunder/Helles'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer for beer lovers, the German-style helles is a malt accented lager beer that balances a pleasant malt sweetness and body with floral Noble hops and restrained bitterness. The helles is a masterclass in restraint, subtly and drinkability which makes it an enduring style for true beer lovers and an elusive style for craft brewers to recreate. The German helles reminds beer lovers that the simple things in life are usually the most rewarding and worth pursuing.' WHERE ProductStyleName = 'Helles Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer rich in malt with a balance of clean, hop bitterness. Bread or biscuit-like malt aroma and flavor is common. Originating in Germany, this style used to be seasonally available in the spring (âMarzenâ meaning âMarchâ), with the fest-style versions tapped in October.' WHERE ProductStyleName = 'Marzen/Oktoberfest'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer rich in malt with a balance of clean, hop bitterness. Bread or biscuit-like malt aroma and flavor is common. Originating in Germany, this style used to be seasonally available in the spring (âMarzenâ meaning âMarchâ), with the fest-style versions tapped in October.' WHERE ProductStyleName = 'Oktoberfest/Marzen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A bona fide English beer classic, English-style brown ale is easily one of the most iconic beer styles. Toasty, robust and with a bit of chocolate maltiness, the English brown ale is a meal in a glass, but offers unlimited opportunities for memorable food pairings. Neither flashy nor boring, the English brown is a beer with enough variation to keep devotees ordering them time and time again.' WHERE ProductStyleName = 'English Brown Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A distinctive quality of these ales is that their yeast undergoes an aging process (often for years) in bulk storage or through bottle conditioning, which contributes to a rich, wine-like and often sweet oxidation character. Old ales are copper-red to very dark in color. Complex estery character may emerge.' WHERE ProductStyleName = 'English Old Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'Amber Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'American Amber'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'American Amber/Red Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A wood- or barrel-aged beer is any lager, ale or hybrid beer, either a traditional style or a unique experimental beer, that has been aged for a period of time in a wooden barrel or in contact with wood. This beer is aged with the intention of imparting the unique character of the wood and/or the flavor of what has previously been in the barrel. Todayâs craft brewers are using wood (mostly oak) to influence flavor and aromatics. Beer may be aged in wooden barrels (new or previously used to age wine or spirits), or chips, spirals and cubes may be added to the conditioning tanks that normally house beer. A variety of types of wood are used including oak, apple, alder, hickory and more. The interior of most barrels is charred or toasted to further enhance the flavor of the wood.' WHERE ProductStyleName = 'Barrel-Aged Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Also called âheller bockâ (meaning âpale bockâ), the German-style Maibock is paler in color and more hop-centric than traditional bock beers. A lightly toasted and/or bready malt character is often evident.' WHERE ProductStyleName = 'Maibock/Helles Bock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American barley wine ranges from amber to deep red/copper-garnet in color. A caramel and/or toffee aroma and flavor are often part of the malt character along with high residual malty sweetness. Complexity of alcohols is evident. Fruity-ester character is often high. As with many American versions of a style, this barley wine ale is typically more hop-forward and bitter than its U.K. counterpart. Low levels of age-induced oxidation can harmonize with other flavors and enhance the overall experience. Sometimes sold as vintage releases.' WHERE ProductStyleName = 'American Barleywine'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American barley wine ranges from amber to deep red/copper-garnet in color. A caramel and/or toffee aroma and flavor are often part of the malt character along with high residual malty sweetness. Complexity of alcohols is evident. Fruity-ester character is often high. As with many American versions of a style, this barley wine ale is typically more hop-forward and bitter than its U.K. counterpart. Low levels of age-induced oxidation can harmonize with other flavors and enhance the overall experience. Sometimes sold as vintage releases.' WHERE ProductStyleName = 'Barleywine'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American craft beer lovers are huge fans of the IPA. The quest for more of the India pale ale flavor has led them to the imperial India pale ale, a stronger version of the American IPA, which boasts even more hoppy flavor, aroma and bitterness. Imperial India pale ale is darker in color than the American IPA, substantially more bitter, and high in alcohol by volume. This all-American take on the IPA leaves craft beer fans with plenty of new creations to try.' WHERE ProductStyleName = 'India Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Craft Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Light Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American wheat beers are some of the most approachable beers in the craft beer world, and the versatility of wheat beer allows it to be combined with a variety of ingredients or enjoyed on its own alongside a wide variety of food options. The sizable portion of wheat malt used to brew wheat beer lends a lighter, distinctive experience compared to beers brewed with barley exclusively. Typically lighter in appearance, wheat beer can be made using either ale or lager yeast, and American wheat beer can be brewed with at least 30 percent malted wheat. Like the traditional German hefeweizen, these beers are typically served unfiltered and can have a cloudy appearance when roused. Traditionally hoppier than its German cousin, American wheat beer differs in that it does not offer flavors of banana or clove, which is indicative of the weizen yeast strain. Nevertheless, the American wheat beer is known worldwide as a refreshing summer style.' WHERE ProductStyleName = 'American Wheat Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An Americanized version of a Dunkelweizen, these beers can range in color from garnet or deep amber to ruby brown. Often cloudy with long-lasting heads, this style tends to be light- to medium-bodied with a high level of carbonation. Hop character might be low or moderately high with some fruitiness from ale fermentation, though most examples use of a fairly neutral ale yeast, resulting in a clean fermentation with little to no diacetyl. Flavors of caramel and toasted malts might be present too, just don''t expect German Weizen flavors and aromas of banana esters and clove-like phenols.' WHERE ProductStyleName = 'American Dark Wheat Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An herb and spice beer is a lager or ale that contains flavors derived from flowers, roots, seeds or certain fruits or vegetables. Typically the hop character is low, allowing the added ingredient to shine through. The appearance, mouthfeel and aromas vary depending on the herb or spice used. This beer style encompasses innovative examples as well as traditional holiday and winter ales.' WHERE ProductStyleName = 'Herb/Spiced Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An herb and spice beer is a lager or ale that contains flavors derived from flowers, roots, seeds or certain fruits or vegetables. Typically the hop character is low, allowing the added ingredient to shine through. The appearance, mouthfeel and aromas vary depending on the herb or spice used. This beer style encompasses innovative examples as well as traditional holiday and winter ales.' WHERE ProductStyleName = 'Herbed/Spiced Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Arguably one of the most recognizable beer styles, the German-style hefeweizen offers a striking beer experience thanks to the use of distinctive wheat malt, unique yeast and uncharateristic appearance. This wheat beer breaks from the German beer mold, showcasing yeast-driven fruit and spice as well as bearing an eye-catching mystique. Donât let the cloudy hefeweizen deter you, this beer is one of the worldâs most enjoyable styles for beer geeks and neophytes, alike. The refreshing qualities of this highlycarbonated style have kept it alive for centuries. Try one for yourself and experience why that is, firsthand.' WHERE ProductStyleName = 'Hefeweizen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers in this category are gold to light amber in color. Often bottle-conditioned, with some yeast character and high carbonation. Belgian-style saison may have Brettanomyces or lactic character, and fruity, horsey, goaty and/or leather-like aromas and flavors. Specialty ingredients, including spices, may contribute a unique and signature character. Commonly called âfarmhouse alesâ and originating as summertime beers in Belgium, these are not just warm-weather treats. U.S. craft brewers brew them year-round and have taken to adding a variety of additional ingredients.' WHERE ProductStyleName = 'Belgian-Style Saison'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeÃ±o chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chile Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeÃ±o chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chili Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeÃ±o chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chili Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Belgian-style witbier is brewed using unmalted wheat, sometimes oats and malted barley. Witbiers are spiced with coriander and orange peel. A style that dates back hundreds of years, it fell into relative obscurity until it was revived by Belgian brewer Pierre Celis in the 1960s. This style is currently enjoying a renaissance, especially in the American market. âWitâ means âwhite.â' WHERE ProductStyleName = 'Belgian White'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Belgian-style witbier is brewed using unmalted wheat, sometimes oats and malted barley. Witbiers are spiced with coriander and orange peel. A style that dates back hundreds of years, it fell into relative obscurity until it was revived by Belgian brewer Pierre Celis in the 1960s. This style is currently enjoying a renaissance, especially in the American market. âWitâ means âwhite.â' WHERE ProductStyleName = 'Witbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Biere de Garde translates as âbeer for keeping.â This style is popping up more and more from U.S. producers. Blond, amber and brown versions exist. Biere de garde examples are light amber to chestnut brown or red in color. This style is characterized by a toasted malt aroma and slight malt sweetness. Flavor of alcohol is evident. Often bottle-conditioned, with some yeast character.' WHERE ProductStyleName = 'BiÃ¨re de Garde'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Both lagers and ales can be brewed with honey. Some brewers will choose to experiment with ingredients, while others will add honey to traditional styles. Overall the character of honey should be evident but not totally overwhelming. A wide variety of honey beers are available. U.S. brewers may add honey to the boil kettle (as a sugar source) or post-boil (to preserve more volatile aromatics).' WHERE ProductStyleName = 'Honey Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Characterized by floral, fruity, citrus-like, piney or resinous American-variety hop character, the IPA beer style is all about hop flavor, aroma and bitterness. This has been the most-entered category at the Great American Beer Festival for more than a decade, and is the top-selling craft beer style in supermarkets and liquor stores across the U.S.' WHERE ProductStyleName = 'American IPA '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Characterized by floral, fruity, citrus-like, piney, resinous American hops, the American pale ale is a medium-bodied beer with low to medium caramel, and carries with it a toasted maltiness. American pale ale is one of the most food-friendly styles to enjoy, since the pale ale works wonderfully with lighter fare such as salads and chicken, but can still stand up to a hearty bowl of chili; a variety of different cheeses, including cheddar; seafood, like steamed clams or fish, and even desserts. The American pale aleâs affinity to food can be attributed to the simplicity of its ingredients, which include toasty pale malt, a clean fermenting ale beer yeast, and the counterbalance of American hops to help tease out the flavor or cleanse the palate, preparing you for another bite.' WHERE ProductStyleName = 'American Pale Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Chocolate beer can be an ale or lager that benefits from the addition of any type of chocolate or cocoa. Traditionally added to porters, stouts and brown ales, where the grain bill better complements the confectionery ingredient, it can be added to other styles as well. Chocolate character can range from subtle to overt, but any chocolate beer is generally expected to offer some balance between beer and bon-bon. The style can vary greatly in approach as well as flavor profile depending on the brewer.' WHERE ProductStyleName = 'Chocolate Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor â but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Blonde Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor â but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Cream Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor â but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Style Tripel Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Style Tripel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Tripel Style Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Tripel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This styleâs fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'German Alt/Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This styleâs fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'German Style Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This styleâs fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Definitively American, the imperial porter should have no roasted barley flavors or strong burnt/black malt character. Medium caramel and cocoa-like sweetness is present, with complementing hop character and malt-derived sweetness.' WHERE ProductStyleName = 'American Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Definitively American, the imperial porter should have no roasted barley flavors or strong burnt/black malt character. Medium caramel and cocoa-like sweetness is present, with complementing hop character and malt-derived sweetness.' WHERE ProductStyleName = 'Imperial Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Dry stout is black beer with a dry-roasted character thanks to the use of roasted barley. The emphasis on coffee-like roasted barley and a moderate degree of roasted malt aromas define much of the character. Hop bitterness is medium to medium high. This beer is often dispensed via nitrogen gas taps that lend a smooth, creamy body to the palate.' WHERE ProductStyleName = 'Irish Dry Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Emphasizing hop aroma and flavor without bracing bitterness, the New England IPAÂ  leans heavily on late and dry hopping techniques to deliver a bursting juicy, tropical hop experience. The skillful balance of technique and ingredient selection, often including the addition of wheat or oats, lends an alluring haze to this popular take on the American IPA.' WHERE ProductStyleName = 'New England IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Emphasizing hop aroma and flavor without bracing bitterness, the New England IPAÂ  leans heavily on late and dry hopping techniques to deliver a bursting juicy, tropical hop experience. The skillful balance of technique and ingredient selection, often including the addition of wheat or oats, lends an alluring haze to this popular take on the American IPA.' WHERE ProductStyleName = 'New England Style IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'ESB stands for âextra special bitter.â This style is known for its balance and the interplay between malt and hop bitterness. English pale ales display earthy, herbal English-variety hop character. Medium to high hop bitterness, flavor and aroma should be evident. The yeast strains used in these beers lend a fruitiness to their aromatics and flavor, referred to as esters. The residual malt and defining sweetness of this richly flavored, full-bodied bitter is medium to medium-high.' WHERE ProductStyleName = 'English Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Fruit beer is made with fruit, or fruit extracts that are added during any portion of the brewing process, providing obvious yet harmonious fruit qualities. This idea is expanded to âfield beersâ that utilize vegetables and herbs.' WHERE ProductStyleName = 'Fruit Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Fruit beer is made with fruit, or fruit extracts that are added during any portion of the brewing process, providing obvious yet harmonious fruit qualities. This idea is expanded to âfield beersâ that utilize vegetables and herbs.' WHERE ProductStyleName = 'Fruit/Vegetable Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'If you are one of the 2 million Americans who suffer from celiac disease, trying craft beers may seem impossible, or at least challenging. But with the growing interest in gluten-free options, many people have found that they no longer have to miss out on enjoying craft beer. Many brewers have recognized the desire for gluten-free customers to enjoy their beer without the concern of ingesting gluten, leading many craft brewers to utilize alternative grains during the brewing process that do not contain gluten. Dedicated gluten-free breweries have also found success catering to people dealing with gluten intolerance as well as healthminded beer drinkers who choose to follow a gluten-reduced or gluten-free diet, but donât want to give up their favorite beverage.' WHERE ProductStyleName = 'Gluten Free Euro Pale Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'If you are one of the 2 million Americans who suffer from celiac disease, trying craft beers may seem impossible, or at least challenging. But with the growing interest in gluten-free options, many people have found that they no longer have to miss out on enjoying craft beer. Many brewers have recognized the desire for gluten-free customers to enjoy their beer without the concern of ingesting gluten, leading many craft brewers to utilize alternative grains during the brewing process that do not contain gluten. Dedicated gluten-free breweries have also found success catering to people dealing with gluten intolerance as well as healthminded beer drinkers who choose to follow a gluten-reduced or gluten-free diet, but donât want to give up their favorite beverage.' WHERE ProductStyleName = 'Gluten Free Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'In darker versions, malt flavor can optionally include low roasted malt characters (evident as cocoa/chocolate or caramel) and/or aromatic toffee-like, caramel, or biscuit-like characters. Low-level roasted malt astringency is acceptable when balanced with low to medium malt sweetness. Hop flavor is low to medium-high. Hop bitterness is low to medium. These beers can be made using either ale or lager yeast. The addition of rye to a beer can add a spicy or pumpernickel character to the flavor and finish. Color can also be enhanced and may become more red from the use of rye. The ingredient has come into vogue in recent years in everything from stouts to lagers, but is especially popular with craft brewers in India pale ales. To be considered an example of the style, the grain bill should include sufficient rye such that rye character is evident in the beer.' WHERE ProductStyleName = 'Rye Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Donât let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'Amber Ale/Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Donât let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'American Amber/Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Donât let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'American Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Light bodied, pale, fizzy lagers made popular by the large macro-breweries (large breweries) of America after Prohibition. Low bitterness, thin malts, and moderate alcohol. Focus is less on flavor and more on mass-production and consumption, cutting flavor and sometimes costs with adjunct cereal grains, like rice and corn.' WHERE ProductStyleName = 'American Adjunct Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Like many other beer styles that have become prized by American brewers and beer lovers alike, American stout is a distinct variant of a European stout beer counterpart. True to style, American stouts showcase generous quantities of the American hops fans have come to expect, and much like other stout beer types, American stout can be enjoyed year-round but is commonly considered a beer for the fall or winter months. The stout is a terrific companion to bold, hearty foods. Look for hearty game meats, as well as soups and strong cheeses to be particularly suitable for pairing for American stouts, in addition to a variety of after-dinner desserts.' WHERE ProductStyleName = 'American Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beerâs acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berline Weissbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beerâs acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weissbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beerâs acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weisse'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beerâs acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weissebier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Malt and caramel are part of the flavor and aroma profile of the English-style mild while licorice and roast malt tones may sometimes contribute as well. Hop bitterness is very low to low. U.S. brewers are known to make lighter-colored versions as well as the more common âdark mild.â These beers are very low in alcohol, yet often are still medium-bodied due to increased dextrin malts.' WHERE ProductStyleName = 'English Mild Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Often known as cassis, framboise, kriek, or peche, a fruit lambic takes on the color and flavor of the fruit it is brewed with. It can be dry or sweet, clear or cloudy, depending on the ingredients. Notes of Brettanomyces yeast are often present at varied levels. Sourness is an important part of the flavor profile, though sweetness may compromise the intensity. These flavored lambic beers may be very dry or mildly sweet.' WHERE ProductStyleName = 'Belgian Style Lambic'
UPDATE product.ProductStyle SET ProductStyleDescription = 'One of the most approachable styles, a golden or blonde ale is an easy-drinking beer that is visually appealing and has no particularly dominating malt or hop characteristics. Rounded and smooth, it is an American classic known for its simplicity. Sometimes referred to as âgolden ale.â These beers can have honey, spices and fruit added, and may be fermented with lager or ale yeast.' WHERE ProductStyleName = 'American Blonde Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'One of the most approachable styles, a golden or blonde ale is an easy-drinking beer that is visually appealing and has no particularly dominating malt or hop characteristics. Rounded and smooth, it is an American classic known for its simplicity. Sometimes referred to as âgolden ale.â These beers can have honey, spices and fruit added, and may be fermented with lager or ale yeast.' WHERE ProductStyleName = 'Golden Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Originally from the DÃ¼sseldorf area of Germany, the German-Style Brown/Altbier strikes a balance between hop and malt flavors and aromas, but can have low fruity esters and some peppery and floral hop aromas. Before Germany had lager beer, it had ales. Alt, meaning âold,â pays homage to one rebel region in Germany which did not lean into lagering. U.S. producers celebrate the ale revolution beautifully with this top-fermented German beer style.' WHERE ProductStyleName = 'Altbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Oud Bruins, not restricted to, but concentrated in Flanders, are light to medium-bodied and deep copper to brown in color. They are extremely varied, characterized by a slight vinegar or lactic sourness and spiciness to smooth and sweet. A fruity-estery character is apparent with no hop flavor or aroma. Low to medium bitterness. Very small quantities of diacetyl are acceptable. Roasted malt character in aroma and flavor is acceptable, at low levels. Oak-like or woody characteristics may be pleasantly integrated into the overall palate. Typically, old and new Brown Ales are blended, like Lambics.' WHERE ProductStyleName = 'Flanders Oud Bruin'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin Yam Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin/Yam Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Quite possibly the most iconic beer style in modern history, the pilsner â also spelled pilsener â captured the attention of beer drinkers across the world and inspired a myriad of regional imitations. This lightly colored, exquisitely balanced lager remains one of the most loved beers to enjoy, and one of the most challenging for the brewer to create. Pilsners are characteristically light in color and have a very short finish. The world over, pilsner-style lagers have become the standard beer for many reasons, and American craft brewers have worked hard to put their own unique spin on this classic German beer.' WHERE ProductStyleName = 'German Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Quite possibly the most iconic beer style in modern history, the pilsner â also spelled pilsener â captured the attention of beer drinkers across the world and inspired a myriad of regional imitations. This lightly colored, exquisitely balanced lager remains one of the most loved beers to enjoy, and one of the most challenging for the brewer to create. Pilsners are characteristically light in color and have a very short finish. The world over, pilsner-style lagers have become the standard beer for many reasons, and American craft brewers have worked hard to put their own unique spin on this classic German beer.' WHERE ProductStyleName = 'Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Roasted malt, caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma for the American brown ale. American-style brown ales have evident low to medium hop flavor and aroma and medium to high hop bitterness. The history of this style dates back to U.S. homebrewers who were inspired by English-style brown ales and porters. It sits in flavor between those British styles and is more bitter than both.' WHERE ProductStyleName = 'American Brown Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Scottish-style ales vary depending on strength and flavor, but in general retain a malt-forward character with some degree of caramel-like malt flavors and a soft and chewy mouthfeel. Some examples feature a light smoked peat flavor. Hops do not play a huge role in this style. The numbers commonly associated with brands of this style (60/70/80 and others) reflect the Scottish tradition of listing the cost, in shillings, of a hogshead (large cask) of beer. Overly smoked versions would be considered specialty examples. Smoke or peat should be restrained.' WHERE ProductStyleName = 'Scottish Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Session beer is not defined by flavors or aromas, which can place it in almost any style category. Instead, what makes a session beer is primarily refreshment and drinkability. Any style of beer can be made lower in strength than described in the classic style guidelines. The goal should be to reach a balance between the styleâs character and the lower alcohol content. Drinkability is a factor in the overall balance of these beers. Beer should not exceed 5 percent ABV.' WHERE ProductStyleName = 'Session IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sometimes called black lagers, they may remind some of German-style dunkels, but schwarzbiers are drier, darker and more roastoriented.These very dark brown to black beers have a surprisingly pale-colored foam head (not excessively brown) with good cling quality. They have a mild roasted malt character without the associated bitterness. Malt flavor and aroma is at low to medium levels of sweetness.' WHERE ProductStyleName = 'Schwarzbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sometimes referred to as a âDortmunder export,â the European-Style Export has the malt-forward flavor and sweetness of a German-style helles, but the bitter base of a German-style pilsener. This lager is all about balance, with medium hop character and firm but low malt sweetness. Look for toasted malt flavors and spicy floral hop aromas.' WHERE ProductStyleName = 'Dortmunder/Export Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Straw to medium amber, the contemporary Gose is cloudy from suspended yeast. A wide variety of herbal, spice, floral or fruity aromas other than found in traditional Leipzig-Style Gose are present, in harmony with other aromas. Salt (table salt) character is traditional in low amounts, but may vary from absent to present. Body is low to medium-low. Low to medium lactic acid character is evident in all examples as sharp, refreshing sourness.' WHERE ProductStyleName = 'Gose Aged In Cabernet Barrels'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Straw to medium amber, the contemporary Gose is cloudy from suspended yeast. A wide variety of herbal, spice, floral or fruity aromas other than found in traditional Leipzig-Style Gose are present, in harmony with other aromas. Salt (table salt) character is traditional in low amounts, but may vary from absent to present. Body is low to medium-low. Low to medium lactic acid character is evident in all examples as sharp, refreshing sourness.' WHERE ProductStyleName = 'Gose'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Strong, bitter and completely misunderstood, the English India pale ale (or English IPA) bridges the gap between past and present. No other style represents modern craft brewing excitement quite like the IPA, and while this English beer differs widely from the American version it inspires, this strong member of the English pale ale family has plenty of its own to offer â including all of the history behind this variety.' WHERE ProductStyleName = 'English IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sweet stout, also referred to as cream stout or milk stout, is black in color. Malt sweetness, chocolate and caramel should dominate the flavor profile and contribute to the aroma. It also should have a low to medium-low roasted malt/barley-derived bitterness. Milk sugar (lactose) lends the style more body. This beer does use lactose sugar, so people with an intolerance should probably avoid this style.' WHERE ProductStyleName = 'Milk/Sweet Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sweet stout, also referred to as cream stout or milk stout, is black in color. Malt sweetness, chocolate and caramel should dominate the flavor profile and contribute to the aroma. It also should have a low to medium-low roasted malt/barley-derived bitterness. Milk sugar (lactose) lends the style more body. This beer does use lactose sugar, so people with an intolerance should probably avoid this style.' WHERE ProductStyleName = 'Sweet Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The acidity present in sour beer is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash, or produced during fermentation by the use of various microorganisms. These beers may derive their sour flavor from pure cultured forms of souring agents or from the influence of barrel aging.' WHERE ProductStyleName = 'American Sour Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The acidity present in sour beer is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash, or produced during fermentation by the use of various microorganisms. These beers may derive their sour flavor from pure cultured forms of souring agents or from the influence of barrel aging.' WHERE ProductStyleName = 'American Sour Blonde Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The addition of oatmeal adds a smooth, rich body to the oatmeal stout. This beer style is dark brown to black in color. Roasted malt character is caramel-like and chocolate-like, and should be smooth and not bitter. Coffee-like roasted barley and malt aromas are prominent. This low- to medium-alcohol style is packed with darker malt flavors and a rich and oily body from oatmeal.' WHERE ProductStyleName = 'Oatmeal Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American amber ale is one of the mostÂ widely enjoyed styles throughout the United StatesÂ and serves as a cornerstone style of the American craft brewing revolution. American ambers are darker in color than their pale ale cousins, the presence of caramel and crystal malts lending a toasted, toffee flavor, along with the perception of a fuller body when compared to beers without such malts. Amber beer showcases a medium-high to high malt character with medium to low caramel character derived from the use of roasted crystal malts. The American amber is characterized by American-variety hops, which lend the amber ale notes of citrus, fruit and pine to balance the sweetness of the malt. As with many amber beer types, American amber ale is a highly versatile companion to American cuisine, particularly foods that are grilled or barbecued, as roasted malts complement seared, charred and caramelized proteins making this ale beer type a perennial favorite at backyard cookouts.' WHERE ProductStyleName = 'American Amber Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'American Black Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'Black IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'Black IPA/Cascadian Dark Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American cream ale is a mild, pale, light-bodied ale, made using a warm fermentation (top or bottom fermenting yeast) and cold lagering. Despite being called an ale, when being judged in competitions it is acceptable for brewers to use lager yeast.' WHERE ProductStyleName = 'American Cream Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American-style imperial stout is the strongest in alcohol and body of the stouts. Black in color, these beers typically have an extremely rich malty flavor and aroma with full, sweet malt character. Bitterness can come from roasted malts or hop additions.' WHERE ProductStyleName = 'American Double/Imperial Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Baltic-style Porter is a smooth, cold-fermented and cold-lagered beer brewed with lager yeast. Because of its alcoholic strength, it may include very low to low complex alcohol flavors and/or lager fruitiness such as berries, grapes and plums (but not banana; ale-like fruitiness from warm-temperature fermentation is not appropriate). This style has the malt flavors of a brown porter and the roast of a schwarzbier, but is bigger in alcohol and body.' WHERE ProductStyleName = 'Baltic Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style blonde ale is typically easy-drinking, with a low but pleasing hop bitterness. This is a light- to medium-bodied ale, with a low malt aroma that has a spiced and sometimes fruity-ester character. Sugar is sometimes added to lighten the perceived body. This style is medium in sweetness and not as bitter as Belgian-style tripels or golden strong ales. It is usually brilliantly clear. The overall impression is balance between light sweetness, spice and low to medium fruity ester flavors.' WHERE ProductStyleName = 'Belgian Blonde/Golden'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style dubbel ranges from brown to very dark in color. They have a malty sweetness and can have cocoa and caramel aromas and flavors. Hop bitterness is medium-low to medium. Yeast-generated fruity esters (especially banana) can be apparent. Often bottle-conditioned, a slight yeast haze and flavor may be evident. âDubbelâ meaning âdouble,â this beer is still not so big in intensity as to surpass the Belgian-style quadrupel that is often considered its sibling.' WHERE ProductStyleName = 'Belgian Dubbel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Flanders is an ale with character and balance, thanks to lactic sourness and acetic acid. Cherry-like flavors are acceptable, as is malt sweetness that can lend bitterness and a cocoa-like character. Oak or other wood-like flavors may be present, even if the beer was not aged in barrels. Overall, the style is characterized by slight to strong lactic sourness, and Flanders âredsâ sometimes include a balanced degree of acetic acid. Brettanomyces-produced flavors may be absent or very low. This style is a marvel in flavor complexity, combining malt, yeast, microorganisms, acidity and low astringency from barrel aging.' WHERE ProductStyleName = 'Flanders Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style golden strong ale is fruity, complex and often on the higher end of the ABV spectrum, yet are approachable to many different palates. Look for a characteristic spiciness from Belgian yeast and a highly attenuated dry finish. This style is traditionally drier and lighter in color than a Belgian-style tripel.' WHERE ProductStyleName = 'Belgian Strong Golden Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style pale ale is gold to copper in color and can have caramel or toasted malt flavor. The style is characterized by low but noticeable hop bitterness, flavor and aroma. These beers were inspired by British pale ales. They are very sessionable.' WHERE ProductStyleName = 'Belgian Style Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Belgian Style Quadruple'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Quadrupel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Quadruple Style Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Bohemian pilsener has a slightly sweet and evident malt character and a toasted, biscuit-like, bready malt character. Hop bitterness is perceived as medium with a low to medium-low level of noble-type hop aroma and flavor. This style originated in 1842, with âpilsenerâ originally indicating an appellation in the Czech Republic. Classic examples of this style used to be conditioned in wooden tanks and had a less sharp hop bitterness despite the similar IBU ranges to German-style pilsener. Lowlevel diacetyl is acceptable. Bohemian-style pilseners are darker in color and higher in final gravity than their German counterparts.' WHERE ProductStyleName = 'Bohemian Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The California common is brewed with lager yeast but fermented at ale fermentation temperatures. There is a noticeable degree of toasted malt and/or caramel-like malt character in flavor and often in aroma. Often referred to as âsteam beerâ and made famous by San Franciscoâs Anchor Brewing Company. Seek out woody and mint flavor from the Northern Brewer hops.' WHERE ProductStyleName = 'California Common/Steam Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The California common is brewed with lager yeast but fermented at ale fermentation temperatures. There is a noticeable degree of toasted malt and/or caramel-like malt character in flavor and often in aroma. Often referred to as âsteam beerâ and made famous by San Franciscoâs Anchor Brewing Company. Seek out woody and mint flavor from the Northern Brewer hops.' WHERE ProductStyleName = 'Steam Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The English-style bitter is a very sessionable, lower-alcohol, malt-driven style. Broad style description commonly associated with cask-conditioned beers. The light- to medium-bodied ordinary bitter is gold to copper in color, with a low residual malt sweetness. Hop bitterness is medium.' WHERE ProductStyleName = 'English Bitter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The English-style brown porter has no roasted barley or strong burnt/black malt character. Low to medium malt sweetness, caramel and chocolate is acceptable. Hop bitterness is medium. Softer, sweeter and more caramel-like than a robust porter, with less alcohol and body. Porters are the precursor style to stouts.' WHERE ProductStyleName = 'English Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The German-style Dunkelweizen can be considered a cross between a German-style dunkel and a hefeweizen. Distinguished by its sweet maltiness and chocolate-like character, it can also have banana and clove (and occasionally vanilla or bubblegum) esters from weizen ale yeast.' WHERE ProductStyleName = 'Dunkelweizen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The German-style Weizenbock is a wheat version of a German-style bock, or a bigger and beefier dunkelweizen. Malt mellanoidins and weizen ale yeast are the star ingredients. If served with yeast, the appearance may appropriately be very cloudy. With flavors of bready malt and dark fruits like plum, raisin, and grape, this style is low on bitterness and high on carbonation. Balanced clovelike phenols and fruity, banana-like esters produce a well-rounded aroma.' WHERE ProductStyleName = 'Weizenbock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Irish-style red ale is a balanced beer that uses a moderate amount of kilned malts and roasted barley in the recipe, which gives the beer the color for which it is named. Featuring an approachable hop bitterness which rests on the palate, this typically ambercolored beer is brewed as a lager or an ale, and can often have a medium, candy-like caramel malt sweetness. This style may contain adjuncts such as corn, rice, and sugar, which help dry out the beerâs finish and lessen the body. It also often contains roasted barley, lending low roasted notes, darker color and possible creation of a tan collar of foam on top. With notes of caramel, toffee and sometimes low-level diacetyl (butter), think of the Irish red ale beer style as a cousin to lightly-toasted and buttered bread.' WHERE ProductStyleName = 'Irish Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Light Lager is generally a lighter version of a brewery''s premium lager, some are lower in alcohol but all are lower in calories and carbohydrates compared to other beers. Typically a high amount of cereal adjuncts like rice or corn are used to help lighten the beer as much as possible. Very low in malt flavor with a light and dry body. The hop character is low and should only balance with no signs of flavor or aroma. European versions are about half the alcohol (2.5-3.5% ABV) as their regular beer, yet show more flavor (some use 100 percent malt) then the American counterparts. For the most part, this style has the least amount of flavor than any other style of beer.' WHERE ProductStyleName = 'Light Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Robust Porter features more bitter and roasted malt flavor than a brown porter, but not quite as much as a stout. Robust porters have a roast malt flavor, often reminiscent of cocoa, but no roast barley flavor. Their caramel and malty sweetness is in harmony with the sharp bitterness of black malt. Hop bitterness is evident. With U.S. craft brewers doing so much experimentation in beer styles and ingredients, the lines between certain stouts and porters are often blurred. Yet many deliberate examples of these styles do exist. Diacetyl is acceptable at very low levels.' WHERE ProductStyleName = 'Robust Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Scotch ale is overwhelmingly malty, with a rich and dominant sweet malt flavor and aroma. A caramel character is often part of the profile. Some examples feature a light smoked peat flavor. This style could be considered the Scottish version of an Englishstyle barley wine. Overly smoked versions would be considered specialty examples.' WHERE ProductStyleName = 'Scotch Ale/Wee Heavy'
UPDATE product.ProductStyle SET ProductStyleDescription = 'These unique beers vary in color and can take on the hues of added fruits or other ingredients. Horsey, goaty, leathery, phenolic and some fruity acidic character derived from Brettanomyces organisms may be evident, but in balance with other components of an American Brett beer. Brett beer and sour beer are not synonymous.' WHERE ProductStyleName = 'Brett Blend Barrel Fermented/Aged Farmhouse Saison '
UPDATE product.ProductStyle SET ProductStyleDescription = 'These unique beers vary in color and can take on the hues of added fruits or other ingredients. Horsey, goaty, leathery, phenolic and some fruity acidic character derived from Brettanomyces organisms may be evident, but in balance with other components of an American Brett beer. Brett beer and sour beer are not synonymous.' WHERE ProductStyleName = 'Brett Saison'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Traditional bock beers are all-malt brews and are high in malt sweetness. Malt character should be a balance of sweetness and toasted or nut-like malt. âBockâ translates as âgoatâ!' WHERE ProductStyleName = 'Bock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Typically the base for the smoke porter beer style is a robust porter that is given smoky depth thanks to wood-smoked malt. Traditionally, brewers will cite the specific wood used to smoke the malt, and different woods will lend different flavors to the finished product. Smoke flavors dissipate over time.' WHERE ProductStyleName = 'Smoked Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Vienna Lager ranges from copper to reddish brown in color. The beer is characterized by malty aroma and slight malt sweetness. The malt aroma and flavor should have a notable degree of toasted and/or slightly roasted malt character. Hop bitterness is low to medium-low.' WHERE ProductStyleName = 'Vienna Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'We have west coast American brewers to thank for this somewhat reactionary style. Take an India Pale Ale and feed it steroids, and you''ll end up with a Double or Imperial IPA. Although generally recognizable alongside its sister styles in the IPA family, you should expect something more robust, malty, and alcoholic with a characteristically intense hop profile in both taste and aroma. In short, these are boldly flavored, medium-bodied beers that range in color from deep gold to medium amber. The "imperial" usage comes from Russian Imperial Stout, a style of strong Stout originally brewed in England during the late 1700s for the Russian imperial court. Today Double IPA is often the preferred name in the United States.' WHERE ProductStyleName = 'American Double/Imperial IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'When malt is kilned over an open flame, the smoke flavor becomes infused into the beer, leaving a taste that can vary from dense campfire, to slight wisps of smoke. Any style of beer can be smoked; the goal is to reach a balance between the styleâs character and the smoky properties. Originating in Germany as rauchbier, this style is open to interpretation by U.S. craft brewers. Classic base styles include German-style Marzen/Oktoberfest, German-style bock, German-style dunkel, Vienna-style lager and more. Smoke flavors dissipate over time.' WHERE ProductStyleName = 'Smoked Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'âDoppelâ meaning âdouble,â this style is a bigger and stronger version of the lower-gravity German-style bock beers. Originally made by monks in Munich, the doppelbock beer style is very food-friendly and rich in melanoidins reminiscent of toasted bread. Color is copper to dark brown. Malty sweetness is dominant but should not be cloying. Malt character is more reminiscent of fresh and lightly toasted Munich-style malt, more so than caramel or toffee malt. Doppelbocks are full-bodied, and alcoholic strength is on the higher end.' WHERE ProductStyleName = 'Doppelbock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'âDoppelâ meaning âdouble,â this style is a bigger and stronger version of the lower-gravity German-style bock beers. Originally made by monks in Munich, the doppelbock beer style is very food-friendly and rich in melanoidins reminiscent of toasted bread. Color is copper to dark brown. Malty sweetness is dominant but should not be cloying. Malt character is more reminiscent of fresh and lightly toasted Munich-style malt, more so than caramel or toffee malt. Doppelbocks are full-bodied, and alcoholic strength is on the higher end.' WHERE ProductStyleName = 'Dopplebock'
GO
--End table product.ProductStyle

EXEC utility.DropObject 'permissionable'
GO
--End file Build File - 05 - Data.sql

--Begin file Build File - 06 - Import Data.sql
TRUNCATE TABLE product.Product
TRUNCATE TABLE product.ProductProducer
GO

DECLARE @cDistributorName VARCHAR(250)
DECLARE @cPackageSize VARCHAR(50)
DECLARE @cPackageTypeName VARCHAR(50)
DECLARE @cProducerName VARCHAR(250)
DECLARE @cProductName VARCHAR(250)
DECLARE @cProductStyleName VARCHAR(250)
DECLARE @nABV NUMERIC(18,2)
DECLARE @nCharIndex INT
DECLARE @nIBU NUMERIC(18,2)
DECLARE @nImportProductID INT
DECLARE @tOutput TABLE (ProductID INT NOT NULL PRIMARY KEY)
DECLARE @tProducer TABLE (ProducerID INT, ProducerName VARCHAR(250))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		LTRIM(ID.Name) AS ProductName,
		ID.Brewery AS ProducerName,
		ID.Distributor AS DistributorName,

		CASE
			WHEN ID.Style IN ('Amber Lager', 'American Amber', 'American Amber/Red Lager')
			THEN 'American Amber/Red Lager'
			WHEN ID.Style IN ('American Craft Lager', 'American Lager')
			THEN 'American Lager'
			WHEN ID.Style IN ('American Barleywine', 'Barleywine')
			THEN 'Barleywine'
			WHEN ID.Style IN ('Belgian Style Quadruple', 'Quadrupel', 'Quadruple Style Ale')
			THEN 'Belgian Style Quadruple'
			WHEN ID.Style IN ('Belgian Style Tripel Ale', 'Belgian Style Tripel')
			THEN 'Belgian Style Tripel'
			WHEN ID.Style IN ('Belgian Tripel Style Ale', 'Belgian Tripel')
			THEN 'Belgian Tripel'
			WHEN ID.Style IN ('Chile Beer', 'Chili Ale', 'Chili Beer')
			THEN 'Chile Beer'
			WHEN ID.Style IN ('Doppelbock', 'Dopplebock')
			THEN 'Doppelbock'
			WHEN ID.Style IN ('Fruit Beer', 'Fruit/Vegetable Beer')
			THEN 'Fruit/Vegetable Beer'
			WHEN ID.Style IN ('Herb/Spiced Beer', 'Herbed/Spiced Beer')
			THEN 'Herb/Spiced Beer'
			WHEN ID.Style IN ('German Alt/Kolsch', 'German Style Kolsch', 'Kolsch')
			THEN 'Kolsch'
			WHEN ID.Style IN ('Pumpkin Yam Beer', 'Pumpkin/Yam Beer')
			THEN 'Pumpkin/Yam Beer'
			ELSE ID.Style 
		END AS ProductStyleName,

		100 * ISNULL(ID.[%], 0) AS ABV,
		CASE WHEN ISNUMERIC(ID.IBU) = 1 THEN CAST(ID.IBU AS NUMERIC(18,2)) ELSE 0 END AS IBU,
		ID.[Size(gal)] AS PackageSize,
		'Draft' AS PackageTypeName
	FROM product.ImportDraft ID
	WHERE ID.Name IS NOT NULL
	ORDER BY 1, 2

OPEN oCursor
FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageSize, @cPackageTypeName
WHILE @@fetch_status = 0
	BEGIN

	SELECT TOP 1 
		@nImportProductID = ID.ImportProductID
	FROM product.ImportDraft ID
	WHERE ID.Name = @cProductName
		AND ID.Brewery = @cProducerName
		AND ID.Distributor = @cDistributorName
		AND ID.Style = @cProductStyleName
	ORDER BY ID.ImportProductID

	DELETE FROM @tOutput
	DELETE FROM @tProducer

	IF CHARINDEX('/', @cProducerName) > 0
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		SELECT 
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = LTT.ListItem), 0), 
			LTT.ListItem 
		FROM core.ListToTable(@cProducerName, '/') LTT

		END
	ELSE
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		VALUES
			(
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = @cProducerName), 0), 
			@cProducerName
			)

		END
	--ENDIF

	INSERT INTO product.Product
		(ProductName, PackageSize, PackageTypeID, ProductTypeID, ProductStyleID, ABV, IBU, ImportProductID)
	OUTPUT INSERTED.ProductID INTO @tOutput
	VALUES
		(
		@cProductName,
		@cPackageSize,
		ISNULL((SELECT PKT.PackageTypeID FROM dropdown.PackageType PKT WHERE PKT.PackageTypeName = @cPackageTypeName), 0),

		CASE
			WHEN @cProductStyleName LIKE '%Cider%'
			THEN (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Cider')
			ELSE (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Beer')
		END,

		ISNULL((SELECT PS.ProductStyleID FROM product.ProductStyle PS WHERE PS.ProductStyleName = @cProductStyleName), 0),
		@nABV, 
		@nIBU,
		@nImportProductID
		)

	INSERT INTO product.ProductProducer
		(ProductID, ProducerID)
	SELECT
		(SELECT O.ProductID FROM @tOutput O),
		P.ProducerID
	FROM @tProducer P

	print 'Imported ' + CAST(@nImportProductID AS VARCHAR(5)) + ' / Product: ' + @cProductName + ' / Producer: ' + @cProducerName
	
	FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageTypeName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

DECLARE @cDistributorName VARCHAR(250)
DECLARE @cPackageTypeName VARCHAR(50)
DECLARE @cProducerName VARCHAR(250)
DECLARE @cProductName VARCHAR(250)
DECLARE @cProductStyleName VARCHAR(250)
DECLARE @nABV NUMERIC(18,2)
DECLARE @nCharIndex INT
DECLARE @nIBU NUMERIC(18,2)
DECLARE @nImportProductID INT
DECLARE @tOutput TABLE (ProductID INT NOT NULL PRIMARY KEY)
DECLARE @tProducer TABLE (ProducerID INT, ProducerName VARCHAR(250))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		LTRIM(IMP.Name) AS ProductName,
		IMP.Brewery AS ProducerName,
		IMP.Distributor AS DistributorName,

		CASE
			WHEN IMP.Style IN ('Amber Lager', 'American Amber', 'American Amber/Red Lager')
			THEN 'American Amber/Red Lager'
			WHEN IMP.Style IN ('American Craft Lager', 'American Lager')
			THEN 'American Lager'
			WHEN IMP.Style IN ('American Barleywine', 'Barleywine')
			THEN 'Barleywine'
			WHEN IMP.Style IN ('Belgian Style Quadruple', 'Quadrupel', 'Quadruple Style Ale')
			THEN 'Belgian Style Quadruple'
			WHEN IMP.Style IN ('Belgian Style Tripel Ale', 'Belgian Style Tripel')
			THEN 'Belgian Style Tripel'
			WHEN IMP.Style IN ('Belgian Tripel Style Ale', 'Belgian Tripel')
			THEN 'Belgian Tripel'
			WHEN IMP.Style IN ('Chile Beer', 'Chili Ale', 'Chili Beer')
			THEN 'Chile Beer'
			WHEN IMP.Style IN ('Doppelbock', 'Dopplebock')
			THEN 'Doppelbock'
			WHEN IMP.Style IN ('Fruit Beer', 'Fruit/Vegetable Beer')
			THEN 'Fruit/Vegetable Beer'
			WHEN IMP.Style IN ('Herb/Spiced Beer', 'Herbed/Spiced Beer')
			THEN 'Herb/Spiced Beer'
			WHEN IMP.Style IN ('German Alt/Kolsch', 'German Style Kolsch', 'Kolsch')
			THEN 'Kolsch'
			WHEN IMP.Style IN ('Pumpkin Yam Beer', 'Pumpkin/Yam Beer')
			THEN 'Pumpkin/Yam Beer'
			ELSE IMP.Style 
		END AS ProductStyleName,

		ISNULL(IMP.[%], 0) AS ABV,
		CASE WHEN ISNUMERIC(IMP.IBU) = 1 THEN CAST(IMP.IBU AS NUMERIC(18,2)) ELSE 0 END AS IBU,
		CASE WHEN IMP.Type = 'Bottles' THEN 'Bottle' ELSE IMP.Type END AS PackageTypeName
	FROM product.ImportPackage IMP
	WHERE IMP.Name IS NOT NULL

	ORDER BY 1, 2

OPEN oCursor
FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageTypeName
WHILE @@fetch_status = 0
	BEGIN

	SELECT TOP 1 
		@nImportProductID = IMP.ImportProductID
	FROM product.ImportPackage IMP
	WHERE IMP.Name = @cProductName
		AND IMP.Brewery = @cProducerName
		AND IMP.Distributor = @cDistributorName
		AND IMP.Style = @cProductStyleName
	ORDER BY IMP.ImportProductID

	DELETE FROM @tOutput
	DELETE FROM @tProducer

	IF CHARINDEX('/', @cProducerName) > 0
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		SELECT 
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = LTT.ListItem), 0), 
			LTT.ListItem 
		FROM core.ListToTable(@cProducerName, '/') LTT

		END
	ELSE
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		VALUES
			(
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = @cProducerName), 0), 
			@cProducerName
			)

		END
	--ENDIF

	INSERT INTO product.Product
		(ProductName, PackageTypeID, ProductTypeID, ProductStyleID, ABV, IBU, ImportProductID)
	OUTPUT INSERTED.ProductID INTO @tOutput
	VALUES
		(
		@cProductName,
		ISNULL((SELECT PKT.PackageTypeID FROM dropdown.PackageType PKT WHERE PKT.PackageTypeName = @cPackageTypeName), 0),

		CASE
			WHEN @cProductStyleName LIKE '%Cider%'
			THEN (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Cider')
			ELSE (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Beer')
		END,

		ISNULL((SELECT PS.ProductStyleID FROM product.ProductStyle PS WHERE PS.ProductStyleName = @cProductStyleName), 0),
		@nABV, 
		@nIBU,
		@nImportProductID
		)

	INSERT INTO product.ProductProducer
		(ProductID, ProducerID)
	SELECT
		(SELECT O.ProductID FROM @tOutput O),
		P.ProducerID
	FROM @tProducer P

	print 'Imported ' + CAST(@nImportProductID AS VARCHAR(5)) + ' / Product: ' + @cProductName + ' / Producer: ' + @cProducerName
	
	FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageTypeName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE P SET P.ImagePath = '/assets/img/breweries/3floydslogo.jpg' FROM product.Producer P WHERE P.ProducerName = '3 Floyds'
UPDATE P SET P.ImagePath = '/assets/img/breweries/512logo.jpg' FROM product.Producer P WHERE P.ProducerName = '(512)'
UPDATE P SET P.ImagePath = '/assets/img/breweries/903logo.jpg' FROM product.Producer P WHERE P.ProducerName = '903'
UPDATE P SET P.ImagePath = '/assets/img/breweries/abitalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Abita'
UPDATE P SET P.ImagePath = '/assets/img/breweries/acelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ace'
UPDATE P SET P.ImagePath = '/assets/img/breweries/adelbertslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Adelbert''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alaskanlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alaskan'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alesmithlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'AleSmith'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alltechlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alltech'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alpinelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alpine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alvinnelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Alvinne'
UPDATE P SET P.ImagePath = '/assets/img/breweries/andersonvalleylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Anderson Valley'
UPDATE P SET P.ImagePath = '/assets/img/breweries/anheuserbuschlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Bud'
UPDATE P SET P.ImagePath = '/assets/img/breweries/apogeelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Apogee'
UPDATE P SET P.ImagePath = '/assets/img/breweries/arguslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Argus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/armadillologo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Armadillo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/atwaterlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Atwater'
UPDATE P SET P.ImagePath = '/assets/img/breweries/audacitylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Audacity'
UPDATE P SET P.ImagePath = '/assets/img/breweries/austinbeerworkslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Austin Beerworks'
UPDATE P SET P.ImagePath = '/assets/img/breweries/austineastciderslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Austin Eastciders'
UPDATE P SET P.ImagePath = '/assets/img/breweries/averylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Avery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ayingerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ayinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bacchuslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bacchus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/backcountrylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Backcountry'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bairdlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Baird'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ballastpointlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ballast Point'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bayoutechelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bayou Teche'
UPDATE P SET P.ImagePath = '/assets/img/breweries/beachwoodlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Beachwood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bearrepubliclogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bear Republic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bellslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bell''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bfmlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'BFM'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bigbendlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Big Bend'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bigskylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Big Sky'
UPDATE P SET P.ImagePath = '/assets/img/breweries/birradelborgologo.png' FROM product.Producer P WHERE P.ProducerName = 'Birra del Borgo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bishopciderlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bishop'
UPDATE P SET P.ImagePath = '/assets/img/breweries/blueowllogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Blue Owl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/boulevardlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Boulevard'
UPDATE P SET P.ImagePath = '/assets/img/breweries/braindeadlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Braindead'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brashlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brash'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brasseriedesillylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brasserie de Silly'
UPDATE P SET P.ImagePath = '/assets/img/breweries/breckinridgelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Breckenridge'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bridgeportlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Bridgeport'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brooklynlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brooklyn'
UPDATE P SET P.ImagePath = '/assets/img/breweries/buffalobayoulogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Buffalo Bayou'
UPDATE P SET P.ImagePath = '/assets/img/breweries/buffalobillslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Buffalo Bill''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cedarcreeklogo.png' FROM product.Producer P WHERE P.ProducerName = 'Cedar Creek'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cigarcitylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Cigar City'
UPDATE P SET P.ImagePath = '/assets/img/breweries/clownshoeslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Clown Shoes'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cobralogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Cobra'
UPDATE P SET P.ImagePath = '/assets/img/breweries/collectivebrewingprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Collective Brewing'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coloradologo.png' FROM product.Producer P WHERE P.ProducerName = 'Colorado'
UPDATE P SET P.ImagePath = '/assets/img/breweries/communitylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Community'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coopaleworkslogo.png' FROM product.Producer P WHERE P.ProducerName = 'COOP Ale Works'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coorslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Coors'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coronalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Corona'
UPDATE P SET P.ImagePath = '/assets/img/breweries/crabbieslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Crabbie''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/craftedartisanmeaderylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Crafted Artisan Meadery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/crazymountainlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Crazy Mountain'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deepellumlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Deep Ellum'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deliriumlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Delirium'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deproeflogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'De Proef'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deschuteslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Deschutes'
UPDATE P SET P.ImagePath = '/assets/img/breweries/destihllogo.png' FROM product.Producer P WHERE P.ProducerName = 'Destihl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/detrochlogo.png' FROM product.Producer P WHERE P.ProducerName = 'De Troch'
UPDATE P SET P.ImagePath = '/assets/img/breweries/dogfishheadlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Dogfish Head'
UPDATE P SET P.ImagePath = '/assets/img/breweries/duvellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Duvel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/einbeckerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Einbecker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/elevationlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Elevation'
UPDATE P SET P.ImagePath = '/assets/img/breweries/elgobernadorlogo.png' FROM product.Producer P WHERE P.ProducerName = 'El Gobernador'
UPDATE P SET P.ImagePath = '/assets/img/breweries/epiclogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Epic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/erdingerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Erdinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/eviltwinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Evil Twin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/finnriverlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Finnriver'
UPDATE P SET P.ImagePath = '/assets/img/breweries/firestonewalkerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Firestone Walker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/flyingdoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Flying Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/founderslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Founders'
UPDATE P SET P.ImagePath = '/assets/img/breweries/fourcornerslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Four Corners'
UPDATE P SET P.ImagePath = '/assets/img/breweries/franziskanerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Franziskaner'
UPDATE P SET P.ImagePath = '/assets/img/breweries/fullsaillogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Full Sail'
UPDATE P SET P.ImagePath = '/assets/img/breweries/funkwerkslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Funkwerks'
UPDATE P SET P.ImagePath = '/assets/img/breweries/gavrochelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Gavroche'
UPDATE P SET P.ImagePath = '/assets/img/breweries/gooseislandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Goose Island'
UPDATE P SET P.ImagePath = '/assets/img/breweries/grapevinelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Grapevine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greatdividelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Great Divide'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greenflashlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Green Flash'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greenslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Green''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/guinnesslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Guinness'
UPDATE P SET P.ImagePath = '/assets/img/breweries/halfacrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Half Acre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hebrewlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'He''Brew'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hereticlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Heretic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hitachinonestlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hitachino Nest'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hollowsfentimanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hollows & Fentimans'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hopfusionlogo.png' FROM product.Producer P WHERE P.ProducerName = 'HopFusion'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hopsgrainlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hops & Grain'
UPDATE P SET P.ImagePath = '/assets/img/breweries/humboldtlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Humboldt'
UPDATE P SET P.ImagePath = '/assets/img/breweries/independencelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Independence'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ironmaidenlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Iron Maiden'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ishiilogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ishii'
UPDATE P SET P.ImagePath = '/assets/img/breweries/jesterkinglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Jester King'
UPDATE P SET P.ImagePath = '/assets/img/breweries/jollypumpkinlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Jolly Pumpkin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/juliusechterlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Julius Echter'
UPDATE P SET P.ImagePath = '/assets/img/breweries/karbachlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Karbach'
UPDATE P SET P.ImagePath = '/assets/img/breweries/kasteellogo.png' FROM product.Producer P WHERE P.ProducerName = 'Kasteel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/kostritzerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Kostritzer'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lagunitaslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lagunitas'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lakewoodlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Lakewood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/laughingdoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Laughing Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lazymagnolialogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lazy Magnolia'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lefebvrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lefebvre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leffelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leffe'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lefthandlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Left Hand'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leibingerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leibinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leprechaunlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leprechaun'
UPDATE P SET P.ImagePath = '/assets/img/breweries/les3fourquetslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Les 3 Fourquets'
UPDATE P SET P.ImagePath = '/assets/img/breweries/liefmanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Liefman''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/liveoaklogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Live Oak'
UPDATE P SET P.ImagePath = '/assets/img/breweries/locustlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Locust'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lonepintlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lone Pint'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lonestarlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Lone Star'
UPDATE P SET P.ImagePath = '/assets/img/breweries/magichatlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Magic Hat'
UPDATE P SET P.ImagePath = '/assets/img/breweries/martinhouselogo.png' FROM product.Producer P WHERE P.ProducerName = 'Martin House'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mauilogo.png' FROM product.Producer P WHERE P.ProducerName = 'Maui'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mckenzieslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Mckenzie''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/meridianhivelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Meridian Hive'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mikkellerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Mikkeller'
UPDATE P SET P.ImagePath = '/assets/img/breweries/millerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Miller'
UPDATE P SET P.ImagePath = '/assets/img/breweries/missionlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Mission'
UPDATE P SET P.ImagePath = '/assets/img/breweries/modelologo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Modelo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/monkslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Monk''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/moonlightmeaderylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Moonlight Meadery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/moylanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Moylans'
UPDATE P SET P.ImagePath = '/assets/img/breweries/naughtylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Naughty'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nebraskalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Nebraska'
UPDATE P SET P.ImagePath = '/assets/img/breweries/newbelgiumlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'New Belgium'
UPDATE P SET P.ImagePath = '/assets/img/breweries/newhollandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'New Holland'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ninkasilogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Ninkasi'
UPDATE P SET P.ImagePath = '/assets/img/breweries/noblereylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Noble Rey'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nolabellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'No Label'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nolilogo.png' FROM product.Producer P WHERE P.ProducerName = 'No-Li'
UPDATE P SET P.ImagePath = '/assets/img/breweries/northcoastlogo.png' FROM product.Producer P WHERE P.ProducerName = 'North Coast'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oasislogo.png' FROM product.Producer P WHERE P.ProducerName = 'Oasis'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oddwoodlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Oddwood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/odelllogo.png' FROM product.Producer P WHERE P.ProducerName = 'Odell'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oldspeckledhenlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Old Speckled Hen'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ommeganglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ommegang'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oskarblueslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Oskar Blues'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pantherislandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Panther Island'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pedernaleslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Pedernales'
UPDATE P SET P.ImagePath = '/assets/img/breweries/perenniallogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Perennial'
UPDATE P SET P.ImagePath = '/assets/img/breweries/peticolaslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Peticolas'
UPDATE P SET P.ImagePath = '/assets/img/breweries/petruslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Petrus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pinkuslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Pinkus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/poperingslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Poperings'
UPDATE P SET P.ImagePath = '/assets/img/breweries/prairielogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Prairie'
UPDATE P SET P.ImagePath = '/assets/img/breweries/professorfritzbriemlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Professor Fritz Briem'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rabbitholelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rabbit Hole'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rahrlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rahr'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rangercreeklogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Ranger Creek'
UPDATE P SET P.ImagePath = '/assets/img/breweries/realalelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Real Ale'
UPDATE P SET P.ImagePath = '/assets/img/breweries/redstonelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Redstone'
UPDATE P SET P.ImagePath = '/assets/img/breweries/revolverlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Revolver'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rincelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rince'
UPDATE P SET P.ImagePath = '/assets/img/breweries/robinsonslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Robinsons'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rognesslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rogness'
UPDATE P SET P.ImagePath = '/assets/img/breweries/roguelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Rogue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/russianriverlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Russian River'
UPDATE P SET P.ImagePath = '/assets/img/breweries/saintarnoldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Saint Arnold'
UPDATE P SET P.ImagePath = '/assets/img/breweries/saisonlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Saison'
UPDATE P SET P.ImagePath = '/assets/img/breweries/samadamslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sam Adams'
UPDATE P SET P.ImagePath = '/assets/img/breweries/santafelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Santa Fe'
UPDATE P SET P.ImagePath = '/assets/img/breweries/santanlogo.png' FROM product.Producer P WHERE P.ProducerName = 'SanTan'
UPDATE P SET P.ImagePath = '/assets/img/breweries/savetheworldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Save The World'
UPDATE P SET P.ImagePath = '/assets/img/breweries/schillinglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Schilling '
UPDATE P SET P.ImagePath = '/assets/img/breweries/schlenkerlalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Schlenkerla'
UPDATE P SET P.ImagePath = '/assets/img/breweries/schmaltzlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Schmaltz '
UPDATE P SET P.ImagePath = '/assets/img/breweries/seadoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sea Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shacksburylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shacksbury'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shannonlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shannon'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shinerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shiner'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shipyardlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Shipyard'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sierramadrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Sierra Madre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sierranevadalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Sierra Nevada'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sixpointlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sixpoint'
UPDATE P SET P.ImagePath = '/assets/img/breweries/skalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ska'
UPDATE P SET P.ImagePath = '/assets/img/breweries/smallbrewpublogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Small Brewpub'
UPDATE P SET P.ImagePath = '/assets/img/breweries/smuttynoselogo.png' FROM product.Producer P WHERE P.ProducerName = 'Smuttynose'
UPDATE P SET P.ImagePath = '/assets/img/breweries/southernstarlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Southern Star'
UPDATE P SET P.ImagePath = '/assets/img/breweries/southerntierlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Southern Tier'
UPDATE P SET P.ImagePath = '/assets/img/breweries/squatterslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Squatters'
UPDATE P SET P.ImagePath = '/assets/img/breweries/starnoldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'St Arnold'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stellalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stella'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stevenspointlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stevens Point'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stfeuillienlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'St. Feuillien'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stiegllogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stiegl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stlouisbeerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'St Louis'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stonelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Stone'
UPDATE P SET P.ImagePath = '/assets/img/breweries/strangelandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Strange Land'
UPDATE P SET P.ImagePath = '/assets/img/breweries/summitlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Summit'
UPDATE P SET P.ImagePath = '/assets/img/breweries/surlylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Surly'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sweetwaterlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sweetwater'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tecatelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tecate'
UPDATE P SET P.ImagePath = '/assets/img/breweries/terrapinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Terrapin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/terrapinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Terrapin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texasaleprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Texas Ale Project'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texaskeeperlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Texas Keeper'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texasselectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Texas Select'
UPDATE P SET P.ImagePath = '/assets/img/breweries/thebruerylogo.gif' FROM product.Producer P WHERE P.ProducerName = 'The Bruery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/themanhattanprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'The Manhattan Project'
UPDATE P SET P.ImagePath = '/assets/img/breweries/threenationslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Three Nations'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tietonlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tieton'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tommyknockerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Tommyknocker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/toollogo.png' FROM product.Producer P WHERE P.ProducerName = 'To Ol'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tuppslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tupps'
UPDATE P SET P.ImagePath = '/assets/img/breweries/twistedpinelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Twisted Pine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/twistedxlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Twisted X'
UPDATE P SET P.ImagePath = '/assets/img/breweries/uintalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Uinta'
UPDATE P SET P.ImagePath = '/assets/img/breweries/unibrouelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Unibroue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/unitylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Unity'
UPDATE P SET P.ImagePath = '/assets/img/breweries/uplandlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Upland'
UPDATE P SET P.ImagePath = '/assets/img/breweries/upslopelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Upslope'
UPDATE P SET P.ImagePath = '/assets/img/breweries/urbanfamilylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Urban Family'
UPDATE P SET P.ImagePath = '/assets/img/breweries/urthellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Urthel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/vandiestlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Van Diest'
UPDATE P SET P.ImagePath = '/assets/img/breweries/verzetlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Verzet'
UPDATE P SET P.ImagePath = '/assets/img/breweries/victorylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Victory'
UPDATE P SET P.ImagePath = '/assets/img/breweries/virtuelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Virtue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wanderingaenguslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wandering Aengus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wasatchlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Wasatch'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wellsandyounglogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Well''s & Young'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wexfordlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wexford'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wildacrelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Wild Acre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wildbeerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wild Beer'
UPDATE P SET P.ImagePath = '/assets/img/breweries/woodchucklogo.png' FROM product.Producer P WHERE P.ProducerName = 'Woodchuck'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wyndridgelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wyndridge'
GO

UPDATE D SET D.ImagePath = '/assets/img/breweries/512logo.jpg' FROM product.Distributor D WHERE D.DistributorName = '(512)'
UPDATE D SET D.ImagePath = '/assets/img/breweries/austinbeerworkslogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Austin Beerworks'
UPDATE D SET D.ImagePath = '/assets/img/breweries/buffalobayoulogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Buffalo Bayou'
UPDATE D SET D.ImagePath = '/assets/img/breweries/communitylogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Community'
UPDATE D SET D.ImagePath = '/assets/img/breweries/fourcornerslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Four Corners'
UPDATE D SET D.ImagePath = '/assets/img/breweries/grapevinelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Grapevine'
UPDATE D SET D.ImagePath = '/assets/img/breweries/hopfusionlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'HopFusion'
UPDATE D SET D.ImagePath = '/assets/img/breweries/independencelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Independence'
UPDATE D SET D.ImagePath = '/assets/img/breweries/liveoaklogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Live Oak'
UPDATE D SET D.ImagePath = '/assets/img/breweries/martinhouselogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Martin House'
UPDATE D SET D.ImagePath = '/assets/img/breweries/pantherislandlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Panther Island'
UPDATE D SET D.ImagePath = '/assets/img/breweries/peticolaslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Peticolas'
UPDATE D SET D.ImagePath = '/assets/img/breweries/revolverlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Revolver'
UPDATE D SET D.ImagePath = '/assets/img/breweries/savetheworldlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Save The World'
UPDATE D SET D.ImagePath = '/assets/img/breweries/shannonlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Shannon'
UPDATE D SET D.ImagePath = '/assets/img/breweries/texaskeeperlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Texas Keeper'
UPDATE D SET D.ImagePath = '/assets/img/breweries/threenationslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Three Nations'
UPDATE D SET D.ImagePath = '/assets/img/breweries/twistedxlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Twisted X'
UPDATE D SET D.ImagePath = '/assets/img/breweries/wildacrelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Wild Acre'
UPDATE D SET D.ImagePath = '/assets/img/distributors/andrews.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Andrews'
UPDATE D SET D.ImagePath = '/assets/img/distributors/beardedeel.png' FROM product.Distributor D WHERE D.DistributorName = 'Bearded Eel'
UPDATE D SET D.ImagePath = '/assets/img/distributors/benekeith.png' FROM product.Distributor D WHERE D.DistributorName = 'Ben E Keith'
UPDATE D SET D.ImagePath = '/assets/img/distributors/cuveecoffee.png' FROM product.Distributor D WHERE D.DistributorName = 'Cuvee Coffee'
UPDATE D SET D.ImagePath = '/assets/img/distributors/favoritebrands.png' FROM product.Distributor D WHERE D.DistributorName = 'Favorite Brands'
UPDATE D SET D.ImagePath = '/assets/img/distributors/flood.png' FROM product.Distributor D WHERE D.DistributorName = 'Flood'
UPDATE D SET D.ImagePath = '/assets/img/distributors/fullclip.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Full Clip'
UPDATE D SET D.ImagePath = '/assets/img/distributors/fullclip.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Fullclip'
UPDATE D SET D.ImagePath = '/assets/img/distributors/oakhighlands.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Oak Highlands'
UPDATE D SET D.ImagePath = '/assets/img/distributors/pioneer.png' FROM product.Distributor D WHERE D.DistributorName = 'Pioneer'
UPDATE D SET D.ImagePath = '/assets/img/distributors/sonsofjohn.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Sons of John'
UPDATE D SET D.ImagePath = '/assets/img/distributors/tupps.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Tupps Brewery'
UPDATE D SET D.ImagePath = '/assets/img/distributors/whistlepost.png' FROM product.Distributor D WHERE D.DistributorName = 'Whistle Post'
GO

;
WITH BD AS
	(
	SELECT 
		P.ProductID, 
		P.ProductName,
		P.ProductStyleID,
		(SELECT IMP.Style FROM product.ImportPackage IMP WHERE IMP.ImportProductID = P.ProductID) AS ProductStyleName
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
			AND P.ProductStyleID = 0 
	)

UPDATE P
SET P.ProductStyleID = PS.ProductStyleID
FROM product.Product P
	JOIN BD ON BD.ProductID = P.ProductID
	JOIN product.ProductStyle PS ON PS.ProductStyleName = BD.ProductStyleName
		AND BD.ProductStyleName IS NOT NULL
GO

TRUNCATE TABLE client.ClientProduct
GO

DECLARE @nProductID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		P.ProductID
	FROM product.Product P
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
			AND PT.PackageTypeName = 'Draft'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @nProductID
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO client.ClientProduct
		(ClientID,ProductID,DistributorID,ServingSize,ServingCount,PackagePrice,MenuPrice)
	SELECT
		1 AS ClientID,
		D.ProductID,
		D.DistributorID,
		D.ServingSize,
		D.ServingCount,
		D.PackagePrice,
		D.MenuPrice
	FROM
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY ID.[Size(gal)] ORDER BY ISNULL(ID.[Keg Price], 0) DESC) AS RowIndex,
			P.ProductID,
			ISNULL((SELECT D.DistributorID from product.Distributor D WHERE D.DistributorName = ID.Distributor), 0) AS DistributorID,
			ID.[SpecGlass(oz)] AS ServingSize,
			ISNULL(ID.Servings, 0) AS ServingCount,
			ISNULL(ID.[Keg Price], 0) AS PackagePrice,
			ISNULL(ID.F10, 0) AS MenuPrice
		FROM product.ImportDraft ID
			JOIN product.Product P ON P.ProductName = ID.Name
				AND P.ProductID = @nProductID
		) D
	WHERE D.RowIndex = 1

	FETCH oCursor INTO @nProductID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

DECLARE @nProductID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		P.ProductID
	FROM product.Product P
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
			AND PT.PackageTypeName <> 'Draft'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @nProductID
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO client.ClientProduct
		(ClientID,ProductID,DistributorID,ServingSize,ServingCount,PackagePrice,InStock,MenuPrice)
	SELECT
		1 AS ClientID,
		D.ProductID,
		D.DistributorID,
		D.ServingSize,
		D.ServingCount,
		D.PackagePrice,
		D.InStock,
		D.MenuPrice
	FROM
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY IP.Size ORDER BY ISNULL(IP.[Sale Price], 0) DESC) AS RowIndex,
			P.ProductID,
			ISNULL((SELECT D.DistributorID from product.Distributor D WHERE D.DistributorName = IP.Distributor), 0) AS DistributorID,
			IP.Size AS ServingSize,
			ISNULL(IP.Count, 0) AS ServingCount,
			ISNULL(IP.[Case Price], 0) AS PackagePrice,
			ISNULL(IP.[On Hand], 0) AS InStock,
			ISNULL(IP.[Sale Price], 0) AS MenuPrice
		FROM product.ImportPackage IP
			JOIN product.Product P ON P.ProductName = IP.Name
				AND P.ProductID = @nProductID
		) D
	WHERE D.RowIndex = 1

	FETCH oCursor INTO @nProductID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

;
WITH PMD AS
	(
	SELECT
		PM.PricingModelID,
		CP.ClientProductID,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge) / C.BaseRound, 0) * C.BaseRound AS ModelPrice
	FROM client.PricingModel PM
		JOIN client.Client C ON C.ClientID = PM.ClientID
		JOIN client.ClientProduct CP ON CP.ClientID = PM.ClientID
			AND ISNULL(CP.ServingCount , 0) > 0
	)

UPDATE CP
SET 
	CP.MenuPriceDateTime = getDate(),
	CP.MenuPricePersonID = 1,
	CP.PricingModelID = PMD.PricingModelID
FROM client.ClientProduct CP
	JOIN PMD ON PMD.ClientProductID = CP.ClientProductID
		AND PMD.ModelPrice = CP.MenuPrice
GO
--End file Build File - 06 - Import Data.sql

--Begin build tracking
INSERT INTO core.BuildLog (BuildKey) VALUES ('Build - 1.0 - 2018.12.19 23.03.20')
GO
--End build tracking

