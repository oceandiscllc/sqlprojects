--Begin trigger client.TR_Client
EXEC utility.DropObject 'client.TR_Client'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.Client table
-- ================================================================
CREATE TRIGGER client.TR_Client ON client.Client AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Client',
		I.ClientID,
		(
		SELECT I.*
		FOR XML RAW('Client'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.Client ENABLE TRIGGER TR_Client
GO
--End trigger client.TR_Client

--Begin trigger client.TR_ClientContact
EXEC utility.DropObject 'client.TR_ClientContact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientContact table
-- =======================================================================
CREATE TRIGGER client.TR_ClientContact ON client.ClientContact AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientContact',
		I.ClientContactID,
		(
		SELECT I.*
		FOR XML RAW('ClientContact'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientContact ENABLE TRIGGER TR_ClientContact
GO
--End trigger client.TR_ClientContact

--Begin trigger client.TR_ClientOrder
EXEC utility.DropObject 'client.TR_ClientOrder'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.03.04
-- Description:	A trigger to log changes to the client.ClientOrder table
-- =====================================================================
CREATE TRIGGER client.TR_ClientOrder ON client.ClientOrder AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientOrder',
		I.ClientOrderID,
		(
		SELECT I.*
		FOR XML RAW('ClientOrder'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientOrder ENABLE TRIGGER TR_ClientOrder
GO
--End trigger client.TR_ClientOrder

--Begin trigger client.TR_ClientOrderItem
EXEC utility.DropObject 'client.TR_ClientOrderItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.03.04
-- Description:	A trigger to log changes to the client.ClientOrderItem table
-- =========================================================================
CREATE TRIGGER client.TR_ClientOrderItem ON client.ClientOrderItem AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientOrderItem',
		I.ClientOrderID,
		(
		SELECT I.*
		FOR XML RAW('ClientOrderItem'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientOrderItem ENABLE TRIGGER TR_ClientOrderItem
GO
--End trigger client.TR_ClientOrderItem

--Begin trigger client.TR_ClientPerson
EXEC utility.DropObject 'client.TR_ClientPerson'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientPerson table
-- ======================================================================
CREATE TRIGGER client.TR_ClientPerson ON client.ClientPerson AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientPerson',
		I.ClientPersonID,
		(
		SELECT I.*
		FOR XML RAW('ClientPerson'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientPerson ENABLE TRIGGER TR_ClientPerson
GO
--End trigger client.TR_ClientPerson

--Begin trigger client.TR_ClientProduct
EXEC utility.DropObject 'client.TR_ClientProduct'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientProduct table
-- =======================================================================
CREATE TRIGGER client.TR_ClientProduct ON client.ClientProduct AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientProduct',
		I.ClientProductID,
		(
		SELECT I.*
		FOR XML RAW('ClientProduct'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientProduct ENABLE TRIGGER TR_ClientProduct
GO
--End trigger client.TR_ClientProduct

--Begin trigger client.TR_ClientProductInventory
EXEC utility.DropObject 'client.TR_ClientProductInventory'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ClientProductInventory table
-- =======================================================================
CREATE TRIGGER client.TR_ClientProductInventory ON client.ClientProductInventory AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ClientProductInventory',
		I.ClientProductInventoryID,
		(
		SELECT I.*
		FOR XML RAW('ClientProductInventory'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ClientProductInventory ENABLE TRIGGER TR_ClientProductInventory
GO
--End trigger client.TR_ClientProductInventory

--Begin trigger client.TR_Contact
EXEC utility.DropObject 'client.TR_Contact'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.Contact table
-- =================================================================
CREATE TRIGGER client.TR_Contact ON client.Contact AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Contact',
		I.ContactID,
		(
		SELECT I.*
		FOR XML RAW('Contact'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.Contact ENABLE TRIGGER TR_Contact
GO
--End trigger client.TR_Contact

--Begin trigger client.TR_PricingModel
EXEC utility.DropObject 'client.TR_PricingModel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.PricingModel table
-- ======================================================================
CREATE TRIGGER client.TR_PricingModel ON client.PricingModel AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'PricingModel',
		I.PricingModelID,
		(
		SELECT I.*
		FOR XML RAW('PricingModel'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.PricingModel ENABLE TRIGGER TR_PricingModel
GO
--End trigger client.TR_PricingModel

--Begin trigger client.TR_ProductLocation
EXEC utility.DropObject 'client.TR_ProductLocation'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the client.ProductLocation table
-- ===============================================================================
CREATE TRIGGER client.TR_ProductLocation ON client.ProductLocation AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ProductLocation',
		I.ProductLocationID,
		(
		SELECT I.*
		FOR XML RAW('ProductLocation'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE client.ProductLocation ENABLE TRIGGER TR_ProductLocation
GO
--End trigger client.TR_ProductLocation

--Begin trigger person.TR_Person
EXEC utility.DropObject 'person.TR_Person'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the person.Person table
-- ================================================================
CREATE TRIGGER person.TR_Person ON person.Person AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Person',
		I.PersonID,
		(
		SELECT I.*
		FOR XML RAW('Person'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE person.Person ENABLE TRIGGER TR_Person
GO
--End trigger person.TR_Person

--Begin trigger product.TR_Distributor
EXEC utility.DropObject 'product.TR_Distributor'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Distributor table
-- ======================================================================
CREATE TRIGGER product.TR_Distributor ON product.Distributor AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Distributor',
		I.DistributorID,
		(
		SELECT I.*
		FOR XML RAW('Distributor'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Distributor ENABLE TRIGGER TR_Distributor
GO
--End trigger product.TR_Distributor

--Begin trigger product.TR_Producer
EXEC utility.DropObject 'product.TR_Producer'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Producer table
-- ===================================================================
CREATE TRIGGER product.TR_Producer ON product.Producer AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Producer',
		I.ProducerID,
		(
		SELECT I.*
		FOR XML RAW('Producer'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Producer ENABLE TRIGGER TR_Producer
GO
--End trigger product.TR_Producer

--Begin trigger product.TR_Product
EXEC utility.DropObject 'product.TR_Product'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.Product table
-- ==================================================================
CREATE TRIGGER product.TR_Product ON product.Product AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'Product',
		I.ProductID,
		(
		SELECT I.*
		FOR XML RAW('Product'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.Product ENABLE TRIGGER TR_Product
GO
--End trigger product.TR_Product

--Begin trigger product.TR_ProductProducer
EXEC utility.DropObject 'product.TR_ProductProducer'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.ProductProducer table
-- ==========================================================================
CREATE TRIGGER product.TR_ProductProducer ON product.ProductProducer AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ProductProducer',
		I.ProductProducerID,
		(
		SELECT I.*
		FOR XML RAW('ProductProducer'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.ProductProducer ENABLE TRIGGER TR_ProductProducer
GO
--End trigger product.TR_ProductProducer

--Begin trigger product.TR_ProductStyle
EXEC utility.DropObject 'product.TR_ProductStyle'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:     	Todd Pires
-- Create Date:	2019.01.21
-- Description:	A trigger to log changes to the product.ProductStyle table
-- =======================================================================
CREATE TRIGGER product.TR_ProductStyle ON product.ProductStyle AFTER INSERT, UPDATE
AS
BEGIN
	SET ARITHABORT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		I.UpdatePersonID,
		CASE WHEN EXISTS (SELECT 1 FROM DELETED D) THEN 'update' ELSE 'insert' END,
		'ProductStyle',
		I.ProductStyleID,
		(
		SELECT I.*
		FOR XML RAW('ProductStyle'), ELEMENTS
		)
	FROM INSERTED I

END
GO

ALTER TABLE product.ProductStyle ENABLE TRIGGER TR_ProductStyle
GO
--End trigger product.TR_ProductStyle

EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
EXEC utility.DropObject 'eventlog.LogClientAction'
EXEC utility.DropObject 'eventlog.LogLoginAction'
EXEC utility.DropObject 'person.SetAccountLockedOut'
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO