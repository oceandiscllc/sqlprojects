--Begin table client.ClientProductLocation
TRUNCATE TABLE client.ClientProductLocation
GO

EXEC utility.InsertIdentityValue 'client.ClientProductLocation', 'ClientProductLocationID', 0
GO
--End table client.ClientProductLocation

--Begin table client.ClientProduct
UPDATE CP
SET CP.ServingCount = 24
FROM client.ClientProduct CP
WHERE CP.ServingCount < 3
GO
--End table client.ClientProduct

--Begin table client.MenuCategory
TRUNCATE TABLE client.MenuCategory
GO

EXEC utility.InsertIdentityValue 'client.MenuCategory', 'MenuCategoryID', 0
GO
--End table client.MenuCategory

--Begin table dropdown.OrderStatus
TRUNCATE TABLE dropdown.OrderStatus
GO

EXEC utility.InsertIdentityValue 'dropdown.OrderStatus', 'OrderStatusID', 0
GO

INSERT INTO dropdown.OrderStatus
	(OrderStatusCode, OrderStatusName, DisplayOrder) 
VALUES 
	('New', 'New', 1),
	('Pending', 'Pending', 2),
	('Receiving', 'Receiving', 3),
	('Fulfilled', 'Fulfilled', 4)
GO
--End table dropdown.OrderStatus

--Begin table dropdown.PackageType
UPDATE PT
SET PT.UnitOfIssue = 
	CASE
		WHEN PT.PackageTypeName IN ('Bomber','Bottle','Can')
		THEN 'Case'
		WHEN PT.PackageTypeName = 'Draft'
		THEN 'Keg'
	END
FROM dropdown.PackageType PT
GO
--End table dropdown.PackageType

--Begin table core.SystemSetup
EXEC core.SystemSetupAddUpdate 'SMTPPassword', 'Password for SMTP account to send email', 'Yankee!1'
EXEC core.SystemSetupAddUpdate 'SMTPPort', 'Port for SMTP to send email', '25'
EXEC core.SystemSetupAddUpdate 'SMTPServerName', 'SMTP server for sending email', 'http://mail.makemy.menu'
EXEC core.SystemSetupAddUpdate 'SMTPUsername', 'SMTP username for sending email', 'no-reply@makemy.menu'
EXEC core.SystemSetupAddUpdate 'SMTPUseSSL', 'This determines if the mail server should use SSL to send mail', 'false'
GO
--End table core.SystemSetup

--Begin table product.Product
UPDATE P
SET P.PackageSize = CP.ServingCount
FROM client.ClientProduct CP
	JOIN product.Product P ON P.ProductID = CP.ProductID
	JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
		AND PT.PackageTypeName <> 'Draft'
		AND P.PackageSize IS NULL
GO
--End table product.Product
