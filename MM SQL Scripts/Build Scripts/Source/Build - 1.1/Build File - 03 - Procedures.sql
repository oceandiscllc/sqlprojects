--Begin procedure client.AddClientOrderItemByProductID
EXEC utility.DropObject 'client.AddClientOrderItemByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.18
-- Description:	A stored procedure to add products to a client order
-- =================================================================
CREATE PROCEDURE client.AddClientOrderItemByProductID

@ClientOrderID INT = 0,
@ProductID VARCHAR(MAX) = '',
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO client.ClientOrderItem
		(ClientOrderID, ClientProductID, UpdatePersonID)
	SELECT
		@ClientOrderID,
		CP.ClientProductID,
		@UpdatePersonID
	FROM core.ListToTable(@ProductID, ',') LTT
		JOIN client.ClientProduct CP ON CP.ProductID = CAST(LTT.ListItem AS INT)

END
GO
--End procedure client.AddClientOrderItemByProductID

--Begin procedure client.CheckOrderPointByClientProductID
EXEC utility.DropObject 'client.CheckOrderPointByClientProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.16
-- Description:	A stored procedure to determine if a ClientProduct item is at or below it's order point
-- ====================================================================================================
CREATE PROCEDURE client.CheckOrderPointByClientProductID

@ClientProductID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CASE
			WHEN CP.OrderPoint = 0 
				OR CP.OrderPoint <= (SELECT SUM(CPI.Quantity) FROM client.ClientProductInventory CPI WHERE CPI.ClientProductID = CP.ClientProductID)
				OR EXISTS 
					(
					SELECT 1
					FROM client.ClientOrderItem COI
						JOIN client.ClientOrder CO ON CO.ClientOrderID = COI.ClientOrderID
						JOIN dropdown.OrderStatus OS ON OS.OrderStatusID = CO.OrderStatusID
							AND CO.DistributorID = CP.DistributorID
							AND CP.ClientID = CO.ClientID
							AND CP.ClientProductID = COI.ClientProductID
							AND OS.OrderStatusCode IN ('New', 'Pending')
					)
			THEN 0
			ELSE 1
		END AS IsAtOrderPoint
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID

END
GO
--End procedure client.CheckOrderPointByClientProductID

--Begin procedure client.CloneClientOrderByClientOrderID
EXEC utility.DropObject 'client.CloneClientOrderByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.04
-- Description:	A stored procedure to return data from the client.ClientProduct table
-- ==================================================================================
CREATE PROCEDURE client.CloneClientOrderByClientOrderID

@ClientOrderID INT,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientOrderID INT
	DECLARE @tOutput TABLE (ClientOrderID INT PRIMARY KEY)

	INSERT INTO client.ClientOrder
		(ClientID, DistributorID, OrderStatusID, UpdatePersonID)
	OUTPUT INSERTED.ClientOrderID INTO @tOutput
	SELECT
		CO.ClientID,
		CO.DistributorID,
		(SELECT OS.OrderStatusID FROM dropdown.OrderStatus OS WHERE OS.OrderStatusCode = 'New'),
		@UpdatePersonID
	FROM client.ClientOrder CO
	WHERE CO.ClientOrderID = @ClientOrderID

	SELECT @nClientOrderID = O.ClientOrderID FROM @tOutput O

	INSERT INTO client.ClientOrderItem
		(ClientOrderID, ClientProductID, Quantity, UpdatePersonID)
	SELECT
		@nClientOrderID,
		COI.ClientProductID,
		COI.Quantity,
		@UpdatePersonID
	FROM client.ClientOrderItem COI
	WHERE COI.ClientOrderID = @ClientOrderID

END
GO
--End procedure client.CloneClientOrderByClientOrderID

--Begin procedure client.CloseClientOrderByClientOrderID
EXEC utility.DropObject 'client.CloseClientOrderByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- Author:			Todd Pires
-- Create date:	2019.03.14
-- Description:	A stored procedure to close out an order
-- =====================================================
CREATE PROCEDURE client.CloseClientOrderByClientOrderID

@ClientOrderID INT,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	EXEC client.SetOrderStatusByClientOrderID @ClientOrderID, 'Fulfilled', @UpdatePersonID

	DECLARE @nClientID INT
	DECLARE @nClientProductID INT
	DECLARE @nQuantityAdded INT
	DECLARE @nQuantityInStock INT
	DECLARE @nQuantityReceived INT
	DECLARE @nProductLocationID INT
	DECLARE @nSingleItemCapacity INT

	DECLARE oCursor1 CURSOR LOCAL FAST_FORWARD FOR
		SELECT
			CO.ClientID,
			COI.ClientProductID,

			CASE
				WHEN PT.PackageTypeName = 'Draft'
				THEN COI.Quantity
				ELSE COI.Quantity * P.PackageSize
			END AS Quantity

		FROM client.ClientOrderItem COI
			JOIN client.ClientOrder CO ON CO.ClientOrderID = COI.ClientOrderID
			JOIN client.ClientProduct CP ON CP.ClientProductID = COI.ClientProductID
			JOIN product.Product P ON P.ProductID = CP.ProductID
			JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
				AND CO.ClientOrderID = @ClientOrderID
	
	OPEN oCursor1
	FETCH oCursor1 INTO @nClientID, @nClientProductID, @nQuantityReceived
	WHILE @@fetch_status = 0
		BEGIN

		DECLARE oCursor2 CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				PL.ProductLocationID,
				PL.SingleItemCapacity
			FROM client.ProductLocation PL
			WHERE PL.ProductLocationID > 0
				AND PL.ClientID = @nClientID
			ORDER BY PL.FillSequence
	
		OPEN oCursor2
		FETCH oCursor2 INTO @nProductLocationID, @nSingleItemCapacity
		WHILE @@fetch_status = 0
			BEGIN

			IF @nQuantityReceived > 0
				BEGIN

				IF NOT EXISTS (SELECT 1 FROM client.ClientProductInventory CPI WHERE CPI.ClientProductID = @nClientProductID AND CPI.ProductLocationID = @nProductLocationID)
					BEGIN

					SET @nQuantityAdded = IIF(@nQuantityReceived > @nSingleItemCapacity, @nSingleItemCapacity, @nQuantityReceived)
					SET @nQuantityReceived -= @nQuantityAdded

					INSERT INTO client.ClientProductInventory 
						(ClientProductID, ProductLocationID, Quantity, UpdatePersonID, UpdateDateTime)
					VALUES
						(@nClientProductID, @nProductLocationID, @nQuantityAdded, @UpdatePersonID, getDate())

					END
				ELSE
					BEGIN

					SELECT @nQuantityAdded = IIF(@nSingleItemCapacity - CPI.Quantity > @nQuantityReceived, @nQuantityReceived, @nSingleItemCapacity - CPI.Quantity)
					FROM client.ClientProductInventory CPI 
					WHERE CPI.ClientProductID = @nClientProductID 
						AND CPI.ProductLocationID = @nProductLocationID

					SET @nQuantityReceived -= @nQuantityAdded

					UPDATE CPI
					SET
						CPI.Quantity = CPI.Quantity + @nQuantityAdded, 
						CPI.UpdatePersonID = @UpdatePersonID,  
						CPI.UpdateDateTime = getDate()
					FROM client.ClientProductInventory CPI
					WHERE CPI.ClientProductID = @nClientProductID
						AND CPI.ProductLocationID = @nProductLocationID

					END
				--ENDIF

				END
			--ENDIF

			FETCH oCursor2 INTO @nProductLocationID, @nSingleItemCapacity
	
			END
		--END WHILE

		CLOSE oCursor2
		DEALLOCATE oCursor2

		FETCH oCursor1 INTO @nClientID, @nClientProductID, @nQuantityReceived
	
		END
	--END WHILE

	CLOSE oCursor1
	DEALLOCATE oCursor1

END
GO
--End procedure client.CloseClientOrderByClientOrderID

--Begin procedure client.DeleteClientOrderByClientOrderID
EXEC utility.DropObject 'client.DeleteClientOrderByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.04
-- Description:	A stored procedure to delete data from the client.ClientOrder and client.ClientOrderItem tables
-- ============================================================================================================
CREATE PROCEDURE client.DeleteClientOrderByClientOrderID

@ClientOrderID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CO
	FROM client.ClientOrder CO
	WHERE CO.ClientOrderID = @ClientOrderID

	DELETE COI
	FROM client.ClientOrderItem COI
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientOrder CO
		WHERE CO.ClientOrderID = COI.ClientOrderID
		)

END
GO
--End procedure client.DeleteClientOrderByClientOrderID

--Begin procedure client.DeleteMenuCategoryByMenuCategoryID
EXEC utility.DropObject 'client.DeleteMenuCategoryByMenuCategoryID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.22
-- Description:	A stored procedure to delete data from the client.MenuCategory table
-- =================================================================================
CREATE PROCEDURE client.DeleteMenuCategoryByMenuCategoryID

@MenuCategoryID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'MenuCategory',
		@MenuCategoryID,
		(
		SELECT MC.*
		FOR XML RAW('MenuCategory'), ELEMENTS
		)
	FROM client.MenuCategory MC
	WHERE MC.MenuCategoryID = @MenuCategoryID

	DELETE MC
	FROM client.MenuCategory MC
	WHERE MC.MenuCategoryID = @MenuCategoryID

END
GO
--End procedure client.DeleteMenuCategoryByMenuCategoryID

--Begin procedure client.DeletePricingModelByPricingModelID
EXEC utility.DropObject 'client.DeletePricingModelByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.08
-- Description:	A stored procedure to delete data from the client.PricingModel table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- =================================================================================
CREATE PROCEDURE client.DeletePricingModelByPricingModelID

@PricingModelID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'PricingModel',
		@PricingModelID,
		(
		SELECT PM.*
		FOR XML RAW('PricingModel'), ELEMENTS
		)
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	DELETE PM
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	UPDATE CP
	SET 
		CP.PricingModelID = 0,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.PricingModel PM
		WHERE PM.PricingModelID = CP.PricingModelID
		)

END
GO
--End procedure client.DeletePricingModelByPricingModelID

--Begin procedure client.DeleteProductLocationByProductLocationID
EXEC utility.DropObject 'client.DeleteProductLocationByProductLocationID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.02
-- Description:	A stored procedure to delete data from the client.ProductLocation table
-- ====================================================================================
CREATE PROCEDURE client.DeleteProductLocationByProductLocationID

@ProductLocationID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'ProductLocation',
		@ProductLocationID,
		(
		SELECT PL.*
		FOR XML RAW('ProductLocation'), ELEMENTS
		)
	FROM client.ProductLocation PL
	WHERE PL.ProductLocationID = @ProductLocationID

	DELETE PL
	FROM client.ProductLocation PL
	WHERE PL.ProductLocationID = @ProductLocationID

END
GO
--End procedure client.DeleteProductLocationByProductLocationID

--Begin procedure client.GetClientOrderByClientOrderID
EXEC utility.DropObject 'client.GetClientOrderByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.04
-- Description:	A stored procedure to return data from the client.ClientProduct table
-- ==================================================================================
CREATE PROCEDURE client.GetClientOrderByClientOrderID

@ClientOrderID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		core.FormatDate(getDate()) AS OrderDateFormatted,
		CO.ClientOrderID,
		CO.EmailAddressCc,

		CASE
			WHEN OS.OrderStatusCode = 'New'
			THEN (SELECT STUFF((SELECT '; ' + ISNULL(C.EmailAddress1, C.EmailAddress2) FROM client.Contact C JOIN client.ClientContact CC ON CC.ContactID = C.ContactID AND CC.ClientID = CO.ClientID AND C.EntityTypeCode = 'Distributor' AND C.EntityID = CO.DistributorID ORDER BY 1 FOR XML PATH('')), 1, 2, ''))
			ELSE CO.EmailAddressTo
		END AS EmailAddressTo,

		CO.Notes,
		D.DistributorID,
		D.DistributorName,
		D.ImagePath,
		OS.OrderStatusCode,
		OS.OrderStatusName
	FROM client.ClientOrder CO
		JOIN product.Distributor D ON D.DistributorID = CO.DistributorID
		JOIN dropdown.OrderStatus OS ON OS.OrderStatusID = CO.OrderStatusID
			AND CO.ClientOrderID = @ClientOrderID

END
GO
--End procedure client.GetClientOrderByClientOrderID

--Begin procedure client.GetClientProductDataByUniversalProductCode
EXEC utility.DropObject 'client.GetClientProductDataByUniversalProductCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.04
-- Description:	A stored procedure to return data from the client.ClientProduct table
-- ==================================================================================
CREATE PROCEDURE client.GetClientProductDataByUniversalProductCode

@UniversalProductCode VARCHAR(25)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CP.ClientProductID,
		P.ProductName,
		product.GetProducerNameByProductID(CP.ProductID) AS ProducerNameFormatted
	FROM client.ClientProduct CP
		JOIN product.Product P ON P.ProductID = CP.ProductID
			AND P.UniversalProductCode = @UniversalProductCode

END
GO
--End procedure client.GetClientProductDataByUniversalProductCode

--Begin procedure client.GetClientProductInventoryDashboardData
EXEC utility.DropObject 'client.GetClientProductInventoryDashboardData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.08
-- Description:	A stored procedure to return data from the client.ClientProductInventory table
-- ===========================================================================================
CREATE PROCEDURE client.GetClientProductInventoryDashboardData

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		CP.ClientProductID,
		product.GetProducerNameByProductID(P.ProductID) AS ProducerNameFormatted,
		P.ProductName,
		PT.PackageTypeName,
		'<div class="progress location' + CAST(PL.ProductLocationID AS VARCHAR(5)) + '"><div class="progress-bar ' 
			+ CASE 
					WHEN CAST(CAST(CPI.Quantity AS NUMERIC(18,0)) / CAST(PL.SingleItemCapacity AS NUMERIC(18,0)) * 100 AS NUMERIC(18)) >= 67 
					THEN 'bg-success' 
					WHEN CAST(CAST(CPI.Quantity AS NUMERIC(18,0)) / CAST(PL.SingleItemCapacity AS NUMERIC(18,0)) * 100 AS NUMERIC(18)) >= 33 
					THEN 'bg-warning' 
					ELSE 'bg-danger' 
				END
			+ '" role="progressbar" aria-valuenow="' 
			+ CAST(CPI.Quantity AS VARCHAR(10)) 
			+ '" aria-valuemin="0" aria-valuemax="' 
			+ CAST(PL.SingleItemCapacity AS VARCHAR(10)) 
			+ '" style="width:' 
			+ CAST(CAST(CAST(CPI.Quantity AS NUMERIC(18,0)) / CAST(PL.SingleItemCapacity AS NUMERIC(18,0)) * 100 AS NUMERIC(18)) AS VARCHAR(4)) 
			+ '%">' + PL.ProductLocationName + ' (' + CAST(CPI.Quantity AS VARCHAR(10))  + ')' + '</div></div>' AS ProgressBar	
	FROM client.ClientProductInventory CPI
		JOIN client.ClientProduct CP ON CP.ClientProductID = CPI.ClientProductID
		JOIN product.Product P ON P.ProductID = CP.ProductID
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
		JOIN client.ProductLocation PL ON PL.ProductLocationID = CPI.ProductLocationID
			AND CP.ClientID = @ClientID
	ORDER BY 2, 3, 1

END
GO
--End procedure client.GetClientProductInventoryDashboardData

--Begin procedure client.GetClientProductLocationData
EXEC utility.DropObject 'client.GetClientProductLocationData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.10
-- Description:	A stored procedure to return data from the client.ClientProductLocation table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ==========================================================================================
CREATE PROCEDURE client.GetClientProductLocationData

@ClientID INT,
@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ClientProductLocationID,
		T.ClientProductLocationName,
		T.DisplayOrder,
		T.IsActive
	FROM client.ClientProductLocation T
	WHERE (T.ClientProductLocationID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ClientProductLocationName, T.ClientProductLocationID

END
GO
--End procedure client.GetClientProductLocationData

--Begin procedure client.GetContactByContactID
EXEC utility.DropObject 'client.GetContactByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create date:	2019.01.22
-- Description:	A stored procedure to delete data from the client.Contact table
-- ============================================================================
CREATE PROCEDURE client.GetContactByContactID

@ContactID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Contact
	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.ContactID,
		C.EmailAddress1,
		C.EmailAddress2,
		C.EntityID,
		C.EntityTypeCode,

		CASE
			WHEN C.EntityTypeCode = 'Distributor'
			THEN (SELECT D.DistributorName FROM product.Distributor D WHERE D.DistributorID = C.EntityID)
			ELSE (SELECT P.ProducerName FROM product.Producer P WHERE P.ProducerID = C.EntityID)
		END AS ContactCompanyName,

		C.FirstName,
		C.ISOCountryCode3,
		C.ISORegionCode2,
		C.JobTitle,
		C.LastName,
		C.Municipality,
		C.Notes,
		C.PhoneNumber,
		C.PostalCode
	FROM client.Contact C
	WHERE C.ContactID = @ContactID

END
GO
--End procedure client.GetContactByContactID

--Begin procedure client.GetMenuCategoryDataByClientID
EXEC utility.DropObject 'client.GetMenuCategoryDataByClientID'
GO
--End procedure client.GetMenuCategoryDataByClientID

--Begin procedure client.GetProductLocationDataByClientID
EXEC utility.DropObject 'client.GetProductLocationDataByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.02
-- Description:	A stored procedure to return data from the client.ProductLocation table
-- ====================================================================================
CREATE PROCEDURE client.GetProductLocationDataByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		'ProductLocation' + CAST(PL.ProductLocationID AS VARCHAR(5)) AS ProductLocationIDFormatted,
		PL.ProductLocationID,
		PL.ProductLocationName
	FROM client.ProductLocation PL
	WHERE PL.ClientID = @ClientID
	ORDER BY PL.DisplayOrder, PL.ProductLocationName, PL.ProductLocationID

END
GO
--End procedure client.GetProductLocationDataByClientID

--Begin procedure client.SaveClientOrderItem
EXEC utility.DropObject 'client.SaveClientOrderItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.16
-- Description:	A stored procedure to add a clientorderitem to a clientorder
-- =========================================================================
CREATE PROCEDURE client.SaveClientOrderItem

@ClientProductID INT = 0,
@Quantity INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientOrderID INT
	DECLARE @tOutput TABLE (ClientOrderID INT NOT NULL PRIMARY KEY)

	SELECT @nClientOrderID = CO.ClientOrderID
	FROM client.ClientOrder CO
		JOIN client.ClientProduct CP ON CP.ClientID = CO.ClientID
		JOIN dropdown.OrderStatus OS ON OS.OrderStatusID = CO.OrderStatusID
			AND CO.DistributorID = CP.DistributorID
			AND CP.ClientProductID = @ClientProductID
			AND OS.OrderStatusCode = 'New'

	IF @nClientOrderID IS NULL
		BEGIN

		INSERT INTO client.ClientOrder
			(ClientID, DistributorID, OrderStatusID, UpdatePersonID)
		OUTPUT INSERTED.ClientOrderID INTO @tOutput
		SELECT
			CP.ClientID,
			CP.DistributorID,
			(SELECT OS.OrderStatusID FROM dropdown.OrderStatus OS WHERE OS.OrderStatusCode = 'New'),
			@UpdatePersonID
		FROM client.ClientProduct CP
		WHERE CP.ClientProductID = @ClientProductID

		SELECT @nClientOrderID = O.ClientOrderID FROM @tOutput O

		END
	--ENDIF

	IF NOT EXISTS (SELECT 1 FROM client.ClientOrderItem COI WHERE COI.ClientOrderID = @nClientOrderID AND COI.ClientProductID = @ClientProductID)
		BEGIN

		INSERT INTO client.ClientOrderItem
			(ClientOrderID, ClientProductID, Quantity, UpdatePersonID)
		VALUES
			(
			@nClientOrderID,
			@ClientProductID,
			@Quantity,
			@UpdatePersonID
			)

		END
	ELSE
		BEGIN

		UPDATE COI
		SET 
			COI.Quantity = @Quantity,
			COI.UpdatePersonID = @UpdatePersonID,
			COI.UpdateDateTime = getDate()
		FROM client.ClientOrderItem COI
		WHERE COI.ClientOrderID = @nClientOrderID 
			AND COI.ClientProductID = @ClientProductID

		END
	--ENDIF

END
GO
--End procedure client.SaveClientOrderItem
	
--Begin procedure client.SetIsMenuItem
EXEC utility.DropObject 'client.SetIsMenuItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.29
-- Description:	A stored procedure to update the IsMenuItem bit in the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ==============================================================================================
CREATE PROCEDURE client.SetIsMenuItem

@ClientProductID INT = 0,
@IsMenuItem BIT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.IsMenuItem = @IsMenuItem,
		CP.MenuPriceDateTime = CASE WHEN @IsMenuItem = 1 THEN getDate() ELSE NULL END,
		CP.MenuPricePersonID = CASE WHEN @IsMenuItem = 1 THEN @UpdatePersonID ELSE 0 END,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID 

END
GO
--End procedure client.SetIsMenuItem

--Begin procedure client.SetMenuPrice
EXEC utility.DropObject 'client.SetMenuPrice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.SetMenuPrice

@ClientProductID INT = 0,
@Upcharge NUMERIC(18,2) = 0,
@PricingModelID INT = 0,
@MenuPrice NUMERIC(18,2) = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @PricingModelID != 0
		BEGIN

		SELECT @MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + @Upcharge) / C.BaseRound, 0) * C.BaseRound
		FROM client.ClientProduct CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
				AND CP.ClientProductID = @ClientProductID
				AND PM.PricingModelID = @PricingModelID

		END
	--ENDIF

	UPDATE CP
	SET 
		CP.MenuPrice = @MenuPrice,
		CP.PricingModelID = @PricingModelID,
		CP.Upcharge = @Upcharge,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID

	SELECT FORMAT(@MenuPrice, 'C', 'en-us') AS MenuPrice

END
GO
--End procedure client.SetMenuPrice

--Begin procedure client.SetOrderStatusByClientOrderID
EXEC utility.DropObject 'client.SetOrderStatusByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create date:	2019.03.14
-- Description:	A stored procedure to update the status of an order
-- ================================================================
CREATE PROCEDURE client.SetOrderStatusByClientOrderID

@ClientOrderID INT = 0,
@OrderStatusCode VARCHAR(50),
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CO
	SET 
		CO.OrderStatusID = ISNULL((SELECT OS.OrderStatusID FROM dropdown.OrderStatus OS WHERE OS.OrderStatusCode = @OrderStatusCode), 0),
		CO.UpdatePersonID = @UpdatePersonID,
		CO.UpdateDateTime = getDate()
	FROM client.ClientOrder CO
	WHERE CO.ClientOrderID = @ClientOrderID 

END
GO
--End procedure client.SetOrderStatusByClientOrderID

--Begin procedure client.SubmitClientOrderByClientOrderID
EXEC utility.DropObject 'client.SubmitClientOrderByClientOrderID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:			Todd Pires
-- Create date:	2019.03.04
-- Description:	A stored procedure to submit an order
-- ==================================================
CREATE PROCEDURE client.SubmitClientOrderByClientOrderID

@ClientOrderID INT,
@EmailAddressTo VARCHAR(MAX),
@EmailAddressCc VARCHAR(MAX),
@Notes VARCHAR(MAX),
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE COI
	FROM client.ClientOrderItem COI
	WHERE COI.ClientOrderID = @ClientOrderID
		AND COI.Quantity = 0

	UPDATE CO
	SET 
		CO.EmailAddressCc = @EmailAddressCc,
		CO.EmailAddressTo = @EmailAddressTo,
		CO.Notes = @Notes,
		CO.OrderStatusID = (SELECT OS.OrderStatusID FROM dropdown.OrderStatus OS WHERE OS.OrderStatusCode = 'Pending'),
		CO.UpdateDateTime = getDate(),
		CO.UpdatePersonID = @UpdatePersonID
	FROM client.ClientOrder CO
	WHERE CO.ClientOrderID = @ClientOrderID

	SELECT
		C.ClientName,
		C.EmailAddress,
		C.PhoneNumber,
		core.GetSystemSetupValueBySystemSetupKey('SMTPPassword', '') AS SMTPPassword,
		core.GetSystemSetupValueBySystemSetupKey('SMTPPort', '') AS SMTPPort,
		core.GetSystemSetupValueBySystemSetupKey('SMTPServerName', '') AS SMTPServerName,
		core.GetSystemSetupValueBySystemSetupKey('SMTPUsername', '') AS SMTPUsername,
		core.GetSystemSetupValueBySystemSetupKey('SMTPUseSSL', '') AS SMTPUseSSL
	FROM client.ClientOrder CO
		JOIN client.Client C ON C.CLientID = CO.ClientID
			AND CO.ClientOrderID = @ClientOrderID

	SELECT
		CAST(COI.Quantity AS VARCHAR(5)) + ' '  + PT.UnitOfIssue AS Quantity,
		product.GetProducerNameByProductID(CP.ProductID) AS ProducerNameFormatted,
		P.ProductName + ' ' + PT.PackageTypeName + CASE WHEN P.PackageSize IS NOT NULL THEN ' (' + P.PackageSize + ')' ELSE '' END AS ProductNameFormatted
	FROM client.ClientOrderItem COI
		JOIN client.ClientProduct CP ON CP.ClientProductID = COI.ClientProductID
		JOIN product.Product P ON P.ProductID = CP.ProductID
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
			AND COI.ClientOrderID = @ClientOrderID
	ORDER BY PT.UnitOfIssue, ProducerNameFormatted, ProductNameFormatted

END
GO
--End procedure client.SubmitClientOrderByClientOrderID

--Begin procedure client.UpdateMenuPricesByClientID
EXEC utility.DropObject 'client.UpdateMenuPricesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByClientID

@ClientID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ClientID = @ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = CP.PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByClientID

--Begin procedure client.UpdateMenuPricesByPricingModelID
EXEC utility.DropObject 'client.UpdateMenuPricesByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByPricingModelID

@PricingModelID INT = 0,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound,
		CP.UpdatePersonID = @UpdatePersonID
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = @PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByPricingModelID

--Begin procedure dropdown.GetOrderStatusData
EXEC utility.DropObject 'dropdown.GetOrderStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:			Todd Pires
-- Create date:	2019.02.16
-- Description:	A stored procedure to return data from the dropdown.OrderStatus table
-- ==================================================================================
CREATE PROCEDURE dropdown.GetOrderStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.OrderStatusID,
		T.OrderStatusCode,
		T.OrderStatusName
	FROM dropdown.OrderStatus T
	WHERE (T.OrderStatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.OrderStatusName, T.OrderStatusID

END
GO
--End procedure dropdown.GetOrderStatusData

--Begin procedure person.DeletePersonByPersonID
EXEC utility.DropObject 'person.DeletePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to delete data from the person.Person table based on a PersonID
--
-- Author:			Todd Pires
-- Create date:	2019.01.21
-- Description:	Added the @UpdatePersonID parameter for event logging
-- ===============================================================================================
CREATE PROCEDURE person.DeletePersonByPersonID

@PersonID INT,
@ClientID INT,
@UpdatePersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'ClientPerson',
		CP.PersonID,
		(
		SELECT CP.*
		FOR XML RAW('ClientPerson'), ELEMENTS
		)
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	DELETE CP
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	INSERT INTO eventlog.EventLog
		(PersonID, EventCode, EntityTypeCode, EntityID, EventData)
	SELECT
		@UpdatePersonID,
		'delete',
		'Person',
		P.PersonID,
		(
		SELECT P.*
		FOR XML RAW('Person'), ELEMENTS
		)
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

	DELETE P
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

END
GO
--End procedure person.DeletePersonByPersonID

--Begin procedure product.GetMenuDataByClientID
EXEC utility.DropObject 'product.GetMenuDataByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create date:	2018.12.07
-- Description:	A stored procedure to return menu data
-- ===================================================
CREATE PROCEDURE product.GetMenuDataByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.ClientName,
		C.ImagePath,
		C.ISOCountryCode3,
		C.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(C.ISORegionCode2) AS RegionName,
		C.Municipality,
		C.PhoneNumber,
		C.PostalCode,
		C.WebsiteURL
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	--ClientProduct
	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN client.MenuCategory MC ON MC.MenuCategoryID = CP.MenuCategoryID
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Draft'
			AND PT2.ProductTypeName <> 'Wine'
	ORDER BY MC.DisplayOrder, 4, P1.ProductName, P1.ProductID

	--ClientProduct
	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN client.MenuCategory MC ON MC.MenuCategoryID = CP.MenuCategoryID
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName IN ('Bottle', 'Can')
			AND PT2.ProductTypeName = 'Beer'
	ORDER BY 4, P1.ProductName, P1.ProductID

	--ClientProduct
	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN client.MenuCategory MC ON MC.MenuCategoryID = CP.MenuCategoryID
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Bomber'
	ORDER BY 4, P1.ProductName, P1.ProductID

	--ClientProduct
	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN client.MenuCategory MC ON MC.MenuCategoryID = CP.MenuCategoryID
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT2.ProductTypeName = 'Wine'
	ORDER BY 4, P1.ProductName, P1.ProductID

	--ClientProduct
	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN client.MenuCategory MC ON MC.MenuCategoryID = CP.MenuCategoryID
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName NOT IN ('Bomber', 'Draft')
			AND PT2.ProductTypeName = 'Cider'
	ORDER BY 4, PT1.DisplayOrder, P1.ProductName, P1.ProductID

END
GO
--End procedure product.GetMenuDataByClientID

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.06
-- Description:	A stored procedure to return data from the product.Product table based on a ProductID
-- ==================================================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientProductID INT = ISNULL((SELECT CP.ClientProductID FROM client.ClientProduct CP WHERE CP.ClientID = @ClientID AND CP.ProductID = @ProductID), 0)

	--Product
	SELECT
		P.ABV,
		P.IBU,
		P.PackageSize,
		P.ProductID,
		P.ProductName,
		P.UniversalProductCode,
		PKT.PackageTypeID,
		PKT.PackageTypeName,
		PRT.ProductTypeID,
		PRT.ProductTypeName,
		PS.ProductStyleDescription,
		PS.ProductStyleID,
		PS.ProductStyleName
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P.ProductStyleID
		JOIN dropdown.ProductType	PRT ON PRT.ProductTypeID = P.ProductTypeID
			AND P.ProductID = @ProductID

	--ClientProduct
	SELECT
		CP.ClientProductID,
		CP.Description,
		CP.IconHTML,
		CP.ImagePath,
		CP.IsFeatured,
		CP.IsMenuItem,
		CP.MenuCategoryID,
		CP.OrderPoint,
		CP.ServingSize,
		CP.ServingCount,
		CP.PackagePrice,
		CP.MenuPrice,
		CP.UpCharge,
		core.FormatDateTime(CP.MenuPriceDateTime) AS MenuPriceDateTimeFormatted,
		person.FormatPersonNameByPersonID(CP.MenuPricePersonID, 'LastFirst') AS PersonNameFormatted,
		D.DistributorID,
		D.DistributorName
	FROM client.ClientProduct CP
		JOIN product.Distributor D ON D.DistributorID = CP.DistributorID
			AND CP.ClientProductID = @nClientProductID

	--ClientProductInventory
	SELECT
		CPI.Quantity,
		core.FormatDateTime(CPI.UpdateDateTime) AS UpdateDateTimeFormatted,
		PL.ProductLocationID,
		PL.ProductLocationName
	FROM client.ClientProductInventory CPI
		JOIN client.ProductLocation PL ON PL.ProductLocationID = CPI.ProductLocationID
			AND CPI.ClientProductID = @nClientProductID

	--MenuCategory
	SELECT
		MC.MenuCategoryID,
		MC.MenuCategoryName,
		MC.DisplayOrder
	FROM client.MenuCategory MC
	WHERE MC.ClientID = @ClientID
	ORDER BY MC.DisplayOrder, MC.MenuCategoryName, MC.MenuCategoryID

	--PricingModel
	EXEC client.GetPricingModel @nClientProductID

	--ProductProducer
	SELECT
		C.CountryName,
		P.ImagePath,
		P.ISOCountryCode3,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM product.ProductProducer PP
		JOIN product.Producer P ON P.ProducerID = PP.ProducerID
		JOIN dropdown.Country C ON C.ISOCountryCode3 = P.ISOCountryCode3
			AND PP.ProductID = @ProductID
	ORDER BY P.ProducerName, P.ProducerID

END
GO
--End procedure product.GetProductByProductID