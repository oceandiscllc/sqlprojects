
--Begin function client.FormatContactNameByContactID
EXEC utility.DropObject 'client.FormatContactNameByContactID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:			Todd Pires
-- Create Date:	2019.01.22
-- Description:	A function to return a Contact name in a specified format
-- ======================================================================

CREATE FUNCTION client.FormatContactNameByContactID
(
@ContactID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(C.FirstName, ''),
		@cLastName = ISNULL(C.LastName, '')
	FROM client.Contact C
	WHERE C.ContactID = @ContactID
	
	IF @Format LIKE '%FirstLast%'
		SET @cRetVal = @cFirstName + ' ' + @cLastName
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function client.FormatContactNameByContactID

--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 101)

END
GO
--End function core.FormatDate