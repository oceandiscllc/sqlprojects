--Begin table client.ClientContact
DECLARE @TableName VARCHAR(250) = 'client.ClientContact'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientContact
	(
	ClientContactID INT IDENTITY(1, 1) NOT NULL,
	ContactID INT,
	ClientID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ContactID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientContact', 'ClientID,ContactID'
GO
--End table client.ClientContact

--Begin table client.Contact
DECLARE @TableName VARCHAR(250) = 'client.Contact'

EXEC utility.DropObject @TableName

CREATE TABLE client.Contact
	(
	ContactID INT IDENTITY(1, 1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	JobTitle VARCHAR(100),
	EmailAddress1 VARCHAR(320),
	EmailAddress2 VARCHAR(320),
	PhoneNumber VARCHAR(50),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	PostalCode VARCHAR(10),
	ISOCountryCode3 CHAR(3),
	Notes VARCHAR(MAX),
	UpdatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ContactID'
EXEC utility.SetIndexClustered @TableName, 'IX_Contact', 'LastName,FirstName'
GO
--End table client.Contact

--Begin table client.ClientOrder
DECLARE @TableName VARCHAR(250) = 'client.ClientOrder'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientOrder
	(
	ClientOrderID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	DistributorID INT,
	OrderStatusID INT,
	Notes VARCHAR(MAX),
	EmailAddressTo VARCHAR(MAX),
	EmailAddressCC VARCHAR(MAX),
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DistributorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'OrderStatusID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientOrderID'
GO
--End table client.ClientOrder

--Begin table client.ClientOrderItem
DECLARE @TableName VARCHAR(250) = 'client.ClientOrderItem'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientOrderItem
	(
	ClientOrderItemID INT IDENTITY(1, 1) NOT NULL,
	ClientOrderID INT,
	ClientProductID INT,
	Quantity INT,
	IsFulfilled BIT,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientOrderID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFulfilled', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientOrderItemID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientOrderItem', 'ClientOrderID,ClientOrderItemID'
GO
--End table client.ClientOrderItem

--Begin table client.ClientProduct
DECLARE @TableName VARCHAR(250) = 'client.ClientProduct'

EXEC utility.DropColumn @TableName, 'InStock'

EXEC utility.AddColumn @TableName, 'MenuCategoryID', 'INT', '0'
EXEC utility.AddColumn @TableName, 'OrderPoint', 'INT', '0'
GO
--End table client.ClientProduct

--Begin table client.ClientProductInventory
DECLARE @TableName VARCHAR(250) = 'client.ClientProductInventory'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientProductInventory
	(
	ClientProductInventoryID INT IDENTITY(1, 1) NOT NULL,
	ClientProductID INT,
	ProductLocationID INT,
	Quantity INT,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductLocationID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'Quantity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientProductInventoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientProductInventory', 'ClientProductID'
GO
--End table client.ClientProductInventory

--Begin table client.MenuCategory
DECLARE @TableName VARCHAR(250) = 'client.MenuCategory'

EXEC utility.DropObject @TableName

CREATE TABLE client.MenuCategory
	(
	MenuCategoryID INT IDENTITY(0, 1) NOT NULL,
	ClientID INT,
	MenuCategoryName VARCHAR(250),
	DisplayOrder INT,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'MenuCategoryID'
EXEC utility.SetIndexClustered @TableName, 'IX_MenuCategory', 'ClientID,DisplayOrder,MenuCategoryName'
GO
--End table client.MenuCategory

--Begin table client.ProductLocation
DECLARE @TableName VARCHAR(250) = 'client.ProductLocation'

EXEC utility.DropObject @TableName

CREATE TABLE client.ProductLocation
	(
	ProductLocationID INT IDENTITY(0, 1) NOT NULL,
	ClientID INT,
	ProductLocationName VARCHAR(250),
	Capacity INT, 
	SingleItemCapacity INT, 
	FillSequence INT, 
	DisplayOrder INT,
	IsActive BIT,
	UpdatePersonID INT,
	UpdateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'Capacity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'FillSequence', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'SingleItemCapacity', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpdateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'UpdatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProductLocationID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProductLocation', 'ClientID,ProductLocationName'

EXEC utility.InsertIdentityValue @TableName, 'ProductLocationID', 0
GO
--End table client.ProductLocation

--Begin table dropdown.OrderStatus
DECLARE @TableName VARCHAR(250) = 'dropdown.OrderStatus'

EXEC utility.DropObject 'dropdown.OrderStatus'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.OrderStatus
	(
	OrderStatusID INT IDENTITY(0,1) NOT NULL,
	OrderStatusCode VARCHAR(50),
	OrderStatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'OrderStatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_OrderStatus', 'DisplayOrder,OrderStatusName', 'OrderStatusID'
GO
--End table dropdown.OrderStatus

--Begin table dropdown.PackageType
DECLARE @TableName VARCHAR(250) = 'dropdown.PackageType'

EXEC utility.AddColumn @TableName, 'UnitOfIssue', 'VARCHAR(25)'
GO
--End table dropdown.PackageType

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.AddColumn @TableName, 'UniversalProductCode', 'VARCHAR(25)'
GO
--End table product.Product

EXEC utility.AddColumn 'client.Client', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientContact', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientPerson', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientProduct', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.ClientProductLocation', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.Contact', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'client.PricingModel', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'person.Person', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Distributor', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Producer', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.Product', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductProducer', 'UpdatePersonID', 'INT', '0'
EXEC utility.AddColumn 'product.ProductStyle', 'UpdatePersonID', 'INT', '0'