--Begin function core.FormatDate
EXEC utility.DropObject 'core.FormatDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format Date data
-- ===========================================

CREATE FUNCTION core.FormatDate
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(11)

AS
BEGIN

	RETURN CONVERT(CHAR(11), @DateTimeData, 100)

END
GO
--End function core.FormatDate

--Begin function core.FormatDateTime
EXEC utility.DropObject 'core.FormatDateTime'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format DateTime data
-- ===============================================

CREATE FUNCTION core.FormatDateTime
(
@DateTimeData DATETIME
)

RETURNS VARCHAR(20)

AS
BEGIN

	RETURN CONVERT(CHAR(20), @DateTimeData, 100)

END
GO
--End function core.FormatDateTime

--Begin function core.FormatTime
EXEC utility.DropObject 'core.FormatTime'
GO

-- ===========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format Time data
-- ===========================================

CREATE FUNCTION core.FormatTime
(
@TimeData TIME
)

RETURNS VARCHAR(8)

AS
BEGIN

	RETURN ISNULL(LTRIM(RIGHT(CONVERT(CHAR(20), @TimeData, 100), 9)), '')


END
GO
--End function core.FormatTime

--Begin function core.GetDescendantMenuItemsByMenuItemCode
EXEC utility.DropObject 'core.GetDescendantMenuItemsByMenuItemCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the decendant menu items of a specific menuitemcode
-- =====================================================================================

CREATE FUNCTION core.GetDescendantMenuItemsByMenuItemCode
(
@MenuItemCode VARCHAR(50)
)

RETURNS @tReturn table 
	(
	MenuItemID INT PRIMARY KEY NOT NULL,
	NodeLevel INT
	) 

AS
BEGIN

	WITH HD (MenuItemID,ParentMenuItemID,NodeLevel)
		AS 
		(
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			1 
		FROM core.MenuItem T 
		WHERE T.MenuItemCode = @MenuItemCode
	
		UNION ALL
		
		SELECT
			T.MenuItemID, 
			T.ParentMenuItemID, 
			HD.NodeLevel + 1 AS NodeLevel
		FROM core.MenuItem T 
			JOIN HD ON HD.MenuItemID = T.ParentMenuItemID 
		)
	
	INSERT INTO @tReturn
		(MenuItemID,NodeLevel)
	SELECT 
		HD.MenuItemID,
		HD.NodeLevel
	FROM HD

	RETURN
END
GO
--End function core.GetDescendantMenuItemsByMenuItemCode

--Begin function core.GetEntityTypeNameByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNameByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the name of an entity type based on an entity type code
-- =========================================================================================

CREATE FUNCTION core.GetEntityTypeNameByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeName
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNameByEntityTypeCode

--Begin function core.GetEntityTypeNamePluralByEntityTypeCode
EXEC utility.DropObject 'core.GetEntityTypeNamePluralByEntityTypeCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return the plural name of an entity type based on an entity type code
-- ================================================================================================

CREATE FUNCTION core.GetEntityTypeNamePluralByEntityTypeCode
(
@EntityTypeCode VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRetVal VARCHAR(250)

	SELECT
		@cRetVal = ET.EntityTypeNamePlural
	FROM core.EntityType ET
	WHERE ET.EntityTypeCode = @EntityTypeCode

	RETURN @cRetVal

END
GO
--End function core.GetEntityTypeNamePluralByEntityTypeCode

--Begin function core.GetSystemSetupValueBySystemSetupKey
EXEC utility.DropObject 'core.GetSystemSetupValueBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a SystemSetupValue from a SystemSetupKey from the core.SystemSetup table
-- ==========================================================================================================

CREATE FUNCTION core.GetSystemSetupValueBySystemSetupKey
(
@SystemSetupKey VARCHAR(250),
@DefaultSystemSetupValue VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN
	
	DECLARE @cSystemSetupValue VARCHAR(MAX)
	
	SELECT @cSystemSetupValue = SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	
	RETURN ISNULL(@cSystemSetupValue, @DefaultSystemSetupValue)

END
GO
--End function core.GetSystemSetupValueBySystemSetupKey

--Begin function core.ListToTable
EXEC utility.DropObject 'core.ListToTable'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a table from a delimted list of values - based on a script by Kshitij Satpute from SQLSystemCentral.com
-- =========================================================================================================================================

CREATE FUNCTION core.ListToTable
(
@List NVARCHAR(MAX), 
@Delimiter VARCHAR(5)
)

RETURNS @tTable TABLE (ListItemID INT NOT NULL IDENTITY(1,1), ListItem NVARCHAR(MAX))  

AS
BEGIN

	DECLARE @xXML XML = (SELECT CONVERT(XML,'<r>' + REPLACE(@List, @Delimiter, '</r><r>') + '</r>'))
 
	INSERT INTO @tTable(ListItem)
	SELECT T.value('.', 'NVARCHAR(MAX)')
	FROM @xXML.nodes('/r') AS x(T)
	
	RETURN

END
GO
--End function core.ListToTable

--Begin function core.NullIfEmpty
EXEC utility.DropObject 'core.NullIfEmpty'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format string data
-- =============================================

CREATE FUNCTION core.NullIfEmpty
(
@String VARCHAR(MAX)
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	RETURN IIF(@String IS NULL OR LEN(RTRIM(@String)) = 0, NULL, @String)

END
GO
--End function core.NullIfEmpty

--Begin function core.YesNoFormat
EXEC utility.DropObject 'core.YesNoFormat'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format a bit as a Yes/No string
-- ==========================================================

CREATE FUNCTION core.YesNoFormat
(
@Value BIT
)

RETURNS VARCHAR(4)

AS
BEGIN

	RETURN CASE WHEN @Value = 1 THEN 'Yes ' ELSE 'No ' END

END
GO
--End function core.YesNoFormat

--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function person.FormatPersonNameByPersonID
EXEC utility.DropObject 'person.FormatPersonNameByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a person name in a specified format
-- =====================================================================

CREATE FUNCTION person.FormatPersonNameByPersonID
(
@PersonID INT,
@Format VARCHAR(50)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cFirstName VARCHAR(100)
	DECLARE @cLastName VARCHAR(100)
	DECLARE @cRetVal VARCHAR(310)
	DECLARE @cSuffix VARCHAR(50)
	DECLARE @cTitle VARCHAR(50)
	
	SET @cRetVal = ''
	
	SELECT
		@cFirstName = ISNULL(P.FirstName, ''),
		@cLastName = ISNULL(P.LastName, ''),
		@cSuffix = ISNULL(P.Suffix, ''),
		@cTitle = ISNULL(P.Title, '')
	FROM person.Person P
	WHERE P.PersonID = @PersonID
	
	IF @Format LIKE '%FirstLast%'
		BEGIN
			
		SET @cRetVal = @cFirstName + ' ' + @cLastName

		IF @Format LIKE '%Suffix'
			SET @cRetVal += @cSuffix
		--ENDIF
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cTitle + ' ' + RTRIM(LTRIM(@cRetVal))
		--ENDIF
			
		END
	--ENDIF
			
	IF @Format LIKE '%LastFirst%'
		BEGIN
			
		IF LEN(RTRIM(@cLastName)) > 0
			SET @cRetVal = @cLastName + ', '
		--ENDIF
				
		SET @cRetVal = @cRetVal + @cFirstName + ' '
	
		IF @Format LIKE 'Title%' AND LEN(RTRIM(@cTitle)) > 0
			SET @cRetVal = @cRetVal + @cTitle
		--ENDIF
			
		END
	--ENDIF
	
	RETURN RTRIM(LTRIM(@cRetVal))

END
GO
--End function person.FormatPersonNameByPersonID

--Begin function person.HashPassword
EXEC utility.DropObject 'person.HashPassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to hash a password
-- ==========================================

CREATE FUNCTION person.HashPassword
(
@Password VARCHAR(50),
@PasswordSalt VARCHAR(50)
)

RETURNS VARCHAR(64)

AS
BEGIN

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @nI INT = 0

	SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @Password + @PasswordSalt), 2)

	WHILE (@nI < 65536)
		BEGIN

		SET @cPasswordHash = CONVERT(VARCHAR(64), HASHBYTES('SHA2_256', @cPasswordHash + @PasswordSalt), 2)
		SET @nI = @nI + 1

		END
	--END WHILE
		
	RETURN @cPasswordHash

END
GO
--End function person.HashPassword

--Begin function person.HasPermission
EXEC utility.DropObject 'person.HasPermission'
GO
--End function person.HasPermission

--Begin function person.IsSuperAdministrator
EXEC utility.DropObject 'person.IsSuperAdministrator'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to format a numeric value for javascript
-- ================================================================

CREATE FUNCTION person.IsSuperAdministrator
(
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	RETURN CASE WHEN EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @PersonID AND P.IsSuperAdministrator = 1) THEN 1 ELSE 0 END

END
GO
--End function person.IsSuperAdministrator

--Begin function person.ValidatePassword
EXEC utility.DropObject 'person.ValidatePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to validate a password
-- ==============================================

CREATE FUNCTION person.ValidatePassword
(
@Password VARCHAR(50),
@PersonID INT
)

RETURNS BIT

AS
BEGIN

	DECLARE @bReturn BIT = 0
	DECLARE @cPassword VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nI INT = 0

	SELECT 
		@cPassword = P.Password,
		@cPasswordSalt = P.PasswordSalt
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	IF @cPassword IS NOT NULL AND @cPasswordSalt IS NOT NULL
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

		IF @cPasswordHash = @cPassword
			SET @bReturn = 1
		--ENDIF

		END
	--ENDIF

	RETURN @bReturn

END
GO
--End function person.ValidatePassword

--Begin function utility.StripCharacters
EXEC utility.DropObject 'utility.StripCharacters'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A function to return a string stripped of any characters not passed as the pattern match
-- =====================================================================================================

CREATE FUNCTION utility.StripCharacters
(
@String NVARCHAR(MAX), 
@Pattern VARCHAR(255)
)

RETURNS NVARCHAR(MAX)

AS
BEGIN

	SET @Pattern =  '%[' + @Pattern + ']%'

	WHILE PatIndex(@Pattern, @String) > 0
		SET @String = Stuff(@String, PatIndex(@Pattern, @String), 1, '')
	--END WHILE

	RETURN @String

END
GO
--End function utility.StripCharacters