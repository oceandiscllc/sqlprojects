--Begin procedure client.DeletePricingModelByPricingModelID
EXEC utility.DropObject 'client.DeletePricingModelByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =================================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.08
-- Description:	A stored procedure to delete data from the client.PricingModel table
-- =================================================================================
CREATE PROCEDURE client.DeletePricingModelByPricingModelID

@PricingModelID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PM
	FROM client.PricingModel PM
	WHERE PM.PricingModelID = @PricingModelID

	UPDATE CP
	SET CP.PricingModelID = 0
	FROM client.ClientProduct CP
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.PricingModel PM
		WHERE PM.PricingModelID = CP.PricingModelID
		)

END
GO
--End procedure client.DeletePricingModelByPricingModelID

--Begin procedure client.GetClientByClientID
EXEC utility.DropObject 'client.GetClientByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create date:	2018.02.07
-- Description:	A stored procedure to return data from the client.Client table
-- ===========================================================================
CREATE PROCEDURE client.GetClientByClientID

@ClientID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	--Client
	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.BaseFactor,
		C.BaseCharge,
		C.BaseRound,
		C.ClientID,
		C.ClientName,
		C.EmailAddress,
		C.FirstName,
		C.ImagePath,
		C.ISOCountryCode3,
		dropdown.GetCountryNameByISOCountryCode(C.ISOCountryCode3) AS CountryName,
		C.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(C.ISORegionCode2) AS RegionName,
		C.LastName,
		C.Municipality,
		C.PhoneNumber,
		C.PostalCode,
		C.WebsiteURL,
		S.StatusID,
		S.StatusName
	FROM client.Client C
		JOIN dropdown.Status S ON S.StatusID = C.StatusID
			AND C.ClientID = @ClientID

END
GO
--End procedure client.GetClientByClientID

--Begin procedure client.GetPricingModel
EXEC utility.DropObject 'client.GetPricingModel'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.04
-- Description:	A stored procedure to get data from the client.PricingModel table
-- ==============================================================================
CREATE PROCEDURE client.GetPricingModel

@ClientProductID INT

AS
BEGIN
	SET NOCOUNT ON;

	--PricingModel
	SELECT
		--C.BaseFactor,
		--C.BaseCharge,
		--C.BaseRound,
		--CP.PackagePrice,
		--CP.ServingCount,
		--CP.PackagePrice / CP.ServingCount AS BasePrice,
		--(CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge AS ComputedPrice,
		C.BaseRound,
		CP.ClientProductID,
		CP.MenuPrice AS ClientProductMenuPrice,
		CP.PricingModelID AS ClientProductPricingModelID,
		CP.UpCharge,
		PM.PricingModelCharge,
		PM.PricingModelFactor,
		PM.PricingModelID,
		PM.PricingModelName,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge) / C.BaseRound, 0) * C.BaseRound AS MenuPrice,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge) / C.BaseRound, 0) * C.BaseRound AS ModelPrice
	FROM client.PricingModel PM
		JOIN client.Client C ON C.ClientID = PM.ClientID
		JOIN client.ClientProduct CP ON CP.ClientID = PM.ClientID
			AND CP.ClientProductID = @ClientProductID

END
GO
--End procedure client.GetPricingModel

--Begin procedure client.SetIsMenuItem
EXEC utility.DropObject 'client.SetIsMenuItem'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================
-- Author:			Todd Pires
-- Create date:	2018.11.29
-- Description:	A stored procedure to update the IsMenuItem bit in the client.ClientProduct table
-- ==============================================================================================
CREATE PROCEDURE client.SetIsMenuItem

@ClientProductID INT = 0,
@IsMenuItem BIT = 0,
@nPersonID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET 
		CP.IsMenuItem = @IsMenuItem,
		CP.MenuPriceDateTime = CASE WHEN @IsMenuItem = 1 THEN getDate() ELSE NULL END,
		CP.MenuPricePersonID = CASE WHEN @IsMenuItem = 1 THEN @nPersonID ELSE 0 END
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID 

END
GO
--End procedure client.SetIsMenuItem

--Begin procedure client.SetMenuPrice
EXEC utility.DropObject 'client.SetMenuPrice'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.SetMenuPrice

@ClientProductID INT = 0,
@Upcharge NUMERIC(18,2) = 0,
@PricingModelID INT = 0,
@MenuPrice NUMERIC(18,2) = 0

AS
BEGIN
	SET NOCOUNT ON;

	IF @PricingModelID != 0
		BEGIN

		SELECT @MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + @Upcharge) / C.BaseRound, 0) * C.BaseRound
		FROM client.ClientProduct CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
			JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
				AND CP.ClientProductID = @ClientProductID
				AND PM.PricingModelID = @PricingModelID

		END
	--ENDIF

	UPDATE CP
	SET 
		CP.MenuPrice = @MenuPrice,
		CP.PricingModelID = @PricingModelID,
		CP.Upcharge = @Upcharge
	FROM client.ClientProduct CP
	WHERE CP.ClientProductID = @ClientProductID

	SELECT FORMAT(@MenuPrice, 'C', 'en-us') AS MenuPrice

END
GO
--End procedure client.SetMenuPrice

--Begin procedure client.UpdateMenuPricesByClientID
EXEC utility.DropObject 'client.UpdateMenuPricesByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByClientID

@ClientID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ClientID = @ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = CP.PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByClientID

--Begin procedure client.UpdateMenuPricesByPricingModelID
EXEC utility.DropObject 'client.UpdateMenuPricesByPricingModelID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create date:	2018.12.03
-- Description:	A stored procedure to update the client.ClientProduct table
-- ========================================================================
CREATE PROCEDURE client.UpdateMenuPricesByPricingModelID

@PricingModelID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE CP
	SET CP.MenuPrice = ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.Upcharge) / C.BaseRound, 0) * C.BaseRound
	FROM client.ClientProduct CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
		JOIN client.PricingModel PM ON PM.ClientID = CP.ClientID
			AND CP.ServingCount > 0
			AND PM.PricingModelID = @PricingModelID

END
GO
--End procedure client.UpdateMenuPricesByPricingModelID

--Begin procedure dropdown.GetCountryData
EXEC utility.DropObject 'dropdown.GetCountryData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Country table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetCountryData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.CountryID,
		T.ISOCountryCode2,
		T.ISOCountryCode3,
		T.CountryName
	FROM dropdown.Country T
	WHERE (T.CountryID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.CountryName, T.CountryID

END
GO
--End procedure dropdown.GetCountryData

--Begin procedure dropdown.GetRegionData
EXEC utility.DropObject 'dropdown.GetRegionData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.Region table
-- ==============================================================================
CREATE PROCEDURE dropdown.GetRegionData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.RegionID,
		T.ISOCountryCode3,
		T.ISORegionCode2,
		T.RegionName
	FROM dropdown.Region T
	WHERE (T.RegionID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.RegionName, T.RegionID

END
GO
--End procedure dropdown.GetRegionData

--Begin procedure dropdown.GetPackageTypeData
EXEC utility.DropObject 'dropdown.GetPackageTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.PackageType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetPackageTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.PackageTypeID,
		T.PackageTypeCode,
		T.PackageTypeName,
		T.PackageTypeSizePlural
	FROM dropdown.PackageType T
	WHERE (T.PackageTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.PackageTypeName, T.PackageTypeID

END
GO
--End procedure dropdown.GetPackageTypeData

--Begin procedure dropdown.GetProductTypeData
EXEC utility.DropObject 'dropdown.GetProductTypeData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the dropdown.ProductType table
-- ===================================================================================
CREATE PROCEDURE dropdown.GetProductTypeData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductTypeID,
		T.ProductTypeCode,
		T.ProductTypeName
	FROM dropdown.ProductType T
	WHERE (T.ProductTypeID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.ProductTypeName, T.ProductTypeID

END
GO
--End procedure dropdown.GetProductTypeData

--Begin procedure dropdown.GetStatusData
EXEC utility.DropObject 'dropdown.GetStatusData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================
-- Author:			Todd Pires
-- Create date:	2018.01.26
-- Description:	A stored procedure to return data from the dropdown.Status table
-- =============================================================================
CREATE PROCEDURE dropdown.GetStatusData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.StatusID,
		T.StatusCode,
		T.StatusName
	FROM dropdown.Status T
	WHERE (T.StatusID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DisplayOrder, T.StatusName, T.StatusID

END
GO
--End procedure dropdown.GetStatusData

--Begin procedure eventlog.GetEventLogDataByEventLogID
EXEC utility.DropObject 'eventlog.GetEventLogDataByEventLogID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create date:	2018.03.21
-- Description:	A stored procedure to return data from the eventlog.EventLog table
-- ===============================================================================
CREATE PROCEDURE eventlog.GetEventLogDataByEventLogID

@EventLogID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		EL.Comments,
		EL.CreateDateTime,
		core.FormatDateTime(EL.CreateDateTime) AS CreateDateTimeFormatted,
		EL.EntityID, 
		EL.EventCode, 
		eventlog.getEventNameByEventCode(EL.EventCode) AS EventCodeName,
		EL.EventData, 
		EL.EventLogID, 
		EL.PersonID,
		person.FormatPersonNameByPersonID(EL.PersonID, 'LastFirst') AS PersonNameFormatted,
		ET.EntityTypeCode, 
		ET.EntityTypeName
	FROM eventlog.EventLog EL
		JOIN core.EntityType ET ON ET.EntityTypeCode = EL.EntityTypeCode
			AND EL.EventLogID = @EventLogID

END
GO
--End procedure eventlog.GetEventLogDataByEventLogID

--Begin procedure eventlog.LogAnnouncementAction
EXEC utility.DropObject 'eventlog.LogAnnouncementAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogAnnouncementAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Announcement'

	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Announcement'), ELEMENTS
			)
		FROM core.Announcement T
		WHERE T.AnnouncementID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogAnnouncementAction

--Begin procedure eventlog.LogClientAction
EXEC utility.DropObject 'eventlog.LogClientAction'
GO

-- ==========================================================================
-- Client:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogClientAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Client'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*
			FOR XML RAW('Client'), ELEMENTS
			)
		FROM Client.Client T
		WHERE T.ClientID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogClientAction

--Begin procedure eventlog.LogLoginAction
EXEC utility.DropObject 'eventlog.LogLoginAction'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2017.02.01
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogLoginAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	IF @EventCode IN ('FailedLogin', 'LogIn', 'LogOut', 'ResetPassword')
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			'Login',
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogLoginAction

--Begin procedure eventlog.LogPersonAction
EXEC utility.DropObject 'eventlog.LogPersonAction'
GO

-- ==========================================================================
-- Author:			Todd Pires
-- Create date: 2018.03.21
-- Description:	A stored procedure to add data to the eventlog.EventLog table
-- ==========================================================================
CREATE PROCEDURE eventlog.LogPersonAction

@EntityID INT,
@EventCode VARCHAR(50),
@PersonID INT,
@Comments NVARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cEntityTypeCode VARCHAR(50) = 'Person'
	
	IF @EventCode IN ('Create', 'Delete', 'Update')
		BEGIN

		DECLARE @cPersonPermissionableXML VARCHAR(MAX) = ''
		
		SELECT @cPersonPermissionableXML = COALESCE(@cPersonPermissionableXML, '') + D.PersonPermissionable
		FROM
			(
			SELECT
				(SELECT T.PermissionableLineage FOR XML RAW(''), ELEMENTS) AS PersonPermissionable
			FROM person.PersonPermissionable T 
			WHERE T.PersonID = @EntityID
			) D

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments, EventData)
		SELECT
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments,
			(
			SELECT T.*,
			CAST('<PersonPermissionableData>' + ISNULL(@cPersonPermissionableXML, '') + '</PersonPermissionableData>' AS XML)
			FOR XML RAW('Person'), ELEMENTS
			)
		FROM person.Person T
		WHERE T.PersonID = @EntityID

		END
	ELSE 
		BEGIN

		INSERT INTO eventlog.EventLog
			(PersonID, EventCode, EntityTypeCode, EntityID, Comments)
		VALUES
			(
			@PersonID,
			@EventCode,
			@cEntityTypeCode,
			@EntityID,
			@Comments
			)

		END
	--ENDIF

END
GO
--End procedure eventlog.LogPersonAction

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.EmailAddress,
		P.FirstName,
		P.InvalidLoginAttempts,
		P.IsAccountLockedOut,
		P.IsActive,
		P.IsSuperAdministrator,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.Password,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PasswordSalt,
		P.PersonID,
		person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') AS PersonNameFormatted,
		P.PhoneNumber,
		P.Suffix,
		P.Title
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.DeletePersonByPersonID
EXEC utility.DropObject 'person.DeletePersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to delete data from the person.Person table based on a PersonID
-- ===============================================================================================
CREATE PROCEDURE person.DeletePersonByPersonID

@PersonID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DELETE CP
	FROM client.ClientPerson CP
	WHERE CP.ClientID = @ClientID
		AND CP.PersonID = @PersonID

	DELETE P
	FROM person.Person P
	WHERE NOT EXISTS
		(
		SELECT 1
		FROM client.ClientPerson CP
		WHERE CP.PersonID = P.PersonID
		)
		AND P.IsSuperAdministrator = 0

END
GO
--End procedure person.DeletePersonByPersonID

--Begin procedure product.GetDistributorByDistributorID
EXEC utility.DropObject 'product.GetDistributorByDistributorID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Distributor table based on a DistributorID
-- ==========================================================================================================
CREATE PROCEDURE product.GetDistributorByDistributorID

@DistributorID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Distributor
	SELECT
		D.DistributorID,
		D.DistributorName,
		D.ImagePath
	FROM product.Distributor D
	WHERE D.DistributorID = @DistributorID

	SELECT DISTINCT
		C.CountryName,
		P.ImagePath,
		P.ISOCountryCode3,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM client.ClientProduct CP
		JOIN product.ProductProducer PP ON PP.ProductID = CP.ProductID
		JOIN product.Producer P ON P.ProducerID = PP.ProducerID
		JOIN dropdown.Country C ON C.ISOCountryCode3 = P.ISOCountryCode3
			AND CP.DistributorID = @DistributorID
			AND CP.ClientID = @ClientID
			AND P.IsActive = 1
	ORDER BY P.ProducerName, P.ProducerID

END
GO
--End procedure product.GetDistributorByDistributorID

--Begin procedure product.GetDistributorData
EXEC utility.DropObject 'product.GetDistributorData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.Distributor table
-- ===================================================================================
CREATE PROCEDURE product.GetDistributorData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.DistributorID,
		T.DistributorName,
		T.ImagePath
	FROM product.Distributor T
	WHERE (T.DistributorID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.DistributorName, T.DistributorID

END
GO
--End procedure product.GetDistributorData

--Begin procedure product.GetMenuDataByClientID
EXEC utility.DropObject 'product.GetMenuDataByClientID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================
-- Author:			Todd Pires
-- Create date:	2018.12.07
-- Description:	A stored procedure to return menu data
-- ===================================================
CREATE PROCEDURE product.GetMenuDataByClientID

@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		C.Address1,
		C.Address2,
		C.Address3,
		C.ClientName,
		C.ImagePath,
		C.ISOCountryCode3,
		C.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(C.ISORegionCode2) AS RegionName,
		C.Municipality,
		C.PhoneNumber,
		C.PostalCode,
		C.WebsiteURL
	FROM client.Client C
	WHERE C.ClientID = @ClientID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Draft'
			AND PT2.ProductTypeName <> 'Wine'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName IN ('Bottle', 'Can')
			AND PT2.ProductTypeName = 'Beer'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName = 'Bomber'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT2.ProductTypeName = 'Wine'
	ORDER BY 4, P1.ProductName, P1.ProductID

	SELECT
		CP.MenuPrice,
		P1.ABV,
		P1.IBU,
		product.GetProducerNameListByProductID(P1.ProductID) AS ProducerNameFormatted,
		P1.ProductName,
		PS.ProductStyleName,
		PT1.PackageTypeName,
		PT2.ProductTypeName,
		(SELECT TOP 1 P2.ImagePath FROM product.Producer P2 JOIN product.ProductProducer PP ON PP.ProducerID = P2.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P2.ProducerName) AS ImagePath,
		(SELECT TOP 1 P3.Municipality + ', ' + ISNULL(core.NullIfEmpty(dropdown.GetRegionNameByISORegionCode(P3.ISORegionCode2)), dropdown.GetCountryNameByISOCountryCode(P3.ISOCountryCode3)) FROM product.Producer P3 JOIN product.ProductProducer PP ON PP.ProducerID = P3.ProducerID AND PP.ProductID = CP.ProductID ORDER BY P3.ProducerName) AS Location
	FROM client.ClientProduct CP
		JOIN product.Product P1 ON P1.ProductID = CP.ProductID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P1.ProductStyleID
		JOIN dropdown.PackageType PT1 ON PT1.PackageTypeID = P1.PackageTypeID
		JOIN dropdown.ProductType PT2 ON PT2.ProductTypeID = P1.ProductTypeID
			AND CP.ClientID = @ClientID
			AND CP.IsMenuItem = 1
			AND PT1.PackageTypeName NOT IN ('Bomber', 'Draft')
			AND PT2.ProductTypeName = 'Cider'
	ORDER BY 4, PT1.DisplayOrder, P1.ProductName, P1.ProductID

END
GO
--End procedure product.GetMenuDataByClientID

--Begin procedure product.GetProducerByProducerID
EXEC utility.DropObject 'product.GetProducerByProducerID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Producer table based on a ProducerID
-- ====================================================================================================
CREATE PROCEDURE product.GetProducerByProducerID

@ProducerID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Producer
	SELECT
		P.ImagePath,
		P.ISOCountryCode3,
		dropdown.GetCountryNameByISOCountryCode(P.ISOCountryCode3) AS CountryName,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM product.Producer P
	WHERE P.ProducerID = @ProducerID

END
GO
--End procedure product.GetProducerByProducerID

--Begin procedure product.GetProducerData
EXEC utility.DropObject 'product.GetProducerData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.Producer table
-- ===================================================================================
CREATE PROCEDURE product.GetProducerData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	 SELECT
		C.CountryName,
		T.ImagePath,
		T.ISOCountryCode3,
		T.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(T.ISORegionCode2) AS RegionName,
		T.Municipality,
		T.ProducerID,
		T.ProducerName
	 FROM product.Producer T
	  JOIN dropdown.Country C ON C.ISOCountryCode3 = T.ISOCountryCode3
	 		AND (T.ProducerID > 0 OR @IncludeZero = 1)
	  	AND T.IsActive = 1
	 ORDER BY T.DisplayOrder, T.ProducerName, T.ProducerID

END
GO
--End procedure product.GetProducerData

--Begin procedure product.GetProductsByProducerID
EXEC utility.DropObject 'product.GetProductByProducerIDAndClientID'
EXEC utility.DropObject 'product.GetProductsByProducerID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Product table based on a ProductID
-- ==================================================================================================
CREATE PROCEDURE product.GetProductsByProducerID

@ProducerID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Product
	SELECT
		P.ProductID,
		P.ProductName,
		P.ABV,
		P.IBU,
		PKT.PackageTypeName,
		PRT.ProductTypeName,
		PS.ProductStyleName,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientProduct CP WHERE CP.ProductID = P.ProductID) THEN 1 ELSE 0 END AS IsClientProduct,	
		product.GetIsMenuItemButtonHTML(P.ProductID, @ClientID)	AS IsMenuItemButton,
		CASE WHEN EXISTS (SELECT 1 FROM client.ClientProduct CP WHERE CP.ProductID = P.ProductID AND CP.IsMenuItem = 1) THEN 1 ELSE 0 END AS IsMenuItem,
		product.GetAdditionalProductProducerNames(P.ProductID, @ProducerID) AS AdditionalProductProducerNames
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
		JOIN dropdown.ProductType PRT ON PRT.ProductTypeID = P.ProductTypeID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P.ProductStyleID
			AND EXISTS
				(
				SELECT 1
				FROM product.ProductProducer PP
				WHERE PP.ProductID = P.ProductID
					AND PP.ProducerID = @ProducerID
				)
	ORDER BY PKT.DisplayOrder, PRT.ProductTypeName, P.ProductName, P.ProductID

END
GO
--End procedure product.GetProductsByProducerID

--Begin procedure product.GetProductByProductID
EXEC utility.DropObject 'product.GetProductByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.06
-- Description:	A stored procedure to return data from the product.Product table based on a ProductID
-- ==================================================================================================
CREATE PROCEDURE product.GetProductByProductID

@ProductID INT,
@ClientID INT

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @nClientProductID INT = ISNULL((SELECT CP.ClientProductID FROM client.ClientProduct CP WHERE CP.ClientID = @ClientID AND CP.ProductID = @ProductID), 0)

	--Product
	SELECT
		P.ABV,
		P.IBU,
		P.PackageSize,
		P.ProductID,
		P.ProductName,
		PKT.PackageTypeID,
		PKT.PackageTypeName,
		PRT.ProductTypeID,
		PRT.ProductTypeName,
		PS.ProductStyleDescription,
		PS.ProductStyleID,
		PS.ProductStyleName
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
		JOIN product.ProductStyle PS ON PS.ProductStyleID = P.ProductStyleID
		JOIN dropdown.ProductType	PRT ON PRT.ProductTypeID = P.ProductTypeID
			AND P.ProductID = @ProductID

	--ClientProduct
	SELECT
		CP.ClientProductID,
		CP.Description,
		CP.IconHTML,
		CP.ImagePath,
		CP.IsFeatured,
		CP.IsMenuItem,
		CP.ServingSize,
		CP.ServingCount,
		CP.PackagePrice,
		CP.MenuPrice,
		CP.UpCharge,
		core.FormatDateTime(CP.MenuPriceDateTime) AS MenuPriceDateTimeFormatted,
		person.FormatPersonNameByPersonID(CP.MenuPricePersonID, 'LastFirst') AS PersonNameFormatted,
		D.DistributorID,
		D.DistributorName
	FROM client.ClientProduct CP
		JOIN product.Distributor D ON D.DistributorID = CP.DistributorID
			AND CP.ClientProductID = @nClientProductID

	--PricingModel
	EXEC client.GetPricingModel @nClientProductID

	--ProductProducer
	SELECT
		C.CountryName,
		P.ImagePath,
		P.ISOCountryCode3,
		P.ISORegionCode2,
		dropdown.GetRegionNameByISORegionCode(P.ISORegionCode2) AS RegionName,
		P.Municipality,
		P.ProducerID,
		P.ProducerName
	FROM product.ProductProducer PP
		JOIN product.Producer P ON P.ProducerID = PP.ProducerID
		JOIN dropdown.Country C ON C.ISOCountryCode3 = P.ISOCountryCode3
			AND PP.ProductID = @ProductID
	ORDER BY P.ProducerName, P.ProducerID

END
GO
--End procedure product.GetProductByProductID

--Begin procedure product.GetProductStyleData
EXEC utility.DropObject 'product.GetProductStyleData'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================================
-- Author:			Todd Pires
-- Create date:	2017.12.15
-- Description:	A stored procedure to return data from the product.ProductStyle table
-- ===================================================================================
CREATE PROCEDURE product.GetProductStyleData

@IncludeZero BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T.ProductStyleCode,
		T.ProductStyleDescription,
		T.ProductStyleID,
		T.ProductStyleName
  FROM product.ProductStyle T
	WHERE (T.ProductStyleID > 0 OR @IncludeZero = 1)
		AND T.IsActive = 1
	ORDER BY T.ProductStyleName, T.ProductStyleID

END
GO
--End procedure product.GetProductStyleData
