TRUNCATE TABLE product.Product
TRUNCATE TABLE product.ProductProducer
GO

DECLARE @cDistributorName VARCHAR(250)
DECLARE @cPackageSize VARCHAR(50)
DECLARE @cPackageTypeName VARCHAR(50)
DECLARE @cProducerName VARCHAR(250)
DECLARE @cProductName VARCHAR(250)
DECLARE @cProductStyleName VARCHAR(250)
DECLARE @nABV NUMERIC(18,2)
DECLARE @nCharIndex INT
DECLARE @nIBU NUMERIC(18,2)
DECLARE @nImportProductID INT
DECLARE @tOutput TABLE (ProductID INT NOT NULL PRIMARY KEY)
DECLARE @tProducer TABLE (ProducerID INT, ProducerName VARCHAR(250))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		LTRIM(ID.Name) AS ProductName,
		ID.Brewery AS ProducerName,
		ID.Distributor AS DistributorName,

		CASE
			WHEN ID.Style IN ('Amber Lager', 'American Amber', 'American Amber/Red Lager')
			THEN 'American Amber/Red Lager'
			WHEN ID.Style IN ('American Craft Lager', 'American Lager')
			THEN 'American Lager'
			WHEN ID.Style IN ('American Barleywine', 'Barleywine')
			THEN 'Barleywine'
			WHEN ID.Style IN ('Belgian Style Quadruple', 'Quadrupel', 'Quadruple Style Ale')
			THEN 'Belgian Style Quadruple'
			WHEN ID.Style IN ('Belgian Style Tripel Ale', 'Belgian Style Tripel')
			THEN 'Belgian Style Tripel'
			WHEN ID.Style IN ('Belgian Tripel Style Ale', 'Belgian Tripel')
			THEN 'Belgian Tripel'
			WHEN ID.Style IN ('Chile Beer', 'Chili Ale', 'Chili Beer')
			THEN 'Chile Beer'
			WHEN ID.Style IN ('Doppelbock', 'Dopplebock')
			THEN 'Doppelbock'
			WHEN ID.Style IN ('Fruit Beer', 'Fruit/Vegetable Beer')
			THEN 'Fruit/Vegetable Beer'
			WHEN ID.Style IN ('Herb/Spiced Beer', 'Herbed/Spiced Beer')
			THEN 'Herb/Spiced Beer'
			WHEN ID.Style IN ('German Alt/Kolsch', 'German Style Kolsch', 'Kolsch')
			THEN 'Kolsch'
			WHEN ID.Style IN ('Pumpkin Yam Beer', 'Pumpkin/Yam Beer')
			THEN 'Pumpkin/Yam Beer'
			ELSE ID.Style 
		END AS ProductStyleName,

		100 * ISNULL(ID.[%], 0) AS ABV,
		CASE WHEN ISNUMERIC(ID.IBU) = 1 THEN CAST(ID.IBU AS NUMERIC(18,2)) ELSE 0 END AS IBU,
		ID.[Size(gal)] AS PackageSize,
		'Draft' AS PackageTypeName
	FROM product.ImportDraft ID
	WHERE ID.Name IS NOT NULL
	ORDER BY 1, 2

OPEN oCursor
FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageSize, @cPackageTypeName
WHILE @@fetch_status = 0
	BEGIN

	SELECT TOP 1 
		@nImportProductID = ID.ImportProductID
	FROM product.ImportDraft ID
	WHERE ID.Name = @cProductName
		AND ID.Brewery = @cProducerName
		AND ID.Distributor = @cDistributorName
		AND ID.Style = @cProductStyleName
	ORDER BY ID.ImportProductID

	DELETE FROM @tOutput
	DELETE FROM @tProducer

	IF CHARINDEX('/', @cProducerName) > 0
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		SELECT 
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = LTT.ListItem), 0), 
			LTT.ListItem 
		FROM core.ListToTable(@cProducerName, '/') LTT

		END
	ELSE
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		VALUES
			(
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = @cProducerName), 0), 
			@cProducerName
			)

		END
	--ENDIF

	INSERT INTO product.Product
		(ProductName, PackageSize, PackageTypeID, ProductTypeID, ProductStyleID, ABV, IBU, ImportProductID)
	OUTPUT INSERTED.ProductID INTO @tOutput
	VALUES
		(
		@cProductName,
		@cPackageSize,
		ISNULL((SELECT PKT.PackageTypeID FROM dropdown.PackageType PKT WHERE PKT.PackageTypeName = @cPackageTypeName), 0),

		CASE
			WHEN @cProductStyleName LIKE '%Cider%'
			THEN (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Cider')
			ELSE (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Beer')
		END,

		ISNULL((SELECT PS.ProductStyleID FROM product.ProductStyle PS WHERE PS.ProductStyleName = @cProductStyleName), 0),
		@nABV, 
		@nIBU,
		@nImportProductID
		)

	INSERT INTO product.ProductProducer
		(ProductID, ProducerID)
	SELECT
		(SELECT O.ProductID FROM @tOutput O),
		P.ProducerID
	FROM @tProducer P

	print 'Imported ' + CAST(@nImportProductID AS VARCHAR(5)) + ' / Product: ' + @cProductName + ' / Producer: ' + @cProducerName
	
	FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageSize, @cPackageTypeName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

DECLARE @cDistributorName VARCHAR(250)
DECLARE @cPackageTypeName VARCHAR(50)
DECLARE @cProducerName VARCHAR(250)
DECLARE @cProductName VARCHAR(250)
DECLARE @cProductStyleName VARCHAR(250)
DECLARE @nABV NUMERIC(18,2)
DECLARE @nCharIndex INT
DECLARE @nIBU NUMERIC(18,2)
DECLARE @nImportProductID INT
DECLARE @tOutput TABLE (ProductID INT NOT NULL PRIMARY KEY)
DECLARE @tProducer TABLE (ProducerID INT, ProducerName VARCHAR(250))

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT DISTINCT
		LTRIM(IMP.Name) AS ProductName,
		IMP.Brewery AS ProducerName,
		IMP.Distributor AS DistributorName,

		CASE
			WHEN IMP.Style IN ('Amber Lager', 'American Amber', 'American Amber/Red Lager')
			THEN 'American Amber/Red Lager'
			WHEN IMP.Style IN ('American Craft Lager', 'American Lager')
			THEN 'American Lager'
			WHEN IMP.Style IN ('American Barleywine', 'Barleywine')
			THEN 'Barleywine'
			WHEN IMP.Style IN ('Belgian Style Quadruple', 'Quadrupel', 'Quadruple Style Ale')
			THEN 'Belgian Style Quadruple'
			WHEN IMP.Style IN ('Belgian Style Tripel Ale', 'Belgian Style Tripel')
			THEN 'Belgian Style Tripel'
			WHEN IMP.Style IN ('Belgian Tripel Style Ale', 'Belgian Tripel')
			THEN 'Belgian Tripel'
			WHEN IMP.Style IN ('Chile Beer', 'Chili Ale', 'Chili Beer')
			THEN 'Chile Beer'
			WHEN IMP.Style IN ('Doppelbock', 'Dopplebock')
			THEN 'Doppelbock'
			WHEN IMP.Style IN ('Fruit Beer', 'Fruit/Vegetable Beer')
			THEN 'Fruit/Vegetable Beer'
			WHEN IMP.Style IN ('Herb/Spiced Beer', 'Herbed/Spiced Beer')
			THEN 'Herb/Spiced Beer'
			WHEN IMP.Style IN ('German Alt/Kolsch', 'German Style Kolsch', 'Kolsch')
			THEN 'Kolsch'
			WHEN IMP.Style IN ('Pumpkin Yam Beer', 'Pumpkin/Yam Beer')
			THEN 'Pumpkin/Yam Beer'
			ELSE IMP.Style 
		END AS ProductStyleName,

		ISNULL(IMP.[%], 0) AS ABV,
		CASE WHEN ISNUMERIC(IMP.IBU) = 1 THEN CAST(IMP.IBU AS NUMERIC(18,2)) ELSE 0 END AS IBU,
		CASE WHEN IMP.Type = 'Bottles' THEN 'Bottle' ELSE IMP.Type END AS PackageTypeName
	FROM product.ImportPackage IMP
	WHERE IMP.Name IS NOT NULL

	ORDER BY 1, 2

OPEN oCursor
FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageTypeName
WHILE @@fetch_status = 0
	BEGIN

	SELECT TOP 1 
		@nImportProductID = IMP.ImportProductID
	FROM product.ImportPackage IMP
	WHERE IMP.Name = @cProductName
		AND IMP.Brewery = @cProducerName
		AND IMP.Distributor = @cDistributorName
		AND IMP.Style = @cProductStyleName
	ORDER BY IMP.ImportProductID

	DELETE FROM @tOutput
	DELETE FROM @tProducer

	IF CHARINDEX('/', @cProducerName) > 0
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		SELECT 
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = LTT.ListItem), 0), 
			LTT.ListItem 
		FROM core.ListToTable(@cProducerName, '/') LTT

		END
	ELSE
		BEGIN

		INSERT INTO @tProducer 
			(ProducerID, ProducerName) 
		VALUES
			(
			ISNULL((SELECT P.ProducerID FROM product.Producer P WHERE P.ProducerName = @cProducerName), 0), 
			@cProducerName
			)

		END
	--ENDIF

	INSERT INTO product.Product
		(ProductName, PackageTypeID, ProductTypeID, ProductStyleID, ABV, IBU, ImportProductID)
	OUTPUT INSERTED.ProductID INTO @tOutput
	VALUES
		(
		@cProductName,
		ISNULL((SELECT PKT.PackageTypeID FROM dropdown.PackageType PKT WHERE PKT.PackageTypeName = @cPackageTypeName), 0),

		CASE
			WHEN @cProductStyleName LIKE '%Cider%'
			THEN (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Cider')
			ELSE (SELECT PRT.ProductTypeID FROM dropdown.ProductType PRT WHERE PRT.ProductTypeName = 'Beer')
		END,

		ISNULL((SELECT PS.ProductStyleID FROM product.ProductStyle PS WHERE PS.ProductStyleName = @cProductStyleName), 0),
		@nABV, 
		@nIBU,
		@nImportProductID
		)

	INSERT INTO product.ProductProducer
		(ProductID, ProducerID)
	SELECT
		(SELECT O.ProductID FROM @tOutput O),
		P.ProducerID
	FROM @tProducer P

	print 'Imported ' + CAST(@nImportProductID AS VARCHAR(5)) + ' / Product: ' + @cProductName + ' / Producer: ' + @cProducerName
	
	FETCH oCursor INTO @cProductName, @cProducerName, @cDistributorName, @cProductStyleName, @nABV, @nIBU, @cPackageTypeName

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

UPDATE P SET P.ImagePath = '/assets/img/breweries/3floydslogo.jpg' FROM product.Producer P WHERE P.ProducerName = '3 Floyds'
UPDATE P SET P.ImagePath = '/assets/img/breweries/512logo.jpg' FROM product.Producer P WHERE P.ProducerName = '(512)'
UPDATE P SET P.ImagePath = '/assets/img/breweries/903logo.jpg' FROM product.Producer P WHERE P.ProducerName = '903'
UPDATE P SET P.ImagePath = '/assets/img/breweries/abitalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Abita'
UPDATE P SET P.ImagePath = '/assets/img/breweries/acelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ace'
UPDATE P SET P.ImagePath = '/assets/img/breweries/adelbertslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Adelbert''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alaskanlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alaskan'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alesmithlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'AleSmith'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alltechlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alltech'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alpinelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Alpine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/alvinnelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Alvinne'
UPDATE P SET P.ImagePath = '/assets/img/breweries/andersonvalleylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Anderson Valley'
UPDATE P SET P.ImagePath = '/assets/img/breweries/anheuserbuschlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Bud'
UPDATE P SET P.ImagePath = '/assets/img/breweries/apogeelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Apogee'
UPDATE P SET P.ImagePath = '/assets/img/breweries/arguslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Argus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/armadillologo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Armadillo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/atwaterlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Atwater'
UPDATE P SET P.ImagePath = '/assets/img/breweries/audacitylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Audacity'
UPDATE P SET P.ImagePath = '/assets/img/breweries/austinbeerworkslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Austin Beerworks'
UPDATE P SET P.ImagePath = '/assets/img/breweries/austineastciderslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Austin Eastciders'
UPDATE P SET P.ImagePath = '/assets/img/breweries/averylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Avery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ayingerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ayinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bacchuslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bacchus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/backcountrylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Backcountry'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bairdlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Baird'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ballastpointlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ballast Point'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bayoutechelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bayou Teche'
UPDATE P SET P.ImagePath = '/assets/img/breweries/beachwoodlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Beachwood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bearrepubliclogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bear Republic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bellslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bell''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bfmlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'BFM'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bigbendlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Big Bend'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bigskylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Big Sky'
UPDATE P SET P.ImagePath = '/assets/img/breweries/birradelborgologo.png' FROM product.Producer P WHERE P.ProducerName = 'Birra del Borgo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bishopciderlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Bishop'
UPDATE P SET P.ImagePath = '/assets/img/breweries/blueowllogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Blue Owl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/boulevardlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Boulevard'
UPDATE P SET P.ImagePath = '/assets/img/breweries/braindeadlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Braindead'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brashlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brash'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brasseriedesillylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brasserie de Silly'
UPDATE P SET P.ImagePath = '/assets/img/breweries/breckinridgelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Breckenridge'
UPDATE P SET P.ImagePath = '/assets/img/breweries/bridgeportlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Bridgeport'
UPDATE P SET P.ImagePath = '/assets/img/breweries/brooklynlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Brooklyn'
UPDATE P SET P.ImagePath = '/assets/img/breweries/buffalobayoulogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Buffalo Bayou'
UPDATE P SET P.ImagePath = '/assets/img/breweries/buffalobillslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Buffalo Bill''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cedarcreeklogo.png' FROM product.Producer P WHERE P.ProducerName = 'Cedar Creek'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cigarcitylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Cigar City'
UPDATE P SET P.ImagePath = '/assets/img/breweries/clownshoeslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Clown Shoes'
UPDATE P SET P.ImagePath = '/assets/img/breweries/cobralogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Cobra'
UPDATE P SET P.ImagePath = '/assets/img/breweries/collectivebrewingprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Collective Brewing'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coloradologo.png' FROM product.Producer P WHERE P.ProducerName = 'Colorado'
UPDATE P SET P.ImagePath = '/assets/img/breweries/communitylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Community'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coopaleworkslogo.png' FROM product.Producer P WHERE P.ProducerName = 'COOP Ale Works'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coorslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Coors'
UPDATE P SET P.ImagePath = '/assets/img/breweries/coronalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Corona'
UPDATE P SET P.ImagePath = '/assets/img/breweries/crabbieslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Crabbie''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/craftedartisanmeaderylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Crafted Artisan Meadery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/crazymountainlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Crazy Mountain'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deepellumlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Deep Ellum'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deliriumlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Delirium'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deproeflogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'De Proef'
UPDATE P SET P.ImagePath = '/assets/img/breweries/deschuteslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Deschutes'
UPDATE P SET P.ImagePath = '/assets/img/breweries/destihllogo.png' FROM product.Producer P WHERE P.ProducerName = 'Destihl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/detrochlogo.png' FROM product.Producer P WHERE P.ProducerName = 'De Troch'
UPDATE P SET P.ImagePath = '/assets/img/breweries/dogfishheadlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Dogfish Head'
UPDATE P SET P.ImagePath = '/assets/img/breweries/duvellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Duvel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/einbeckerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Einbecker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/elevationlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Elevation'
UPDATE P SET P.ImagePath = '/assets/img/breweries/elgobernadorlogo.png' FROM product.Producer P WHERE P.ProducerName = 'El Gobernador'
UPDATE P SET P.ImagePath = '/assets/img/breweries/epiclogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Epic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/erdingerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Erdinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/eviltwinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Evil Twin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/finnriverlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Finnriver'
UPDATE P SET P.ImagePath = '/assets/img/breweries/firestonewalkerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Firestone Walker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/flyingdoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Flying Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/founderslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Founders'
UPDATE P SET P.ImagePath = '/assets/img/breweries/fourcornerslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Four Corners'
UPDATE P SET P.ImagePath = '/assets/img/breweries/franziskanerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Franziskaner'
UPDATE P SET P.ImagePath = '/assets/img/breweries/fullsaillogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Full Sail'
UPDATE P SET P.ImagePath = '/assets/img/breweries/funkwerkslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Funkwerks'
UPDATE P SET P.ImagePath = '/assets/img/breweries/gavrochelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Gavroche'
UPDATE P SET P.ImagePath = '/assets/img/breweries/gooseislandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Goose Island'
UPDATE P SET P.ImagePath = '/assets/img/breweries/grapevinelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Grapevine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greatdividelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Great Divide'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greenflashlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Green Flash'
UPDATE P SET P.ImagePath = '/assets/img/breweries/greenslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Green''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/guinnesslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Guinness'
UPDATE P SET P.ImagePath = '/assets/img/breweries/halfacrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Half Acre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hebrewlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'He''Brew'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hereticlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Heretic'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hitachinonestlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hitachino Nest'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hollowsfentimanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hollows & Fentimans'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hopfusionlogo.png' FROM product.Producer P WHERE P.ProducerName = 'HopFusion'
UPDATE P SET P.ImagePath = '/assets/img/breweries/hopsgrainlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Hops & Grain'
UPDATE P SET P.ImagePath = '/assets/img/breweries/humboldtlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Humboldt'
UPDATE P SET P.ImagePath = '/assets/img/breweries/independencelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Independence'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ironmaidenlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Iron Maiden'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ishiilogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ishii'
UPDATE P SET P.ImagePath = '/assets/img/breweries/jesterkinglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Jester King'
UPDATE P SET P.ImagePath = '/assets/img/breweries/jollypumpkinlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Jolly Pumpkin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/juliusechterlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Julius Echter'
UPDATE P SET P.ImagePath = '/assets/img/breweries/karbachlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Karbach'
UPDATE P SET P.ImagePath = '/assets/img/breweries/kasteellogo.png' FROM product.Producer P WHERE P.ProducerName = 'Kasteel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/kostritzerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Kostritzer'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lagunitaslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lagunitas'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lakewoodlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Lakewood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/laughingdoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Laughing Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lazymagnolialogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lazy Magnolia'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lefebvrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lefebvre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leffelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leffe'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lefthandlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Left Hand'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leibingerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leibinger'
UPDATE P SET P.ImagePath = '/assets/img/breweries/leprechaunlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Leprechaun'
UPDATE P SET P.ImagePath = '/assets/img/breweries/les3fourquetslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Les 3 Fourquets'
UPDATE P SET P.ImagePath = '/assets/img/breweries/liefmanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Liefman''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/liveoaklogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Live Oak'
UPDATE P SET P.ImagePath = '/assets/img/breweries/locustlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Locust'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lonepintlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Lone Pint'
UPDATE P SET P.ImagePath = '/assets/img/breweries/lonestarlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Lone Star'
UPDATE P SET P.ImagePath = '/assets/img/breweries/magichatlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Magic Hat'
UPDATE P SET P.ImagePath = '/assets/img/breweries/martinhouselogo.png' FROM product.Producer P WHERE P.ProducerName = 'Martin House'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mauilogo.png' FROM product.Producer P WHERE P.ProducerName = 'Maui'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mckenzieslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Mckenzie''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/meridianhivelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Meridian Hive'
UPDATE P SET P.ImagePath = '/assets/img/breweries/mikkellerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Mikkeller'
UPDATE P SET P.ImagePath = '/assets/img/breweries/millerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Miller'
UPDATE P SET P.ImagePath = '/assets/img/breweries/missionlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Mission'
UPDATE P SET P.ImagePath = '/assets/img/breweries/modelologo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Modelo'
UPDATE P SET P.ImagePath = '/assets/img/breweries/monkslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Monk''s'
UPDATE P SET P.ImagePath = '/assets/img/breweries/moonlightmeaderylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Moonlight Meadery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/moylanslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Moylans'
UPDATE P SET P.ImagePath = '/assets/img/breweries/naughtylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Naughty'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nebraskalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Nebraska'
UPDATE P SET P.ImagePath = '/assets/img/breweries/newbelgiumlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'New Belgium'
UPDATE P SET P.ImagePath = '/assets/img/breweries/newhollandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'New Holland'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ninkasilogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Ninkasi'
UPDATE P SET P.ImagePath = '/assets/img/breweries/noblereylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Noble Rey'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nolabellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'No Label'
UPDATE P SET P.ImagePath = '/assets/img/breweries/nolilogo.png' FROM product.Producer P WHERE P.ProducerName = 'No-Li'
UPDATE P SET P.ImagePath = '/assets/img/breweries/northcoastlogo.png' FROM product.Producer P WHERE P.ProducerName = 'North Coast'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oasislogo.png' FROM product.Producer P WHERE P.ProducerName = 'Oasis'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oddwoodlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Oddwood'
UPDATE P SET P.ImagePath = '/assets/img/breweries/odelllogo.png' FROM product.Producer P WHERE P.ProducerName = 'Odell'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oldspeckledhenlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Old Speckled Hen'
UPDATE P SET P.ImagePath = '/assets/img/breweries/ommeganglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ommegang'
UPDATE P SET P.ImagePath = '/assets/img/breweries/oskarblueslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Oskar Blues'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pantherislandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Panther Island'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pedernaleslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Pedernales'
UPDATE P SET P.ImagePath = '/assets/img/breweries/perenniallogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Perennial'
UPDATE P SET P.ImagePath = '/assets/img/breweries/peticolaslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Peticolas'
UPDATE P SET P.ImagePath = '/assets/img/breweries/petruslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Petrus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/pinkuslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Pinkus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/poperingslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Poperings'
UPDATE P SET P.ImagePath = '/assets/img/breweries/prairielogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Prairie'
UPDATE P SET P.ImagePath = '/assets/img/breweries/professorfritzbriemlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Professor Fritz Briem'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rabbitholelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rabbit Hole'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rahrlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rahr'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rangercreeklogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Ranger Creek'
UPDATE P SET P.ImagePath = '/assets/img/breweries/realalelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Real Ale'
UPDATE P SET P.ImagePath = '/assets/img/breweries/redstonelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Redstone'
UPDATE P SET P.ImagePath = '/assets/img/breweries/revolverlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Revolver'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rincelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rince'
UPDATE P SET P.ImagePath = '/assets/img/breweries/robinsonslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Robinsons'
UPDATE P SET P.ImagePath = '/assets/img/breweries/rognesslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Rogness'
UPDATE P SET P.ImagePath = '/assets/img/breweries/roguelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Rogue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/russianriverlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Russian River'
UPDATE P SET P.ImagePath = '/assets/img/breweries/saintarnoldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Saint Arnold'
UPDATE P SET P.ImagePath = '/assets/img/breweries/saisonlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Saison'
UPDATE P SET P.ImagePath = '/assets/img/breweries/samadamslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sam Adams'
UPDATE P SET P.ImagePath = '/assets/img/breweries/santafelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Santa Fe'
UPDATE P SET P.ImagePath = '/assets/img/breweries/santanlogo.png' FROM product.Producer P WHERE P.ProducerName = 'SanTan'
UPDATE P SET P.ImagePath = '/assets/img/breweries/savetheworldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Save The World'
UPDATE P SET P.ImagePath = '/assets/img/breweries/schillinglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Schilling '
UPDATE P SET P.ImagePath = '/assets/img/breweries/schlenkerlalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Schlenkerla'
UPDATE P SET P.ImagePath = '/assets/img/breweries/schmaltzlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Schmaltz '
UPDATE P SET P.ImagePath = '/assets/img/breweries/seadoglogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sea Dog'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shacksburylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shacksbury'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shannonlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shannon'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shinerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Shiner'
UPDATE P SET P.ImagePath = '/assets/img/breweries/shipyardlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Shipyard'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sierramadrelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Sierra Madre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sierranevadalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Sierra Nevada'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sixpointlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sixpoint'
UPDATE P SET P.ImagePath = '/assets/img/breweries/skalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Ska'
UPDATE P SET P.ImagePath = '/assets/img/breweries/smallbrewpublogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Small Brewpub'
UPDATE P SET P.ImagePath = '/assets/img/breweries/smuttynoselogo.png' FROM product.Producer P WHERE P.ProducerName = 'Smuttynose'
UPDATE P SET P.ImagePath = '/assets/img/breweries/southernstarlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Southern Star'
UPDATE P SET P.ImagePath = '/assets/img/breweries/southerntierlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Southern Tier'
UPDATE P SET P.ImagePath = '/assets/img/breweries/squatterslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Squatters'
UPDATE P SET P.ImagePath = '/assets/img/breweries/starnoldlogo.png' FROM product.Producer P WHERE P.ProducerName = 'St Arnold'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stellalogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stella'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stevenspointlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stevens Point'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stfeuillienlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'St. Feuillien'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stiegllogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Stiegl'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stlouisbeerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'St Louis'
UPDATE P SET P.ImagePath = '/assets/img/breweries/stonelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Stone'
UPDATE P SET P.ImagePath = '/assets/img/breweries/strangelandlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Strange Land'
UPDATE P SET P.ImagePath = '/assets/img/breweries/summitlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Summit'
UPDATE P SET P.ImagePath = '/assets/img/breweries/surlylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Surly'
UPDATE P SET P.ImagePath = '/assets/img/breweries/sweetwaterlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Sweetwater'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tecatelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tecate'
UPDATE P SET P.ImagePath = '/assets/img/breweries/terrapinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Terrapin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/terrapinlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Terrapin'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texasaleprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Texas Ale Project'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texaskeeperlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Texas Keeper'
UPDATE P SET P.ImagePath = '/assets/img/breweries/texasselectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Texas Select'
UPDATE P SET P.ImagePath = '/assets/img/breweries/thebruerylogo.gif' FROM product.Producer P WHERE P.ProducerName = 'The Bruery'
UPDATE P SET P.ImagePath = '/assets/img/breweries/themanhattanprojectlogo.png' FROM product.Producer P WHERE P.ProducerName = 'The Manhattan Project'
UPDATE P SET P.ImagePath = '/assets/img/breweries/threenationslogo.png' FROM product.Producer P WHERE P.ProducerName = 'Three Nations'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tietonlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tieton'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tommyknockerlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Tommyknocker'
UPDATE P SET P.ImagePath = '/assets/img/breweries/toollogo.png' FROM product.Producer P WHERE P.ProducerName = 'To Ol'
UPDATE P SET P.ImagePath = '/assets/img/breweries/tuppslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Tupps'
UPDATE P SET P.ImagePath = '/assets/img/breweries/twistedpinelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Twisted Pine'
UPDATE P SET P.ImagePath = '/assets/img/breweries/twistedxlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Twisted X'
UPDATE P SET P.ImagePath = '/assets/img/breweries/uintalogo.png' FROM product.Producer P WHERE P.ProducerName = 'Uinta'
UPDATE P SET P.ImagePath = '/assets/img/breweries/unibrouelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Unibroue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/unitylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Unity'
UPDATE P SET P.ImagePath = '/assets/img/breweries/uplandlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Upland'
UPDATE P SET P.ImagePath = '/assets/img/breweries/upslopelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Upslope'
UPDATE P SET P.ImagePath = '/assets/img/breweries/urbanfamilylogo.png' FROM product.Producer P WHERE P.ProducerName = 'Urban Family'
UPDATE P SET P.ImagePath = '/assets/img/breweries/urthellogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Urthel'
UPDATE P SET P.ImagePath = '/assets/img/breweries/vandiestlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Van Diest'
UPDATE P SET P.ImagePath = '/assets/img/breweries/verzetlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Verzet'
UPDATE P SET P.ImagePath = '/assets/img/breweries/victorylogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Victory'
UPDATE P SET P.ImagePath = '/assets/img/breweries/virtuelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Virtue'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wanderingaenguslogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wandering Aengus'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wasatchlogo.png' FROM product.Producer P WHERE P.ProducerName = 'Wasatch'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wellsandyounglogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Well''s & Young'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wexfordlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wexford'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wildacrelogo.png' FROM product.Producer P WHERE P.ProducerName = 'Wild Acre'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wildbeerlogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wild Beer'
UPDATE P SET P.ImagePath = '/assets/img/breweries/woodchucklogo.png' FROM product.Producer P WHERE P.ProducerName = 'Woodchuck'
UPDATE P SET P.ImagePath = '/assets/img/breweries/wyndridgelogo.jpg' FROM product.Producer P WHERE P.ProducerName = 'Wyndridge'
GO

UPDATE D SET D.ImagePath = '/assets/img/breweries/512logo.jpg' FROM product.Distributor D WHERE D.DistributorName = '(512)'
UPDATE D SET D.ImagePath = '/assets/img/breweries/austinbeerworkslogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Austin Beerworks'
UPDATE D SET D.ImagePath = '/assets/img/breweries/buffalobayoulogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Buffalo Bayou'
UPDATE D SET D.ImagePath = '/assets/img/breweries/communitylogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Community'
UPDATE D SET D.ImagePath = '/assets/img/breweries/fourcornerslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Four Corners'
UPDATE D SET D.ImagePath = '/assets/img/breweries/grapevinelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Grapevine'
UPDATE D SET D.ImagePath = '/assets/img/breweries/hopfusionlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'HopFusion'
UPDATE D SET D.ImagePath = '/assets/img/breweries/independencelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Independence'
UPDATE D SET D.ImagePath = '/assets/img/breweries/liveoaklogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Live Oak'
UPDATE D SET D.ImagePath = '/assets/img/breweries/martinhouselogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Martin House'
UPDATE D SET D.ImagePath = '/assets/img/breweries/pantherislandlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Panther Island'
UPDATE D SET D.ImagePath = '/assets/img/breweries/peticolaslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Peticolas'
UPDATE D SET D.ImagePath = '/assets/img/breweries/revolverlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Revolver'
UPDATE D SET D.ImagePath = '/assets/img/breweries/savetheworldlogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Save The World'
UPDATE D SET D.ImagePath = '/assets/img/breweries/shannonlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Shannon'
UPDATE D SET D.ImagePath = '/assets/img/breweries/texaskeeperlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Texas Keeper'
UPDATE D SET D.ImagePath = '/assets/img/breweries/threenationslogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Three Nations'
UPDATE D SET D.ImagePath = '/assets/img/breweries/twistedxlogo.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Twisted X'
UPDATE D SET D.ImagePath = '/assets/img/breweries/wildacrelogo.png' FROM product.Distributor D WHERE D.DistributorName = 'Wild Acre'
UPDATE D SET D.ImagePath = '/assets/img/distributors/andrews.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Andrews'
UPDATE D SET D.ImagePath = '/assets/img/distributors/beardedeel.png' FROM product.Distributor D WHERE D.DistributorName = 'Bearded Eel'
UPDATE D SET D.ImagePath = '/assets/img/distributors/benekeith.png' FROM product.Distributor D WHERE D.DistributorName = 'Ben E Keith'
UPDATE D SET D.ImagePath = '/assets/img/distributors/cuveecoffee.png' FROM product.Distributor D WHERE D.DistributorName = 'Cuvee Coffee'
UPDATE D SET D.ImagePath = '/assets/img/distributors/favoritebrands.png' FROM product.Distributor D WHERE D.DistributorName = 'Favorite Brands'
UPDATE D SET D.ImagePath = '/assets/img/distributors/flood.png' FROM product.Distributor D WHERE D.DistributorName = 'Flood'
UPDATE D SET D.ImagePath = '/assets/img/distributors/fullclip.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Full Clip'
UPDATE D SET D.ImagePath = '/assets/img/distributors/fullclip.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Fullclip'
UPDATE D SET D.ImagePath = '/assets/img/distributors/oakhighlands.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Oak Highlands'
UPDATE D SET D.ImagePath = '/assets/img/distributors/pioneer.png' FROM product.Distributor D WHERE D.DistributorName = 'Pioneer'
UPDATE D SET D.ImagePath = '/assets/img/distributors/sonsofjohn.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Sons of John'
UPDATE D SET D.ImagePath = '/assets/img/distributors/tupps.jpg' FROM product.Distributor D WHERE D.DistributorName = 'Tupps Brewery'
UPDATE D SET D.ImagePath = '/assets/img/distributors/whistlepost.png' FROM product.Distributor D WHERE D.DistributorName = 'Whistle Post'
GO

;
WITH BD AS
	(
	SELECT 
		P.ProductID, 
		P.ProductName,
		P.ProductStyleID,
		(SELECT IMP.Style FROM product.ImportPackage IMP WHERE IMP.ImportProductID = P.ProductID) AS ProductStyleName
	FROM product.Product P
		JOIN dropdown.PackageType PKT ON PKT.PackageTypeID = P.PackageTypeID
			AND P.ProductStyleID = 0 
	)

UPDATE P
SET P.ProductStyleID = PS.ProductStyleID
FROM product.Product P
	JOIN BD ON BD.ProductID = P.ProductID
	JOIN product.ProductStyle PS ON PS.ProductStyleName = BD.ProductStyleName
		AND BD.ProductStyleName IS NOT NULL
GO

TRUNCATE TABLE client.ClientProduct
GO

DECLARE @nProductID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		P.ProductID
	FROM product.Product P
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
			AND PT.PackageTypeName = 'Draft'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @nProductID
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO client.ClientProduct
		(ClientID,ProductID,DistributorID,ServingSize,ServingCount,PackagePrice,MenuPrice)
	SELECT
		1 AS ClientID,
		D.ProductID,
		D.DistributorID,
		D.ServingSize,
		D.ServingCount,
		D.PackagePrice,
		D.MenuPrice
	FROM
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY ID.[Size(gal)] ORDER BY ISNULL(ID.[Keg Price], 0) DESC) AS RowIndex,
			P.ProductID,
			ISNULL((SELECT D.DistributorID FROM product.Distributor D WHERE D.DistributorName = ID.Distributor), 0) AS DistributorID,
			ID.[SpecGlass(oz)] AS ServingSize,
			ISNULL(ID.Servings, 0) AS ServingCount,
			ISNULL(ID.[Keg Price], 0) AS PackagePrice,
			ISNULL(ID.F10, 0) AS MenuPrice
		FROM product.ImportDraft ID
			JOIN product.Product P ON P.ProductName = ID.Name
				AND P.ProductID = @nProductID
				AND P.PackageSize = ID.[Size(gal)]
		) D
	WHERE D.RowIndex = 1

	FETCH oCursor INTO @nProductID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

DECLARE @nProductID INT

DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
	SELECT
		P.ProductID
	FROM product.Product P
		JOIN dropdown.PackageType PT ON PT.PackageTypeID = P.PackageTypeID
			AND PT.PackageTypeName <> 'Draft'
	ORDER BY 1

OPEN oCursor
FETCH oCursor INTO @nProductID
WHILE @@fetch_status = 0
	BEGIN

	INSERT INTO client.ClientProduct
		(ClientID,ProductID,DistributorID,ServingSize,ServingCount,PackagePrice,InStock,MenuPrice)
	SELECT
		1 AS ClientID,
		D.ProductID,
		D.DistributorID,
		D.ServingSize,
		D.ServingCount,
		D.PackagePrice,
		D.InStock,
		D.MenuPrice
	FROM
		(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY IP.Size ORDER BY ISNULL(IP.[Sale Price], 0) DESC) AS RowIndex,
			P.ProductID,
			ISNULL((SELECT D.DistributorID FROM product.Distributor D WHERE D.DistributorName = IP.Distributor), 0) AS DistributorID,
			IP.Size AS ServingSize,
			ISNULL(IP.Count, 0) AS ServingCount,
			ISNULL(IP.[Case Price], 0) AS PackagePrice,
			ISNULL(IP.[On Hand], 0) AS InStock,
			ISNULL(IP.[Sale Price], 0) AS MenuPrice
		FROM product.ImportPackage IP
			JOIN product.Product P ON P.ProductName = IP.Name
				AND P.ProductID = @nProductID
		) D
	WHERE D.RowIndex = 1

	FETCH oCursor INTO @nProductID

	END
--END WHILE

CLOSE oCursor
DEALLOCATE oCursor
GO

;
WITH PMD AS
	(
	SELECT
		PM.PricingModelID,
		CP.ClientProductID,
		ROUND(((CP.PackagePrice / CP.ServingCount * C.Basefactor * PM.PricingModelFactor) + C.BaseCharge + PM.PricingModelCharge + CP.UpCharge) / C.BaseRound, 0) * C.BaseRound AS ModelPrice
	FROM client.PricingModel PM
		JOIN client.Client C ON C.ClientID = PM.ClientID
		JOIN client.ClientProduct CP ON CP.ClientID = PM.ClientID
			AND ISNULL(CP.ServingCount , 0) > 0
	)

UPDATE CP
SET 
	CP.MenuPriceDateTime = getDate(),
	CP.MenuPricePersonID = 1,
	CP.PricingModelID = PMD.PricingModelID
FROM client.ClientProduct CP
	JOIN PMD ON PMD.ClientProductID = CP.ClientProductID
		AND PMD.ModelPrice = CP.MenuPrice
GO