--Begin table core.EntityType
TRUNCATE TABLE core.EntityType
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Announcement', 
	@EntityTypeName = 'Announcement', 
	@EntityTypeNamePlural = 'Announcements',
	@SchemaName = 'core', 
	@TableName = 'Announcement', 
	@PrimaryKeyFieldName = 'AnnouncementID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Client',
	@EntityTypeName = 'Client',
	@EntityTypeNamePlural = 'Clients',
	@SchemaName = 'client',
	@TableName = 'Client',
	@PrimaryKeyFieldName = 'ClientID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Distributor',
	@EntityTypeName = 'Distributor',
	@EntityTypeNamePlural = 'Distributors',
	@SchemaName = 'product',
	@TableName = 'Distributor',
	@PrimaryKeyFieldName = 'DistributorID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'EventLog', 
	@EntityTypeName = 'Event Log', 
	@EntityTypeNamePlural = 'Event Log',
	@SchemaName = 'eventlog', 
	@TableName = 'EventLog', 
	@PrimaryKeyFieldName = 'EventLogID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Person', 
	@EntityTypeName = 'Person', 
	@EntityTypeNamePlural = 'People',
	@SchemaName = 'person', 
	@TableName = 'Person', 
	@PrimaryKeyFieldName = 'PersonID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Producer',
	@EntityTypeName = 'Producer',
	@EntityTypeNamePlural = 'Producers',
	@SchemaName = 'product',
	@TableName = 'Producer',
	@PrimaryKeyFieldName = 'ProducerID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'Product',
	@EntityTypeName = 'Product',
	@EntityTypeNamePlural = 'Products',
	@SchemaName = 'product',
	@TableName = 'Product',
	@PrimaryKeyFieldName = 'ProductID'
GO

EXEC core.EntityTypeAddUpdate 
	@EntityTypeCode = 'SystemSetup', 
	@EntityTypeName = 'System Setup Key', 
	@EntityTypeNamePlural = 'System Setup Keys',
	@SchemaName = 'core', 
	@TableName = 'SystemSetup', 
	@PrimaryKeyFieldName = 'SystemSetupID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
TRUNCATE TABLE core.SystemSetup
GO

EXEC core.SystemSetupAddUpdate 'Environment', NULL, 'Dev'
EXEC core.SystemSetupAddUpdate 'FeedBackMailTo', NULL, 'todd.pires@oceandisc.com,stchris2opher@gmail.com'
EXEC core.SystemSetupAddUpdate 'InvalidLoginLimit', NULL, '3'
EXEC core.SystemSetupAddUpdate 'NetworkName', NULL, 'Development'
EXEC core.SystemSetupAddUpdate 'NoReply', NULL, 'NoReply@menumaker.com'
EXEC core.SystemSetupAddUpdate 'ShowDevEnvironmentMessage', NULL, '0'
EXEC core.SystemSetupAddUpdate 'SiteURL', NULL, 'http://MenuMaker.christophercrouch.com'
EXEC core.SystemSetupAddUpdate 'SystemName', NULL, 'MenuMaker'
GO
--End table core.SystemSetup

--Begin table dropdown.Country
TRUNCATE TABLE dropdown.Country
GO

EXEC utility.InsertIdentityValue 'dropdown.Country', 'CountryID', 0
GO

INSERT INTO dropdown.Country
	(ISOCountryCode2, ISOCountryCode3, CountryName, DisplayOrder) 
VALUES 
	(N'AT', N'AUT', N'Austria', 99),
	(N'AU', N'AUS', N'Australia', 99),
	(N'BE', N'BEL', N'Belgium', 99),
	(N'CA', N'CAN', N'Canada', 99),
	(N'CH', N'CHE', N'Switzerland', 99),
	(N'DE', N'DEU', N'Germany', 99),
	(N'DK', N'DNK', N'Denmark', 99),
	(N'EN', N'ENG', N'England', 99),
	(N'ES', N'ESP', N'Spain', 99),
	(N'FR', N'FRA', N'France', 99),
	(N'GU', N'GUM', N'Guam', 99),
	(N'IE', N'IRL', N'Ireland', 99),
	(N'IT', N'ITA', N'Italy', 99),
	(N'JP', N'JPN', N'Japan', 99),
	(N'MX', N'MEX', N'Mexico', 99),
	(N'NL', N'NLD', N'Netherlands', 99),
	(N'NO', N'NOR', N'Norway', 99),
	(N'PH', N'PHL', N'Philippines', 99),
	(N'RU', N'RUS', N'Russian Federation', 99),
	(N'SC', N'SCO', N'Scotland', 99),
	(N'SE', N'SWE', N'Sweden', 99),
	(N'US', N'USA', N'United States', 1)
GO
--End table dropdown.Country

--Begin table dropdown.Region
TRUNCATE TABLE dropdown.Region
GO

EXEC utility.InsertIdentityValue 'dropdown.Region', 'RegionID', 0
GO

INSERT INTO dropdown.Region
	(ISOCountryCode3, ISORegionCode2, RegionName) 
VALUES 
	('USA', 'AL','Alabama'),
	('USA', 'AK','Alaska'),
	('USA', 'AZ','Arizona'),
	('USA', 'AR','Arkansas'),
	('USA', 'CA','California'),
	('USA', 'CO','Colorado'),
	('USA', 'CT','Connecticut'),
	('USA', 'DE','Delaware'),
	('USA', 'DC','District of Columbia'),
	('USA', 'FL','Florida'),
	('USA', 'GA','Georgia'),
	('USA', 'HI','Hawaii'),
	('USA', 'ID','Idaho'),
	('USA', 'IL','Illinois'),
	('USA', 'IN','Indiana'),
	('USA', 'IA','Iowa'),
	('USA', 'KS','Kansas'),
	('USA', 'KY','Kentucky'),
	('USA', 'LA','Louisiana'),
	('USA', 'ME','Maine'),
	('USA', 'MD','Maryland'),
	('USA', 'MA','Massachusetts'),
	('USA', 'MI','Michigan'),
	('USA', 'MN','Minnesota'),
	('USA', 'MS','Mississippi'),
	('USA', 'MO','Missouri'),
	('USA', 'MT','Montana'),
	('USA', 'NE','Nebraska'),
	('USA', 'NV','Nevada'),
	('USA', 'NH','New Hampshire'),
	('USA', 'NJ','New Jersey'),
	('USA', 'NM','New Mexico'),
	('USA', 'NY','New York'),
	('USA', 'NC','North Carolina'),
	('USA', 'ND','North Dakota'),
	('USA', 'OH','Ohio'),
	('USA', 'OK','Oklahoma'),
	('USA', 'OR','Oregon'),
	('USA', 'PA','Pennsylvania'),
	('USA', 'PR','Puerto Rico'),
	('USA', 'RI','Rhode Island'),
	('USA', 'SC','South Carolina'),
	('USA', 'SD','South Dakota'),
	('USA', 'TN','Tennessee'),
	('USA', 'TX','Texas'),
	('USA', 'UT','Utah'),
	('USA', 'VT','Vermont'),
	('USA', 'VA','Virginia'),
	('USA', 'VI','Virgin Islands'),
	('USA', 'WA','Washington'),
	('USA', 'WV','West Virginia'),
	('USA', 'WI','Wisconsin'),
	('USA', 'WY','Wyoming')
GO
--End table dropdown.Region

--Begin table dropdown.PackageType
TRUNCATE TABLE dropdown.PackageType
GO

EXEC utility.InsertIdentityValue 'dropdown.PackageType', 'PackageTypeID', 0
GO

INSERT INTO dropdown.PackageType
	(PackageTypeName, PackageTypeSizePlural, DisplayOrder) 
VALUES 
	('Bomber', 'Bombers', 4),
	('Bottle', 'Bottles', 3),
	('Can', 'Cans', 2),
	('Draft', 'Gallons', 1)
GO
--End table dropdown.PackageType

--Begin table dropdown.ProductType
TRUNCATE TABLE dropdown.ProductType
GO

EXEC utility.InsertIdentityValue 'dropdown.ProductType', 'ProductTypeID', 0
GO

INSERT INTO dropdown.ProductType
	(ProductTypeName, IconHTML) 
VALUES 
	('Beer', '<i class="fas fa-beer"></i>'),
	('Cider', '<i class="fas fa-wine-bottle"></i>'),
	('Wine', '<i class="fas fa-wine-glass-alt"></i>')
GO
--End table dropdown.ProductType

--Begin table dropdown.Status
TRUNCATE TABLE dropdown.Status
GO

EXEC utility.InsertIdentityValue 'dropdown.Status', 'StatusID', 0
GO

INSERT INTO dropdown.Status
	(StatusCode, StatusName, DisplayOrder) 
VALUES 
	('Active', 'Active', 1),
	('Pending', 'Pending', 2),
	('Deleted', 'Deleted', 3)
GO
--End table dropdown.Status

--Begin table client.Client
TRUNCATE TABLE client.Client
GO

INSERT INTO client.Client
	(ClientName, EmailAddress, FirstName, LastName, WebsiteURL, StatusID, Address1, Municipality, ISORegionCode2, PostalCode, ISOCountryCode3, BaseCharge, BaseFactor, BaseRound)
VALUES
	(
	'Magnolia Tree Tavern',
	'eclayton@caprocktexas.com',
	'Eric',
	'Clayton',
	'http://www.magnoliatreetavern.com',
	(SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Active'),
	'1229 7th Ave',
	'Fort Worth',
	'TX',
	'76104',
	'USA',
	1.25,
	1,
	.5
	),
	(
	'Chris'' Place',
	'stchris2opher@gmail.com',
	'Chris',
	'Crouch',
	'http://www.chriscrouch.com',
	(SELECT S.StatusID FROM dropdown.Status S WHERE S.StatusCode = 'Active'),
	'6036 Red Drum Drive',
	'Fort Worth',
	'TX',
	'76179',
	'USA',
	1.75,
	2,
	.25
	)

GO
--End table client.Client

--Begin table client.PricingModel
TRUNCATE TABLE client.PricingModel
GO

INSERT INTO client.PricingModel
	(ClientID, PricingModelName, PricingModelCharge, PricingModelFactor)
VALUES
	(1, 'x 2.0', 0, 2),
	(1, 'x 2.5', 0, 2.5),
	(1, 'x 3.0', 0, 3),
	(2, 'x 2.25', 0, 2.25),
	(2, 'x 2.50', 0, 2.5),
	(2, 'x 2.75', 0, 2.75)	
GO
--End table client.PricingModel

--Begin table person.Person
TRUNCATE TABLE person.Person
GO

INSERT INTO person.Person
	(FirstName,LastName,Title,EmailAddress,LastLoginDateTime,InvalidLoginAttempts,IsAccountLockedOut,IsActive,IsSuperAdministrator,Password,PasswordSalt,PasswordExpirationDateTime)
VALUES
	('Christopher', 'Crouch', 'Mr.', 'stchris2opher@gmail.com', NULL, 0, 0, 1, 1, '2DE46DAAD0102EEC8D1C73B287D154595FA09345D88E20DDF418B5FC6CB8ED0F', 'C9B13203-3259-4128-8221-C18D5C2BDAF1', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Todd', 'Pires', 'Mr.', 'todd.pires@oceandisc.com', NULL, 0, 0, 1, 1, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2020-01-01T00:00:00.000' AS DateTime)),
	('Client1', 'Test User', 'Mr.', 'testuser@client1.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2018-12-01T00:00:00.000' AS DateTime)),
	('Client2', 'Test User', 'Mr.', 'testuser@client2.com', NULL, 0, 0, 1, 0, '492EBFC243D3F610D07815C0941FF98294E26424AC394471085608AD16FE0CD3', '5DA6DA46-B0D0-4538-BEFA-E84BB4288C07', CAST('2018-12-01T00:00:00.000' AS DateTime))
GO
--End table person.Person

--Begin table client.ClientPerson
TRUNCATE TABLE client.ClientPerson
GO

INSERT INTO client.ClientPerson
	(ClientID, PersonID, IsClientAdministrator)
SELECT
	CAST(RIGHT(P.FirstName, 1) AS INT),
	P.PersonID,
	1
FROM person.Person P
WHERE P.FirstName LIKE 'Client%'
GO
--End table client.ClientPerson

--Begin table product.Distributor
TRUNCATE TABLE product.Distributor
GO

EXEC utility.InsertIdentityValue 'product.Distributor', 'DistributorID', 0
GO

INSERT INTO product.Distributor (DistributorName) VALUES ('(512)');
INSERT INTO product.Distributor (DistributorName) VALUES ('Andrews');
INSERT INTO product.Distributor (DistributorName) VALUES ('Austin Beerworks');
INSERT INTO product.Distributor (DistributorName) VALUES ('Bearded Eel');
INSERT INTO product.Distributor (DistributorName) VALUES ('Ben E Keith');
INSERT INTO product.Distributor (DistributorName) VALUES ('Buffalo Bayou');
INSERT INTO product.Distributor (DistributorName) VALUES ('Community');
INSERT INTO product.Distributor (DistributorName) VALUES ('Cuvee Coffee');
INSERT INTO product.Distributor (DistributorName) VALUES ('Favorite Brands');
INSERT INTO product.Distributor (DistributorName) VALUES ('Flood');
INSERT INTO product.Distributor (DistributorName) VALUES ('Four Corners');
INSERT INTO product.Distributor (DistributorName) VALUES ('Full Clip');
INSERT INTO product.Distributor (DistributorName) VALUES ('Fullclip');
INSERT INTO product.Distributor (DistributorName) VALUES ('Grapevine');
INSERT INTO product.Distributor (DistributorName) VALUES ('HopFusion');
INSERT INTO product.Distributor (DistributorName) VALUES ('Independence');
INSERT INTO product.Distributor (DistributorName) VALUES ('Live Oak');
INSERT INTO product.Distributor (DistributorName) VALUES ('Martin House');
INSERT INTO product.Distributor (DistributorName) VALUES ('Oak Highlands');
INSERT INTO product.Distributor (DistributorName) VALUES ('Panther Island');
INSERT INTO product.Distributor (DistributorName) VALUES ('Peticolas');
INSERT INTO product.Distributor (DistributorName) VALUES ('Pioneer');
INSERT INTO product.Distributor (DistributorName) VALUES ('Rainwater Bev');
INSERT INTO product.Distributor (DistributorName) VALUES ('Revolver');
INSERT INTO product.Distributor (DistributorName) VALUES ('Save The World');
INSERT INTO product.Distributor (DistributorName) VALUES ('Shannon');
INSERT INTO product.Distributor (DistributorName) VALUES ('Sons of John');
INSERT INTO product.Distributor (DistributorName) VALUES ('Texas Keeper');
INSERT INTO product.Distributor (DistributorName) VALUES ('Three Nations');
INSERT INTO product.Distributor (DistributorName) VALUES ('Tupps Brewery');
INSERT INTO product.Distributor (DistributorName) VALUES ('Twisted X');
INSERT INTO product.Distributor (DistributorName) VALUES ('Whistle Post');
INSERT INTO product.Distributor (DistributorName) VALUES ('Wild Acre');
GO
--End table product.Distributor

--Begin table product.Producer
TRUNCATE TABLE product.Producer
GO

EXEC utility.InsertIdentityValue 'product.Producer', 'ProducerID', 0
GO

INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('(512)','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('903','Sherman','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('3 Floyds','Munster','IN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Abita','Abita Springs','LA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ace','Sebastopol','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Adelbert''s','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alaskan','Juneau','AK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('AleSmith','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alltech','Lexington','KY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alpine','Alpine','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Alvinne','Zwevegem',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Anderson Valley','Booneville','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Apogee','Clear Lake','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Argus','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Armadillo','Denton','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Atwater','Detroit','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Audacity','Denton','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Austin Beerworks','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Austin Eastciders','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Avery','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ayinger','Aying',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bacchus','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Backcountry','Frisco','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Baird','Tamuning',NULL,'GUM')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ballast Point','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bayou Teche','Amaudville','LA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Beachwood','Long Beach','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bear Republic','Healdsburg','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bell''s','Kalamazoo','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('BFM','Saignelegier',NULL,'CHE')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Big Bend','Farmers Branch','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Big Sky','Missoula','MT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Birra del Borgo','Borgorose',NULL,'ITA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bishop','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Blue Owl','Austin ','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Boulevard','Kansas City','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Braindead','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brash','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brasserie de Silly','Silly',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Breckenridge','Denver','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bridgeport','Portland','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Brooklyn','Brooklyn','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Bud','Saint Louis','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Buffalo Bayou','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Buffalo Bill''s','Hayward','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cedar Creek','Kemp','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cigar City','Tampa','FL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Clown Shoes','Ipswich','MA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Cobra','Lewisville','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Collective Brewing','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Colorado','Colorado Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Community','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('COOP Ale Works','Oklahoma City','OK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Coors','Golden','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Corona','Mexico City',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crabbie''s','Glasgow',NULL,'SCO')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crafted Artisan Meadery','Mogadore','OH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Crazy Mountain','Edwards','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('De Troch','Wambeek',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Deep Ellum','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Delirium','Melle',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Deschutes','Bend','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Destihl','Bloomington','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Dogfish Head','Milton','DE','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Duvel','Breendonk-Puurs',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Einbecker','Einbeck',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('El Gobernador','Villaviciosa',NULL,'ESP')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Elevation','Poncha Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Epic','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Erdinger','Erding',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Evil Twin','Kobenhavn',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Finnriver','Chimacum','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Firestone Walker','Paso Robles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Flying Dog','Frederick','MD','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Founders','Grand Rapids','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Four Corners','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Franziskaner','Munchen',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Full Sail','Hood River','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Funkwerks','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Gavroche','Saint Sylvestre Cappel',NULL,'FRA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Goose Island','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Grapevine','Grapevine','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Great Divide','Denver','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Green Flash','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Green''s','Baildon Shipley',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Guinness','Dublin',NULL,'IRL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Half Acre','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('He''Brew','Clifton Park','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Heretic','Fairfield','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hitachino Nest','Ibaraki-ken Naka-gun',NULL,'JPN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hollows & Fentimans','Hexham',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('HopFusion','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Hops & Grain','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Humboldt','Paso Robles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Independence','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Iron Maiden','Stockport',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ishii','Izu-shi',NULL,'JPN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Jester King','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Jolly Pumpkin','Dexter','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Julius Echter','Wurzburg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Karbach','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Kasteel','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Kostritzer','Bad Köstritz',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lagunitas','Petiluma','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lakewood','Garland','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Laughing Dog','Ponderay','ID','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lazy Magnolia','Kiln','MS','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lefebvre','Rebecq-Quenast',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leffe','Dinant',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Left Hand','Longmont','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leibinger','Ravensburg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Leprechaun','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Les 3 Fourquets','Courtil',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Liefman''s','Oudenaarde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Live Oak','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Locust','Woodinville','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lone Pint','Magnolia','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Lone Star','Los Angeles','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Magic Hat','South Burlington','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Martin House','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Maui','Lahaina','HI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mckenzie''s','West Seneca ','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Meridian Hive','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mikkeller','Copenhagen',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Miller','Milwaukee','WI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Mission','San Diego','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Modelo','Mexico City',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Monk''s','Ertvelde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Moonlight Meadery','Londonderry','NH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Moylans','Novato','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Naughty','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Nebraska','Papillion','NE','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('New Belgium','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('New Holland','Holland','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ninkasi','Eugene ','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('No Label','Katy','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Noble Rey','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('No-Li','Spokane','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('North Coast','Fort Bragg','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oasis','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oddwood','Austin ','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Odell','Fort Collins','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Old Speckled Hen','Suffolk',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ommegang','Cooperstown','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Oskar Blues','Longmont','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Panther Island','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Pedernales','Fredericksburg','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Perennial','Saint Louis','MO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Peticolas','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Petrus','Bavikhove',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Pinkus','Munster',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Poperings','Watou',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Prairie','Krebs','OK','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Professor Fritz Briem','Freising',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rabbit Hole','Justin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rahr','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ranger Creek','San Antonio','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Real Ale','Blanco','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Redstone','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Revolver','Granbury','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rince','Oudenaarde',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Robinsons','Stockport',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rogness','Pflugerville','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Rogue','Newport','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Russian River','Santa Rosa','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Saint Arnold','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Saison','Tourpes',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sam Adams','Jamaica Plain','MA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Santa Fe','Santa Fe','NM','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('SanTan','Chandler','AZ','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Save The World','Marble Falls','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schilling ','Seattle','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schlenkerla','Bamberg',NULL,'DEU')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Schmaltz ','Clifton Park','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sea Dog','Bangor','ME','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shacksbury','Vergennes','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shannon','Keller','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shiner','Shiner','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Shipyard','Portland','ME','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sierra Madre','San Pedro',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sierra Nevada','Chico','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sixpoint','Brooklyn','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Ska','Durango','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Small Brewpub','Oak Cliff','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Smuttynose','Hampton','NH','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Southern Star','Conroe','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Southern Tier','Lakewood','NY','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Squatters','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St Arnold','Houston','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St Louis','Ingelmunster',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('St. Feuillien','Le Roeuix',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stella','Leuven',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stevens Point','Stevens Point','WI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stiegl','Salzburg',NULL,'AUT')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Stone','Escondido','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Strange Land','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Summit','Saint Paul','MN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Surly','Brooklyn Center','MN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('De Proef','Lochristi',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Sweetwater','Atlanta','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tecate','Monterrey',NULL,'MEX')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Terrapin','Athens','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Terrapin','Athens','GA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Ale Project','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Keeper','Austin','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Texas Select','San Antonio','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('The Bruery','Placentia','CA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('The Manhattan Project','Dallas','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Three Nations','Farmers Branch','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tieton','Tieton','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('To Ol','Frederiksberg',NULL,'DNK')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tommyknocker','Idaho Springs','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Tupps','McKinney','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Twisted Pine','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Twisted X','Dripping Springs','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Uinta','Salt Lake City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Unibroue','Chambly',NULL,'CAN')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Unity','Ypsilanti','MI','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Upland','Bloomington','IN','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Upslope','Boulder','CO','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Urban Family','Seattle','WA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Urthel','Ruiselede',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Van Diest','Melle',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Verzet','Anzegem',NULL,'BEL')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Victory','Downingtown','PA','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Virtue','Chicago','IL','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wandering Aengus','Salem','OR','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wasatch','Park City','UT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Well''s & Young','Bedford',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wexford','Suffolk',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wild Acre','Fort Worth','TX','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wild Beer','Westcombe',NULL,'ENG')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Woodchuck','Middlebury','VT','USA')
INSERT INTO product.Producer (ProducerName, Municipality, ISORegionCode2, ISOCountryCode3) VALUES ('Wyndridge','Dallastown','PA','USA')
GO

INSERT INTO product.Producer
	(ProducerName,ImagePath,Municipality,ISORegionCode2,ISOCountryCode3)
VALUES
	('3 Nations', '/assets/img/breweries/3nationslogo.png', 'Farmers Branch', 'TX', 'USA'),
	('Elysian', '/assets/img/breweries/elysianlogo.jpg', 'Seattle', 'WA', 'USA')
GO
--End table product.Producer

--Begin table product.ProductStyle
TRUNCATE TABLE product.ProductStyle
GO

EXEC utility.InsertIdentityValue 'product.ProductStyle', 'ProductStyleID', 0
GO

INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('4 Grain Breakfast Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ale Brewed w/ Star Anise & Licorice Root');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Altbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Amber Ale/Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ameriacn Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Adjunct Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber/Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Amber/Red Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Dark Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Doube/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Double/Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Imperial/Double Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American IPA w/ Mango');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ae');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Ale brewed w/ Blood Orange');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Session Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Sour Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wheat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American Wile Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('American/Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Americn IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Applewine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot American Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Apricot Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Asain Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('BA Cherry Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Baltic Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Baltic Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Adambier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Brett Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Coffee & Chicory Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Sour Ale w/ Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Tart Pomegranate Wit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Triple Ale w/ Marionberries & Meyer Lemons');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel Aged Vanilla Porter w/ Tart Cherry');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Barrel-Aged Tart Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bavarian Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bavarian Witbier brewed w/ Raspberry & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('BBA Scotch Ale/Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Blonde/Golden');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Inspired Wheat Ale w/ Michigan Cherry Juice');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Quad Brewed w/ Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Strong Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Quadruple');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Style Wheat Ale w/ Michigan Cherry Juice');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian White');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian-Style Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Belgian-Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berline Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Berliner Weissebier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Biere de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bière de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bitter Chocolate Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Chamomile Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Currant Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black IPA/Cascadian Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Tea & Chai Spiced Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Black Tea, Lemon & Apple Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry & Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Orange Blossom Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blackberry Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blended Barrel Aged Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale brewed w/ Cherry');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blonde Ale Brewed w/ Lemon Peel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange & Spice Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blood Orange Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blossom Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry & Blackberry Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Berliner Weissbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Blueberry, Blackberry & Black Currant Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bohemiam Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Dopplebock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Malt Liquor');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Imperial Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Milk/Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Stout w/ Peanuts');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Aged Wheatwine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel-Aged Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Barrel-Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Bourbon Peach Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Braggot');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brandy Barrel Aged Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brass Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brett Blend Barrel Fermented/Aged Farmhouse Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brett Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cabernet Barrel Aged Belgian Quad');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('California Common/Steam Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('California White Burgundy Barrel Aged Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cap''n Crunch & Fruity Pebbles Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cascadian Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chai Tea Spiced Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chardonnay Barrel Aged Belgian Strong Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry & Hibiscus Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry & Vanilla Sour Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Ale Aged In Maple Syrup Bourbon Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Hibiscus Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Pie Sour Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cherry, Salt & Lime Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chile Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Chip Cookie Dough Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Chocolate/Peanut Butter Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cider Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon & Vanilla Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon & Vanilla English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cinnamon, Allspice & Clove Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cocoa Nib Aged Golden Ale Fermented w/ Cherries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coconut & Vanilla Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coconut Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee & Chicory Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Baltic Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Coffee Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cranberry/Raspberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Ale brewed w/ Lychee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cream Soda Spiced Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Cucumber Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsener');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Czech Pilsner w/ Falconers Flight Hops');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Double Alt Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Sour w/ Berries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dark Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Doppelbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dortmunder/Export Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dortmunder/Helles');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Barrel Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA Aged in Gin Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double IPA w/Sorachi Ace Hops');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Douple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry Hopped Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dry-Hopped Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Dunkelweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('East Coast IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Barleywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Bitter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Brown Ale Fermented w/ Ghost Peppers');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Mild Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Pale Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Style Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('English Style Brown Ale w/ Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Eruo Dark Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Euro Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Expiramental IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Export Stout Brewed with Strawberries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Extra Special/Strong Bitter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale Brewed with Honey and Tangerines');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Ale Brewed with Smoked Figs');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Farmhouse/Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flanders Oud Bruin');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flanders Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Flowering Citrus Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Foreign/Export Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Foreign/Export Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Fruit/Vegetable Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Funk Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Fusion Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Pilsener');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('German Style Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin & Juice Style Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin & Tonic Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gin Barrel-Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ginger Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gluten Free Euro Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gluten Free Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gold Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale w/ Apricot & Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Ale/Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Chocolate Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Oatmeal Stout brewed w/ Chocolate & Beets');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Golden Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gose Aged In Cabernet Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grand Cru');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit & Mint Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit & Passionfruit Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Grapefruit Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gratzer/Grodziskie');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Green Tea Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gruit/Ancient Herbed Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Guava Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Gueuze');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hard Root Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hatch Chile Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Helles Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Herb/Spiced Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus & Cherry Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus & Ginger Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hibiscus Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Holiday Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Holiday Spiced Coffee Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey & Apple Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey & Vanilla Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Honey Vanilla Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hop Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy American Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Belgian-style Golden Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hoppy Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Hop-Rocketed American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imp Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Bourbon Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Coffee Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Dark Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Milk Stout w/ Coconut');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Oak-Aged Cranberry Spiced Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pumpkin Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Pumpkin Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Red IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Red Rye');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Smoked Black Rye Oaked Raspberry IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Smoked Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Taiji IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial Tart Cherry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Dobule IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Black IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Coconut Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Peanut Butter Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Peppermint Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Porter with Chocolate');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Imperial/Double Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('India Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('India-Style Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('IPA Brewed w/ Citra Hops & Avocado Honey');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('IPL');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Dry Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Irish Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Juniper & Rosemary Biere de Garde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kellerbier/Zwickelbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kombucha Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kriek');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kristallweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Kvass');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lagered Belgian-Style Wit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic/Framboise');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lambic-Fruit');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lemon Ginger Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lemonade Shandy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Lichtenhainer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Light Ale Brewed With Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Light Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Macallan Barrel Aged Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Maibock/Helles Bock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mandarina Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango & Apricot American IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Session Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mango, Passionfruit & Hibiscus Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Marzen/Oktoberfest');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Melomel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Melon Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mesquite Bean Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mexican  Coffee Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mexican Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mezcal & Tequila Barrel Aged Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Milk/Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Mocha Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Multigrain Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Munich Dunkel Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Munich Helles Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Cold Coffee - Nitro');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Hopped Tonic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Lemonade');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('N/A Root Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('New England IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('New England Style IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Non-Alcoholic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('North American Adjunct Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Northwest Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Norwegian Spiced Belgian Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Nut Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged Amber Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged English Mild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Aged IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Bourbon Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak Whiskey Aged Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oak-Aged Raspberry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oat Wine Aged in Whistlepig Rye Whiskey Barrels');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Hefeweizen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Raisin Cookie Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Okroberfest/Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oktoberfest/Marzen');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Orange & Vanilla Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Orange and Lemon IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Oud Bruin');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Ale Brewed w/ Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pale Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Sour Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passion Fruit White IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit & Pineapple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit Citra Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Passionfruit Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach & Honey Pale Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Belgian Blonde w/ Coriander');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Spiked Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peach Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter & Berries Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter & Jelly Cream Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Peanut Butter Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pear Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pickle Juice Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pina Colada IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Passionfruit Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pineapple Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Poblano Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pomegranate Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pomegranate Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pre-Prohibition Style Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Prickly Pear Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Ale w/ Cuvee Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Spiced Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Pumpkin/Yam Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Ramen Noodle Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Berliner Weisse');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Ginger Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Kombucha');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Maibock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Northwest Style Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Raspberry Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rauchbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Currant Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Belgian Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Old Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Saison w/ Raspberries');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel Aged Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel-Aged Belgian-Style Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Red Wine Barrel-Aged Dubbel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rhubarb Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Robust Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Robust Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Roggenbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rose Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Imperial Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Northwest Style Sour');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Pumpkin Spice Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rum Barrel Aged Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout aged in Bourbon Barrels with Coffee');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Russian Imperial Stout Brewed w/ Toasted Coconut');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Whiskey Barrel Aged Rye Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Rye Whisky BA Belgian Style Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison brewed w/ Blood Orange');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Salted Caramel Brownie Brown Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sauvignon Blanc Aged Christmas Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Schwarzbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Ale/Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Ale/Wee Heavy brewed w/ Chocolate');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Barrel Aged Scotch Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scotch Barrel-Aged Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scottish Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Scottish Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Session IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Shandy/Radler');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sherry Barrel Aged Czech Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Single Hop IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Smoked Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('S''more Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Apple Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Cherry Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Farmhouse IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Lemon Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Session Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Session Wheat with Salt & Coriander');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Wee Heavy');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sour Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Southern Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spanish Style Sour Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Specialty American Strong Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Amber Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Brown Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Chocolate Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Golden Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Honey & Banana Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Pilsner');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Spiced Yam Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Steam Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Stout brewed w/ Ginger and Spices');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Crème Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Rhubarb Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strawberry Saison');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strong Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Strong Golden Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer Ale Brewed w/ Hibiscus, Lemon Peel & Chamomile Flowers');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer Cherry Wheat');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Summer-Style IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sweet Mead');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Sweet Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tangerine IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Blonde');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Lambic');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Melomel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Cherry Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Golden Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Peach Kolsch');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Session Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tart Wheat Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tequila Barrel-Aged Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas Red Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas Steam Beer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Texas-Style Champagne Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Toasted Coconut Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tripel');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Triple IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical & Floral Unfiltered IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical Blonde Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Tropical Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered American Double/Imperial IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered Double IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Unfiltered IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Baltic Coffee Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla Porter');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vanilla, Cinnamon & Pecan Cider');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Vienna Lager');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Lime Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Watermelon Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Weizenbock');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hop American Pale Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hop IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wet Hopped Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Ale w/ Strawberry Puree');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wheat Wine Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey BA Black Gose');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Barley Wine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel Aged Oatmeal Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Whiskey Barrel-Aged Strong Black Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('White IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('White Russian Imperial Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Porter Brewed w/ Currants');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Saison/Farmhouse Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wild Strawberry Honeywine');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wine & Mead Hybrid/Ancient Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wine Bare Aged Pretzel Stout w/ Brett');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Spiced Imperial Chocolate Milk Stout');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Winter Warmer');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier brewed w/ Orange Zest');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier w/ Chili, Lime & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Witbier w/ Raspberry & Ginger');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Wood Aged Sour Ale');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Zephyr Oat IPA');
INSERT INTO product.ProductStyle (ProductStyleName) VALUES ('Zwickel Lager');
GO

UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer for beer lovers, the German-style helles is a malt accented lager beer that balances a pleasant malt sweetness and body with floral Noble hops and restrained bitterness. The helles is a masterclass in restraint, subtly and drinkability which makes it an enduring style for true beer lovers and an elusive style for craft brewers to recreate. The German helles reminds beer lovers that the simple things in life are usually the most rewarding and worth pursuing.' WHERE ProductStyleName = 'Dortmunder/Helles'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer for beer lovers, the German-style helles is a malt accented lager beer that balances a pleasant malt sweetness and body with floral Noble hops and restrained bitterness. The helles is a masterclass in restraint, subtly and drinkability which makes it an enduring style for true beer lovers and an elusive style for craft brewers to recreate. The German helles reminds beer lovers that the simple things in life are usually the most rewarding and worth pursuing.' WHERE ProductStyleName = 'Helles Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer rich in malt with a balance of clean, hop bitterness. Bread or biscuit-like malt aroma and flavor is common. Originating in Germany, this style used to be seasonally available in the spring (“Marzen” meaning “March”), with the fest-style versions tapped in October.' WHERE ProductStyleName = 'Marzen/Oktoberfest'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A beer rich in malt with a balance of clean, hop bitterness. Bread or biscuit-like malt aroma and flavor is common. Originating in Germany, this style used to be seasonally available in the spring (“Marzen” meaning “March”), with the fest-style versions tapped in October.' WHERE ProductStyleName = 'Oktoberfest/Marzen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A bona fide English beer classic, English-style brown ale is easily one of the most iconic beer styles. Toasty, robust and with a bit of chocolate maltiness, the English brown ale is a meal in a glass, but offers unlimited opportunities for memorable food pairings. Neither flashy nor boring, the English brown is a beer with enough variation to keep devotees ordering them time and time again.' WHERE ProductStyleName = 'English Brown Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A distinctive quality of these ales is that their yeast undergoes an aging process (often for years) in bulk storage or through bottle conditioning, which contributes to a rich, wine-like and often sweet oxidation character. Old ales are copper-red to very dark in color. Complex estery character may emerge.' WHERE ProductStyleName = 'English Old Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'Amber Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'American Amber'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A widely available, sessionable craft beer style that showcases both malt and hops. Amber lagers are a medium-bodied lager with a toasty or caramel-like malt character. Hop bitterness can range from very low to medium-high. Brewers may use decoction mash and dry-hopping to achieve advanced flavors.' WHERE ProductStyleName = 'American Amber/Red Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'A wood- or barrel-aged beer is any lager, ale or hybrid beer, either a traditional style or a unique experimental beer, that has been aged for a period of time in a wooden barrel or in contact with wood. This beer is aged with the intention of imparting the unique character of the wood and/or the flavor of what has previously been in the barrel. Today’s craft brewers are using wood (mostly oak) to influence flavor and aromatics. Beer may be aged in wooden barrels (new or previously used to age wine or spirits), or chips, spirals and cubes may be added to the conditioning tanks that normally house beer. A variety of types of wood are used including oak, apple, alder, hickory and more. The interior of most barrels is charred or toasted to further enhance the flavor of the wood.' WHERE ProductStyleName = 'Barrel-Aged Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Also called “heller bock” (meaning “pale bock”), the German-style Maibock is paler in color and more hop-centric than traditional bock beers. A lightly toasted and/or bready malt character is often evident.' WHERE ProductStyleName = 'Maibock/Helles Bock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American barley wine ranges from amber to deep red/copper-garnet in color. A caramel and/or toffee aroma and flavor are often part of the malt character along with high residual malty sweetness. Complexity of alcohols is evident. Fruity-ester character is often high. As with many American versions of a style, this barley wine ale is typically more hop-forward and bitter than its U.K. counterpart. Low levels of age-induced oxidation can harmonize with other flavors and enhance the overall experience. Sometimes sold as vintage releases.' WHERE ProductStyleName = 'American Barleywine'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American barley wine ranges from amber to deep red/copper-garnet in color. A caramel and/or toffee aroma and flavor are often part of the malt character along with high residual malty sweetness. Complexity of alcohols is evident. Fruity-ester character is often high. As with many American versions of a style, this barley wine ale is typically more hop-forward and bitter than its U.K. counterpart. Low levels of age-induced oxidation can harmonize with other flavors and enhance the overall experience. Sometimes sold as vintage releases.' WHERE ProductStyleName = 'Barleywine'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American craft beer lovers are huge fans of the IPA. The quest for more of the India pale ale flavor has led them to the imperial India pale ale, a stronger version of the American IPA, which boasts even more hoppy flavor, aroma and bitterness. Imperial India pale ale is darker in color than the American IPA, substantially more bitter, and high in alcohol by volume. This all-American take on the IPA leaves craft beer fans with plenty of new creations to try.' WHERE ProductStyleName = 'India Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Craft Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American lager has little in the way of hop and malt character. A straw to gold, very clean and crisp, highly carbonated lager.' WHERE ProductStyleName = 'American Light Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'American wheat beers are some of the most approachable beers in the craft beer world, and the versatility of wheat beer allows it to be combined with a variety of ingredients or enjoyed on its own alongside a wide variety of food options. The sizable portion of wheat malt used to brew wheat beer lends a lighter, distinctive experience compared to beers brewed with barley exclusively. Typically lighter in appearance, wheat beer can be made using either ale or lager yeast, and American wheat beer can be brewed with at least 30 percent malted wheat. Like the traditional German hefeweizen, these beers are typically served unfiltered and can have a cloudy appearance when roused. Traditionally hoppier than its German cousin, American wheat beer differs in that it does not offer flavors of banana or clove, which is indicative of the weizen yeast strain. Nevertheless, the American wheat beer is known worldwide as a refreshing summer style.' WHERE ProductStyleName = 'American Wheat Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An Americanized version of a Dunkelweizen, these beers can range in color from garnet or deep amber to ruby brown. Often cloudy with long-lasting heads, this style tends to be light- to medium-bodied with a high level of carbonation. Hop character might be low or moderately high with some fruitiness from ale fermentation, though most examples use of a fairly neutral ale yeast, resulting in a clean fermentation with little to no diacetyl. Flavors of caramel and toasted malts might be present too, just don''t expect German Weizen flavors and aromas of banana esters and clove-like phenols.' WHERE ProductStyleName = 'American Dark Wheat Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An herb and spice beer is a lager or ale that contains flavors derived from flowers, roots, seeds or certain fruits or vegetables. Typically the hop character is low, allowing the added ingredient to shine through. The appearance, mouthfeel and aromas vary depending on the herb or spice used. This beer style encompasses innovative examples as well as traditional holiday and winter ales.' WHERE ProductStyleName = 'Herb/Spiced Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'An herb and spice beer is a lager or ale that contains flavors derived from flowers, roots, seeds or certain fruits or vegetables. Typically the hop character is low, allowing the added ingredient to shine through. The appearance, mouthfeel and aromas vary depending on the herb or spice used. This beer style encompasses innovative examples as well as traditional holiday and winter ales.' WHERE ProductStyleName = 'Herbed/Spiced Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Arguably one of the most recognizable beer styles, the German-style hefeweizen offers a striking beer experience thanks to the use of distinctive wheat malt, unique yeast and uncharateristic appearance. This wheat beer breaks from the German beer mold, showcasing yeast-driven fruit and spice as well as bearing an eye-catching mystique. Don’t let the cloudy hefeweizen deter you, this beer is one of the world’s most enjoyable styles for beer geeks and neophytes, alike. The refreshing qualities of this highlycarbonated style have kept it alive for centuries. Try one for yourself and experience why that is, firsthand.' WHERE ProductStyleName = 'Hefeweizen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers in this category are gold to light amber in color. Often bottle-conditioned, with some yeast character and high carbonation. Belgian-style saison may have Brettanomyces or lactic character, and fruity, horsey, goaty and/or leather-like aromas and flavors. Specialty ingredients, including spices, may contribute a unique and signature character. Commonly called “farmhouse ales” and originating as summertime beers in Belgium, these are not just warm-weather treats. U.S. craft brewers brew them year-round and have taken to adding a variety of additional ingredients.' WHERE ProductStyleName = 'Belgian-Style Saison'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeño chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chile Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeño chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chili Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Beers with the addition of hot pepper juice, oils, or actual peppers, most commonly jalapeño chiles. Hotness can range from a subtle spiciness to palate scorching. Most often, chiles are added to pale ales and light lagers, but the base beer style can vary.' WHERE ProductStyleName = 'Chili Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Belgian-style witbier is brewed using unmalted wheat, sometimes oats and malted barley. Witbiers are spiced with coriander and orange peel. A style that dates back hundreds of years, it fell into relative obscurity until it was revived by Belgian brewer Pierre Celis in the 1960s. This style is currently enjoying a renaissance, especially in the American market. “Wit” means “white.”' WHERE ProductStyleName = 'Belgian White'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Belgian-style witbier is brewed using unmalted wheat, sometimes oats and malted barley. Witbiers are spiced with coriander and orange peel. A style that dates back hundreds of years, it fell into relative obscurity until it was revived by Belgian brewer Pierre Celis in the 1960s. This style is currently enjoying a renaissance, especially in the American market. “Wit” means “white.”' WHERE ProductStyleName = 'Witbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Biere de Garde translates as “beer for keeping.” This style is popping up more and more from U.S. producers. Blond, amber and brown versions exist. Biere de garde examples are light amber to chestnut brown or red in color. This style is characterized by a toasted malt aroma and slight malt sweetness. Flavor of alcohol is evident. Often bottle-conditioned, with some yeast character.' WHERE ProductStyleName = 'Bière de Garde'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Both lagers and ales can be brewed with honey. Some brewers will choose to experiment with ingredients, while others will add honey to traditional styles. Overall the character of honey should be evident but not totally overwhelming. A wide variety of honey beers are available. U.S. brewers may add honey to the boil kettle (as a sugar source) or post-boil (to preserve more volatile aromatics).' WHERE ProductStyleName = 'Honey Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Characterized by floral, fruity, citrus-like, piney or resinous American-variety hop character, the IPA beer style is all about hop flavor, aroma and bitterness. This has been the most-entered category at the Great American Beer Festival for more than a decade, and is the top-selling craft beer style in supermarkets and liquor stores across the U.S.' WHERE ProductStyleName = 'American IPA '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Characterized by floral, fruity, citrus-like, piney, resinous American hops, the American pale ale is a medium-bodied beer with low to medium caramel, and carries with it a toasted maltiness. American pale ale is one of the most food-friendly styles to enjoy, since the pale ale works wonderfully with lighter fare such as salads and chicken, but can still stand up to a hearty bowl of chili; a variety of different cheeses, including cheddar; seafood, like steamed clams or fish, and even desserts. The American pale ale’s affinity to food can be attributed to the simplicity of its ingredients, which include toasty pale malt, a clean fermenting ale beer yeast, and the counterbalance of American hops to help tease out the flavor or cleanse the palate, preparing you for another bite.' WHERE ProductStyleName = 'American Pale Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'Chocolate beer can be an ale or lager that benefits from the addition of any type of chocolate or cocoa. Traditionally added to porters, stouts and brown ales, where the grain bill better complements the confectionery ingredient, it can be added to other styles as well. Chocolate character can range from subtle to overt, but any chocolate beer is generally expected to offer some balance between beer and bon-bon. The style can vary greatly in approach as well as flavor profile depending on the brewer.' WHERE ProductStyleName = 'Chocolate Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor — but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Blonde Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor — but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Cream Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Coffee beer can be either a lager beer or an ale beer, with coffee added to boost flavor. While stouts and porters are popular base styles for coffee beer, many craft breweries are experimenting with other styles, like cream ales and India pale ales. Brewers may steep the beans in either water or beer to impart java flavor while taking care to avoid the addition of too much acidity. As with any beer, the addition of an ingredient can have a drastic effect on the flavor — but striking a balance is often the goal of brewers.' WHERE ProductStyleName = 'Coffee Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Style Tripel Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Style Tripel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Tripel Style Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Complex, sometimes mild spicy flavor characterizes this style. Yeast-driven complexity is common. Tripels are often on the higher end of the ABV spectrum, yet are approachable to many different palates. These beers are commonly bottle-conditioned and finish dry. The Belgian-style tripel is similar to Belgian-style golden strong ales, but are generally darker and have a more noticeable malt sweetness.' WHERE ProductStyleName = 'Belgian Tripel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This style’s fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'German Alt/Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This style’s fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'German Style Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Crisp, delicate and oh-so-drinkable, the German-style Kolsch is a beer hybrid, meaning that its production and subsequent beer drinking experience saddles qualities of both lager beers and ale beers. These light and refreshing ale-lager hybrids are perfect for warm summer days and have become a favored style by American craft brewers and beer lovers alike. In addition to their thirst quenching ability, they also are a fun beer to enjoy with food, including traditional German sausages and kraut. The German-style Kolsch is light in color and malt character. This style’s fermentation process yields a light, vinous character which is accompanied by a slightly dry, crisp finish. Ale yeast is used for fermentation, though lager yeast is sometimes used in the bottle or final cold-conditioning process.' WHERE ProductStyleName = 'Kolsch'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Definitively American, the imperial porter should have no roasted barley flavors or strong burnt/black malt character. Medium caramel and cocoa-like sweetness is present, with complementing hop character and malt-derived sweetness.' WHERE ProductStyleName = 'American Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Definitively American, the imperial porter should have no roasted barley flavors or strong burnt/black malt character. Medium caramel and cocoa-like sweetness is present, with complementing hop character and malt-derived sweetness.' WHERE ProductStyleName = 'Imperial Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Dry stout is black beer with a dry-roasted character thanks to the use of roasted barley. The emphasis on coffee-like roasted barley and a moderate degree of roasted malt aromas define much of the character. Hop bitterness is medium to medium high. This beer is often dispensed via nitrogen gas taps that lend a smooth, creamy body to the palate.' WHERE ProductStyleName = 'Irish Dry Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Emphasizing hop aroma and flavor without bracing bitterness, the New England IPA  leans heavily on late and dry hopping techniques to deliver a bursting juicy, tropical hop experience. The skillful balance of technique and ingredient selection, often including the addition of wheat or oats, lends an alluring haze to this popular take on the American IPA.' WHERE ProductStyleName = 'New England IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Emphasizing hop aroma and flavor without bracing bitterness, the New England IPA  leans heavily on late and dry hopping techniques to deliver a bursting juicy, tropical hop experience. The skillful balance of technique and ingredient selection, often including the addition of wheat or oats, lends an alluring haze to this popular take on the American IPA.' WHERE ProductStyleName = 'New England Style IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'ESB stands for “extra special bitter.” This style is known for its balance and the interplay between malt and hop bitterness. English pale ales display earthy, herbal English-variety hop character. Medium to high hop bitterness, flavor and aroma should be evident. The yeast strains used in these beers lend a fruitiness to their aromatics and flavor, referred to as esters. The residual malt and defining sweetness of this richly flavored, full-bodied bitter is medium to medium-high.' WHERE ProductStyleName = 'English Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Fruit beer is made with fruit, or fruit extracts that are added during any portion of the brewing process, providing obvious yet harmonious fruit qualities. This idea is expanded to “field beers” that utilize vegetables and herbs.' WHERE ProductStyleName = 'Fruit Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Fruit beer is made with fruit, or fruit extracts that are added during any portion of the brewing process, providing obvious yet harmonious fruit qualities. This idea is expanded to “field beers” that utilize vegetables and herbs.' WHERE ProductStyleName = 'Fruit/Vegetable Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'If you are one of the 2 million Americans who suffer from celiac disease, trying craft beers may seem impossible, or at least challenging. But with the growing interest in gluten-free options, many people have found that they no longer have to miss out on enjoying craft beer. Many brewers have recognized the desire for gluten-free customers to enjoy their beer without the concern of ingesting gluten, leading many craft brewers to utilize alternative grains during the brewing process that do not contain gluten. Dedicated gluten-free breweries have also found success catering to people dealing with gluten intolerance as well as healthminded beer drinkers who choose to follow a gluten-reduced or gluten-free diet, but don’t want to give up their favorite beverage.' WHERE ProductStyleName = 'Gluten Free Euro Pale Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'If you are one of the 2 million Americans who suffer from celiac disease, trying craft beers may seem impossible, or at least challenging. But with the growing interest in gluten-free options, many people have found that they no longer have to miss out on enjoying craft beer. Many brewers have recognized the desire for gluten-free customers to enjoy their beer without the concern of ingesting gluten, leading many craft brewers to utilize alternative grains during the brewing process that do not contain gluten. Dedicated gluten-free breweries have also found success catering to people dealing with gluten intolerance as well as healthminded beer drinkers who choose to follow a gluten-reduced or gluten-free diet, but don’t want to give up their favorite beverage.' WHERE ProductStyleName = 'Gluten Free Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'In darker versions, malt flavor can optionally include low roasted malt characters (evident as cocoa/chocolate or caramel) and/or aromatic toffee-like, caramel, or biscuit-like characters. Low-level roasted malt astringency is acceptable when balanced with low to medium malt sweetness. Hop flavor is low to medium-high. Hop bitterness is low to medium. These beers can be made using either ale or lager yeast. The addition of rye to a beer can add a spicy or pumpernickel character to the flavor and finish. Color can also be enhanced and may become more red from the use of rye. The ingredient has come into vogue in recent years in everything from stouts to lagers, but is especially popular with craft brewers in India pale ales. To be considered an example of the style, the grain bill should include sufficient rye such that rye character is evident in the beer.' WHERE ProductStyleName = 'Rye Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Don’t let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'Amber Ale/Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Don’t let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'American Amber/Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Irish-style red ales are an approachable style for people who are new to craft beer, but are also enjoyed and appreciated by even the most discerning of craft connoisseurs. Don’t let these tasty, sessionable beers be relegated to a once-a-year holiday, Irish style reds can be enjoyed throughout the year in a variety of occasions and with a lot of different food options.' WHERE ProductStyleName = 'American Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Light bodied, pale, fizzy lagers made popular by the large macro-breweries (large breweries) of America after Prohibition. Low bitterness, thin malts, and moderate alcohol. Focus is less on flavor and more on mass-production and consumption, cutting flavor and sometimes costs with adjunct cereal grains, like rice and corn.' WHERE ProductStyleName = 'American Adjunct Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Like many other beer styles that have become prized by American brewers and beer lovers alike, American stout is a distinct variant of a European stout beer counterpart. True to style, American stouts showcase generous quantities of the American hops fans have come to expect, and much like other stout beer types, American stout can be enjoyed year-round but is commonly considered a beer for the fall or winter months. The stout is a terrific companion to bold, hearty foods. Look for hearty game meats, as well as soups and strong cheeses to be particularly suitable for pairing for American stouts, in addition to a variety of after-dinner desserts.' WHERE ProductStyleName = 'American Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beer’s acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berline Weissbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beer’s acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weissbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beer’s acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weisse'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Low in alcohol, refreshingly tart, and often served with a flavored syrup like Woodruff or raspberry, the Berliner-style Weisse presents a harmony between yeast and lactic acid. These beers are very pale in color, and may be cloudy as they are often unfiltered. Hops are not a feature of this style, but these beers often do showcase esters. Traditional versions often showcase Brettanomyces yeast. Growing in popularity in the U.S., where many brewers are now adding traditional and exotic fruits to the recipe, resulting in flavorful finishes with striking, colorful hues. These beers are incredible when pairing. Bitterness, alcohol and residual sugar are very low, allowing the beer’s acidity, white bread and graham cracker malt flavors to shine. Carbonation is very high, adding to the refreshment factor this style delivers. Many examples of this style contain no hops and thus no bitterness at all.' WHERE ProductStyleName = 'Berliner Weissebier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Malt and caramel are part of the flavor and aroma profile of the English-style mild while licorice and roast malt tones may sometimes contribute as well. Hop bitterness is very low to low. U.S. brewers are known to make lighter-colored versions as well as the more common “dark mild.” These beers are very low in alcohol, yet often are still medium-bodied due to increased dextrin malts.' WHERE ProductStyleName = 'English Mild Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Often known as cassis, framboise, kriek, or peche, a fruit lambic takes on the color and flavor of the fruit it is brewed with. It can be dry or sweet, clear or cloudy, depending on the ingredients. Notes of Brettanomyces yeast are often present at varied levels. Sourness is an important part of the flavor profile, though sweetness may compromise the intensity. These flavored lambic beers may be very dry or mildly sweet.' WHERE ProductStyleName = 'Belgian Style Lambic'
UPDATE product.ProductStyle SET ProductStyleDescription = 'One of the most approachable styles, a golden or blonde ale is an easy-drinking beer that is visually appealing and has no particularly dominating malt or hop characteristics. Rounded and smooth, it is an American classic known for its simplicity. Sometimes referred to as “golden ale.” These beers can have honey, spices and fruit added, and may be fermented with lager or ale yeast.' WHERE ProductStyleName = 'American Blonde Ale '
UPDATE product.ProductStyle SET ProductStyleDescription = 'One of the most approachable styles, a golden or blonde ale is an easy-drinking beer that is visually appealing and has no particularly dominating malt or hop characteristics. Rounded and smooth, it is an American classic known for its simplicity. Sometimes referred to as “golden ale.” These beers can have honey, spices and fruit added, and may be fermented with lager or ale yeast.' WHERE ProductStyleName = 'Golden Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Originally from the Düsseldorf area of Germany, the German-Style Brown/Altbier strikes a balance between hop and malt flavors and aromas, but can have low fruity esters and some peppery and floral hop aromas. Before Germany had lager beer, it had ales. Alt, meaning “old,” pays homage to one rebel region in Germany which did not lean into lagering. U.S. producers celebrate the ale revolution beautifully with this top-fermented German beer style.' WHERE ProductStyleName = 'Altbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Oud Bruins, not restricted to, but concentrated in Flanders, are light to medium-bodied and deep copper to brown in color. They are extremely varied, characterized by a slight vinegar or lactic sourness and spiciness to smooth and sweet. A fruity-estery character is apparent with no hop flavor or aroma. Low to medium bitterness. Very small quantities of diacetyl are acceptable. Roasted malt character in aroma and flavor is acceptable, at low levels. Oak-like or woody characteristics may be pleasantly integrated into the overall palate. Typically, old and new Brown Ales are blended, like Lambics.' WHERE ProductStyleName = 'Flanders Oud Bruin'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin Yam Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Perhaps the most seasonal of seasonal beers, the pumpkin beer style can be brewed with pumpkin, just pumpkin spices, or even winter squash. Since the fruit does not have much of a taste by itself, many craft brewers have taken to adding spices typically found in pumpkin pie, like cinnamon and clove. However, these flavors should not overpower the beer. Pumpkin can be found in everything from stouts to pale ales and pilsners.' WHERE ProductStyleName = 'Pumpkin/Yam Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Quite possibly the most iconic beer style in modern history, the pilsner — also spelled pilsener — captured the attention of beer drinkers across the world and inspired a myriad of regional imitations. This lightly colored, exquisitely balanced lager remains one of the most loved beers to enjoy, and one of the most challenging for the brewer to create. Pilsners are characteristically light in color and have a very short finish. The world over, pilsner-style lagers have become the standard beer for many reasons, and American craft brewers have worked hard to put their own unique spin on this classic German beer.' WHERE ProductStyleName = 'German Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Quite possibly the most iconic beer style in modern history, the pilsner — also spelled pilsener — captured the attention of beer drinkers across the world and inspired a myriad of regional imitations. This lightly colored, exquisitely balanced lager remains one of the most loved beers to enjoy, and one of the most challenging for the brewer to create. Pilsners are characteristically light in color and have a very short finish. The world over, pilsner-style lagers have become the standard beer for many reasons, and American craft brewers have worked hard to put their own unique spin on this classic German beer.' WHERE ProductStyleName = 'Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Roasted malt, caramel-like and chocolate-like characters should be of medium intensity in both flavor and aroma for the American brown ale. American-style brown ales have evident low to medium hop flavor and aroma and medium to high hop bitterness. The history of this style dates back to U.S. homebrewers who were inspired by English-style brown ales and porters. It sits in flavor between those British styles and is more bitter than both.' WHERE ProductStyleName = 'American Brown Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Scottish-style ales vary depending on strength and flavor, but in general retain a malt-forward character with some degree of caramel-like malt flavors and a soft and chewy mouthfeel. Some examples feature a light smoked peat flavor. Hops do not play a huge role in this style. The numbers commonly associated with brands of this style (60/70/80 and others) reflect the Scottish tradition of listing the cost, in shillings, of a hogshead (large cask) of beer. Overly smoked versions would be considered specialty examples. Smoke or peat should be restrained.' WHERE ProductStyleName = 'Scottish Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Session beer is not defined by flavors or aromas, which can place it in almost any style category. Instead, what makes a session beer is primarily refreshment and drinkability. Any style of beer can be made lower in strength than described in the classic style guidelines. The goal should be to reach a balance between the style’s character and the lower alcohol content. Drinkability is a factor in the overall balance of these beers. Beer should not exceed 5 percent ABV.' WHERE ProductStyleName = 'Session IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sometimes called black lagers, they may remind some of German-style dunkels, but schwarzbiers are drier, darker and more roastoriented.These very dark brown to black beers have a surprisingly pale-colored foam head (not excessively brown) with good cling quality. They have a mild roasted malt character without the associated bitterness. Malt flavor and aroma is at low to medium levels of sweetness.' WHERE ProductStyleName = 'Schwarzbier'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sometimes referred to as a “Dortmunder export,” the European-Style Export has the malt-forward flavor and sweetness of a German-style helles, but the bitter base of a German-style pilsener. This lager is all about balance, with medium hop character and firm but low malt sweetness. Look for toasted malt flavors and spicy floral hop aromas.' WHERE ProductStyleName = 'Dortmunder/Export Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Straw to medium amber, the contemporary Gose is cloudy from suspended yeast. A wide variety of herbal, spice, floral or fruity aromas other than found in traditional Leipzig-Style Gose are present, in harmony with other aromas. Salt (table salt) character is traditional in low amounts, but may vary from absent to present. Body is low to medium-low. Low to medium lactic acid character is evident in all examples as sharp, refreshing sourness.' WHERE ProductStyleName = 'Gose Aged In Cabernet Barrels'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Straw to medium amber, the contemporary Gose is cloudy from suspended yeast. A wide variety of herbal, spice, floral or fruity aromas other than found in traditional Leipzig-Style Gose are present, in harmony with other aromas. Salt (table salt) character is traditional in low amounts, but may vary from absent to present. Body is low to medium-low. Low to medium lactic acid character is evident in all examples as sharp, refreshing sourness.' WHERE ProductStyleName = 'Gose'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Strong, bitter and completely misunderstood, the English India pale ale (or English IPA) bridges the gap between past and present. No other style represents modern craft brewing excitement quite like the IPA, and while this English beer differs widely from the American version it inspires, this strong member of the English pale ale family has plenty of its own to offer — including all of the history behind this variety.' WHERE ProductStyleName = 'English IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sweet stout, also referred to as cream stout or milk stout, is black in color. Malt sweetness, chocolate and caramel should dominate the flavor profile and contribute to the aroma. It also should have a low to medium-low roasted malt/barley-derived bitterness. Milk sugar (lactose) lends the style more body. This beer does use lactose sugar, so people with an intolerance should probably avoid this style.' WHERE ProductStyleName = 'Milk/Sweet Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Sweet stout, also referred to as cream stout or milk stout, is black in color. Malt sweetness, chocolate and caramel should dominate the flavor profile and contribute to the aroma. It also should have a low to medium-low roasted malt/barley-derived bitterness. Milk sugar (lactose) lends the style more body. This beer does use lactose sugar, so people with an intolerance should probably avoid this style.' WHERE ProductStyleName = 'Sweet Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The acidity present in sour beer is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash, or produced during fermentation by the use of various microorganisms. These beers may derive their sour flavor from pure cultured forms of souring agents or from the influence of barrel aging.' WHERE ProductStyleName = 'American Sour Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The acidity present in sour beer is usually in the form of lactic, acetic and other organic acids naturally developed with acidified malt in the mash, or produced during fermentation by the use of various microorganisms. These beers may derive their sour flavor from pure cultured forms of souring agents or from the influence of barrel aging.' WHERE ProductStyleName = 'American Sour Blonde Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The addition of oatmeal adds a smooth, rich body to the oatmeal stout. This beer style is dark brown to black in color. Roasted malt character is caramel-like and chocolate-like, and should be smooth and not bitter. Coffee-like roasted barley and malt aromas are prominent. This low- to medium-alcohol style is packed with darker malt flavors and a rich and oily body from oatmeal.' WHERE ProductStyleName = 'Oatmeal Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American amber ale is one of the most widely enjoyed styles throughout the United States and serves as a cornerstone style of the American craft brewing revolution. American ambers are darker in color than their pale ale cousins, the presence of caramel and crystal malts lending a toasted, toffee flavor, along with the perception of a fuller body when compared to beers without such malts. Amber beer showcases a medium-high to high malt character with medium to low caramel character derived from the use of roasted crystal malts. The American amber is characterized by American-variety hops, which lend the amber ale notes of citrus, fruit and pine to balance the sweetness of the malt. As with many amber beer types, American amber ale is a highly versatile companion to American cuisine, particularly foods that are grilled or barbecued, as roasted malts complement seared, charred and caramelized proteins making this ale beer type a perennial favorite at backyard cookouts.' WHERE ProductStyleName = 'American Amber Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'American Black Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'Black IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American black ale is characterized by the perception of caramel malt and dark roasted malt flavor and aroma. Hop bitterness is perceived to be medium-high to high. Hop flavor and aroma are medium-high. Fruity, citrus, piney, floral and herbal character from hops of all origins may contribute to the overall experience. This beer is often called a black IPA or Cascadian dark ale.' WHERE ProductStyleName = 'Black IPA/Cascadian Dark Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American cream ale is a mild, pale, light-bodied ale, made using a warm fermentation (top or bottom fermenting yeast) and cold lagering. Despite being called an ale, when being judged in competitions it is acceptable for brewers to use lager yeast.' WHERE ProductStyleName = 'American Cream Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The American-style imperial stout is the strongest in alcohol and body of the stouts. Black in color, these beers typically have an extremely rich malty flavor and aroma with full, sweet malt character. Bitterness can come from roasted malts or hop additions.' WHERE ProductStyleName = 'American Double/Imperial Stout'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Baltic-style Porter is a smooth, cold-fermented and cold-lagered beer brewed with lager yeast. Because of its alcoholic strength, it may include very low to low complex alcohol flavors and/or lager fruitiness such as berries, grapes and plums (but not banana; ale-like fruitiness from warm-temperature fermentation is not appropriate). This style has the malt flavors of a brown porter and the roast of a schwarzbier, but is bigger in alcohol and body.' WHERE ProductStyleName = 'Baltic Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style blonde ale is typically easy-drinking, with a low but pleasing hop bitterness. This is a light- to medium-bodied ale, with a low malt aroma that has a spiced and sometimes fruity-ester character. Sugar is sometimes added to lighten the perceived body. This style is medium in sweetness and not as bitter as Belgian-style tripels or golden strong ales. It is usually brilliantly clear. The overall impression is balance between light sweetness, spice and low to medium fruity ester flavors.' WHERE ProductStyleName = 'Belgian Blonde/Golden'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style dubbel ranges from brown to very dark in color. They have a malty sweetness and can have cocoa and caramel aromas and flavors. Hop bitterness is medium-low to medium. Yeast-generated fruity esters (especially banana) can be apparent. Often bottle-conditioned, a slight yeast haze and flavor may be evident. “Dubbel” meaning “double,” this beer is still not so big in intensity as to surpass the Belgian-style quadrupel that is often considered its sibling.' WHERE ProductStyleName = 'Belgian Dubbel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Flanders is an ale with character and balance, thanks to lactic sourness and acetic acid. Cherry-like flavors are acceptable, as is malt sweetness that can lend bitterness and a cocoa-like character. Oak or other wood-like flavors may be present, even if the beer was not aged in barrels. Overall, the style is characterized by slight to strong lactic sourness, and Flanders “reds” sometimes include a balanced degree of acetic acid. Brettanomyces-produced flavors may be absent or very low. This style is a marvel in flavor complexity, combining malt, yeast, microorganisms, acidity and low astringency from barrel aging.' WHERE ProductStyleName = 'Flanders Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style golden strong ale is fruity, complex and often on the higher end of the ABV spectrum, yet are approachable to many different palates. Look for a characteristic spiciness from Belgian yeast and a highly attenuated dry finish. This style is traditionally drier and lighter in color than a Belgian-style tripel.' WHERE ProductStyleName = 'Belgian Strong Golden Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style pale ale is gold to copper in color and can have caramel or toasted malt flavor. The style is characterized by low but noticeable hop bitterness, flavor and aroma. These beers were inspired by British pale ales. They are very sessionable.' WHERE ProductStyleName = 'Belgian Style Pale Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Belgian Style Quadruple'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Quadrupel'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Belgian-style Quadrupel is amber to dark brown in color. Caramel, dark sugar and malty sweet flavors dominate, with medium-low to medium-high hop bitterness. Quads have a relatively light body compared to their alcoholic strength. If aged, oxidative qualities should be mild and not distracting. Sometimes referred to as Belgian strong dark.' WHERE ProductStyleName = 'Quadruple Style Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Bohemian pilsener has a slightly sweet and evident malt character and a toasted, biscuit-like, bready malt character. Hop bitterness is perceived as medium with a low to medium-low level of noble-type hop aroma and flavor. This style originated in 1842, with “pilsener” originally indicating an appellation in the Czech Republic. Classic examples of this style used to be conditioned in wooden tanks and had a less sharp hop bitterness despite the similar IBU ranges to German-style pilsener. Lowlevel diacetyl is acceptable. Bohemian-style pilseners are darker in color and higher in final gravity than their German counterparts.' WHERE ProductStyleName = 'Bohemian Pilsner'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The California common is brewed with lager yeast but fermented at ale fermentation temperatures. There is a noticeable degree of toasted malt and/or caramel-like malt character in flavor and often in aroma. Often referred to as “steam beer” and made famous by San Francisco’s Anchor Brewing Company. Seek out woody and mint flavor from the Northern Brewer hops.' WHERE ProductStyleName = 'California Common/Steam Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The California common is brewed with lager yeast but fermented at ale fermentation temperatures. There is a noticeable degree of toasted malt and/or caramel-like malt character in flavor and often in aroma. Often referred to as “steam beer” and made famous by San Francisco’s Anchor Brewing Company. Seek out woody and mint flavor from the Northern Brewer hops.' WHERE ProductStyleName = 'Steam Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The English-style bitter is a very sessionable, lower-alcohol, malt-driven style. Broad style description commonly associated with cask-conditioned beers. The light- to medium-bodied ordinary bitter is gold to copper in color, with a low residual malt sweetness. Hop bitterness is medium.' WHERE ProductStyleName = 'English Bitter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The English-style brown porter has no roasted barley or strong burnt/black malt character. Low to medium malt sweetness, caramel and chocolate is acceptable. Hop bitterness is medium. Softer, sweeter and more caramel-like than a robust porter, with less alcohol and body. Porters are the precursor style to stouts.' WHERE ProductStyleName = 'English Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The German-style Dunkelweizen can be considered a cross between a German-style dunkel and a hefeweizen. Distinguished by its sweet maltiness and chocolate-like character, it can also have banana and clove (and occasionally vanilla or bubblegum) esters from weizen ale yeast.' WHERE ProductStyleName = 'Dunkelweizen'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The German-style Weizenbock is a wheat version of a German-style bock, or a bigger and beefier dunkelweizen. Malt mellanoidins and weizen ale yeast are the star ingredients. If served with yeast, the appearance may appropriately be very cloudy. With flavors of bready malt and dark fruits like plum, raisin, and grape, this style is low on bitterness and high on carbonation. Balanced clovelike phenols and fruity, banana-like esters produce a well-rounded aroma.' WHERE ProductStyleName = 'Weizenbock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Irish-style red ale is a balanced beer that uses a moderate amount of kilned malts and roasted barley in the recipe, which gives the beer the color for which it is named. Featuring an approachable hop bitterness which rests on the palate, this typically ambercolored beer is brewed as a lager or an ale, and can often have a medium, candy-like caramel malt sweetness. This style may contain adjuncts such as corn, rice, and sugar, which help dry out the beer’s finish and lessen the body. It also often contains roasted barley, lending low roasted notes, darker color and possible creation of a tan collar of foam on top. With notes of caramel, toffee and sometimes low-level diacetyl (butter), think of the Irish red ale beer style as a cousin to lightly-toasted and buttered bread.' WHERE ProductStyleName = 'Irish Red Ale'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Light Lager is generally a lighter version of a brewery''s premium lager, some are lower in alcohol but all are lower in calories and carbohydrates compared to other beers. Typically a high amount of cereal adjuncts like rice or corn are used to help lighten the beer as much as possible. Very low in malt flavor with a light and dry body. The hop character is low and should only balance with no signs of flavor or aroma. European versions are about half the alcohol (2.5-3.5% ABV) as their regular beer, yet show more flavor (some use 100 percent malt) then the American counterparts. For the most part, this style has the least amount of flavor than any other style of beer.' WHERE ProductStyleName = 'Light Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Robust Porter features more bitter and roasted malt flavor than a brown porter, but not quite as much as a stout. Robust porters have a roast malt flavor, often reminiscent of cocoa, but no roast barley flavor. Their caramel and malty sweetness is in harmony with the sharp bitterness of black malt. Hop bitterness is evident. With U.S. craft brewers doing so much experimentation in beer styles and ingredients, the lines between certain stouts and porters are often blurred. Yet many deliberate examples of these styles do exist. Diacetyl is acceptable at very low levels.' WHERE ProductStyleName = 'Robust Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'The Scotch ale is overwhelmingly malty, with a rich and dominant sweet malt flavor and aroma. A caramel character is often part of the profile. Some examples feature a light smoked peat flavor. This style could be considered the Scottish version of an Englishstyle barley wine. Overly smoked versions would be considered specialty examples.' WHERE ProductStyleName = 'Scotch Ale/Wee Heavy'
UPDATE product.ProductStyle SET ProductStyleDescription = 'These unique beers vary in color and can take on the hues of added fruits or other ingredients. Horsey, goaty, leathery, phenolic and some fruity acidic character derived from Brettanomyces organisms may be evident, but in balance with other components of an American Brett beer. Brett beer and sour beer are not synonymous.' WHERE ProductStyleName = 'Brett Blend Barrel Fermented/Aged Farmhouse Saison '
UPDATE product.ProductStyle SET ProductStyleDescription = 'These unique beers vary in color and can take on the hues of added fruits or other ingredients. Horsey, goaty, leathery, phenolic and some fruity acidic character derived from Brettanomyces organisms may be evident, but in balance with other components of an American Brett beer. Brett beer and sour beer are not synonymous.' WHERE ProductStyleName = 'Brett Saison'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Traditional bock beers are all-malt brews and are high in malt sweetness. Malt character should be a balance of sweetness and toasted or nut-like malt. “Bock” translates as “goat”!' WHERE ProductStyleName = 'Bock'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Typically the base for the smoke porter beer style is a robust porter that is given smoky depth thanks to wood-smoked malt. Traditionally, brewers will cite the specific wood used to smoke the malt, and different woods will lend different flavors to the finished product. Smoke flavors dissipate over time.' WHERE ProductStyleName = 'Smoked Porter'
UPDATE product.ProductStyle SET ProductStyleDescription = 'Vienna Lager ranges from copper to reddish brown in color. The beer is characterized by malty aroma and slight malt sweetness. The malt aroma and flavor should have a notable degree of toasted and/or slightly roasted malt character. Hop bitterness is low to medium-low.' WHERE ProductStyleName = 'Vienna Lager'
UPDATE product.ProductStyle SET ProductStyleDescription = 'We have west coast American brewers to thank for this somewhat reactionary style. Take an India Pale Ale and feed it steroids, and you''ll end up with a Double or Imperial IPA. Although generally recognizable alongside its sister styles in the IPA family, you should expect something more robust, malty, and alcoholic with a characteristically intense hop profile in both taste and aroma. In short, these are boldly flavored, medium-bodied beers that range in color from deep gold to medium amber. The "imperial" usage comes from Russian Imperial Stout, a style of strong Stout originally brewed in England during the late 1700s for the Russian imperial court. Today Double IPA is often the preferred name in the United States.' WHERE ProductStyleName = 'American Double/Imperial IPA'
UPDATE product.ProductStyle SET ProductStyleDescription = 'When malt is kilned over an open flame, the smoke flavor becomes infused into the beer, leaving a taste that can vary from dense campfire, to slight wisps of smoke. Any style of beer can be smoked; the goal is to reach a balance between the style’s character and the smoky properties. Originating in Germany as rauchbier, this style is open to interpretation by U.S. craft brewers. Classic base styles include German-style Marzen/Oktoberfest, German-style bock, German-style dunkel, Vienna-style lager and more. Smoke flavors dissipate over time.' WHERE ProductStyleName = 'Smoked Beer'
UPDATE product.ProductStyle SET ProductStyleDescription = '“Doppel” meaning “double,” this style is a bigger and stronger version of the lower-gravity German-style bock beers. Originally made by monks in Munich, the doppelbock beer style is very food-friendly and rich in melanoidins reminiscent of toasted bread. Color is copper to dark brown. Malty sweetness is dominant but should not be cloying. Malt character is more reminiscent of fresh and lightly toasted Munich-style malt, more so than caramel or toffee malt. Doppelbocks are full-bodied, and alcoholic strength is on the higher end.' WHERE ProductStyleName = 'Doppelbock'
UPDATE product.ProductStyle SET ProductStyleDescription = '“Doppel” meaning “double,” this style is a bigger and stronger version of the lower-gravity German-style bock beers. Originally made by monks in Munich, the doppelbock beer style is very food-friendly and rich in melanoidins reminiscent of toasted bread. Color is copper to dark brown. Malty sweetness is dominant but should not be cloying. Malt character is more reminiscent of fresh and lightly toasted Munich-style malt, more so than caramel or toffee malt. Doppelbocks are full-bodied, and alcoholic strength is on the higher end.' WHERE ProductStyleName = 'Dopplebock'
GO
--End table product.ProductStyle

EXEC utility.DropObject 'permissionable'
GO