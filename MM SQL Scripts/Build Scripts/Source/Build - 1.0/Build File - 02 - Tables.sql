--Begin table client.Client
DECLARE @TableName VARCHAR(250) = 'client.Client'

EXEC utility.DropObject @TableName

CREATE TABLE client.Client
	(
	ClientID INT IDENTITY(1,1) NOT NULL,
	ClientName VARCHAR(250),
	EmailAddress VARCHAR(320),
	FirstName VARCHAR(100),
	LastName VARCHAR(100),
	WebsiteURL VARCHAR(1000),
	ImagePath VARCHAR(250),
	StatusID INT,

	PhoneNumber VARCHAR(50),
	Address1 VARCHAR(50),
	Address2 VARCHAR(50),
	Address3 VARCHAR(50),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	PostalCode VARCHAR(10),
	ISOCountryCode3 CHAR(3),

	BaseCharge NUMERIC(18,2),
	BaseFactor NUMERIC(18,2),
	BaseRound NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'BaseCharge', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BaseFactor', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'BaseRound', 'NUMERIC(18,2)', '.5'
EXEC utility.SetDefaultConstraint @TableName, 'StatusID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ClientID'
GO
--End table client.Client

--Begin table client.ClientDistributor
DECLARE @TableName VARCHAR(250) = 'client.ClientDistributor'

EXEC utility.DropObject @TableName
GO
--End table client.ClientDistributor

--Begin table client.ClientPerson
DECLARE @TableName VARCHAR(250) = 'client.ClientPerson'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientPerson
	(
	ClientPersonID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	PersonID INT,
	IsClientAdministrator BIT 
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsClientAdministrator', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientPersonID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientPerson', 'ClientID,PersonID'
GO
--End table client.ClientPerson

--Begin table client.ClientProduct
DECLARE @TableName VARCHAR(250) = 'client.ClientProduct'

EXEC utility.DropObject @TableName

CREATE TABLE client.ClientProduct
	(
	ClientProductID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	ProductID INT,
	Description VARCHAR(1000),
	IconHTML VARCHAR(1000),
	ImagePath VARCHAR(250),
	IsFeatured BIT,
	DisplayOrder INT,
	DistributorID INT,
	IsMenuItem BIT,
	ServingSize VARCHAR(50),
	ServingCount INT,
	PackagePrice NUMERIC(18,2),
	PricingModelID INT,
	UpCharge NUMERIC(18,2),
	MenuPrice NUMERIC(18,2),
	MenuPriceDateTime DATETIME,
	MenuPricePersonID INT,
	InStock INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'DistributorID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'InStock', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsFeatured', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsMenuItem', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuPrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'MenuPricePersonID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PackagePrice', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ServingCount', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'UpCharge', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ClientProductID'
EXEC utility.SetIndexClustered @TableName, 'IX_ClientProduct', 'ClientID,ProductID'
GO
--End table client.ClientProduct

--Begin table client.PricingModel
DECLARE @TableName VARCHAR(250) = 'client.PricingModel'

EXEC utility.DropObject @TableName

CREATE TABLE client.PricingModel
	(
	PricingModelID INT IDENTITY(1, 1) NOT NULL,
	ClientID INT,
	PricingModelName VARCHAR(250),
	PricingModelCharge NUMERIC(18,2),
	PricingModelFactor NUMERIC(18,2)
	)

EXEC utility.SetDefaultConstraint @TableName, 'ClientID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelCharge', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PricingModelFactor', 'NUMERIC(18,2)', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'PricingModelID'
EXEC utility.SetIndexClustered @TableName, 'IX_PricingModel', 'ClientID,PricingModelName'
GO
--End table client.PricingModel

--Begin table dropdown.Country
DECLARE @TableName VARCHAR(250) = 'dropdown.Country'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Country
	(
	CountryID INT IDENTITY(0, 1) NOT NULL,
	CountryName NVARCHAR(250),
	ISOCountryCode2 CHAR(2), 
	ISOCountryCode3 CHAR(3),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'CountryID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_CountryName', 'DisplayOrder,CountryName', 'CountryID'
GO
--End table dropdown.Country

--Begin table dropdown.Region
DECLARE @TableName VARCHAR(250) = 'dropdown.Region'

EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Region
	(
	RegionID INT IDENTITY(0, 1) NOT NULL,
	RegionName NVARCHAR(250),
	ISOCountryCode3 CHAR(3),
	ISORegionCode2 CHAR(2), 
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'RegionID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_RegionName', 'DisplayOrder,RegionName', 'RegionID'
GO
--End table dropdown.Region

--Begin table dropdown.PackageType
DECLARE @TableName VARCHAR(250) = 'dropdown.PackageType'

EXEC utility.DropObject 'dropdown.PackageType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.PackageType
	(
	PackageTypeID INT IDENTITY(0,1) NOT NULL,
	PackageTypeCode VARCHAR(50),
	PackageTypeName VARCHAR(50),
	PackageTypeSizePlural VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PackageTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_PackageType', 'DisplayOrder,PackageTypeName', 'PackageTypeID'
GO
--End table dropdown.PackageType

--Begin table dropdown.ProductType
DECLARE @TableName VARCHAR(250) = 'dropdown.ProductType'

EXEC utility.DropObject 'dropdown.ProductType'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.ProductType
	(
	ProductTypeID INT IDENTITY(0,1) NOT NULL,
	ProductTypeCode VARCHAR(50),
	ProductTypeName VARCHAR(50),
	IconHTML VARCHAR(1000),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductTypeID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProductType', 'DisplayOrder,ProductTypeName', 'ProductTypeID'
GO
--End table dropdown.ProductType

--Begin table dropdown.Status
DECLARE @TableName VARCHAR(250) = 'dropdown.Status'

EXEC utility.DropObject 'dropdown.Status'
EXEC utility.DropObject @TableName

CREATE TABLE dropdown.Status
	(
	StatusID INT IDENTITY(0,1) NOT NULL,
	StatusCode VARCHAR(50),
	StatusName VARCHAR(50),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'StatusID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_Status', 'DisplayOrder,StatusName', 'StatusID'
GO
--End table dropdown.Status

--Begin table eventlog.EventLog
DECLARE @TableName VARCHAR(250) = 'eventlog.EventLog'

EXEC utility.DropObject 'core.EventLog'
EXEC utility.DropObject @TableName

CREATE TABLE eventlog.EventLog
	(
	EventLogID INT IDENTITY(1,1) NOT NULL,
	PersonID INT,
	EventCode VARCHAR(50),
	EntityTypeCode VARCHAR(50),
	EntityID INT,
	Comments VARCHAR(MAX),
	EventData XML,
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'EntityID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'EventLogID'
EXEC utility.SetIndexClustered @TableName, 'IX_EventLog', 'CreateDateTime DESC'
GO
--End table eventlog.EventLog

--Begin table product.Distributor
DECLARE @TableName VARCHAR(250) = 'product.Distributor'

EXEC utility.DropObject 'dropdown.Distributor'
EXEC utility.DropObject @TableName

CREATE TABLE product.Distributor
	(
	DistributorID INT IDENTITY(0, 1) NOT NULL,
	DistributorName VARCHAR(250),
	ImagePath VARCHAR(250),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'DistributorID'
GO
--End table product.Distributor

--Begin table product.Producer
DECLARE @TableName VARCHAR(250) = 'product.Producer'

EXEC utility.DropObject 'dropdown.Producer'
EXEC utility.DropObject @TableName

CREATE TABLE product.Producer
	(
	ProducerID INT IDENTITY(0, 1) NOT NULL,
	ProducerName VARCHAR(250),
	ImagePath VARCHAR(250),
	Municipality VARCHAR(50),
	ISORegionCode2 CHAR(2),
	ISOCountryCode3 CHAR(3),
	DisplayOrder INT,
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'DisplayOrder', 'INT', '100'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProducerID'
GO
--End table product.Producer

--Begin table product.Product
DECLARE @TableName VARCHAR(250) = 'product.Product'

EXEC utility.DropObject @TableName

CREATE TABLE product.Product
	(
	ProductID INT IDENTITY(1, 1) NOT NULL,
	ProductName NVARCHAR(250),
	ProductTypeID INT,
	ProductStyleID INT,
	PackageTypeID INT,
	PackageSize VARCHAR(50),
	ABV NUMERIC(18,2),
	IBU NUMERIC(18,2),
	ImportProductID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ABV', 'NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IBU', ' NUMERIC(18,2)', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ImportProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'PackageTypeID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductStyleID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProductTypeID', 'INT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductID'
GO
--End table product.Product

--Begin table product.ProductProducer
DECLARE @TableName VARCHAR(250) = 'product.ProductProducer'

EXEC utility.DropObject @TableName

CREATE TABLE product.ProductProducer
	(
	ProductProducerID INT IDENTITY(1, 1) NOT NULL,
	ProductID INT,
	ProducerID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'ProductID', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'ProducerID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'ProductProducerID'
EXEC utility.SetIndexClustered @TableName, 'IX_ProductProducer', 'ProductID,ProducerID'
GO
--End table product.ProductProducer

--Begin table product.ProductStyle
DECLARE @TableName VARCHAR(250) = 'product.ProductStyle'

EXEC utility.DropObject 'dropdown.ProductStyle'
EXEC utility.DropObject @TableName

CREATE TABLE product.ProductStyle
	(
	ProductStyleID INT IDENTITY(0,1) NOT NULL,
	ProductStyleCode VARCHAR(50),
	ProductStyleName NVARCHAR(250),
	ProductStyleDescription VARCHAR(MAX),
	IconHTML VARCHAR(1000),
	IsActive BIT
	)

EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'

EXEC utility.SetPrimaryKeyClustered @TableName, 'ProductStyleID'
EXEC utility.SetIndexNonClustered @TableName, 'IX_ProductStyle', 'ProductStyleName', 'ProductStyleID'
GO
--End table product.ProductStyle