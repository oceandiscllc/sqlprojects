--Begin procedure core.DeleteAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.DeleteAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to delete data from the core.Announcement table
-- ===============================================================================
CREATE PROCEDURE core.DeleteAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.DeleteAnnouncementByAnnouncementID

--Begin procedure core.EntityTypeAddUpdate
EXEC Utility.DropObject 'core.EntityTypeAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update a record in the core.EntityType table
-- =====================================================================================
CREATE PROCEDURE core.EntityTypeAddUpdate

@EntityTypeCode VARCHAR(50),
@EntityTypeName VARCHAR(250),
@EntityTypeNamePlural VARCHAR(50) = NULL,
@SchemaName VARCHAR(50) = NULL,
@TableName VARCHAR(50) = NULL,
@PrimaryKeyFieldName VARCHAR(50) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.EntityType ET
	USING (SELECT @EntityTypeCode AS EntityTypeCode) T2
		ON T2.EntityTypeCode = ET.EntityTypeCode
	WHEN MATCHED THEN UPDATE 
	SET 
		ET.EntityTypeName = @EntityTypeName, 
		ET.EntityTypeNamePlural = @EntityTypeNamePlural,
		ET.SchemaName = @SchemaName, 
		ET.TableName = @TableName, 
		ET.PrimaryKeyFieldName = @PrimaryKeyFieldName
	WHEN NOT MATCHED THEN
	INSERT (EntityTypeCode,EntityTypeName,EntityTypeNamePlural,SchemaName,TableName,PrimaryKeyFieldName)
	VALUES
		(
		@EntityTypeCode,
		@EntityTypeName, 
		@EntityTypeNamePlural, 
		@SchemaName, 
		@TableName, 
		@PrimaryKeyFieldName
		);

END
GO
--End procedure core.EntityTypeAddUpdate

--Begin procedure core.GetAnnouncementByAnnouncementID
EXEC Utility.DropObject 'core.GetAnnouncementByAnnouncementID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementByAnnouncementID

@AnnouncementID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		A.EndDate,
		core.FormatDate(A.EndDate) AS EndDateFormatted,
		A.StartDate,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted
	FROM core.Announcement A
	WHERE A.AnnouncementID = @AnnouncementID
	
END
GO
--End procedure core.GetAnnouncementByAnnouncementID

--Begin procedure core.GetAnnouncements
EXEC Utility.DropObject 'core.GetAnnouncements'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncements

AS
BEGIN
	SET NOCOUNT ON;

	SELECT	
		A.AnnouncementID,
		core.FormatDateTime(A.CreateDateTime) AS CreateDateTimeFormatted,
		person.FormatPersonNameByPersonID(A.CreatePersonID, 'LastFirst') AS CreatePersonNameFormatted,
		core.FormatDate(A.StartDate) AS StartDateFormatted,
		core.FormatDate(A.EndDate) AS EndDateFormatted
	FROM core.Announcement A
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncements

--Begin procedure core.GetAnnouncementsByDate
EXEC Utility.DropObject 'core.GetAnnouncementsByDate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.Announcement table
-- ============================================================================
CREATE PROCEDURE core.GetAnnouncementsByDate

AS
BEGIN
	SET NOCOUNT ON;

	DELETE A
	FROM core.Announcement A
	WHERE A.EndDate < getDate()

	SELECT	
		A.AnnouncementID,
		A.AnnouncementText
	FROM core.Announcement A
	WHERE getDate() BETWEEN A.StartDate AND A.EndDate
	ORDER BY A.StartDate, A.EndDate
	
END
GO
--End procedure core.GetAnnouncementsByDate

--Begin procedure core.GetSystemSetupDataBySystemSetupID
EXEC utility.DropObject 'core.GetSystemSetupDataBySystemSetupID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupDataBySystemSetupID

@SystemSetupID INT = 0

AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		SS.Description, 
		SS.SystemSetupID, 
		SS.SystemSetupKey, 
		SS.SystemSetupValue 
	FROM core.SystemSetup SS
	WHERE SS.SystemSetupID = @SystemSetupID

END
GO
--End procedure core.GetSystemSetupDataBySystemSetupID

--Begin procedure core.GetSystemSetupValuesBySystemSetupKey
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKey'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKey

@SystemSetupKey VARCHAR(250)

AS
BEGIN
	
	SELECT SS.SystemSetupValue 
	FROM core.SystemSetup SS 
	WHERE SS.SystemSetupKey = @SystemSetupKey
	ORDER BY SS.SystemSetupID

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKey

--Begin procedure core.GetSystemSetupValuesBySystemSetupKeyList
EXEC Utility.DropObject 'core.GetSystemSetupValuesBySystemSetupKeyList'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the core.SystemSetup table
-- ===========================================================================
CREATE PROCEDURE core.GetSystemSetupValuesBySystemSetupKeyList

@SystemSetupKeyList VARCHAR(MAX) = ''

AS
BEGIN
	
	IF @SystemSetupKeyList = ''
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
		ORDER BY SS.SystemSetupKey

		END
	ELSE
		BEGIN

		SELECT 
			SS.SystemSetupKey,
			SS.SystemSetupValue 
		FROM core.SystemSetup SS
			JOIN core.ListToTable(@SystemSetupKeyList, ',') LTT ON LTT.ListItem = SS.SystemSetupKey
		ORDER BY SS.SystemSetupKey

		END
	--ENDIF

END
GO
--End procedure core.GetSystemSetupValuesBySystemSetupKeyList

--Begin procedure core.MenuItemAddUpdate
EXEC utility.DropObject 'core.MenuItemAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update a menu item
-- ===========================================================
CREATE PROCEDURE core.MenuItemAddUpdate

@AfterMenuItemCode VARCHAR(50) = NULL,
@BeforeMenuItemCode VARCHAR(50) = NULL,
@DeleteMenuItemCode VARCHAR(50) = NULL,
@Icon VARCHAR(50) = NULL,
@IsActive BIT = 1,
@IsForNewTab BIT = 0,
@NewMenuItemCode VARCHAR(50) = NULL,
@NewMenuItemLink VARCHAR(500) = NULL,
@NewMenuItemText VARCHAR(250) = NULL,
@ParentMenuItemCode VARCHAR(50) = NULL,
@PermissionableLineageList VARCHAR(MAX) = NULL

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cMenuItemCode VARCHAR(50)
	DECLARE @nIsInserted BIT = 0
	DECLARE @nIsUpdate BIT = 1
	DECLARE @nMenuItemID INT
	DECLARE @nNewMenuItemID INT
	DECLARE @nOldParentMenuItemID INT = 0
	DECLARE @nParentMenuItemID INT = 0

	IF @DeleteMenuItemCode IS NOT NULL
		BEGIN
		
		SELECT 
			@nNewMenuItemID = MI.MenuItemID,
			@nParentMenuItemID = MI.ParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		UPDATE MI
		SET MI.ParentMenuItemID = @nParentMenuItemID
		FROM core.MenuItem MI 
		WHERE MI.ParentMenuItemID = @nNewMenuItemID
		
		DELETE MI
		FROM core.MenuItem MI 
		WHERE MI.MenuItemCode = @DeleteMenuItemCode
		
		END
	ELSE
		BEGIN

		IF EXISTS (SELECT 1 FROM core.MenuItem MI WHERE MI.MenuItemCode = @NewMenuItemCode)
			BEGIN
			
			SELECT 
				@nNewMenuItemID = MI.MenuItemID,
				@nParentMenuItemID = MI.ParentMenuItemID,
				@nOldParentMenuItemID = MI.ParentMenuItemID
			FROM core.MenuItem MI 
			WHERE MI.MenuItemCode = @NewMenuItemCode
	
			IF @ParentMenuItemCode IS NOT NULL AND LEN(RTRIM(@ParentMenuItemCode)) = 0
				SET @nParentMenuItemID = 0
			ELSE IF @ParentMenuItemCode IS NOT NULL
				BEGIN
	
				SELECT @nParentMenuItemID = MI.MenuItemID
				FROM core.MenuItem MI 
				WHERE MI.MenuItemCode = @ParentMenuItemCode
	
				END
			--ENDIF
			
			END
		ELSE
			BEGIN
	
			DECLARE @tOutput TABLE (MenuItemID INT)
	
			SET @nIsUpdate = 0
			
			SELECT @nParentMenuItemID = MI.MenuItemID
			FROM core.MenuItem MI
			WHERE MI.MenuItemCode = @ParentMenuItemCode
			
			INSERT INTO core.MenuItem 
				(ParentMenuItemID,MenuItemText,MenuItemCode,MenuItemLink,Icon,IsActive,IsForNewTab)
			OUTPUT INSERTED.MenuItemID INTO @tOutput
			VALUES
				(
				@nParentMenuItemID,
				@NewMenuItemText,
				@NewMenuItemCode,
				@NewMenuItemLink,
				@Icon,
				@IsActive,
				@IsForNewTab
				)
	
			SELECT @nNewMenuItemID = O.MenuItemID 
			FROM @tOutput O
			
			END
		--ENDIF
	
		IF @nIsUpdate = 1
			BEGIN
	
			UPDATE MI
			SET 
				MI.IsActive = @IsActive,
				MI.IsForNewTab = @IsForNewTab
			FROM core.MenuItem MI
			WHERE MI.MenuItemID = @nNewMenuItemID

			IF @Icon IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.Icon = 
					CASE 
						WHEN LEN(RTRIM(@Icon)) = 0
						THEN NULL
						ELSE @Icon
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemLink IS NOT NULL
				BEGIN
				
				UPDATE MI
				SET MI.MenuItemLink = 
					CASE 
						WHEN LEN(RTRIM(@NewMenuItemLink)) = 0
						THEN NULL
						ELSE @NewMenuItemLink
					END
				FROM core.MenuItem MI
				WHERE MI.MenuItemID = @nNewMenuItemID
				
				END
			--ENDIF

			IF @NewMenuItemText IS NOT NULL
				UPDATE core.MenuItem SET MenuItemText = @NewMenuItemText WHERE MenuItemID = @nNewMenuItemID
			--ENDIF

			IF @nOldParentMenuItemID <> @nParentMenuItemID
				UPDATE core.MenuItem SET ParentMenuItemID = @nParentMenuItemID WHERE MenuItemID = @nNewMenuItemID
			--ENDIF
			END
		--ENDIF

		IF @PermissionableLineageList IS NOT NULL
			BEGIN
			
			DELETE MIPL
			FROM core.MenuItemPermissionableLineage MIPL
			WHERE MIPL.MenuItemID = @nNewMenuItemID

			IF LEN(RTRIM(@PermissionableLineageList)) > 0
				BEGIN
				
				INSERT INTO core.MenuItemPermissionableLineage
					(MenuItemID, PermissionableLineage)
				SELECT
					@nNewMenuItemID,
					LTT.ListItem
				FROM core.ListToTable(@PermissionableLineageList, ',') LTT
				
				END
			--ENDIF

			END
		--ENDIF
	
		DECLARE @tTable1 TABLE (DisplayOrder INT NOT NULL IDENTITY(1,1) PRIMARY KEY, MenuItemID INT)
	
		DECLARE oCursor CURSOR LOCAL FAST_FORWARD FOR
			SELECT
				MI.MenuItemID,
				MI.MenuItemCode
			FROM core.MenuItem MI
			WHERE MI.ParentMenuItemID = @nParentMenuItemID
				AND MI.MenuItemID <> @nNewMenuItemID
			ORDER BY MI.DisplayOrder
	
		OPEN oCursor
		FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
		WHILE @@fetch_status = 0
			BEGIN
	
			IF @cMenuItemCode = @BeforeMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
	
			INSERT INTO @tTable1 (MenuItemID) VALUES (@nMenuItemID)
	
			IF @cMenuItemCode = @AfterMenuItemCode AND @nIsInserted = 0
				BEGIN
				
				INSERT INTO @tTable1 (MenuItemID) VALUES (@nNewMenuItemID)
				SET @nIsInserted = 1
				
				END
			--ENDIF
			
			FETCH oCursor INTO @nMenuItemID, @cMenuItemCode
	
			END
		--END WHILE
		
		CLOSE oCursor
		DEALLOCATE oCursor	
	
		UPDATE MI
		SET MI.DisplayOrder = T1.DisplayOrder
		FROM core.MenuItem MI
			JOIN @tTable1 T1 ON T1.MenuItemID = MI.MenuItemID
		
		END
	--ENDIF
	
END
GO
--End procedure core.MenuItemAddUpdate

--Begin procedure core.SystemSetupAddUpdate
EXEC Utility.DropObject 'core.SystemSetupAddUpdate'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add / update System setup key records
-- ========================================================================
CREATE PROCEDURE core.SystemSetupAddUpdate

@SystemSetupKey VARCHAR(250),
@Description VARCHAR(MAX),
@SystemSetupValue VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	MERGE core.SystemSetup SS
	USING (SELECT @SystemSetupKey AS SystemSetupKey) T2
		ON T2.SystemSetupKey = SS.SystemSetupKey
	WHEN MATCHED THEN UPDATE 
	SET 
		SS.SystemSetupKey = @SystemSetupKey, 
		SS.Description = @Description, 
		SS.SystemSetupValue = @SystemSetupValue
	WHEN NOT MATCHED THEN
	INSERT (SystemSetupKey,Description,SystemSetupValue)
	VALUES
		(
		@SystemSetupKey, 
		@Description, 
		@SystemSetupValue 
		);
	
END
GO
--End procedure core.SystemSetupAddUpdate

--Begin procedure core.UpdateParentPermissionableLineageByMenuItemCode
EXEC utility.DropObject 'core.UpdateParentPermissionableLineageByMenuItemCode'
GO
--End procedure core.UpdateParentPermissionableLineageByMenuItemCode

--Begin procedure permissionable.DeletePermissionable
EXEC Utility.DropObject 'permissionable.DeletePermissionable'
GO
--End procedure permissionable.DeletePermissionable

--Begin procedure permissionable.DeletePermissionableTemplate
EXEC Utility.DropObject 'permissionable.DeletePermissionableTemplate'
GO
--End procedure permissionable.DeletePermissionableTemplate

--Begin procedure permissionable.GetPermissionableByPermissionableID
EXEC utility.DropObject 'permissionable.GetPermissionableByPermissionableID'
GO
--End procedure permissionable.GetPermissionableByPermissionableID

--Begin procedure permissionable.GetPermissionableGroups
EXEC Utility.DropObject 'permissionable.GetPermissionableGroups'
GO
--End procedure permissionable.GetPermissionableGroups

--Begin procedure permissionable.GetPermissionables
EXEC utility.DropObject 'permissionable.GetPermissionables'
GO
--End procedure permissionable.GetPermissionables

--Begin procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionableTemplateByPermissionableTemplateID'
GO
--End procedure permissionable.GetPermissionableTemplateByPermissionableTemplateID

--Begin procedure permissionable.GetPermissionsByPermissionableTemplateID
EXEC utility.DropObject 'permissionable.GetPermissionsByPermissionableTemplateID'
GO
--End procedure permissionable.GetPermissionsByPermissionableTemplateID

--Begin procedure permissionable.SavePermissionable
EXEC utility.DropObject 'permissionable.SavePermissionable'
GO
--End procedure permissionable.SavePermissionable

--Begin procedure permissionable.SavePermissionableGroup
EXEC utility.DropObject 'permissionable.SavePermissionableGroup'
GO
--End procedure permissionable.SavePermissionableGroup

--Begin procedure person.GeneratePassword
EXEC utility.DropObject 'person.GeneratePassword'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to create a password and a salt
-- ===============================================================
CREATE PROCEDURE person.GeneratePassword

@Password VARCHAR(50)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50) = NewID()
	DECLARE @nI INT = 0

	SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)

	SELECT
		@cPasswordHash AS Password,
		@cPasswordSalt AS PasswordSalt,
		CAST(ISNULL((SELECT SS.SystemSetupValue FROM core.SystemSetup SS WHERE SS.SystemSetupKey = 'PasswordDuration'), 90) AS INT) AS PasswordDuration

END
GO
--End procedure person.GeneratePassword

--Begin procedure person.GetPersonByPersonID
EXEC utility.DropObject 'person.GetPersonByPersonID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.GetPersonByPersonID

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	--Person
	SELECT
		P.EmailAddress,
		P.FirstName,
		P.IsAccountLockedOut,
		P.IsActive,
		P.LastLoginDateTime,
		core.FormatDateTime(P.LastLoginDateTime) AS LastLoginDateTimeFormatted,
		P.LastName,
		P.MiddleName,
		P.PasswordExpirationDateTime,
		core.FormatDateTime(P.PasswordExpirationDateTime) AS PasswordExpirationDateTimeFormatted,
		P.PersonID,
		CASE WHEN @PersonID = 0 THEN 'New User' ELSE person.FormatPersonNameByPersonID(P.PersonID, 'LastFirst') END AS PersonNameFormatted,
		P.PhoneNumber,
		P.Suffix,
		P.Title
	FROM person.Person P
	WHERE P.PersonID = @PersonID

	SELECT
		C.ClientID,
		C.ClientName,
		CP.IsClientAdministrator
	FROM client.ClientPerson CP
		JOIN client.Client C ON C.ClientID = CP.ClientID
			AND CP.PersonID = @PersonID
	ORDER BY C.ClientName, C.ClientID

END
GO
--End procedure person.GetPersonByPersonID

--Begin procedure person.SetAccountLockedOut
EXEC utility.DropObject 'person.SetAccountLockedOut'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to set the person.Person.IsAccountLockedOut bit to 1
-- ====================================================================================
CREATE PROCEDURE person.SetAccountLockedOut

@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	UPDATE P
	SET P.IsAccountLockedOut = 1
	FROM person.Person P
	WHERE P.PersonID = @PersonID

END
GO
--End procedure person.SetAccountLockedOut

--Begin procedure person.SetPersonPermissionables
EXEC utility.DropObject 'person.SetPersonPermissionables'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to add data to the person.PersonPermissionables table based on a PersonID
-- =========================================================================================================
CREATE PROCEDURE person.SetPersonPermissionables

@PersonID INT,
@PermissionableIDList VARCHAR(MAX)

AS
BEGIN
	SET NOCOUNT ON;

	DELETE PP
	FROM person.PersonPermissionable PP
	WHERE PP.PersonID = @PersonID
		
	INSERT INTO person.PersonPermissionable
		(PersonID, PermissionableLineage)
	SELECT
		@PersonID,
		P.PermissionableLineage
	FROM permissionable.Permissionable P
	WHERE P.IsGlobal = 1

	IF @PermissionableIDList IS NOT NULL AND LEN(RTRIM(@PermissionableIDList)) > 0
		BEGIN

		INSERT INTO person.PersonPermissionable
			(PersonID, PermissionableLineage)
		SELECT
			@PersonID,
			P.PermissionableLineage
		FROM permissionable.Permissionable P
			JOIN core.ListToTable(@PermissionableIDList, ',') LTT ON CAST(LTT.ListItem AS INT) = P.PermissionableID

		END
	--ENDIF

END
GO
--End procedure person.SetPersonPermissionables

--Begin procedure person.ValidateEmailAddress
EXEC Utility.DropObject 'person.ValidateEmailAddress'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to get data from the person.Person table
-- ========================================================================
CREATE PROCEDURE person.ValidateEmailAddress

@EmailAddress VARCHAR(320),
@PersonID INT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT COUNT(P.PersonID) AS PersonCount
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress
		AND P.PersonID <> @PersonID

END
GO
--End procedure person.ValidateEmailAddress

--Begin procedure person.ValidateLogin
EXEC utility.DropObject 'person.ValidateLogin'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Author:			Todd Pires
-- Create Date:	2018.10.07
-- Description:	A stored procedure to validate user logins
-- =======================================================
CREATE PROCEDURE person.ValidateLogin

@EmailAddress VARCHAR(320),
@Password VARCHAR(50),
@IncrementInvalidLoginAttempts BIT = 1

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bIsValidPassword BIT = 0
	DECLARE @cPasswordDB VARCHAR(64)
	DECLARE @cPasswordHash VARCHAR(64)
	DECLARE @cPasswordSalt VARCHAR(50)
	DECLARE @nInvalidLoginAttempts INT = 0
	DECLARE @nInvalidLoginLimit INT
	DECLARE @nPersonID INT

	DECLARE @tPerson TABLE (PersonID INT NOT NULL DEFAULT 0 PRIMARY KEY)

	SELECT
		@cPasswordDB = P.Password,
		@cPasswordSalt = P.PasswordSalt,
		@nPersonID = P.PersonID
	FROM person.Person P
	WHERE P.EmailAddress = @EmailAddress

	SET @nPersonID = ISNULL(@nPersonID, 0)

	INSERT INTO @tPerson (PersonID) VALUES (@nPersonID)

	IF @nPersonID > 0
		BEGIN

		SET @cPasswordHash = person.HashPassword(@Password, @cPasswordSalt)
		SET @bIsValidPassword = CASE WHEN @cPasswordHash = @cPasswordDB THEN 1 ELSE 0 END

		IF @bIsValidPassword = 0
			BEGIN
		
			SELECT @nInvalidLoginLimit = CAST(core.GetSystemSetupValueBySystemSetupKey('InvalidLoginLimit', '3') AS INT)

			IF @IncrementInvalidLoginAttempts = 1
				SET @nInvalidLoginAttempts = @nInvalidLoginAttempts + 1
			--ENDIF

			UPDATE person.Person
			SET InvalidLoginAttempts = @nInvalidLoginAttempts
			WHERE PersonID = @nPersonID
			
			END
		ELSE 
			BEGIN

			UPDATE P
			SET 
				P.InvalidLoginAttempts = 0,
				P.LastLoginDateTime = getDate()
			FROM person.Person P
			WHERE PersonID = @nPersonID

			END
		--ENDIF
	
		END
	--ENDIF	
		
	--Person
	SELECT 
		TP.PersonID,

		CASE 
			WHEN TP.PersonID > 0 AND ISNULL((SELECT P.PasswordExpirationDateTime FROM person.Person P WHERE P.PersonID = TP.PersonID), SYSUTCDateTime()) < SYSUTCDateTime() 
			THEN 1 
			ELSE 0 
		END AS IsPasswordExpired,

		ISNULL((SELECT P.IsSuperAdministrator FROM person.Person P WHERE P.PersonID = TP.PersonID), 0) AS IsSuperAdministrator,
		@bIsValidPassword AS IsValidPassword,

		CASE
			WHEN TP.PersonID > 0
			THEN 1
			ELSE 0
		END AS IsValidEmailAddress,

		person.FormatPersonNameByPersonID(TP.PersonID, 'FirstLast') AS PersonNameFormatted
	FROM @tPerson TP

	IF EXISTS (SELECT 1 FROM person.Person P WHERE P.PersonID = @nPersonID AND P.IsSuperAdministrator = 1)
		BEGIN

		--PersonClient
		SELECT 
			C.ClientID,
			C.ClientName,
			1 AS IsClientAdministrator
		FROM client.Client C
		ORDER BY C.ClientName, C.ClientID

		--PersonClient
		SELECT 
			C.ClientID
		FROM client.Client C
		ORDER BY C.ClientID

		END
	ELSE
		BEGIN

		--PersonClient
		SELECT 
			C.ClientID,
			C.ClientName,
			CP.IsClientAdministrator
		FROM client.ClientPerson CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
				AND CP.PersonID = @nPersonID
		ORDER BY C.ClientName, C.ClientID

		--PersonClientAdministrator
		SELECT 
			C.ClientID
		FROM client.ClientPerson CP
			JOIN client.Client C ON C.ClientID = CP.ClientID
				AND CP.PersonID = @nPersonID
				AND CP.IsClientAdministrator = 1
		ORDER BY C.ClientID

		END
	--ENDIF

END
GO
--End procedure person.ValidateLogin