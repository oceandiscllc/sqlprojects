--Begin table core.Announcement
DECLARE @TableName VARCHAR(250) = 'core.Announcement'

EXEC utility.DropObject @TableName

CREATE TABLE core.Announcement
	(
	AnnouncementID INT IDENTITY(1,1) NOT NULL,
	StartDate DATE,
	EndDate DATE,
	AnnouncementText VARCHAR(MAX),
	CreateDateTime DATETIME,
	CreatePersonID INT
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'
EXEC utility.SetDefaultConstraint @TableName, 'CreatePersonID', 'INT', '0'

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'AnnouncementID'
EXEC utility.SetIndexClustered @TableName, 'IX_Announcement', 'StartDate, EndDate'
GO
--End table core.Announcement

--Begin table core.BuildLog
DECLARE @TableName VARCHAR(250) = 'core.BuildLog'

EXEC utility.DropObject @TableName

CREATE TABLE core.BuildLog
	(
	BuildLogID INT IDENTITY(1,1) NOT NULL,
	BuildKey VARCHAR(100),
	CreateDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'CreateDateTime', 'DATETIME', 'getDate()'

EXEC utility.SetPrimaryKeyClustered @TableName, 'BuildLogID DESC'
GO
--End table core.BuildLog

--Begin table core.EntityType
DECLARE @TableName VARCHAR(250) = 'core.EntityType'

EXEC utility.DropObject @TableName

CREATE TABLE core.EntityType
	(
	EntityTypeID INT IDENTITY(1,1) NOT NULL,
	EntityTypeCode VARCHAR(50),
	EntityTypeName VARCHAR(250),
	EntityTypeNamePlural VARCHAR(250),
	SchemaName VARCHAR(50),
	TableName VARCHAR(50),
	PrimaryKeyFieldName VARCHAR(50)
	)

EXEC utility.SetPrimaryKeyClustered @TableName, 'EntityTypeID'
GO
--End table core.EntityType

--Begin table core.SystemSetup
DECLARE @TableName VARCHAR(250) = 'core.SystemSetup'

EXEC utility.DropObject @TableName

CREATE TABLE core.SystemSetup
	(
	SystemSetupID INT IDENTITY(1,1) NOT NULL,
	SystemSetupKey VARCHAR(250),
	Description VARCHAR(500),
	SystemSetupValue VARCHAR(MAX)
	)

EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SystemSetupID'
EXEC utility.SetIndexClustered @TableName, 'IX_SystemSetup', 'SystemSetupKey'
GO
--End table core.SystemSetup

--Begin table person.Person
DECLARE @TableName VARCHAR(250) = 'person.Person'

EXEC utility.DropObject @TableName

CREATE TABLE person.Person
	(
	PersonID INT IDENTITY(1, 1) NOT NULL,
	Title VARCHAR(50),
	FirstName VARCHAR(100),
	MiddleName VARCHAR(100),
	LastName VARCHAR(100),
	Suffix VARCHAR(50),
	PhoneNumber VARCHAR(50),
	EmailAddress VARCHAR(320),
	LastLoginDateTime DATETIME,
	InvalidLoginAttempts INT,
	IsAccountLockedOut BIT,
	IsActive BIT,
	IsSuperAdministrator BIT,
	Password VARCHAR(64),
	PasswordSalt VARCHAR(50),
	PasswordExpirationDateTime DATETIME
	)

EXEC utility.SetDefaultConstraint @TableName, 'InvalidLoginAttempts', 'INT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsAccountLockedOut', 'BIT', '0'
EXEC utility.SetDefaultConstraint @TableName, 'IsActive', 'BIT', '1'
EXEC utility.SetDefaultConstraint @TableName, 'IsSuperAdministrator', 'BIT', '0'

EXEC utility.SetPrimaryKeyClustered @TableName, 'PersonID'
GO
--End table person.Person