
--Begin function dropdown.GetCountryNameByISOCountryCode
EXEC utility.DropObject 'dropdown.GetCountryNameByISOCountryCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a country name from an ISOCountryCode
-- =======================================================================

CREATE FUNCTION dropdown.GetCountryNameByISOCountryCode
(
@ISOCountryCode VARCHAR(3)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cCountryName VARCHAR(50)

	SELECT @cCountryName = C.CountryName
	FROM dropdown.Country C
	WHERE (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 2 AND C.ISOCountryCode2 = @ISOCountryCode)
		OR (LEN(LTRIM(RTRIM(@ISOCountryCode))) = 3 AND C.ISOCountryCode3 = @ISOCountryCode)

	RETURN ISNULL(@cCountryName, '')

END
GO
--End function dropdown.GetCountryNameByISOCountryCode

--Begin function dropdown.GetRegionNameByISORegionCode
EXEC utility.DropObject 'dropdown.GetRegionNameByISORegionCode'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================
-- Author:			Todd Pires
-- Create date:	2017.09.17
-- Description:	A function to return a Region name from an ISORegionCode
-- =======================================================================

CREATE FUNCTION dropdown.GetRegionNameByISORegionCode
(
@ISORegionCode2 VARCHAR(2)
)

RETURNS VARCHAR(250)

AS
BEGIN

	DECLARE @cRegionName VARCHAR(50)

	SELECT @cRegionName = R.RegionName
	FROM dropdown.Region R
	WHERE R.ISORegionCode2 = @ISORegionCode2

	RETURN ISNULL(@cRegionName, '')

END
GO
--End function dropdown.GetRegionNameByISOCountryCode

--Begin function product.GetIsMenuItemButtonHTML
EXEC utility.DropObject 'product.GetIsMenuItemButtonHTML'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================
-- Author:			Todd Pires
-- Create Date:	2018.11.05
-- Description:	A function to return an IsMenuItem button for a product
-- ====================================================================

CREATE FUNCTION product.GetIsMenuItemButtonHTML
(
@ProductID INT,
@ClientID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @bIsMenuItem BIT
	DECLARE @cClassName VARCHAR(50)
	DECLARE @cIsMenuItemButtonHTML VARCHAR(MAX)
	DECLARE @cPacakgeTypeName VARCHAR(50)
	DECLARE @cProductName VARCHAR(50)
	DECLARE @nClientProductID INT

	SELECT TOP 1 
		@cProductName = P1.ProducerName + ' ' + P2.ProductName,
		@cPacakgeTypeName = ISNULL((SELECT PT.PackageTypeName FROM dropdown.PackageType PT WHERE P2.PackageTypeID = PT.PackageTypeID), ''),
		@cClassName = CAST(@ClientID AS VARCHAR(10)) + '-' + CAST(@ProductID AS VARCHAR(10)) + '-' + CAST(P1.ProducerID AS VARCHAR(10))
	FROM product.ProductProducer PP
		JOIN product.Producer P1 ON P1.ProducerID = PP.ProducerID
		JOIN product.Product P2 ON P2.ProductID = PP.ProductID
			AND P2.ProductID = @ProductID
	ORDER BY P1.ProducerName + ' ' + P2.ProductName

	SELECT
		@bIsMenuItem = CP.IsMenuItem,
		@nClientProductID = CP.ClientProductID
	FROM client.ClientProduct CP 
	WHERE CP.Clientid = @ClientID 
		AND CP.ProductID = @ProductID

	IF @nClientProductID IS NULL OR @bIsMenuItem = 0
		BEGIN

		SET @cIsMenuItemButtonHTML = '<span class="badge badge-pill badge-success ' 
			+ @cClassName 
			+ '" data-buttonclass="' 
			+ @cClassName 
			+ '" data-clientproductid="' 
			+ CAST(ISNULL(@nClientProductID, 0) AS VARCHAR(10)) 
			+ '" data-ismenuitem="0" data-productid="' 
			+ CAST(@ProductID AS VARCHAR(10)) 
			+ '" data-packagetypename="'
			+ @cPacakgeTypeName
			+ '" data-productname="'
			+ @cProductName
			+ '" onclick="manageMenuItem(''' + @cClassName + ''')" style="cursor:pointer" title="Add to menu"><i class="fas fa-plus"></i></span>'

		END
	ELSE
		BEGIN

		SET @cIsMenuItemButtonHTML = '<span class="badge badge-pill badge-danger ' 
			+ @cClassName 
			+ '" data-buttonclass="' 
			+ @cClassName 
			+ '" data-clientproductid="' 
			+ CAST(ISNULL(@nClientProductID, 0) AS VARCHAR(10)) 
			+ '" data-ismenuitem="1" data-productid="' 
			+ CAST(@ProductID AS VARCHAR(10)) 
			+ '" data-packagetypename="'
			+ @cPacakgeTypeName
			+ '" data-productname="'
			+ @cProductName
			+ '" onclick="manageMenuItem(''' + @cClassName + ''')" style="cursor:pointer" title="Remove from menu"><i class="fas fa-minus"></i></span>'

		END
	--ENDIF

	RETURN @cIsMenuItemButtonHTML

END
GO
--End function product.GetIsMenuItemButtonHTML

--Begin function product.GetAdditionalProductProducerNames
EXEC utility.DropObject 'product.GetAdditionalProductProducerNames'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ========================================================
-- Author:			Todd Pires
-- Create Date:	2018.11.05
-- Description:	A function to return product producer names
-- ========================================================

CREATE FUNCTION product.GetAdditionalProductProducerNames
(
@ProductID INT, 
@ProducerID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = ''

	IF EXISTS (SELECT 1 FROM product.ProductProducer PP JOIN product.Producer P ON P.ProducerID = PP.ProducerID AND PP.ProductID = @ProductID AND PP.ProducerID <> @ProducerID)
		BEGIN

		SELECT @cReturn = COALESCE(@cReturn + ', ', '') + P.ProducerName
		FROM product.ProductProducer PP 
			JOIN product.Producer P ON P.ProducerID = PP.ProducerID 
				AND PP.ProductID = @ProductID
				AND PP.ProducerID <> @ProducerID
		ORDER BY P.ProducerName

		SET @cReturn = STUFF(@cReturn, 1, 2, '')

		END
	--ENDIF

	RETURN @cReturn

END
GO
--End function product.GetAdditionalProductProducerNames

--Begin function product.GetProducerNameByProductID
EXEC utility.DropObject 'product.GetProducerNameByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX)

	SELECT @cReturn = 
		STUFF(
			(
			SELECT '|' + P.ProducerName AS [text()]
			FROM product.Producer P
				JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
					AND PP.ProductID = @ProductID
			ORDER BY P.ProducerName
			FOR XML PATH('')
			), 1, 1, '')

	RETURN @cReturn

END
GO
--End function product.GetProducerNameByProductID

--Begin function product.GetProducerNameHTMLByProductID
EXEC utility.DropObject 'product.GetProducerNameHTMLByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameHTMLByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX)

	SELECT @cReturn = 
		REPLACE(STUFF(REPLACE(REPLACE(
			(
			SELECT '|<a href="/producer/view/id/' + CAST(P.ProducerID AS VARCHAR(10)) + '">' + P.ProducerName + '</a>' AS [text()]
			FROM product.Producer P
				JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
					AND PP.ProductID = @ProductID
			ORDER BY P.ProducerName
			FOR XML PATH('')
			), '&lt;', '<'), '&gt;', '>'), 1, 1, ''), '|', ' &amp;<br />')

	RETURN @cReturn

END
GO
--End function product.GetProducerNameHTMLByProductID

--Begin function product.GetProducerNameListByProductID
EXEC utility.DropObject 'product.GetProducerNameListByProductID'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================
-- Author:			Todd Pires
-- Create Date:	2018.12.07
-- Description:	A function to return a ProducerName for a product
-- ==============================================================

CREATE FUNCTION product.GetProducerNameListByProductID
(
@ProductID INT
)

RETURNS VARCHAR(MAX)

AS
BEGIN

	DECLARE @cReturn VARCHAR(MAX) = ''

	SELECT @cReturn = COALESCE(@cReturn + ' & ', '') + P.ProducerName
	FROM product.Producer P
		JOIN product.ProductProducer PP ON PP.ProducerID = P.ProducerID
			AND PP.ProductID = @ProductID
	ORDER BY P.ProducerName

	RETURN STUFF(@cReturn, 1, 3, '')

END
GO
--End function product.GetProducerNameListByProductID
